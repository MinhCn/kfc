package kfc.vietnam.push_notification;

import kfc.vietnam.common.object.NotificationData;

/**
 * Created by hdadmin on 2/25/2017.
 */

public class EventClickNotification {
   public NotificationData notificationData;

    public EventClickNotification(NotificationData notificationData) {
        this.notificationData = notificationData;
    }
}
