package kfc.vietnam.push_notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.NotificationResourceType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.NotificationData;
import kfc.vietnam.common.object.PushNotificationDataWrapper;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by VietRuyn on 25/09/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.

        //block notification from user
        SharedPreferences pref = getSharedPreferences(Const.SHARED_PREF, 0);
        boolean notification = pref.getBoolean(SharedPreferenceKeyType.BLOCK_NOTIFICATION.toString() , false);
        if (!notification){
            return;
        }
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Body: " + remoteMessage.getNotification().getBody());
        Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);
        Log.e("JSON_OBJECT", object.toString());
        try {
            String value = object.getString("data");
            if (TextUtils.isEmpty(value))
            {
                return;
            }
            NotificationData notificationData = new Gson().fromJson(value, NotificationData.class);
            EventBus.getDefault().post(notificationData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




}
