package kfc.vietnam.push_notification;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.object.StoreDeviceTokenDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hdadmin on 1/16/2017.
 */

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            SharedPreferences sharedpreferences = getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
            String keydevice = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            String device_token = sharedpreferences.getString("device_token", "");
            Log.e("device_token", device_token);
            Log.e("keydevice", keydevice);
            Log.e("device_token", device_token);
            saveDeviceToken(device_token,keydevice , "android");
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private void saveDeviceToken(String token, String keydevice,
                                 String platform) {

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            return;
        }

        Call<StoreDeviceTokenDataWrapper> call1 = ApiClient.getJsonClient().saveDeviceToken(token,
                keydevice, platform);
        call1.enqueue(new Callback<StoreDeviceTokenDataWrapper>() {

            @Override
            public void onFailure(Call<StoreDeviceTokenDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

            }

            @Override
            public void onResponse(Call<StoreDeviceTokenDataWrapper> arg0, Response<StoreDeviceTokenDataWrapper> arg1) {
                StoreDeviceTokenDataWrapper obj = arg1.body();
            }
        });
    }

}