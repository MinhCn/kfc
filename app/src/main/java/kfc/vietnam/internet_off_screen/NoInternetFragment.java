package kfc.vietnam.internet_off_screen;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import kfc.vietnam.R;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoInternetFragment extends Fragment implements View.OnClickListener {

    private RelativeLayout layoutImage;
    private MainActivity mainActivity;

    public NoInternetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainActivity = (MainActivity)getActivity();
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_no_internet, container, false);
        layoutImage = (RelativeLayout) view.findViewById(R.id.layout_empty);
        final ImageView image = (ImageView) layoutImage.findViewById(R.id.image);
        layoutImage.post(new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams layoutParams = image.getLayoutParams();
                layoutParams.width = layoutImage.getWidth() * 3/4;
                image.setLayoutParams(layoutParams);

                ViewGroup.LayoutParams layoutParams2 = layoutImage.getLayoutParams();
                layoutParams2.height = layoutImage.getHeight() + layoutImage.getHeight() * 1/3;
                layoutImage.setLayoutParams(layoutParams2);
            }
        });

        view.findViewById(R.id.btn_retry).setOnClickListener(this);
        view.findViewById(R.id.btn_setting).setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btn_retry:
                //reload previous fragment
                mainActivity.backToPreviousScreen();
                break;
            case R.id.btn_setting:
                startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                break;
        }
    }
}
