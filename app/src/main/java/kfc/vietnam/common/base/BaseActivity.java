package kfc.vietnam.common.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewStub;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.NotificationResourceType;
import kfc.vietnam.common.object.NotificationData;
import kfc.vietnam.common.utility.NoficationView;
import kfc.vietnam.food_menu_screen.fragment.DetailComboOrProductSelectedFragment;
import kfc.vietnam.main_screen.ui.MainActivity;
import kfc.vietnam.promotion_screen.fragment.DetailsPromotionFragment;
import kfc.vietnam.push_notification.EventClickNotification;

/**
 * Created by hdadmin on 2/25/2017.
 */

public class BaseActivity extends AppCompatActivity {
    private NoficationView noficationView;
    private NotificationData notificationData;
    private boolean isNotification;
    private EventBus eventBus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus = EventBus.getDefault();
        eventBus.register(this);
    }


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(R.layout.activity_base);
        noficationView = (NoficationView) findViewById(R.id.notifiView);

        noficationView.setOnClick(new NoficationView.OnclickNotifi() {
            @Override
            public void onClick() {
                if (notificationData != null && BaseActivity.this instanceof MainActivity) {
                    showScreenClickNotification();
                } else if (notificationData != null){
                    finish();
                    EventBus.getDefault().postSticky(new EventClickNotification(notificationData));
                }
            }
        });
        ViewStub content = (ViewStub) findViewById(R.id.contentView);
        content.setLayoutResource(layoutResID);
        content.inflate();

    }

    private void showScreenClickNotification() {
        MainActivity activity = (MainActivity) BaseActivity.this;
        if (notificationData.getType() == NotificationResourceType.PROMOTION.getValue()) {
            Bundle bundle = new Bundle();
            bundle.putString("id_new", String.valueOf(notificationData.getId()));

            DetailsPromotionFragment f = new DetailsPromotionFragment();
            f.setArguments(bundle);
            activity.changeFragment(f);

        } else {
            Bundle b = new Bundle();
            b.putString("id", String.valueOf(notificationData.getId()));
            b.putString("category_type", notificationData.getTypeProduct());

            DetailComboOrProductSelectedFragment f = new DetailComboOrProductSelectedFragment();
            f.setArguments(b);
            activity.changeFragment(f);
            activity.processChangeTopMenu(DetailComboOrProductSelectedFragment.class);
        }
        notificationData = null;
    }


    protected void showNotification(NotificationData notificationData) {
        this.notificationData = notificationData;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noficationView.setNewMessageEvent(BaseActivity.this.notificationData);
                noficationView.showChatNotification();
            }
        });
        hideNotification();

    }

    private void hideNotification() {
        isNotification = true;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 5000);
    }

    private Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (isNotification) {
                noficationView.hideNotification();
            }
        }
    };

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(runnable);
        handler = null;
        eventBus.unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationEvent(NotificationData event) {
        showNotification(event);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onClickNotificationEvent(EventClickNotification event) {
        if (this instanceof MainActivity) {
            noficationView.setVisibility(View.GONE);
            this.notificationData = event.notificationData;
            showScreenClickNotification();
            eventBus.removeStickyEvent(EventClickNotification.class);
        }
    }

    ;
}
