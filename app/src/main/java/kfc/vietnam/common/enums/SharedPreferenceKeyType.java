package kfc.vietnam.common.enums;

/**
 * Skype: anhduc.1985
 * Yahoo: khongthequen_bluestar
 * Telephone: +84 986 606 477
 * Email: khongthequenbluestar@gmail.com, khongthequen_bluestar@yahoo.com
 *
 * @author Anh Duc
 */
public enum SharedPreferenceKeyType {
    USER_INFO_DATAFILE, USER_INFO, SHOPPING_CART_DATAFILE, SHOPPING_CART,
    DELIVER_ADDRESS_DATAFILE,USER_PASSWORD, DELIVER_ADDRESS, LANGUAGE_CODE_DATAFILE,
    LANGUAGE_CODE, CHECK_LOGIN, USER_NOTIFICATION , FIRST_MENU, FIRST_MENU_DETAIL ,
    FIRST_PRODUCT_DETAIL, FIRST_PRODUCT_DETAIL_CHANGE_FOOD , FIRST_CHANGE_FOOD ,
    FIRST_ALLOW_NOTIFI , BLOCK_NOTIFICATION , CHOOSE_VN, CHOOSE_EN, CHECK_VERSION
}
