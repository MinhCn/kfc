package kfc.vietnam.common.enums;

/**
 * Skype: anhduc.1985
 * Yahoo: khongthequen_bluestar
 * Telephone: +84 986 606 477
 * Email: khongthequenbluestar@gmail.com, khongthequen_bluestar@yahoo.com
 *
 * @author Anh Duc
 */
public enum ProductChangeType {
//    SNACKS_AND_DESSERTS("annhe"), DRINKS("nuoc");
//    private String value;
//
//    private ProductChangeType(String value) {
//        this.value = value;
//    }
//
//    public String getValue() {
//        return value;
//    }

    SNACKS_AND_DESSERTS(4), DRINKS(5), GA_RAN(1), BURGER_COM(6);
    private int value;

    ProductChangeType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
