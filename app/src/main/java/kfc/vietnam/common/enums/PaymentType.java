package kfc.vietnam.common.enums;

/**
 * Skype: anhduc.1985
 * Yahoo: khongthequen_bluestar
 * Telephone: +84 986 606 477
 * Email: khongthequenbluestar@gmail.com, khongthequen_bluestar@yahoo.com
 *
 * @author Anh Duc
 */
public enum PaymentType {
    PAYMENT_AT_HOME("3"), PAYMENT_ONLINE("1"), PAYMENT_ATM("2");
    private String value;

    private PaymentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
