package kfc.vietnam.common.enums;

/**
 * Skype: anhduc.1985
 * Yahoo: khongthequen_bluestar
 * Telephone: +84 986 606 477
 * Email: khongthequenbluestar@gmail.com, khongthequen_bluestar@yahoo.com
 *
 * @author Anh Duc
 */
public enum LoginType {
    KFC("1"), FACEBOOK("2"), GOOGLE_PLUS("3");
    private String value;

    private LoginType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
