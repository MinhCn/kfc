package kfc.vietnam.common.enums;

/**
 * Skype: anhduc.1985
 * Yahoo: khongthequen_bluestar
 * Telephone: +84 986 606 477
 * Email: khongthequenbluestar@gmail.com, khongthequen_bluestar@yahoo.com
 *
 * @author Anh Duc
 */
public enum ChangeDishPositionType {
    SNACKS(0), DRINKS(1), GARAN(2), BURGER(3);
    private int value;

    ChangeDishPositionType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
