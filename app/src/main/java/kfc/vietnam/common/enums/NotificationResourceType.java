package kfc.vietnam.common.enums;

/**
 * Created by hdadmin on 1/15/2017.
 */

public enum  NotificationResourceType {
    PROMOTION(2), PRODUCT(1);

    private int value;

    private NotificationResourceType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
