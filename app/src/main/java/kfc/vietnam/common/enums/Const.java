package kfc.vietnam.common.enums;

/**
 * Created by dungnguyen on 10/27/16.
 */

public class Const {
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String SHARED_PREF = "AppSettings";
    public static final String DOMAIN_GOOGLE_PLAY = "https://play.google.com/store/apps/details?id=";
    public static final String ACTION_NOTIFICATION = "action_notification";
    public static final String DATA_NOTIFICATION = "data_notification";
}
