package kfc.vietnam.common.enums;

/**
 * Skype: anhduc.1985
 * Yahoo: khongthequen_bluestar
 * Telephone: +84 986 606 477
 * Email: khongthequenbluestar@gmail.com, khongthequen_bluestar@yahoo.com
 *
 * @author Anh Duc
 */
public enum RequestResultCodeType {
    REQUEST_CODE_LOGIN_IN_SHOPPING_CART(0), RESULT_CODE_LOGIN_IN_SHOPPING_CART(1), REQUEST_CODE_LOGIN_ON_NAVIGATION(2),
    RESULT_CODE_LOGIN_ON_NAVIGATION(3), REQUEST_CHANGE_DISH(4), RESULT_CHANGE_DISH(5), REQUEST_LOGIN(6), REQUEST_CODE_REGISTER_IN_STEP_ONE(7), RESULT_CODE_REGISTER_IN_STEP_ONE(8), RESULT_LOGIN_IN_PROMOTION(9) ,RESULT_SIGN_UP(10) ,RESULT_OPEN_LOGIN(11) , RESULT_OPEN_SIGN_UP(12) ;
    private int value;

    private RequestResultCodeType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
