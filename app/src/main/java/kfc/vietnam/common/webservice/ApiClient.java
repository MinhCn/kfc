package kfc.vietnam.common.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import kfc.vietnam.common.object.AddAddressDataWrapper;
import kfc.vietnam.common.object.AddLikeDataWrapper;
import kfc.vietnam.common.object.AddressListDataWrapper;
import kfc.vietnam.common.object.CategoryDataWrapper;
import kfc.vietnam.common.object.ChangeProductDataWrapper;
import kfc.vietnam.common.object.CheckEmailRegister;
import kfc.vietnam.common.object.ConfigureDataWrapper;
import kfc.vietnam.common.object.ContactDataWrapper;
import kfc.vietnam.common.object.CouponDataWrapper;
import kfc.vietnam.common.object.CouponPopupWrapper;
import kfc.vietnam.common.object.DeatilOrderHistoryWrapper;
import kfc.vietnam.common.object.DeleteFoodFavoriteDataWrapper;
import kfc.vietnam.common.object.DeleteShipAddressDataWrapper;
import kfc.vietnam.common.object.DetailPromotion;
import kfc.vietnam.common.object.FoodFavoriteDataWrapper;
import kfc.vietnam.common.object.ForgetPasswordDataWrapper;
import kfc.vietnam.common.object.GetAddressDataWrapper;
import kfc.vietnam.common.object.IpDataWrapper;
import kfc.vietnam.common.object.LikeNewsDataWrapper;
import kfc.vietnam.common.object.LoginDataWrapper;
import kfc.vietnam.common.object.MarkerOutletDataWrapper;
import kfc.vietnam.common.object.NotificationDataWrapper;
import kfc.vietnam.common.object.OrderDataWrapper;
import kfc.vietnam.common.object.OrderHistoryDataWrapper;
import kfc.vietnam.common.object.ProductDataWrapper;
import kfc.vietnam.common.object.ProductDetailDataWrapper;
import kfc.vietnam.common.object.ProductNoBuyDataWrapper;
import kfc.vietnam.common.object.PromotionWrapper;
import kfc.vietnam.common.object.ProvinceDataWrapper;
import kfc.vietnam.common.object.RegisterDataWrapper;
import kfc.vietnam.common.object.ShowTimeDataWrapper;
import kfc.vietnam.common.object.StoreDeviceTokenDataWrapper;
import kfc.vietnam.common.object.UpdateProfileDataWrapper;
import kfc.vietnam.common.object.UpdateShipAddressDataWrapper;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiClient {
    public static final String ENDPOINT = " http://dev.vietbuzzad.net/kfcvietnamnew/";
//    public static final String ENDPOINT = "https://beta2.kfcvietnam.com.vn/"; DEV
//    public static final String ENDPOINT = "https://kfcvietnam.com.vn/"; //LIVE
    private static RestApiJsonClient apiJsonClient;
    private static RestApiStringClient apiStringClient;

    public static RestApiJsonClient getJsonClient() {
        if (apiJsonClient == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(15, TimeUnit.SECONDS);
            httpClient.connectTimeout(15, TimeUnit.SECONDS);
            // add your other interceptors …

            // add logging as last interceptor
            httpClient.addInterceptor(logging);  // <-- this is the important line!
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();

            apiJsonClient = retrofit.create(RestApiJsonClient.class);
        }
        return apiJsonClient;
    }

    public static RestApiStringClient getStringClient() {
        if (apiStringClient == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(15, TimeUnit.SECONDS);
            httpClient.connectTimeout(15, TimeUnit.SECONDS);
            // add your other interceptors …

            // add logging as last interceptor
            httpClient.addInterceptor(logging);  // <-- this is the important line!

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .client(httpClient.build())
                    .build();

            apiStringClient = retrofit.create(RestApiStringClient.class);
        }
        return apiStringClient;
    }

    public interface RestApiJsonClient {
        //Get các tỉnh thành
        @GET("api/province")
        Call<ProvinceDataWrapper> getProvince(@Query("language") String lang);

        //Get danh sách thực đơn
        @GET("api/category")
        Call<CategoryDataWrapper> getCategory(@Query("language") String lang);

        // Get danh sách tất cả các món ăn
        @GET("api/products")
        Call<ProductDataWrapper> getComboAndProduct(@Query("language") String lang,
                                                    @Query("city") String city);

        // Get danh sách tất cả các món ăn
        @GET("api/getIP")
        Call<IpDataWrapper> getIp(@Query("language") String lang);

        // Get chi tiết của một món ăn
        @GET("api/product/{id}/{user_id}")
        Call<ProductDetailDataWrapper> getComboOrProductDetail(@Path("id") String id,
                                                               @Path("user_id") String userID,
                                                               @Query("language") String lang,
                                                               @Query("city") String city,
                                                               @Query("type") String type);

        // Đổi món ăn
        @FormUrlEncoded
        @POST("api/changeProduct/{id}")
        Call<ChangeProductDataWrapper> changeProduct(@Path("id") String id,
                                                     @Query("language") String lang,
                                                     @Query("city") String city,
                                                     @Field("combo_id") String comboID,
                                                     @Field("group_id") String groupID,
                                                     @Field("product_id") String productID);

        @FormUrlEncoded
        @POST("api/login")
        Call<LoginDataWrapper> login(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("api/register")
        Call<ResponseBody> register(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("password") String pwd,
                                    @Field("gender") int gender,
                                    @Field("phone") String phone,
                                    @Field("day") String day,
                                    @Field("month") String month,
                                    @Field("year") String year);

        // Get danh sách địa chỉ của khách hàng để giao hàng
        @FormUrlEncoded
        @POST("api/listAddress/{user_id}")
        Call<AddressListDataWrapper> getShipAddressList(@Path("user_id") String userID,
                                                        @Field("userID") String md5UserID);

        // Thêm địa chỉ giao hàng cho khách hàng
        @FormUrlEncoded
        @POST("api/addAddress/{user_id}")
        Call<AddAddressDataWrapper> addShipAddress(@Path("user_id") String userID,
                                                   @Field("userID") String md5UserID,
                                                   @Field("full_name") String fullName,
                                                   @Field("city") String city,
                                                   @Field("district") String district,
                                                   @Field("ward") String wardID,
                                                   @Field("address") String address,
                                                   @Field("phone") String phone,
                                                   @Field("email") String email);

        // Xóa địa chỉ giao hàng
        @GET("api/deleteAddress/{address_id}")
        Call<DeleteShipAddressDataWrapper> deleteShipAddress(@Path("address_id") String addressID);

        // Get địa chỉ giao hàng
        @GET("api/address/{user_id}/{address_id}")
        Call<GetAddressDataWrapper> getShipAddress(@Path("user_id") String userID,
                                                   @Query("userID") String md5UserID,
                                                   @Path("address_id") String addressID);

        // Đăng ký đặt hàng
        @FormUrlEncoded
        @POST("api/order")
        Call<OrderDataWrapper> order(@Query("language") String lang,
                                     @Query("city") String city,
                                     @Query("user") String userID,//="" (không đăng nhâp)
                                     @Field("cart") String cart,
                                     @Field("name") String name,
                                     @Field("address") String address,
                                     @Field("district") String district,
                                     @Field("ward") String ward,
                                     @Field("phone") String phone,
                                     @Field("email") String email,
                                     @Field("note") String note,
                                     @Field("discount") String discount,
                                     @Field("maxdiscount") String maxdiscount,
                                     @Field("maxorder") String maxorder,
                                     @Field("minorder") String minorder,
                                     @Field("type") String type,
                                     @Field("code") String code,
                                     @Field("platform") String platform,
                                     @Field("mobile_platform") String mobile_platform,
                                     @Field("city") String city2,
                                     @Field("totalOld") String totalOld,
                                     @Field("total") String total,
                                     @Field("price_discount") String priceDiscount
        );

        // Payment
        @FormUrlEncoded
        @POST("api/payment")
        Call<OrderDataWrapper> payment(@Query("user") String userID,
                                       @Query("language") String lang,
                                       @Query("city") String city,
                                       @Field("paymentType") String paymentType,
                                       @Field("request") String requestId,
                                       @Field("token") String encryptedPaymentData,
                                       @Field("orderID") String orderID,
                                       @Field("log") String paymentLog);

        // Add like
        @FormUrlEncoded
        @POST("api/addlike")
        Call<AddLikeDataWrapper> addLike(@Field("user_id") String userID,

                                         @Field("type") String type,
                                         @Field("product_id") String productID);


        // Mã giảm giá tích điểm
        @FormUrlEncoded
        @POST("api/useCoupon")
        Call<Object> couponCode(@Field("coupon_code") String couponCode);

        // Kiem tra ma giam gia
        @FormUrlEncoded
        @POST("api/coupon/{user_id}/{total_amount}")
        Call<CouponDataWrapper> checkCoupon(@Field("coupon") String couponCode,
                                            @Path("user_id") String user_id,
                                            @Field("userID") String md5UserID,
                                            @Path("total_amount") String total_amount,
                                            @Query("language") String lang);

        // Get location của tất cả các nhà hàng
        @GET("api/store")
        Call<MarkerOutletDataWrapper> getRestaurentLocation(@Query("language") String lang);

        @FormUrlEncoded
        @POST("api/loginFG")
        Call<LoginDataWrapper> loginByFacebookOrGooglePlus(@Field("usertype") String loginType,
                                                           @Field("id") String id,
                                                           @Field("email") String email,
                                                           @Field("name") String name,
                                                           @Field("phone") String phone,
                                                           @Field("gender") String gender,
                                                           @Field("avatar") String urlAvatar,
                                                           @Field("token") String token);

        // Get configuration
        @GET("api/getConfig")
        Call<ConfigureDataWrapper> getConfiguration(@Query("language") String lang);

        // Get showTime
        @GET("/api/checkShowTime/{province}/{ward}")
        Call<ShowTimeDataWrapper> getCheckDeliveryTime(@Path("province") String province,
                                                  @Path("ward") String ward,
                                                  @Query("language") String lang);

        // Get no buy
        @GET("/api/checkProductNoBuy/{province}/{product}/{combo}")
        Call<ProductNoBuyDataWrapper> checkProductNoBuy(@Path("province") String province,
                                                        @Path("product") String product,
                                                        @Path("combo") String combo,
                                                        @Query("language") String lang);


        //Contact
        @FormUrlEncoded
        @POST("api/contact")
        Call<ContactDataWrapper> sendContact(@Field("fullname") String fullname,
                                             @Field("address") String address,
                                             @Field("phone") String phone,
                                             @Field("email") String email,
                                             @Field("message") String message);

        // Get các món ăn ưa thích
        @FormUrlEncoded
        @POST("api/listFavorite/{user_id}")
        Call<FoodFavoriteDataWrapper> getFoodFavorite(@Path("user_id") String userID,
                                                      @Field("userID") String md5UserID,
                                                      @Field("language") String lang,
                                                      @Field("city") String city);

        // Get lịch sử giao dịch
        @FormUrlEncoded
        @POST("api/orderHistory/{user_id}")
        Call<OrderHistoryDataWrapper> getOrderHistory(@Path("user_id") String userID,
                                                      @Field("userID") String md5UserID,
                                                      @Field("language") String lang,
                                                      @Field("city") String city);

        @GET("api/orderHistoryDetail/{id_order}")
        Call<DeatilOrderHistoryWrapper> getDetailOrderHistory(@Path("id_order") String orderID,
                                                              @Query("language") String lang);


        // Get thông báo
        @GET("api/notification")
        Call<NotificationDataWrapper> getNotification(@Query("language") String lang);

        //Get password
        @FormUrlEncoded
        @POST("api/forgetPassword")
        Call<ForgetPasswordDataWrapper> getPassword(@Field("email") String email);

        // Get các bản tin khuyến mãi
        @GET("api/news")
        Call<PromotionWrapper> getNews(@Query("language") String lang);

        @GET("api/newsDetail/{id_promotion}")
        Call<DetailPromotion> getDetailPromotion(@Path("id_promotion") String id , @Query("language") String lang);

        // Like các bản tin khuyến mãi
        @GET("api/likeNews/{news_id}")
        Call<LikeNewsDataWrapper> likeNews(@Path("news_id") String newsID);

        //Update profile
        @FormUrlEncoded
        @POST("api/updateProfile/{user_id}")
        Call<UpdateProfileDataWrapper> updateProfile(@Path("user_id") String userID,
                                                     @Field("userID") String md5UserID,
                                                     @Field("name") String name,
                                                     @Field("email") String email,
                                                     @Field("gender") String gender,//(1: nam - 0: nữ)
                                                     @Field("day") String day,
                                                     @Field("month") String month,
                                                     @Field("year") String year,
                                                     @Field("phone") String phone,
                                                     @Field("newPass") String newPass,
                                                     @Field("newPassRepeat") String newPassRepeat,
                                                     @Field("oldPass") String oldPass);

        //Update profile fb or GG
        @FormUrlEncoded
        @POST("api/updateProfile/{user_id}")
        Call<UpdateProfileDataWrapper> updateProfileFbOrGG(@Path("user_id") String userID,
                                                           @Field("userID") String md5UserID,
                                                           @Field("name") String name,
                                                           @Field("email") String email,
                                                           @Field("gender") String gender,//(1: nam - 0: nữ)
                                                           @Field("day") String day,
                                                           @Field("month") String month,
                                                           @Field("year") String year,
                                                           @Field("phone") String phone);

        //Xóa món ăn ưa thích
        @FormUrlEncoded
        @POST("api/deleteLike/{product_id}")
        Call<DeleteFoodFavoriteDataWrapper> deleteFoodFavorite(@Path("product_id") String product_id,
                                                               @Field("userID") String userID);

        //Update địa chỉ giao hàng
        @FormUrlEncoded
        @POST("api/updateAddress/{user_id}")
        Call<UpdateShipAddressDataWrapper> updateShipAddress(@Path("user_id") String userID,
                                                             @Field("userID") String md5UserID,
                                                             @Field("id") String id,
                                                             @Field("full_name") String fullname,
                                                             @Field("city") String city,
                                                             @Field("district") String district,
                                                             @Field("ward") String wardID,
                                                             @Field("address") String address,
                                                             @Field("phone") String phone,
                                                             @Field("email") String email);

        //Store device token
        @FormUrlEncoded
        @POST("api/token")
        Call<StoreDeviceTokenDataWrapper> saveDeviceToken(@Field("token") String token,
                                                          @Field("keydevice") String keydevice,
                                                          @Field("platform") String platform);

        @FormUrlEncoded
        @POST("api/checkEmail")
        Call<ResponseBody> checkEmailRegister(@Query("language") String lang,
                                              @Field("email") String email,
                                              @Field("type") String type ,
                                              @Field("id") String id);

        @FormUrlEncoded
        @POST("api/sendOTP")
        Call<ResponseBody> sendOTP(@Query("language") String lang,
                                   @Field("email") String email,
                                   @Field("user") String id_user);

        @FormUrlEncoded
        @POST("api/checkOTP")
        Call<ResponseBody> checkOTP (@Query("language") String lang,
                                     @Field("codeOTP") String codeOTP,
                                     @Field("user") String id_user,
                                     @Field("id") String id , @Field("type") String type);

        //true: close, false: open
        @GET("api/checkTime")
        Call<ResponseBody> checkTime(@Query("language") String lang);

        //check coupon popup
        @GET("api/checkShowCouponPopup")
        Call<CouponPopupWrapper> checkShowCouponPopup(@Query("language") String lang);
    }

    public interface RestApiStringClient {
        // Get certificate for payment
        @FormUrlEncoded
        @POST("sign.php")
        Call<ResponseBody> getCertificate(@FieldMap Map<String, String> fields);
    }
}