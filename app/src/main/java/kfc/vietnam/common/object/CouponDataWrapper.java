package kfc.vietnam.common.object;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by admin on 10/30/16.
 */

public class CouponDataWrapper implements Serializable {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("result")
    @Expose
    public boolean result;

    @SerializedName("data")
    @Expose
    public Object data;

    /**
     * Get data result
     * @return
     */
    public CouponResult getData()
    {
        if(result)
        {
            Gson gson = new Gson();
            String json = gson.toJson(data);
            return gson.fromJson(json, CouponResult.class);
        }
        return null;
    }

    public class CouponResult implements Serializable
    {
        @SerializedName("code")
        @Expose
        public String code;

        @SerializedName("discount")
        @Expose
        public Double discount;

        @SerializedName("maxdiscount")
        @Expose
        public Double maxdiscount;

        @SerializedName("type")
        @Expose
        public int type;

        @SerializedName("maxorder")
        @Expose
        public Double maxorder;
        @SerializedName("minorder")
        @Expose
        public Double minorder;
    }
}

