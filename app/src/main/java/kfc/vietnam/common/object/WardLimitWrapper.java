package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WardLimitWrapper implements Serializable {
    @SerializedName("priceMax")
    @Expose
    public Double priceMax;

    @SerializedName("timeMax")
    @Expose
    public String timeMax;

}
