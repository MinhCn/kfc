package kfc.vietnam.common.object;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import kfc.vietnam.food_menu_screen.adapter.MenuPagerAdapter;

public class RelativeLayoutItemMenu extends RelativeLayout {
    private float scale = MenuPagerAdapter.BIG_SCALE;

    public RelativeLayoutItemMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RelativeLayoutItemMenu(Context context) {
        super(context);
    }

    public void setScaleBoth(float scale) {
        this.scale = scale;
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // The main mechanism to display scale animation, you can customize it as your needs
        int w = this.getWidth();
        int h = this.getHeight();
        canvas.scale(scale, scale, w / 2, h / 2);
    }
}
