package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProductChangeWrapper implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("category")
    @Expose
    public String category;

    //nuoc: là drinks
    //annhe: là Snacks
    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("product")
    @Expose
    public ArrayList<ProductWrapper002> product;
}
