package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderObject implements Serializable {
    @SerializedName("quantity")
    @Expose
    public String quantity;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("change_product")
    @Expose
    public ArrayList<String> change_product;

    @SerializedName("combo_product_id")
    @Expose
    public ArrayList<String> combo_product_id;

    @SerializedName("group_id")
    @Expose
    public ArrayList<String> group_id;

    @SerializedName("productBuyMore")
    @Expose
    public ArrayList<String> productBuyMore;
}
