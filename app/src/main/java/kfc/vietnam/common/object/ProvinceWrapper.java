package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProvinceWrapper implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("payment")
    @Expose
    public String payment;

    @SerializedName("district")
    @Expose
    public ArrayList<DistrictWrapper> district;

    public ProvinceWrapper(String id, String name, ArrayList<DistrictWrapper> district) {
        this.id = id;
        this.name = name;
        this.district = district;
    }

    public ProvinceWrapper(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
