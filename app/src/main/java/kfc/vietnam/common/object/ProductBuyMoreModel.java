package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by WIKI on 16/12/2017.
 */

public class ProductBuyMoreModel implements Serializable{
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("image")
    @Expose
    public String image;
}
