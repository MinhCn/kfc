package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class IpDataWrapper implements Serializable {
    @SerializedName("data")
    @Expose
    public String data;
}
