package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CouponPopupWrapper implements Serializable {
    @SerializedName("linkDetail")
    @Expose
    String linkDetail;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("result")
    @Expose
    private Boolean result;

    @SerializedName("message")
    @Expose
    private String message;

    public String getLinkDetail() {
        return linkDetail;
    }

    public void setLinkDetail(String linkDetail) {
        this.linkDetail = linkDetail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
