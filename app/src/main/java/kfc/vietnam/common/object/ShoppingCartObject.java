package kfc.vietnam.common.object;

import java.io.Serializable;
import java.util.ArrayList;

public class ShoppingCartObject implements Serializable {
    public String idCart;

    public ProductWrapper001 dishSelected;

    public ProductWrapper003 productSelected;

    public ArrayList<ProductBuyMoreModel> listProductBuyMoreModel;

    public GroupContentWrapper001 combo1, combo2;

    public ArrayList<ProductWrapper002> lstProductDefault, lstSnacksAndDessertDefault, lstDrinkDefault, lstSnacksAndDessertSelected, lstDrinkSelected;

    //add by @ManhNguyen
    public ArrayList<ProductWrapper002> lstGaRanDefault, lstBurgerComDefault, lstGaRanSelected, lstBurgerComSelected;

    public String categoryType;

    public String amount;

    public int count_element;

    public Double price_one_item;

    public Double price_in_total;

}
