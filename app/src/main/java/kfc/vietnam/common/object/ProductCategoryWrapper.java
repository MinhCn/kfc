package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProductCategoryWrapper implements Serializable {
    @SerializedName("categoryName")
    @Expose
    public String categoryName;

    @SerializedName("categoryID")
    @Expose
    public String categoryID;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("product")
    @Expose
    public ArrayList<ProductWrapper001> product;

}
