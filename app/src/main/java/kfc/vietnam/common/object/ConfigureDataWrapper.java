package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ConfigureDataWrapper implements Serializable {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("result")
    @Expose
    public boolean result;

    @SerializedName("data")
    @Expose
    public ConfigureWrapper data;

    public static class ConfigureWrapper implements Serializable {
        @SerializedName("point")
        @Expose
        public PointWrapper point;

        @SerializedName("coupon")
        @Expose
        public CouponWrapper coupon;

        @SerializedName("timeOpen")
        @Expose
        public TimeOpenWrapper timeOpen;

        @SerializedName("quydinh")
        @Expose
        public RegulationWrapper regulation;

        @SerializedName("lienhe")
        @Expose
        public ContactWrapper lienhe;

        @SerializedName("cyber")
        @Expose
        public CyberDataWrapper cyber;

        @SerializedName("version")
        @Expose
        public VersionWrapper version;

        @SerializedName("year")
        @Expose
        public YearWrapper year;
    }

    public static class PointWrapper implements Serializable {
        @SerializedName("price")
        @Expose
        public String price;

        @SerializedName("point")
        @Expose
        public String point;

        @SerializedName("minPrice")
        @Expose
        public String minPrice;

        @SerializedName("shipPrice")
        @Expose
        public String shipPrice;

        @SerializedName("checkPoint")
        @Expose
        public boolean checkPoint;
    }

    public static class CouponWrapper implements Serializable {
        @SerializedName("checkCoupon")
        @Expose
        public boolean checkCoupon;
    }

    public static class TimeOpenWrapper implements Serializable {
        @SerializedName("open")
        @Expose
        public String open;

        @SerializedName("close")
        @Expose
        public String close;
    }

    public static class RegulationWrapper implements Serializable {
        @SerializedName("title")
        @Expose
        public String title;

        @SerializedName("content")
        @Expose
        public String content;
    }
    public static class ContactWrapper implements Serializable {
        @SerializedName("title")
        @Expose
        public String title;

        @SerializedName("content")
        @Expose
        public String content;
    }

    public static class VersionWrapper implements Serializable {
        @SerializedName("version_android_app")
        @Expose
        public String version_android_app;

        @SerializedName("version_ios_app")
        @Expose
        public String version_ios_app;

        @SerializedName("update_required_android")
        @Expose
        public boolean update_required_android;

        @SerializedName("update_required_ioS")
        @Expose
        public boolean update_required_ioS;
    }

    public static class YearWrapper implements Serializable {
        @SerializedName("year")
        @Expose
        public String year;
    }
}
