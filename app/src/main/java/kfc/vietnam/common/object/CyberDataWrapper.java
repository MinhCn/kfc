package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class CyberDataWrapper implements Serializable {
    @SerializedName("linkPay")
    @Expose
    public String linkPay;

    @SerializedName("accessKey")
    @Expose
    public String accessKey;

    @SerializedName("profileID")
    @Expose
    public String profileID;

    @SerializedName("secretKey")
    @Expose
    public String secretKey;
}
