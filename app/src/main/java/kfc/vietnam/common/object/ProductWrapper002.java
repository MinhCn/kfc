package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProductWrapper002 implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("combo_product_id")
    @Expose
    public int combo_product_id = -1;

    @SerializedName("nameProduct")
    @Expose
    public String nameProduct;

    @SerializedName("combo_id")
    @Expose
    public int combo_id;

    @SerializedName("group_id")
    @Expose
    public int group_id = -1;

    @SerializedName("parent")
    @Expose
    public String parent;

    @SerializedName("parent_group")
    @Expose
    public String parent_group;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("change")
    @Expose
    public String change;

    @SerializedName("buy")
    @Expose
    public String buy;


    public boolean isProductSelected = false;//Biến để sử dụng trong việc sau khi thay đổi món ăn ở màn hình change món ăn thì danh sách giá trị trả về cho detail screen sẽ dựa vào đây
}
