package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProductWrapper001 implements Serializable {
    private static final long serialVersionUID = 394009356273114227L;

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("like")
    @Expose
    public String like;

    @SerializedName("buy")
    @Expose
    public String buy;

    @SerializedName("categoryID")
    @Expose
    public String categoryID;

    @SerializedName("khuyenmai")
    @Expose
    public String khuyenmai;

}
