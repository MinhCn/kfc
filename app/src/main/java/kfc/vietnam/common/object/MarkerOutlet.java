package kfc.vietnam.common.object;

import android.annotation.SuppressLint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressLint("ParcelCreator")
@SuppressWarnings("serial")
public class MarkerOutlet implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("time_open")
    @Expose
    public String time_open;

    @SerializedName("time_close")
    @Expose
    public String time_close;

    @SerializedName("provinceID")
    @Expose
    public String provinceID;

    @SerializedName("districtId")
    @Expose
    public String districtId;

    @SerializedName("latitude")
    @Expose
    public String latitude;

    @SerializedName("longitude")
    @Expose
    public String longitude;

    @SerializedName("image")
    @Expose
    public ArrayList<String> image;



}
