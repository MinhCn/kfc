package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProductDetailDataContentWrapper implements Serializable {
    @SerializedName("product")
    @Expose
    public ProductWrapper003 product;

    @SerializedName("groups")
    @Expose
    public GroupWrapper001 groups;

    @SerializedName("productCombo")
    @Expose
    public ArrayList<ProductWrapper002> productCombo;

    @SerializedName("productChange")
    @Expose
    public ArrayList<ProductChangeWrapper> productChange;

}
