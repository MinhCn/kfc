package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProvinceDataWrapper implements Serializable {
	@SerializedName("message")
	@Expose
	public String						message;

	@SerializedName("result")
	@Expose
	public boolean						result;

	@SerializedName("data")
	@Expose
	public ArrayList<ProvinceWrapper>	data;

}
