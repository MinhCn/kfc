package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class FoodFavoriteDataWrapper implements Serializable {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("result")
    @Expose
    public boolean result;

    @SerializedName("data")
    @Expose
    public ArrayList<FoodFavoriteWrapper> data;

    public static class FoodFavoriteWrapper implements Serializable {
        @SerializedName("id")
        @Expose
        public int id;

        @SerializedName("name")
        @Expose
        public String name;

        @SerializedName("image")
        @Expose
        public String image;

        @SerializedName("type")
        @Expose
        public String type;

        @SerializedName("buy")
        @Expose
        public String buy;

        @SerializedName("product")
        @Expose
        public ArrayList<ProductWrapper> product;
    }

    public static class ProductWrapper implements Serializable {
        @SerializedName("name")
        @Expose
        public String name;
    }
}
