package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by VietRuyn on 30/08/2016.
 */
public class PromotionWrapper implements Serializable {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("result")
    @Expose
    public boolean result;

    @SerializedName("data")
    @Expose
    public ArrayList<DetailPromotionWrapper> data;
}
