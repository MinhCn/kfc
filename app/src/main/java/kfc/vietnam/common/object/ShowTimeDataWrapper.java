package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dung on 1/6/17.
 */

public class ShowTimeDataWrapper implements Serializable {
    @SerializedName("result")
    @Expose
    private boolean result;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private DataWrapper data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataWrapper getData() {
        return data;
    }

    public void setData(DataWrapper data) {
        this.data = data;
    }

    public class DataWrapper implements Serializable {

        @SerializedName("showTime")
        @Expose
        private boolean showTime;
        @SerializedName("minTime")
        @Expose
        private String minTime;

        public boolean isShowTime() {
            return showTime;
        }

        public void setShowTime(boolean showTime) {
            this.showTime = showTime;
        }

        public String getMinTime() {
            return minTime;
        }

        public void setMinTime(String minTime) {
            this.minTime = minTime;
        }
    }

}
