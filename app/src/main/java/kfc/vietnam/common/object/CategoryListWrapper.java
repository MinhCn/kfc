package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class CategoryListWrapper implements Serializable {
	@SerializedName("category")
	@Expose
	public ArrayList<CategoryWrapper>	category;

}
