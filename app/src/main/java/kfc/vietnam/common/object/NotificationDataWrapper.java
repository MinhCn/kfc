package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class NotificationDataWrapper implements Serializable {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("result")
    @Expose
    public boolean result;

    @SerializedName("data")
    @Expose
    public ArrayList<NotificationWrapper> data;

    public static class NotificationWrapper implements Serializable {
        @SerializedName("id")
        @Expose
        public int id;

        @SerializedName("name")
        @Expose
        public String name;

        @SerializedName("link")
        @Expose
        public String link;

        @SerializedName("date")
        @Expose
        public String date;


        public boolean isRead;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public boolean isRead() {
            return isRead;
        }

        public void setRead(boolean read) {
            isRead = read;
        }
    }
}
