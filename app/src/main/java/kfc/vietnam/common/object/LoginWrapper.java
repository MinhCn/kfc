package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class LoginWrapper implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("username")
    @Expose
    public String username;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("birthday")
    @Expose
    public String birthday;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("city_id")
    @Expose
    public String city_id;

    @SerializedName("district_id")
    @Expose
    public String district_id;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("last_login")
    @Expose
    public String last_login;

    @SerializedName("sendmail")
    @Expose
    public String sendmail;

    @SerializedName("dt_create")
    @Expose
    public String dt_create;

    @SerializedName("facebook")
    @Expose
    public String facebook;

    @SerializedName("google")
    @Expose
    public String google;

    @SerializedName("avatar")
    @Expose
    public String avatar;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("usertype")
    @Expose
    public String usertype;

    @SerializedName("bl_active")
    @Expose
    public String bl_active;

    @SerializedName("confirm_code")
    @Expose
    public String confirm_code;


    @SerializedName("point")
    @Expose
    public String point;

}
