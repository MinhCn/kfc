package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class GroupWrapper001 implements Serializable {
    @SerializedName("group_0")
    @Expose
    public ArrayList<GroupContentWrapper001> group_0;

    @SerializedName("group_1")
    @Expose
    public ArrayList<GroupContentWrapper001> group_1;

}
