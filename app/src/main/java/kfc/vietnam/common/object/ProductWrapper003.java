package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class ProductWrapper003 implements Serializable {
    private static final long serialVersionUID = -8385125966395230996L;

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("nameProduct")
    @Expose
    public String nameProduct;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("like")
    @Expose
    public int like;

    @SerializedName("checkLike")
    @Expose
    public boolean checkLike;

    @SerializedName("buy")
    @Expose
    public String buy;

    //add by @ManhNguyen
    @SerializedName("productBuyMore")
    @Expose
    public ArrayList<ProductBuyMoreModel> productBuyMore;

    @SerializedName("categoryID")
    @Expose
    public String categoryID;

    @SerializedName("khuyenmai")
    @Expose
    public String khuyenmai;
}
