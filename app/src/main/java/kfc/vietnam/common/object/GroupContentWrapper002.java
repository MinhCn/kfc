package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class GroupContentWrapper002 implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("combo_product_id")
    @Expose
    public int combo_product_id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("combo_id")
    @Expose
    public int combo_id;

    @SerializedName("group_id")
    @Expose
    public int group_id;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("change")
    @Expose
    public String change;
}
