package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class AddressDataWrapper implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("nameCity")
    @Expose
    public String nameCity;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("nameDistrict")
    @Expose
    public String nameDistrict;

    @SerializedName("district")
    @Expose
    public String district;
    @SerializedName("ward")
    @Expose
    public String ward;
    @SerializedName("nameWard")
    @Expose
    public String nameWard;


    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("price")
    @Expose
    public WardLimitWrapper price;
}
