package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class DistrictWrapper implements Serializable {
    @SerializedName("nhahang")
    @Expose
    public boolean nhahang;

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("ward")
    @Expose
    public ArrayList<WardWapper> lstWard;


    public static class WardWapper implements Serializable {
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("open_time")
        @Expose
        public String open_time;
        @SerializedName("close_time")
        @Expose
        public String close_time;
        @SerializedName("price")
        @Expose
        public WardLimitWrapper price;

        public WardWapper(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    public DistrictWrapper(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
