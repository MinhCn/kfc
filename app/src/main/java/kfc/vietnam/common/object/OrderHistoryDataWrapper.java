package kfc.vietnam.common.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AnhDuc on 7/18/2016.
 */
public class OrderHistoryDataWrapper implements Serializable {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("result")
    @Expose
    public boolean result;

    @SerializedName("data")
    @Expose
    public ArrayList<OrderHistoryWrapper> data;

    public static class OrderHistoryWrapper implements Serializable {
        @SerializedName("id")
        @Expose
        public int id;

        @SerializedName("orderID")
        @Expose
        public String orderID;

        @SerializedName("price_discount")
        @Expose
        public String price_discount;

        @SerializedName("price_ol")
        @Expose
        public String price_ol;

        @SerializedName("price")
        @Expose
        public String price;

        @SerializedName("date")
        @Expose
        public String date;

        @SerializedName("deliveryStatus")
        @Expose
        public String deliveryStatus;

        @SerializedName("order_name")
        @Expose
        public String order_name;

        @SerializedName("order_address")
        @Expose
        public String order_address;

        @SerializedName("order_city")
        @Expose
        public String order_city;

        @SerializedName("order_district")
        @Expose
        public String order_district;

        @SerializedName("order_phone")
        @Expose
        public String order_phone;

        @SerializedName("order_email")
        @Expose
        public String order_email;

        @SerializedName("payment_type")
        @Expose
        public String payment_type;

        @SerializedName("note")
        @Expose
        public String note;

        @SerializedName("order_ward")
        @Expose
        public String order_ward;


        @SerializedName("product")
        @Expose
        public ArrayList<ProductWrapper> product;
    }


    public static class ProductWrapper implements Serializable {
        @SerializedName("name")
        @Expose
        public String name;

        @SerializedName("quantity")
        @Expose
        public String quantity;

        @SerializedName("price")
        @Expose
        public String price;

        @SerializedName("thanhphan")
        @Expose
        public ArrayList<ElementWrapper> thanhphan;

    }

    public static class ElementWrapper implements Serializable {
        @SerializedName("name")
        @Expose
        public String name;
    }
}
