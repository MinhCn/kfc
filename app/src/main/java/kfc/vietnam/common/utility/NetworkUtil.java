package kfc.vietnam.common.utility;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class NetworkUtil {
    public static String IP = "google.com.vn";

    public static double pingIpAddress(String ip) {
        double avgRtt = -1;
        Process process = null;
        try {
            process = new ProcessBuilder()
                    .command("/system/bin/ping", ip)
                    .redirectErrorStream(true)
                    .start();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String inputLine = "";
            while ((inputLine = bufferedReader.readLine()) != null) {
                if (inputLine.length() > 0 && inputLine.contains("icmp_seq"))
                    break;
            }

            if (inputLine != null && !inputLine.isEmpty()) {
                String s = inputLine.substring(inputLine.indexOf("time"), inputLine.length()).trim();
                avgRtt = Double.parseDouble(s.substring(s.indexOf("=") + 1, s.indexOf("ms")).trim());
            }
        } catch (Exception e) {
            Log.d("anhduc", e.getMessage());
        } finally {
            try {
                if (process != null)
                    process.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Log.d("anhduc", " ");
        Log.d("anhduc", "Network latency on address <" + ip + ">: " + avgRtt + " ms");
        return avgRtt;
    }

}
