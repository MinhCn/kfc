package kfc.vietnam.common.utility;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kfc.vietnam.R;
import kfc.vietnam.common.object.ForgetPasswordDataWrapper;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.SplashActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 15/08/2016.
 */
public class DialogForgotPassword extends Dialog implements View.OnClickListener {
    private Context ctx;
    private EditText edtEmail;

    public DialogForgotPassword(Context ctx) {
        super(ctx);
        this.ctx = ctx;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_forgot_password);

        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        getWindow().setLayout((6 * width) / 7, ActionBar.LayoutParams.WRAP_CONTENT);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtEmail.addTextChangedListener(new TextChangedListener(ctx, edtEmail));

        Button btnOK = (Button) findViewById(R.id.btnOK);
        btnOK.setOnClickListener(this);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.btnOK) {
            String email = edtEmail.getText().toString().trim();
            if (patternMail(email)) {
                getForgotPassword(email);
                dismiss();
            } else {
                edtEmail.requestFocus();
                edtEmail.setError(ctx.getString(R.string.StepOneFragment_006));
                edtEmail.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
            }
        } else if (id == R.id.btnCancel) {
            dismiss();
        }
    }

    private void getForgotPassword(final String email) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, ctx.getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ForgetPasswordDataWrapper> call1 = ApiClient.getJsonClient().getPassword(email);
        call1.enqueue(new Callback<ForgetPasswordDataWrapper>() {

            @Override
            public void onFailure(Call<ForgetPasswordDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                mProgressDialog.dismiss();
                AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
                alert.setTitle(ctx.getResources().getString(R.string.alert_title_info));
                alert.setCancelable(false);
                alert.setMessage(ctx.getString(R.string.pls_try_again));
                alert.setNegativeButton(ctx.getResources().getString(R.string.text_cancel_new),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        });
                alert.create().show();
            }

            @Override
            public void onResponse(Call<ForgetPasswordDataWrapper> arg0, Response<ForgetPasswordDataWrapper> arg1) {
                mProgressDialog.dismiss();

                ForgetPasswordDataWrapper obj = arg1.body();

                if (obj.result) {
                    //Toast.makeText(ctx, ctx.getString(R.string.StepOneFragment_007), Toast.LENGTH_SHORT).show();
                    String message = ctx.getResources().getString(R.string.we_sent_email) + "\n" + email + "\n"
                            + ctx.getResources().getString(R.string.pls_check_follow);
                    AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
                    alert.setTitle(ctx.getResources().getString(R.string.alert_title_info));
                    alert.setCancelable(false);
                    alert.setMessage(message);
                    alert.setNegativeButton(ctx.getResources().getString(R.string.text_cancel_new),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    return;
                                }
                            });
                    alert.create().show();

                } else {
//                    Toast.makeText(ctx, ctx.getString(R.string.StepOneFragment_008), Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
                    alert.setTitle(ctx.getResources().getString(R.string.alert_title_info));
                    alert.setCancelable(false);
                    alert.setMessage(ctx.getString(R.string.StepOneFragment_008));
                    alert.setNegativeButton(ctx.getResources().getString(R.string.text_cancel_new),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    return;
                                }
                            });
                    alert.create().show();
                }
            }
        });
    }

    private class TextChangedListener implements TextWatcher {
        private Context mContext;
        EditText editText;

        public TextChangedListener(Context context, EditText edt) {
            super();
            this.mContext = context;
            this.editText = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            editText.setError(null, null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    private boolean patternMail(String mail) {
        String mau = ".*@.*\\.com.*";
        Pattern pattern = Pattern.compile(mau);
        Matcher matcher = pattern.matcher(mail);
        boolean isMatch = matcher.matches();
        return isMatch;
    }

}
