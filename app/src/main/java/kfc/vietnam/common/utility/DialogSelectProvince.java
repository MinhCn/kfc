package kfc.vietnam.common.utility;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by VietRuyn on 15/08/2016.
 */
public class DialogSelectProvince extends Dialog implements View.OnClickListener {
    private Context ctx;
    private MainActivity activity;
    private RelativeLayout rlSelectProvince;
    public TextView tvCurrentProvinceSelected;

    public DialogSelectProvince(Context ctx) {
        super(ctx);
        this.ctx = ctx;
        this.activity = (MainActivity) ctx;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_select_province);
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        int height = metrics.heightPixels;

        Window window = getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.y = (height * 20) / 100;// % pixel of y
        window.setAttributes(wlp);

        rlSelectProvince = (RelativeLayout) findViewById(R.id.rlSelectProvince);
        rlSelectProvince.setOnClickListener(this);
        tvCurrentProvinceSelected = (TextView) findViewById(R.id.tvCurrentProvinceSelected);
        Button btnContinue = (Button) findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.rlSelectProvince) {
            activity.showDialogChooseProvince();
        } else if (id == R.id.btnContinue) {
            if (activity.currentProvinceSelected != null) {
                dismiss();
                activity.checkFirtRunApp();
                activity.saveCartChange();
            }else {
                rlSelectProvince.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
            }
        }
    }
}
