package kfc.vietnam.common.utility;

import android.text.TextUtils;
import android.util.Log;

import org.apache.commons.lang.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class DateFormatUtils extends DateUtils {

    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
    public static final String HH_MM_DD_MM_YYYY = "HH:mm dd/MM/yyyy";
    public static final String HH_MM_DD_MM_YYYY_ = "HH:mm dd-MM-yyyy";
    public static final String DD_MM_YYYY = "dd/MM/yyyy";
    public static final String DD_MM_YY = "dd/MM/yy";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String HH_MM_SS = "HH:mm:ss";
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static final String DD_MM_YYYY_2 = "dd-MM-yyyy";
    public static final String EEE_DD_MM_YYYY = "EEE-dd/MM/yyyy";

    /**
     * chuyển date từ format cũ thành foramt mới
     *
     * @param dateString
     * @param oldFormat
     * @param newFormat
     * @return
     */
    public static String convertFormat(String dateString, String oldFormat, String newFormat) {
        if (TextUtils.isEmpty(dateString)) {
            return "";
        }

        Date date = parse(dateString, oldFormat);
        return format(date, newFormat);
    }

    /**
     * get date theo format
     *
     * @param date
     * @param format
     * @return
     */
    public static String format(Date date, String format) {
        if (date == null) {
            return "";
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * chuyển chuổi date thành Date object theo format của chuổi
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static Date parse(String dateStr, String format) {
        Date result = null;
        try {
            result = parseDate(dateStr, new String[]{format});

        } catch (Exception e) {
            Log.e("DateFormatUtils", "parse error:  " + e.getMessage());
        }
        return result;
    }

    public static Date str2Date(String dateStr, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date startDate = null;
        try {
            startDate = df.parse(dateStr);
            return startDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    /**
     * Lấy chuỗi thời gian hiện tại theo định dạng
     *
     * @param format
     * @return
     */
    public static String getCurrentDate(String format) {
        Date today = Calendar.getInstance().getTime();
        return format(today, format);
    }

    public static Date getCurrentDate() {
        return new Date();
    }

    /**
     * Lấy thời gian của ngày cách numDay ngày hiện tại
     *
     * @param numDay : số ngày lùi về từ ngày hiện tại
     * @param format : format
     * @return
     */
    public static String getLastDate(int numDay, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -numDay);
        return format(calendar.getTime(), format);
    }

    /**
     * Lấy thời gian của ngày này tháng trước
     *
     * @param format
     * @return
     */
    public static String getLastMonth(String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        return format(calendar.getTime(), format);
    }

    public static String getTimeByFormat(long time, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return format(calendar.getTime(), format);
    }

    public static int getDayOfWeeks(String dateStr, String oldFormat) {
        if (TextUtils.isEmpty(dateStr)) {
            return -1;
        }

        Date date = parse(dateStr, oldFormat);
        if (date == null) {
            return -1;
        }

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.DAY_OF_WEEK);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

    }


    public static boolean isToday(Calendar calendar) {
        Calendar today = Calendar.getInstance();
        if (calendar.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH) && calendar.get(Calendar.MONTH) == today.get(Calendar.MONTH)
                && calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
            return true;
        }
        return false;
    }


}
