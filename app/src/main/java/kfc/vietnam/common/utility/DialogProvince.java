package kfc.vietnam.common.utility;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.food_menu_screen.adapter.ProvinceForMapAdapter;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by VietRuyn on 15/08/2016.
 */
public class DialogProvince extends Dialog {
    private Context ctx;
    private MainActivity activity;
    private ArrayList<ProvinceWrapper> lstProvince;
    private TextView textView;
    private onChooseProvinceListener onChooseProvinceListener;

    public interface onChooseProvinceListener{
        public void onChooseProvinceClick();
    }

    public DialogProvince(Context ctx, ArrayList<ProvinceWrapper> lstProvince, TextView textView,
                          onChooseProvinceListener onChooseProvinceListener) {
        super(ctx);
        this.ctx = ctx;
        this.activity = (MainActivity) ctx;
        this.lstProvince = lstProvince;
        this.textView = textView;
        this.onChooseProvinceListener = onChooseProvinceListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_province);

        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        getWindow().setLayout((6 * width) / 7, ActionBar.LayoutParams.WRAP_CONTENT);

        ListView lvProvince = (ListView) findViewById(R.id.lvProvince);
        ProvinceForMapAdapter provinceAdapter = new ProvinceForMapAdapter(ctx, R.layout.custom_item_province_text_white, lstProvince);
        lvProvince.setAdapter(provinceAdapter);
        lvProvince.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProvinceWrapper obj = lstProvince.get(position);
                textView.setText(obj.name);
                activity.sProvineId = obj.id;
                activity.currentProvinceSelected = obj;
                if (activity.dialogSelectProvince != null)
                    if (activity.dialogSelectProvince.tvCurrentProvinceSelected != null)
                        activity.dialogSelectProvince.tvCurrentProvinceSelected.setText(obj.name);
                onChooseProvinceListener.onChooseProvinceClick();
                dismiss();
            }
        });

    }
}
