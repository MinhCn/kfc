package kfc.vietnam.common.utility;

import android.app.Activity;
import android.content.SharedPreferences;

import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by hdadmin on 12/19/2016.
 */

public class SharedPreferenceUtils {

    public static void firstchooseVN(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = pref.edit();
        if (!pref.getBoolean(SharedPreferenceKeyType.CHOOSE_VN.toString(), false)){
            resetEmptyFirstApp(activity);
            prefEditor.putBoolean(SharedPreferenceKeyType.CHOOSE_VN.toString(), true);
        }
        prefEditor.apply();
    }

    public static void firstchooseEN(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = pref.edit();
        if (!pref.getBoolean(SharedPreferenceKeyType.CHOOSE_EN.toString(), false)){
            resetEmptyFirstApp(activity);
            prefEditor.putBoolean(SharedPreferenceKeyType.CHOOSE_EN.toString(), true);
        }
        prefEditor.apply();
    }

    public static void resetEmptyFirstApp(Activity activity){
       resetFirstMenu(activity);
       resetFirstMenuDetail(activity);
       resetFirstProductDetail(activity);
       resetFirstProductDetailFood(activity);
       resetFirstChangeFood(activity);
    }

    public static void resetFirstMenu(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(Const.SHARED_PREF, 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_MENU.toString(), false);
        prefEditor.apply();
    } 
    
    public static void resetFirstMenuDetail(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(Const.SHARED_PREF, 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_MENU_DETAIL.toString(), false);
        prefEditor.apply();
    } 
    
    public static void resetFirstProductDetail(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(Const.SHARED_PREF, 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_PRODUCT_DETAIL.toString(), false);
        prefEditor.apply();
    }

    public static void resetFirstProductDetailFood(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(Const.SHARED_PREF, 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_PRODUCT_DETAIL_CHANGE_FOOD.toString(), false);
        prefEditor.apply();
    }
    
    public static void resetFirstChangeFood(Activity activity){
        SharedPreferences pref = activity.getSharedPreferences(Const.SHARED_PREF, 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_CHANGE_FOOD.toString(), false);
        prefEditor.apply();
    }
}
