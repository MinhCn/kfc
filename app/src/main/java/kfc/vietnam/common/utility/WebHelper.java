package kfc.vietnam.common.utility;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.Toast;

import java.util.List;

/**
 * Created by hdamin 07/12/2016
 */
public class WebHelper {
    /**
     * Open URL by Default Browser from Activity
     *
     * @param activity
     * @param url
     * @return <code>0</code>  : successfully <br/>
     * <code>1</code>  : browser not found <br/>
     * <code>-1</code> : null or empty URL
     */
    public static int openUrlByExternalBrowser(Activity activity, String url) {
        if (url == null || url.trim().isEmpty()) {
            Toast.makeText(activity, "Can not open empty URL", Toast.LENGTH_SHORT).show();
            return -1;
        }
        url = checkHttpUrl(url);
        PackageManager packageManager = activity.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        List<ResolveInfo> appCount = packageManager.queryIntentActivities(intent, 0);
        if (appCount.size() > 0) {
            activity.startActivity(intent);
            return 0;
        } else {
            Toast.makeText(activity, "Not found compatible browser", Toast.LENGTH_SHORT).show();
            return 1;
        }
    }

    /**
     * Check HTTP protocol URL
     *
     * @param url
     * @return <code>url</code>
     */
    public static String checkHttpUrl(String url) {
        if ((!url.startsWith("http://")) && (!url.startsWith("https://"))) {
            url = "http://" + url;
        }
        return url;
    }
}
