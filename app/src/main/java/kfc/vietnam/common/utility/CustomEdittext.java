package kfc.vietnam.common.utility;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by hdadmin on 12/9/2016.
 */

public class CustomEdittext extends EditText {
    private Context mContext;

    public CustomEdittext(Context context) {
        super(context);
        mContext = context;
    }

    public CustomEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CustomEdittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        setError(null);
        InputMethodManager mgr = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(this.getWindowToken(), 0);
        return false;
    }

}
