package kfc.vietnam.common.utility;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.view.ViewPropertyAnimator;

import kfc.vietnam.R;
import kfc.vietnam.common.object.NotificationData;

/**
 * Created by hdadmin on 1/16/2017.
 */

public class NoficationView extends LinearLayout {

    private LinearLayout llMainBottom;

    private boolean isShow;

    private Animation enterAnim;
    private Animation exitAnim;
    private TextView tvTitle;
    private TextView tvContent;
    private ImageView ivIcon;
    private int mHeight = -1;
    private RelativeLayout llMessage;

    private Animator.AnimatorListener animationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
            if (isShow) {
                setVisibility(VISIBLE);
            }
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (!isShow) {
                setVisibility(GONE);
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }

    };
    private GestureDetector gestureDetector;

    public void showChatNotification() {
        if (!isShow) {
            ViewPropertyAnimator.animate(this).setDuration(300).translationY(getHeightView()).setListener(animationListener);
        }
        isShow = true;
    }

    private int getHeightView() {
        if (mHeight == -1) {
            mHeight = getHeight();
        }
        return mHeight;
    }

    public void hideNotification() {
        if (isShow) {
            ViewPropertyAnimator.animate(this).setDuration(300).translationY(-getHeightView()).setListener(animationListener);
        }
        isShow = false;
//        startAnimation(exitAnim);
    }

    public void setNewMessageEvent(NotificationData data) {
        tvTitle.setText(data.getName());
        tvContent.setText(data.getContent());
    }


    public NoficationView(Context context) {
        super(context);
    }

    public NoficationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_notification, this);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvContent = (TextView) view.findViewById(R.id.tvContent);
        llMessage = (RelativeLayout) view.findViewById(R.id.llMessage);
        enterAnim = AnimationUtils.loadAnimation(context, R.anim.slide_in_top);
        exitAnim = AnimationUtils.loadAnimation(context, R.anim.slide_out_top);
        gestureDetector = new GestureDetector(getContext() , new SingleTapConfirm());
        hideNotification();
    }

    public void setOnClick(final OnclickNotifi click){
        llMessage.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (gestureDetector.onTouchEvent(motionEvent)) {
                    hideNotification();
                    click.onClick();
                    return true;
                } else {
                    hideNotification();
                }
                return true;
            }
        });
    }

    public interface OnclickNotifi {
        void onClick();
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }


}
