package kfc.vietnam.common.utility;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

/**
 * Created by hdadmin on 1/15/2017.
 */

public class ListenerLoadImage implements RequestListener<String, GlideDrawable> {
    private View progressBar;
    private View imageView;

    public ListenerLoadImage(View progressBar , View imageView) {
        this.progressBar = progressBar;
        this.imageView = imageView;
        this.progressBar.setVisibility(View.VISIBLE);
        this.imageView.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
        progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        return false;
    }

    @Override
    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
        progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        return false;
    }
}
