package kfc.vietnam.common.utility;

public interface ResultListener<T> {
	
	public void onSucceed(T result);
	public void onFail(String reason);
	
}
