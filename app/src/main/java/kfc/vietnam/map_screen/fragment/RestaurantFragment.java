package kfc.vietnam.map_screen.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.MarkerOutlet;
import kfc.vietnam.common.object.MarkerOutletDataWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.FontText;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.UIUtils;
import kfc.vietnam.common.utility.VNCharacterUtils;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.DistrictForMapAdapter;
import kfc.vietnam.food_menu_screen.adapter.ImageAdapter;
import kfc.vietnam.food_menu_screen.adapter.ProvinceForMapAdapter;
import kfc.vietnam.main_screen.ui.MainActivity;
import kfc.vietnam.map_screen.adapter.RestaurantByDistrictAdapter;
import kfc.vietnam.map_screen.ui.ViewImageRestaurent;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.data;
import static android.support.v4.content.PermissionChecker.checkSelfPermission;

/**
 * Created by VietRuyn on 10/08/2016.
 * Editted by Dung Nguyen 10/28/2016
 * TODO http://stackoverflow.com/a/14828739/6047924
 */
public class RestaurantFragment extends Fragment
        implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback, View.OnClickListener,
        AdapterView.OnItemClickListener, EasyPermissions.PermissionCallbacks {

    private static final String TAG = "RestaurantFragment";
    public static final int MAP_PADDING_TOP = UIUtils.dpToPx(45);
    public static final int MAP_PADDING_ALL = UIUtils.dpToPx(65);

    private static ArrayList<MarkerOutlet> markerOutlets = new ArrayList<>();
    private static ArrayList<MarkerOutlet> markerOutletsPerCity = new ArrayList<>();
    private static ArrayList<MarkerOutlet> markerOutletsInCity = new ArrayList<>();
    private static ArrayList<MarkerOutlet> markerOutletsInDistrict = new ArrayList<>();

    private GoogleMap googleMap = null;
    private MapView mMapView;
    private LinearLayout llListImage, llMyLocation;
    private RelativeLayout rlDirection;
    private CoordinatorLayout coordinatorLayout;
    private RelativeLayout rlRootMaps;
    private ListView listRestaurant;
    private RecyclerView rcvListImage;
    private Spinner spnProvince, spnDistrict;
    private FontText tvNamePlace, tvAddressPlace, tvTimeOpen, tvtimeClose, tvDrag;
    private View restaurantDetailsView;
    private boolean isClickMarker = false;
    private NestedScrollView restaurantsView;

    private ArrayList<ProvinceWrapper> lstProvince = new ArrayList<>();
    private ArrayList<DistrictWrapper> lstDistrict = new ArrayList<>();
    private LocationManager locationManager;
    private GPSTracker mGPS;

    private double longitude;
    private double latitude;

    private double lat;
    private double lng;

    private LatLng latLng;

    private int heightRoot = 0;

    private int positionProvince = -1;
    private int positionDistrict = -1;


    private boolean selectedCity = false;
    private boolean isSelectedByUser; // check if spinner is selected by user of programming

    private RestaurantByDistrictAdapter listAdapter;

    private CustomBottomSheetBehavior customBehaviorList;
    private BottomSheetBehavior behaviorDetails;

    private MainActivity activity;

    private static final int REQUEST_CODE_LOCATION_PERMISSION = 4321;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        activity = (MainActivity) context;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        selectedCity = false;

        DistrictWrapper tmpDistrictWrapper = new DistrictWrapper("0", context.getString(R.string.map_district));
        ArrayList<DistrictWrapper> districtWrapperArrayList = new ArrayList<>();
        districtWrapperArrayList.add(tmpDistrictWrapper);
        this.lstDistrict.add(tmpDistrictWrapper);
        this.lstProvince.add(0,
                new ProvinceWrapper("0", context.getString(R.string.map_city), districtWrapperArrayList));
        this.lstProvince.addAll(activity.lstProvince);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_map_restaurant, container, false);
        initViews(view);

        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        return view;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(RestaurantFragment.class);
                }
            }
        });

        return anim;
    }

    private void initViews(View view) {
        mGPS = new GPSTracker(getActivity());

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator_layout);
        mMapView = (MapView) view.findViewById(R.id.mapView);
        listRestaurant = (ListView) view.findViewById(R.id.listRestaurant);
        rlRootMaps = (RelativeLayout) view.findViewById(R.id.rlRootMaps);
        ViewTreeObserver observer = rlRootMaps.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                heightRoot = rlRootMaps.getMeasuredHeight();
                rlRootMaps.getViewTreeObserver().removeGlobalOnLayoutListener(
                        this);
            }
        });
        llListImage = (LinearLayout) view.findViewById(R.id.llListImage);
        llMyLocation = (LinearLayout) view.findViewById(R.id.llMyLocation);
        rlDirection = (RelativeLayout) view.findViewById(R.id.rlDirection);
        tvNamePlace = (FontText) view.findViewById(R.id.tvNamePlace);
        tvAddressPlace = (FontText) view.findViewById(R.id.tvAddressPlace);
        tvTimeOpen = (FontText) view.findViewById(R.id.tvTimeOpen);
        tvtimeClose = (FontText) view.findViewById(R.id.tvtimeClose);
        tvDrag = (FontText) view.findViewById(R.id.tv_drag);
        rcvListImage = (RecyclerView) view.findViewById(R.id.rcvListImage);


        spnProvince = (Spinner) view.findViewById(R.id.spnProvince);
        ProvinceForMapAdapter provinceAdapter = new ProvinceForMapAdapter(getActivity(),
                R.layout.custom_item_district_province_spinner_for_map, lstProvince);
        spnProvince.setAdapter(provinceAdapter);
        spnProvince.setOnItemSelectedListener(new MyOnItemSelectedListener());
        spnProvince.setSelection(0);

        spnDistrict = (Spinner) view.findViewById(R.id.spnDistrict);
        DistrictForMapAdapter districtAdapter = new DistrictForMapAdapter(getActivity(),
                R.layout.custom_item_district_province_spinner_for_map, lstDistrict);
        spnDistrict.setAdapter(districtAdapter);
        spnDistrict.setOnItemSelectedListener(new MyOnItemSelectedListener());
        spnProvince.setSelection(0);


        llMyLocation.setOnClickListener(this);
        rlDirection.setOnClickListener(this);

        listAdapter = new RestaurantByDistrictAdapter(getContext(), new ArrayList<MarkerOutlet>());
        listRestaurant.setAdapter(listAdapter);
        listRestaurant.setOnItemClickListener(this);

        restaurantsView = (NestedScrollView) view.findViewById(R.id.bottom_sheet_list);
        restaurantDetailsView = view.findViewById(R.id.bottom_sheet_details);

        setupBottomSheets();
    }

    private void setupBottomSheets() {

        customBehaviorList = CustomBottomSheetBehavior.from(restaurantsView);
        customBehaviorList.setState(CustomBottomSheetBehavior.STATE_COLLAPSED);
        customBehaviorList.setAllowUserDragging(false);
        showRestaurants(false);

        customBehaviorList.addBottomSheetCallback(new CustomBottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet,
                                       @CustomBottomSheetBehavior.State int newState) {
                if (newState == CustomBottomSheetBehavior.STATE_COLLAPSED
                        || newState == CustomBottomSheetBehavior.STATE_HIDDEN) {

                    tvDrag.setText(getString(R.string.drag_up));
                    if (latLng != null && !isClickMarker) {
                        googleMap.setPadding(0, MAP_PADDING_TOP, 0, 0);
                        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng), 300, null);
                    }

                } else if (newState == CustomBottomSheetBehavior.STATE_ANCHOR_POINT) {

                    if (latLng != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                googleMap.setPadding(0, MAP_PADDING_TOP, 0, (int) (heightRoot / 2.5) + MAP_PADDING_TOP);
                                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng), 300, null);
                            }
                        });
                    }

                } else if (newState != CustomBottomSheetBehavior.STATE_SETTLING) {
                    tvDrag.setText(getString(R.string.drag_down));
                    Log.d(TAG, String.valueOf(newState));
                }

                restaurantsView.post(new Runnable() {
                    @Override
                    public void run() {
                        restaurantsView.requestLayout();
                        restaurantsView.invalidate();
                    }
                });
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                coordinatorLayout.bringChildToFront(bottomSheet);
            }
        });

        behaviorDetails = BottomSheetBehavior.from(restaurantDetailsView);
        behaviorDetails.setState(BottomSheetBehavior.STATE_HIDDEN);
        behaviorDetails.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    showRestaurants(true);
                   isClickMarker = false;
                    if (lat != 0.0 && lng != 0.0) {
                        googleMap.setPadding(0, MAP_PADDING_TOP, 0, 0);
                        googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        listAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                customBehaviorList.setAllowUserDragging(listAdapter.getCount() > 0);
                Log.d(TAG, "onChanged: " + listAdapter.getCount());
            }
        });
    }

    private void getRestaurantLocation(String lang) {
        activity.disableTab(false);
        final ProgressDialog mProgressDialog =
                new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            activity.disableTab(true);
            mProgressDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<MarkerOutletDataWrapper> call1 = ApiClient.getJsonClient().getRestaurentLocation(lang);
        call1.enqueue(new Callback<MarkerOutletDataWrapper>() {

            @Override
            public void onFailure(Call<MarkerOutletDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                activity.disableTab(true);
                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<MarkerOutletDataWrapper> arg0,
                                   Response<MarkerOutletDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    if (arg1.body().data != null){
                        int data = arg1.body().data.size();
                        if (data > 0) {
                            markerOutlets = arg1.body().data;
                            markerOutletsPerCity = getMarkerOutLetsPerCity(markerOutlets);

                            LatLngBounds bounds = createMarkers(markerOutletsPerCity);
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, MAP_PADDING_ALL);
                            googleMap.setPadding(0, MAP_PADDING_TOP, 0, 0);
                            googleMap.moveCamera(cu);
                            latLng = googleMap.getCameraPosition().target;
                        }
                    }
                }
                activity.disableTab(true);
            }
        });
    }

    private ArrayList<MarkerOutlet> getMarkerOutLetsPerCity(ArrayList<MarkerOutlet> markerOutlets) {
        ArrayList<MarkerOutlet> tmpList = new ArrayList<>();
        ArrayList<String> tmpListProvinceID = new ArrayList<>();
        for (MarkerOutlet marker : markerOutlets) {
            if (!tmpListProvinceID.contains(marker.provinceID)) {
                tmpListProvinceID.add(marker.provinceID);
                tmpList.add(marker);
            }
        }

        return tmpList;
    }

    private ArrayList<MarkerOutlet> getMarkerOutletsInCity(ArrayList<MarkerOutlet> markerOutlets,
                                                           String provinceID) {
        ArrayList<MarkerOutlet> tmpList = new ArrayList<>();
        for (MarkerOutlet marker : markerOutlets) {
            if (marker.provinceID.equals(provinceID)) {
                tmpList.add(marker);
            }
        }

        return tmpList;
    }

    private ArrayList<MarkerOutlet> getMarkerOutletsInDistrict(
            ArrayList<MarkerOutlet> markerOutletsInCity, String districtID) {
        ArrayList<MarkerOutlet> tmpList = new ArrayList<>();
        for (MarkerOutlet marker : markerOutletsInCity) {
            if (marker.districtId.equals(districtID)) {
                tmpList.add(marker);
            }
        }

        return tmpList;
    }

    private ProvinceWrapper getProvinceById(String id) {
        if (!TextUtils.isEmpty(id) && lstProvince != null && !lstProvince.isEmpty()) {
            for (ProvinceWrapper province : lstProvince) {
                if (province.id.equals(id)) {
                    return province;
                }
            }
        }

        return null;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        lat = marker.getPosition().latitude;
        lng = marker.getPosition().longitude;
        MarkerOutlet markerOutlet = (MarkerOutlet) marker.getTag();

        if (markerOutlet != null) {
            if (selectedCity) {
                showRestaurantDetails(markerOutlet);
            } else {
                selectedCity = true;
                googleMap.clear();

                for (int i = 0; i < lstProvince.size(); i++) {
                    ProvinceWrapper tmpProvinceWrapper = lstProvince.get(i);
                    if (markerOutlet.provinceID.equals(tmpProvinceWrapper.id)) {
                        spnProvince.setSelection(i);
                        //Edit by @mManhNguyen
                        List<DistrictWrapper> districtHasRestaurant = new ArrayList<>();
                        for (DistrictWrapper d:tmpProvinceWrapper.district) {
                            if (d.nhahang){
                                districtHasRestaurant.add(d);
                            }
                        }
//                        lstDistrict.addAll(tmpProvinceWrapper.district);
                        lstDistrict.addAll(districtHasRestaurant);
                        ((DistrictForMapAdapter) spnDistrict.getAdapter()).notifyDataSetChanged();
                    }
                }

                markerOutletsInCity = getMarkerOutletsInCity(markerOutlets, markerOutlet.provinceID);

                LatLngBounds bounds = createMarkers(markerOutletsInCity);
                int padding = 100; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                googleMap.animateCamera(cu);
            }
        } else {
            Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMyLocation:
                showMyLocation();
                break;
            case R.id.rlDirection:
                showRestaurants(false);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=" + lat + "," + lng));
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (listAdapter != null && listAdapter.getCount() > i) {
            MarkerOutlet outlet = (MarkerOutlet) listAdapter.getItem(i);
            showRestaurantDetails(outlet);
        }
    }

    private LatLngBounds createMarkers(ArrayList<MarkerOutlet> markers) {
        LatLngBounds.Builder tmpBuilder = new LatLngBounds.Builder();

        for (MarkerOutlet tmp : markers) {
            double gpsLat = 0;
            double gpsLong = 0;

            if (isNumeric(tmp.latitude)) {
                gpsLat = Double.parseDouble(tmp.latitude);
            }
            if (isNumeric(tmp.longitude)) {
                gpsLong = Double.parseDouble(tmp.longitude);
            }

            if (0 != gpsLat && 0 != gpsLong) {
                MarkerOptions markerOptions = new MarkerOptions().icon(
                        BitmapDescriptorFactory.fromResource(R.drawable.ic_maker_restaurent));
                Marker tmpMarker = googleMap.addMarker(markerOptions.position(new LatLng(gpsLat, gpsLong))
                        .title(tmp.name)
                        .snippet(tmp.address));
                tmpMarker.setTag(tmp);
                tmpBuilder.include(tmpMarker.getPosition());
            }
        }

        return tmpBuilder.build();
    }

    private void showMarkers(String cityId, String districtId) {
        markerOutletsInCity = getMarkerOutletsInCity(markerOutlets, cityId);
        ProvinceWrapper province = getProvinceById(cityId);

        if (province != null) {
            lstDistrict.clear();
            DistrictWrapper tmpDistrictWrapper = new DistrictWrapper("0", getActivity().getString(R.string.map_district));
            lstDistrict.add(tmpDistrictWrapper);
            //Edit by @ManhNguyen
            List<DistrictWrapper> districtHasRestaurant = new ArrayList<>();
            for (DistrictWrapper d:province.district) {
                if (d.nhahang){
                    districtHasRestaurant.add(d);
                }
            }
            lstDistrict.addAll(districtHasRestaurant);
//            lstDistrict.addAll(province.district);


            if (!TextUtils.isEmpty(districtId)) {
                showMarkersForDistrict(districtId);
                // set data to spinner
                for (int i = 0; i < lstDistrict.size(); i++) {
                    DistrictWrapper district = lstDistrict.get(i);
                    if (district.id.equals(districtId)) {
                        spnDistrict.setSelection(i);
                        break;
                    }
                }
            } else {
                showMarkersForCity(province);
            }

            for (int i = 0; i < lstProvince.size(); i++) {
                ProvinceWrapper tmp = lstProvince.get(i);
                if (tmp.id.equals(cityId)) {
                    spnProvince.setSelection(i);
                    break;
                }
            }
        }
    }

    private void showMarkersForCity(ProvinceWrapper province) {
        googleMap.clear();
        lstDistrict.clear();

        DistrictWrapper tmpDistrictWrapper = new DistrictWrapper("0", getActivity().getString(R.string.map_district));
        lstDistrict.add(tmpDistrictWrapper);
        //Edit by @ManhNguyen
        List<DistrictWrapper> districtHasRestaurant = new ArrayList<>();
        for (DistrictWrapper d:province.district) {
            if (d.nhahang){
                districtHasRestaurant.add(d);
            }
        }
        lstDistrict.addAll(districtHasRestaurant);
//        lstDistrict.addAll(province.district);

        ((DistrictForMapAdapter) spnDistrict.getAdapter()).notifyDataSetChanged();
        if (!isSelectedByUser) {
            spnDistrict.setSelection(0);
        }

        markerOutletsInCity = getMarkerOutletsInCity(markerOutlets, province.id);
        showData(markerOutletsInCity);
    }

    private void showMarkersForDistrict(String districtId) {

        googleMap.clear();
        markerOutletsInDistrict = getMarkerOutletsInDistrict(markerOutletsInCity, districtId);
        showData(markerOutletsInDistrict);
    }

    private void showData(ArrayList<MarkerOutlet> data) {
        LatLngBounds.Builder tmpBuilder = new LatLngBounds.Builder();
        List<MarkerOutlet> lstOnDistrict = new ArrayList<>();
        for (MarkerOutlet tmp : data) {
            double gpsLat = 0;
            double gpsLong = 0;

            if (isNumeric(tmp.latitude)) {
                gpsLat = Double.parseDouble(tmp.latitude);
            }
            if (isNumeric(tmp.longitude)) {
                gpsLong = Double.parseDouble(tmp.longitude);
            }

            if (0 != gpsLat && 0 != gpsLong) {
                googleMap.setPadding(0, MAP_PADDING_TOP, 0, 0);
                MarkerOptions markerOptions = new MarkerOptions().icon(
                        BitmapDescriptorFactory.fromResource(R.drawable.ic_maker_restaurent));
                Marker marker = googleMap.addMarker(markerOptions.position(new LatLng(gpsLat, gpsLong))
                        .title(tmp.name)
                        .snippet(tmp.address));
                marker.setTag(tmp);
                if (data.size() == 1) marker.showInfoWindow();
                tmpBuilder.include(marker.getPosition());
                lstOnDistrict.add(tmp);
            }
        }
        if (lstOnDistrict.size() > 0) {
            showRestaurants(true);
            listAdapter.setItems(lstOnDistrict);
            listRestaurant.setAdapter(listAdapter);
        } else {
            listAdapter.setItems(new ArrayList<MarkerOutlet>());
        }
        if (data.size() > 0) {
            LatLngBounds bounds = tmpBuilder.build();
            int padding = 100;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    latLng = googleMap.getCameraPosition().target;
                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    private void showRestaurants(boolean isShown) {
        if (isShown) {
            customBehaviorList.setState(CustomBottomSheetBehavior.STATE_COLLAPSED);
            customBehaviorList.setHideable(false);
        } else {
            customBehaviorList.setHideable(true);
            customBehaviorList.setState(CustomBottomSheetBehavior.STATE_HIDDEN);
        }
    }

    private void showRestaurantDetails(final MarkerOutlet outlet) {

        if (!TextUtils.isEmpty(outlet.latitude) && !TextUtils.isEmpty(outlet.longitude)) {
            lat = Double.valueOf(outlet.latitude);
            lng = Double.valueOf(outlet.longitude);
        }

        tvAddressPlace.setText(outlet.address);
        tvNamePlace.setText(outlet.name);
        tvtimeClose.setText(outlet.time_close);
        tvTimeOpen.setText(outlet.time_open);

        if (!outlet.image.isEmpty()) {
            llListImage.setVisibility(View.VISIBLE);
            final ImageAdapter imageAdapter = new ImageAdapter(outlet.image, getActivity(), new ImageAdapter.ListenerItem() {
                @Override
                public void onItemClick(ArrayList<String> listData, int position) {
                    Intent intent = new Intent(getActivity(), ViewImageRestaurent.class);
                    intent.putExtra("list_data", listData);
                    intent.putExtra("position", position);
                    startActivity(intent);
                }

                @Override
                public void onLongItemClick(String url, int position) {

                }
            });
            LinearLayoutManager layoutManager =
                    new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rcvListImage.setLayoutManager(layoutManager);
            rcvListImage.setAdapter(imageAdapter);
        }

        isClickMarker = true;

        showRestaurants(false);
        behaviorDetails.setState(BottomSheetBehavior.STATE_EXPANDED);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
                googleMap.setPadding(0, MAP_PADDING_TOP, 0, (int) (heightRoot / 2.5) + MAP_PADDING_TOP);
            }
        });
    }

    private void getAddressByLocation(double latitude, double longitude) {
        if (isAdded()) {
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String state = addresses.get(0).getAdminArea();
                    String district = addresses.get(0).getSubAdminArea();
                    Log.d("test", "state: distric = " + state + ": " + district);

                    //Add by @ManhNguyen
                    SharedPreferences preferencesLanguage = activity.getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), activity.MODE_PRIVATE);
                    String currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
                    ////Compare string vn or en
                    String[] ids;
                    if (currentLanguageCode.equals("vn")) {
                        Log.d("test 1", "state: district = " + state + ": " + district);
                        ids = getIdsFromAddress(state, district);
                    } else { //en
                        String state_en = VNCharacterUtils.removeAccent(state); //convert sang khong dau
                        String district_en_temp = VNCharacterUtils.removeAccent(district); //convert sang khong dau
                        String district_en;
                        if (district_en_temp.contains("Quan")){
                            district_en = district_en_temp.replace("Quan", "District");
                        } else {
                            district_en = district_en_temp;
                        }
                        Log.d("test 2", "state: district = " + state_en + ": " + district_en);

                        ids = getIdsFromAddress(state_en, district_en);
                    }

                    if (ids != null) {
                        if (ids.length == 2) {
                            isSelectedByUser = true;
                            showMarkers(ids[0], ids[1]);
                            Log.d("test", "ids = " + ids[0] + ": " + ids[1]);
                        } else if (ids.length == 1) {
                            isSelectedByUser = true;
                            showMarkers(ids[0], null);
                            Log.d("test", "ids = " + ids[0]);
                        }
                    }
                }
            } catch (Exception e) {
                Log.d("test", "exception: " + e.getMessage());
            }
        }
    }

    private String[] getIdsFromAddress(String city, String districtName) {

        if (lstProvince != null && !lstProvince.isEmpty()) {
            for (ProvinceWrapper province : lstProvince) {
                if (province.name.contains(city)) {
                    Log.d("province", province.name + " vs " + city);
                    if (province.district != null && !province.district.isEmpty()) {
                        for (DistrictWrapper district : province.district) {
                            if (district.name.contains(districtName)) {
                                Log.d("district", district.name + " vs " + districtName);
                                return new String[]{province.id, district.id};
                            }
                        }
                    }
                    return new String[]{province.id};
                }
            }
        }
        return null;
    }

    private class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            int id = adapterView.getId();
            if (i == 0) {
                return;
            }
            if (id == R.id.spnProvince) {
                if (view != null) {
                    try {
                        positionProvince = i;
                        if (i > 0) {
                            selectedCity = true;
                            Log.e("selectedcity", "true2");
                            latLng = null;
                            lat = 0.0;
                            lng = 0.0;
                        }
                        showMarkersForCity(lstProvince.get(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (id == R.id.spnDistrict) {
                if (view != null) {
                    DistrictWrapper obj = ((DistrictWrapper) view.findViewById(R.id.tvName).getTag());
                    showMarkersForDistrict(obj.id);
                }
            }
            isSelectedByUser = false;
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            selectedCity = false;
        }
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        MapsInitializer.initialize(getActivity());
        UiSettings settings = googleMap.getUiSettings();

        settings.setMyLocationButtonEnabled(false);
        settings.setMapToolbarEnabled(false);

        latLng = null;
        isClickMarker = false;
        customBehaviorList.setState(CustomBottomSheetBehavior.STATE_COLLAPSED);
        behaviorDetails.setState(BottomSheetBehavior.STATE_HIDDEN);
        if (markerOutlets != null) {
            String code = activity.currentLanguageCode;
            getRestaurantLocation(code);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @AfterPermissionGranted(REQUEST_CODE_LOCATION_PERMISSION)
    public void showLocationTask(){
        showMyLocation();
    }

    public void showMyLocation(){
        if (hasCameraPermission()){
            if (googleMap != null) {
                googleMap.setOnMarkerClickListener(RestaurantFragment.this);
                if (checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                }
                googleMap.setMyLocationEnabled(true);
            }
            showRestaurants(false);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                mGPS.getLocation();
                longitude = mGPS.getLongitude();
                latitude = mGPS.getLatitude();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude))
                        .zoom(15)
                        .build();

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                getAddressByLocation(latitude, longitude);
            } else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.gps_not_turn_on))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                startActivity(
                                        new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();
            }
        } else {
            EasyPermissions.requestPermissions(
                    RestaurantFragment.this,
                    getString(R.string.rationale),
                    REQUEST_CODE_LOCATION_PERMISSION,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d("onPermissionsGranted:", requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d("onPermissionsDenied:", requestCode + ":" + perms.size());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}