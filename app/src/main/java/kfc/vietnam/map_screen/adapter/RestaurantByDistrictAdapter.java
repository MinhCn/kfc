package kfc.vietnam.map_screen.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import kfc.vietnam.common.object.MarkerOutlet;
import kfc.vietnam.common.object.MarkerOutletDataWrapper;
import kfc.vietnam.map_screen.ui.RestaurantItemView;

/**
 * Created by admin on 11/1/16.
 */

public class RestaurantByDistrictAdapter extends BaseAdapter {

    List<MarkerOutlet> lstRestaurant = null;
    Context context = null;
    public RestaurantByDistrictAdapter(Context _context, List<MarkerOutlet> _lst){
        super();
        context = _context;
        lstRestaurant = _lst;
    }
    @Override
    public int getCount() {
        return lstRestaurant.size();
    }

    @Override
    public Object getItem(int position) {
        return lstRestaurant.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lstRestaurant.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RestaurantItemView view = new RestaurantItemView(context);
        view.setView((MarkerOutlet) getItem(position));
        return view;
    }

    public void setItems(List<MarkerOutlet> items){
        lstRestaurant = items;
        this.notifyDataSetChanged();
    }
}
