package kfc.vietnam.map_screen.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.common.object.MarkerOutlet;
import kfc.vietnam.common.utility.FontText;

/**
 * Created by admin on 11/1/16.
 */

public class RestaurantItemView extends LinearLayout {
    private Context context;
    private ImageView ivIcon;
    private TextView tvAddress;
    private TextView tvName;
    private RelativeLayout layout;
    private FontText tvTimeClose, tvTimeOpen;


    public RestaurantItemView(Context _context){
        super(_context);
        context = _context;
    }

    public void setView(MarkerOutlet _item){
        layout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.layout_item_restaurant, null);
        ivIcon = (ImageView) layout.findViewById(R.id.ivIcon);
        tvName = (TextView) layout.findViewById(R.id.tvName);
        tvAddress = (TextView) layout.findViewById(R.id.tvAddress);
        tvTimeOpen = (FontText) layout.findViewById(R.id.tvTimeOpen);
        tvTimeClose = (FontText) layout.findViewById(R.id.tvTimeClose);
        tvName.setText(_item.name);
        tvAddress.setText(_item.address);
        tvTimeClose.setText(_item.time_close);
        tvTimeOpen.setText(_item.time_open);
        addView(layout);
    }

}
