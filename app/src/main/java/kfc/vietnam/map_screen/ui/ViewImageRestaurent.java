package kfc.vietnam.map_screen.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.map_screen.adapter.SlideAdapterImage;

/**
 * Created by VietRuyn on 15/11/2016.
 */

public class ViewImageRestaurent extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurent_picture);
        ArrayList<String> urls = getIntent().getExtras().getStringArrayList("list_data");
        int position = getIntent().getExtras().getInt("position");
        ImageView imgClose = (ImageView) findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ViewPager pagerRestaurent = (ViewPager) findViewById(R.id.pagerRestaurent);
        SlideAdapterImage adapterImage = new SlideAdapterImage(this , urls);
        pagerRestaurent.setAdapter(adapterImage);
        pagerRestaurent.setCurrentItem(position);
    }
}

