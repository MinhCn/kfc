package kfc.vietnam.promotion_screen.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.DetailPromotion;
import kfc.vietnam.common.object.DetailPromotionWrapper;
import kfc.vietnam.common.object.LikeNewsDataWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.ListenerLoadImage;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import kfc.vietnam.promotion_screen.listener.ListenerLikeDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.bitmap;
import static android.R.attr.width;

public class DetailsPromotionFragment extends Fragment implements View.OnClickListener, Html.ImageGetter {
    ImageView imgBanner, imgLike;
    TextView tvTime, tvCountLike, tvDescription;
    private ProgressBar pbLoading;
    private LinearLayout llShare;
    private String currentLanguageCode = "";
    private FragmentActivity ctx;
    private ArrayList<ProvinceWrapper> lstProvince;
    private ViewPager pager;
    private DetailPromotionWrapper detailPromotionWrapper;
    private String idNew;
    private MainActivity activity;
    private ListenerLikeDetail listenerLikeDetail;


    @Override
    public void onAttach(Context context) {
        ctx = (FragmentActivity) context;
        super.onAttach(context);

        Bundle args = getArguments();
        if (args.containsKey("promotion")) {
            detailPromotionWrapper = (DetailPromotionWrapper) args.getSerializable("promotion");
        } else if (args.containsKey("id_new")) {
            idNew = args.getString("id_new");
        }
        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
        this.lstProvince = activity.lstProvince;

        ((MainActivity) ctx).rlMenu1.setVisibility(View.GONE);
        ((MainActivity) ctx).rlMenu2.setVisibility(View.VISIBLE);

        ((MainActivity) ctx).llCall.setEnabled(false);
        ((MainActivity) ctx).llShoppingCart.setEnabled(false);
        ((MainActivity) ctx).llProvince.setEnabled(false);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        ((MainActivity) ctx).rlMenu1.setVisibility(View.VISIBLE);
        ((MainActivity) ctx).rlMenu2.setVisibility(View.GONE);

        ((MainActivity) ctx).llCall.setEnabled(true);
        ((MainActivity) ctx).llShoppingCart.setEnabled(true);
        ((MainActivity) ctx).llProvince.setEnabled(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) ctx).llCall.setEnabled(true);
        ((MainActivity) ctx).llShoppingCart.setEnabled(true);
        ((MainActivity) ctx).llProvince.setEnabled(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_promotion_detail, container, false);
        imgBanner = (ImageView) view.findViewById(R.id.imgBanner);
        pbLoading = (ProgressBar) view.findViewById(R.id.pbLoading);
        imgLike = (ImageView) view.findViewById(R.id.imgLike);
        tvTime = (TextView) view.findViewById(R.id.tvTime);
        tvCountLike = (TextView) view.findViewById(R.id.tvCountLike);
        tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        llShare = (LinearLayout) view.findViewById(R.id.llShare);
        if (detailPromotionWrapper != null) {
            getDetailPromotion(String.valueOf(detailPromotionWrapper.getId()));
//            setData(detailPromotionWrapper);
        } else if (!TextUtils.isEmpty(idNew)) {
            getDetailPromotion(idNew);
        }

        imgLike.setOnClickListener(this);
        llShare.setOnClickListener(this);


        return view;
    }

    private void setData(DetailPromotionWrapper detailPromotionWrapper) {
        ((MainActivity) ctx).tvPromotionDetail.setText(detailPromotionWrapper.name);
        ((MainActivity) ctx).rlMenuPromotion.setVisibility(View.VISIBLE);

        if (detailPromotionWrapper.like != null && !detailPromotionWrapper.like.equals("")) {
            tvCountLike.setText(detailPromotionWrapper.like);
        } else {
            tvCountLike.setText("0");
        }

        if (detailPromotionWrapper.isLike) {
            int count = 0;
            if (detailPromotionWrapper.like != null) {
                count = Integer.parseInt((detailPromotionWrapper.like));
            }
//            tvCountLike.setText((count + 1) + "");
            tvCountLike.setText(count + "");
//            imgLike.setImageResource(R.drawable.ic_liked);
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorited_food_3, getActivity().getTheme()));
            } else {
                imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorited_food_3));
            }
        } else {
//            imgLike.setImageResource(R.drawable.ic_favorite_food_3);
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_food_3, getActivity().getTheme()));
            } else {
                imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_food_3));
            }
        }
        tvTime.setText(detailPromotionWrapper.dt_create);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            tvDescription.setText(Html.fromHtml(detailPromotionWrapper.content, Html.FROM_HTML_MODE_LEGACY));
            Spanned spanned = Html.fromHtml(detailPromotionWrapper.content, Html.FROM_HTML_MODE_LEGACY, this, null);
            tvDescription.setText(spanned);
        } else {
            Spanned spanned = Html.fromHtml(detailPromotionWrapper.content, this, null);
            tvDescription.setText(spanned);
//            tvDescription.setText(Html.fromHtml(detailPromotionWrapper.content));
        }

        Glide.with(this)
                .load(detailPromotionWrapper.banner)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new ListenerLoadImage(pbLoading, imgBanner))
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        imgBanner.setImageDrawable(resource);
                    }
                });
    }

    // Sự kiện lắng nghe like detail promotion
    public void setListenerLikeDetail(ListenerLikeDetail listenerLikeDetail) {
        this.listenerLikeDetail = listenerLikeDetail;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgLike:
                if (!detailPromotionWrapper.isLike)
                    likeNews(detailPromotionWrapper.id.toString());
                else
                    Toast.makeText(getContext(), R.string.info_like, Toast.LENGTH_SHORT).show();
                break;
            case R.id.llShare:
                share();
                break;
        }
    }

    private void likeNews(String newsID) {
//        imgLike.setEnabled(false);
        imgLike.setEnabled(true);
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<LikeNewsDataWrapper> call1 = ApiClient.getJsonClient().likeNews(newsID);
        call1.enqueue(new Callback<LikeNewsDataWrapper>() {

            @Override
            public void onFailure(Call<LikeNewsDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<LikeNewsDataWrapper> arg0, Response<LikeNewsDataWrapper> arg1) {
                mProgressDialog.dismiss();

                LikeNewsDataWrapper obj = arg1.body();
                if (obj.result) {
                    detailPromotionWrapper.isLike = true;
                    imgLike.setImageResource(R.drawable.ic_liked);
                    int count = 0;
                    if (detailPromotionWrapper.like != null) {
                        count = Integer.parseInt((detailPromotionWrapper.like));
                    }
                    tvCountLike.setText((count + 1) + "");
                    if (listenerLikeDetail != null) {
                        listenerLikeDetail.onLike(detailPromotionWrapper.id);
                    }
                }

            }
        });
    }


    private void getDetailPromotion(String newsID) {
//        imgLike.setEnabled(false);
        imgLike.setEnabled(true);
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<DetailPromotion> call1 = ApiClient.getJsonClient().getDetailPromotion(newsID, currentLanguageCode);
        call1.enqueue(new Callback<DetailPromotion>() {

            @Override
            public void onFailure(Call<DetailPromotion> arg0, Throwable arg1) {
                arg1.printStackTrace();
                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<DetailPromotion> arg0, Response<DetailPromotion> arg1) {
                mProgressDialog.dismiss();

                DetailPromotion obj = arg1.body();
                if (obj.data != null && obj.result) {
                    DetailPromotionWrapper detailPromotionWrapper = obj.data;
                    if (detailPromotionWrapper != null) {
                        DetailsPromotionFragment.this.detailPromotionWrapper = detailPromotionWrapper;
                        setData(detailPromotionWrapper);
                    }
                }

            }
        });
    }

    public void share() {
        if (detailPromotionWrapper.link != null) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, detailPromotionWrapper.link);
            startActivity(Intent.createChooser(intent, "Share with"));
        }
    }

    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable d = new LevelListDrawable();
//        Drawable empty = getResources().getDrawable(R.drawable.ic_launcher);
//        d.addLevel(0, 0, empty);
//        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int width = displayMetrics.widthPixels;
//        int temp = (width - empty.getIntrinsicWidth())/4;
//        d.setBounds(temp, 0, empty.getIntrinsicWidth() - temp, empty.getIntrinsicHeight());
//
        new LoadImage().execute(source, d);

        return d;
    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
//                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());

                DisplayMetrics displayMetrics = new DisplayMetrics();
                activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int width = displayMetrics.widthPixels;

                if (width > 1080) { //màn hình 2k
                    int temp = (width - bitmap.getWidth())/2 - 60;
                    mDrawable.setBounds(temp, 0, bitmap.getWidth() + temp , bitmap.getHeight());
                } else if (width < 1000) { //màn hình HD, qHD
//                    int temp =  bitmap.getWidth() - width;
                    mDrawable.setBounds(0, 0, width - 30, bitmap.getHeight());
                } else { // màn hình full HD
                    int temp = (width - bitmap.getWidth())/2;
                    mDrawable.setBounds(temp, 0, bitmap.getWidth() - temp, bitmap.getHeight());
                }

                mDrawable.setLevel(1);
                CharSequence t = tvDescription.getText();
                tvDescription.setText(t);
            }
        }
    }
}

