package kfc.vietnam.promotion_screen.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.DetailPromotionWrapper;
import kfc.vietnam.common.object.PromotionWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import kfc.vietnam.promotion_screen.adapter.PromotionPagerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 17/07/2016.
 */
public class PromotionFragment extends Fragment {
    private String currentLanguageCode = "";
    private ArrayList<ProvinceWrapper> lstProvince;

    public final static int LOOPS = 1;
    public PromotionPagerAdapter adapter;
    public ViewPager pager;
    public static int count = 0; //ViewPager items size
    /**
     * You shouldn't define first page = 0.
     * Let define firstpage = 'number viewpager size' to make endless carousel
     */
    public static int FIRST_PAGE = 1;
    private List<ImageView> dots;
    private FragmentActivity ctx;
    private LinearLayout dotsLayout;
    private HorizontalScrollView dotsParentLayout;

//    private static ArrayList<CategoryWrapper> lstCategory;


    private static ArrayList<PromotionWrapper> listPromotionWrappers;
    private static ArrayList<DetailPromotionWrapper> lstCategory;

    @Override
    public void onAttach(Context context) {
        ctx = (FragmentActivity) context;
        super.onAttach(context);

        MainActivity activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
        this.lstProvince = activity.lstProvince;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_promotion_fragment, container, false);

        init(view);

        if (lstCategory == null) {
            getNew(currentLanguageCode);
        } else {
            count = lstCategory.size();
            initViewPage();
            addDots();
            selectDot(1);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        pager.setCurrentItem(FIRST_PAGE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initViewPage() {
        //set page margin between pages for viewpager
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int pageMargin = ((metrics.widthPixels / 9) * 2);
        pager.setPageMargin(-pageMargin);

        adapter = new PromotionPagerAdapter(getActivity(), ((MainActivity) getActivity()).promotionFragment, getChildFragmentManager(), lstCategory, currentLanguageCode, lstProvince);
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        pager.addOnPageChangeListener(adapter);

        // Set current item to the middle page so we can fling to both
        // directions left and right
        pager.setCurrentItem(FIRST_PAGE);
        pager.setOffscreenPageLimit(3);
    }

    private void init(View view) {
        pager = (ViewPager) view.findViewById(R.id.myviewpager);
        dotsLayout = (LinearLayout) view.findViewById(R.id.dots);
        dotsParentLayout = (HorizontalScrollView) view.findViewById(R.id.dotsParentLayout);
    }

    public void addDots() {
        dots = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            ImageView dot = new ImageView(getActivity());
            if (i == 0)
                dot.setImageDrawable(getResources().getDrawable(R.drawable.pager_dot_selected));
            else
                dot.setImageDrawable(getResources().getDrawable(R.drawable.pager_dot_not_selected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
//            params.setMargins(2, 0, 2, 10);
            params.setMargins(10, 0, 10, 0);
            dot.setLayoutParams(params);
            dotsLayout.addView(dot, params);
            dots.add(dot);
        }

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectDot(position);
                if (position > 15)
                    dotsParentLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dotsParentLayout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                        }
                    },100L);
                else
                    dotsParentLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dotsParentLayout.fullScroll(HorizontalScrollView.FOCUS_LEFT);
                        }
                    },100L);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void selectDot(int idx) {
        try {
            Resources res = getResources();
            for (int i = 0; i < count; i++) {
                int drawableId = (i == idx) ? (R.drawable.pager_dot_selected) : (R.drawable.pager_dot_not_selected);
                Drawable drawable = res.getDrawable(drawableId);
                dots.get(i).setImageDrawable(drawable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getNew(String lang) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            if (ctx instanceof MainActivity)
                ((MainActivity)ctx).showNoInternetFragment();
            return;
        }

        Call<PromotionWrapper> call1 = ApiClient.getJsonClient().getNews(lang);
        call1.enqueue(new Callback<PromotionWrapper>() {

            @Override
            public void onFailure(Call<PromotionWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();

                Toast.makeText(ctx, getString(R.string.StepThreeFragment_006), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<PromotionWrapper> arg0, Response<PromotionWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                PromotionWrapper obj = arg1.body();
                Log.i(Tag.LOG.getValue(), "New: " + obj);
                lstCategory = new ArrayList<DetailPromotionWrapper>();
                if (obj.result) {
                    for (DetailPromotionWrapper detailPromotionWrapper : obj.data)
                        lstCategory.add(detailPromotionWrapper);
                    count = lstCategory.size();
                    initViewPage();
                    addDots();
                    selectDot(1);
                } else {
                    Log.e(Tag.LOG.getValue(), "New FAIL");
                    Toast.makeText(ctx, obj.message, Toast.LENGTH_SHORT).show();
                }
                adapter.notifyDataSetChanged();
            }
        });
    }


}
