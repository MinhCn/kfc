package kfc.vietnam.promotion_screen.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import kfc.vietnam.R;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.DetailPromotionWrapper;
import kfc.vietnam.common.object.LikeNewsDataWrapper;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.RelativeLayoutItemMenu;
import kfc.vietnam.common.utility.ListenerLoadImage;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.LoginActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import kfc.vietnam.promotion_screen.adapter.PromotionPagerAdapter;
import kfc.vietnam.promotion_screen.listener.ListenerLikeDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemPromotionFragment extends Fragment implements View.OnClickListener, ListenerLikeDetail {
    private String currentLanguageCode = "";
    private Context ctx;

    private static final String POSITON = "position";
    private static final String SCALE = "scale";
    private static final String DRAWABLE_RESOURE = "resource";

    private int screenWidth;
    private int screenHeight;

    public TextView textView;
    public ImageView imgLike;
    private TextView tvCountLike;
    private ProgressBar pbLoading;
    private MainActivity activity;

    private DetailPromotionWrapper obj;
    private SharedPreferences preferencesUserInfo;

    private static int position;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);
        activity = (MainActivity) ctx;

    }

    public static Fragment newInstance(Context context, int pos, float scale, DetailPromotionWrapper obj, String lang) {
        Bundle b = new Bundle();
        b.putInt(POSITON, pos);
        b.putFloat(SCALE, scale);
        b.putSerializable(DetailPromotionWrapper.class.getName(), obj);
        b.putString("language_code", lang);
        return Fragment.instantiate(context, ItemPromotionFragment.class.getName(), b);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWidthAndHeight();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint({"SetTextI18n", "WrongViewCast"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null)
            return null;

        Bundle b = this.getArguments();
        position = b.getInt(POSITON);
        float scale = b.getFloat(SCALE);
        obj = (DetailPromotionWrapper) b.getSerializable(DetailPromotionWrapper.class.getName());
        this.currentLanguageCode = b.getString("language_code");
        activity.processChangeTopMenu(ItemPromotionFragment.class);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((screenWidth * 3) / (4), (screenHeight * 3) / (5));
        RelativeLayout linearLayout = (RelativeLayout) inflater.inflate(R.layout.custom_item_promotion_fragment, container, false);

        textView = (TextView) linearLayout.findViewById(R.id.text);
        pbLoading = (ProgressBar) linearLayout.findViewById(R.id.pbLoading);
        imgLike = (ImageView) linearLayout.findViewById(R.id.imgLike);
        imgLike.setOnClickListener(this);
        tvCountLike = (TextView) linearLayout.findViewById(R.id.tvCountLike);
        RelativeLayoutItemMenu root = (RelativeLayoutItemMenu) linearLayout.findViewById(R.id.root_container);
        final ImageView imageView = (ImageView) linearLayout.findViewById(R.id.pagerImg);
        textView.setText(obj.name == null ? "" : obj.name);
        tvCountLike.setText(obj.like == null ? "0" : obj.like.toString());
        imageView.setLayoutParams(layoutParams);
        imageView.setContentDescription(obj.description == null ? "" : obj.description);
        if (obj.isLike) {
            if(android.os.Build.VERSION.SDK_INT >= 21){
                imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorited_food_3, getActivity().getTheme()));
            } else {
                imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorited_food_3));
            }
        }
        Glide.with(this)
                .load(obj.image)
//                .placeholder(R.drawable.loading)
//                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new ListenerLoadImage(pbLoading , imageView))
                .bitmapTransform(new RoundedCornersTransformation(ctx, 5, 0, RoundedCornersTransformation.CornerType.TOP))
                .into(imageView);
        imageView.setTag(obj.id + "");

        //handling click event
        imageView.setOnClickListener(this);

        root.setScaleBoth(scale);

        return linearLayout;
    }

    /**
     * Get device screen width and height
     */
    private void getWidthAndHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeight = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.pagerImg:
                Bundle bundle = new Bundle();
                bundle.putSerializable("promotion", obj);

                DetailsPromotionFragment f = new DetailsPromotionFragment();
                f.setListenerLikeDetail(this);
                f.setArguments(bundle);
                ((MainActivity) ctx).changeFragment(f);
                break;
            case R.id.imgLike:
                preferencesUserInfo = getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE);
//                if (preferencesUserInfo.contains(SharedPreferenceKeyType.USER_INFO.toString())) {
                    if (!obj.isLike)
                        likeNews(obj.id + "");
                    else
                        Toast.makeText(getContext() , R.string.info_like , Toast.LENGTH_SHORT).show();

//                }
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Xử lý login khi click add like sản phẩm ở giỏ hàng (Shopping Cart)
        if (requestCode == RequestResultCodeType.RESULT_LOGIN_IN_PROMOTION.getValue()) {
            if (data != null && data.getExtras() != null && data.getExtras().containsKey("user_info")) {
                activity.userInfo = (LoginWrapper) data.getExtras().getSerializable("user_info");
                likeNews(obj.id + "");
            }
        }
    }

    private void likeNews(String newsID) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<LikeNewsDataWrapper> call1 = ApiClient.getJsonClient().likeNews(newsID);
        call1.enqueue(new Callback<LikeNewsDataWrapper>() {

            @Override
            public void onFailure(Call<LikeNewsDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<LikeNewsDataWrapper> arg0, Response<LikeNewsDataWrapper> arg1) {
                mProgressDialog.dismiss();

                LikeNewsDataWrapper like = arg1.body();
                if (like.result) {
                    obj.isLike = true;
                    imgLike.setImageResource(R.drawable.ic_favorited_food_3);
                    tvCountLike.setText(String.valueOf(like.data));
                    obj.setLike(String.valueOf(like.data));
                    obj.setLike(true);
                    PromotionPagerAdapter.lstCategory.set(position, obj);
                }

            }
        });
    }

    @Override
    public void onLike(int id) {
        if (obj.id == id) {
            obj.isLike = true;
            imgLike.setImageResource(R.drawable.ic_favorited_food_3);
            int count = 0;
            if (obj.like != null){
                count = Integer.parseInt((obj.like));
            }
            tvCountLike.setText((count + 1) + "");
            obj.setLike((count + 1) + "");
            obj.setLike(true);
            PromotionPagerAdapter.lstCategory.set(position, obj);
        }
    }

}
