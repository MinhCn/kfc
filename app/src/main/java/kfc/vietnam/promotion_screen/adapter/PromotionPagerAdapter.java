package kfc.vietnam.promotion_screen.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kfc.vietnam.R;
import kfc.vietnam.common.object.DetailPromotionWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.RelativeLayoutItemMenu;
import kfc.vietnam.promotion_screen.fragment.ItemPromotionFragment;
import kfc.vietnam.promotion_screen.fragment.PromotionFragment;

public class PromotionPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
    private String currentLanguageCode = "";
    private Context ctx;

    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.9f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    private float scale;
    private PromotionFragment promotionFragment;
    private static Map<Integer, Fragment> mapItem;
    public static ArrayList<DetailPromotionWrapper> lstCategory;


    public PromotionPagerAdapter(Context context, PromotionFragment promotionFragment, FragmentManager childFragmentManager, ArrayList<DetailPromotionWrapper> lstCategory, String lang, ArrayList<ProvinceWrapper> lstProvince) {
        super(childFragmentManager);
        this.ctx = context;
        this.promotionFragment = promotionFragment;
        this.lstCategory = lstCategory;
        this.currentLanguageCode = lang;
        if (mapItem == null)
            mapItem = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        // make the first pager bigger than others
        try {
            if (position == PromotionFragment.FIRST_PAGE)
                scale = BIG_SCALE;
            else
                scale = SMALL_SCALE;

            position = position % PromotionFragment.count;

        } catch (Exception e) {
            e.printStackTrace();
        }

        Fragment item = ItemPromotionFragment.newInstance(ctx, position, scale, lstCategory.get(position), currentLanguageCode);
        mapItem.put(position, item);
        return item;
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = PromotionFragment.count * PromotionFragment.LOOPS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        try {
            if (positionOffset >= 0f && positionOffset <= 1f) {
                RelativeLayoutItemMenu cur = getRootView(position);
                RelativeLayoutItemMenu next = getRootView(position + 1);

                cur.setScaleBoth(BIG_SCALE - DIFF_SCALE * positionOffset);
                next.setScaleBoth(SMALL_SCALE + DIFF_SCALE * positionOffset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @SuppressWarnings("ConstantConditions")
    private RelativeLayoutItemMenu getRootView(int position) {
        RelativeLayoutItemMenu layoutItemMenu = (RelativeLayoutItemMenu) (mapItem.get(position).getView().findViewById(R.id.root_container));
        return layoutItemMenu;
    }



}