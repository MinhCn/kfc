package kfc.vietnam.main_screen.fragment;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.StoreDeviceTokenDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.DialogFactory;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by AnhDuc on 7/28/2016.
 */
public class SettingsApplication extends Fragment {
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1221;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private Context ctx;
    private MainActivity activity;
    private Switch swPushNoti;
    private String keydevice = "";
    private RelativeLayout rlPushNoti;
    SharedPreferences sharedpreferences;
    private boolean notifiEnable;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;

//        pref = ctx.getSharedPreferences(Const.SHARED_PREF, 0);
//        editor = pref.edit();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_settings_application, container, false);
        rlPushNoti = (RelativeLayout) root.findViewById(R.id.rlPushNoti);
        swPushNoti = (Switch) root.findViewById(R.id.swPushNoti);
        sharedpreferences = activity.getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        notifiEnable = sharedpreferences.getBoolean(SharedPreferenceKeyType.BLOCK_NOTIFICATION.toString() , false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        swPushNoti.setChecked(notifiEnable);

        swPushNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    saveOpenNotification(true);
                    editor.putBoolean("isReceivePush", true);
                    editor.commit();
                    keydevice = Settings.Secure.getString(activity.getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    String device_token = sharedpreferences.getString("device_token", "");
                    Log.e("device_token", device_token);
                    Log.e("keydevice", keydevice);
                    Log.e("device_token", device_token);
                    saveDeviceToken(device_token, "android");

                } else {
                    editor.putBoolean("isReceivePush", false);
                    saveOpenNotification(false);
                }
            }
        });


//        checkPermission();
    }

    public void saveOpenNotification(boolean notifi){
        SharedPreferences pref = getActivity().getSharedPreferences(Const.SHARED_PREF, 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putBoolean(SharedPreferenceKeyType.BLOCK_NOTIFICATION.toString(), notifi);
        prefEditor.apply();
    }


    private void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.READ_PHONE_STATE)) {

                rlPushNoti.setEnabled(true);
                rlPushNoti.setAlpha(1f);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    rlPushNoti.setEnabled(true);
                    rlPushNoti.setAlpha(1f);

                } else {

                    keydevice = "";
                    rlPushNoti.setEnabled(false);
                    rlPushNoti.setAlpha(0.2f);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void saveDeviceToken(String token,
                                 String platform) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<StoreDeviceTokenDataWrapper> call1 = ApiClient.getJsonClient().saveDeviceToken(token, keydevice, platform);
        call1.enqueue(new Callback<StoreDeviceTokenDataWrapper>() {

            @Override
            public void onFailure(Call<StoreDeviceTokenDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<StoreDeviceTokenDataWrapper> arg0, Response<StoreDeviceTokenDataWrapper> arg1) {
                mProgressDialog.dismiss();

                StoreDeviceTokenDataWrapper obj = arg1.body();
            }
        });
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(SettingsApplication.class);
                }
            }
        });

        return anim;
    }

}
