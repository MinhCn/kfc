package kfc.vietnam.main_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.ConfigureDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class ContactKFCFragment extends Fragment {
    private Context ctx;
    private MainActivity activity;
    private WebView tv;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_contact_kfc, container, false);

        tv = (WebView) root.findViewById(R.id.wv);
        tv.getSettings().setSupportZoom(false);
        tv.getSettings().setBuiltInZoomControls(false);
        tv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        getConfiguration();
        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(ContactKFCFragment.class);
                }
            }
        });

        return anim;
    }

    private void getConfiguration() {
        final ProgressDialog mProgressDialog = new ProgressDialog(getContext(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<ConfigureDataWrapper> call1 = ApiClient.getJsonClient().getConfiguration(activity.currentLanguageCode);
        call1.enqueue(new Callback<ConfigureDataWrapper>() {

            @Override
            public void onFailure(Call<ConfigureDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ConfigureDataWrapper> arg0, Response<ConfigureDataWrapper> arg1) {
                mProgressDialog.dismiss();
                ConfigureDataWrapper obj = arg1.body();
                tv.loadData(obj.data.lienhe.content, "text/html; charset=utf-8", "utf-8");
            }
        });
    }

}
