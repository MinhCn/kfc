package kfc.vietnam.main_screen.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kfc.vietnam.R;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class KFC_Info_Fragment extends Fragment {
    private Context ctx;
    private MainActivity activity;
    private WebView wv;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_kfc_info, container, false);

        wv = (WebView) root.findViewById(R.id.wv);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setSupportZoom(true);
        wv.getSettings().setBuiltInZoomControls(false);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d("anhduc", "onPageFinished -> URL: " + url);
                super.onPageFinished(view, url);
            }
        });
        wv.loadUrl("http://dev.vietbuzzad.net/kfcvietnam/" + activity.currentLanguageCode + "/gioithieu.html");

        return root;
    }

}
