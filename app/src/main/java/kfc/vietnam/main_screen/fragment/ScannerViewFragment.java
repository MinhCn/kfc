package kfc.vietnam.main_screen.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.zxing.Result;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.ViewWebActivity;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.main_screen.ui.MainActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScannerViewFragment extends Fragment implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    View root;

    private MainActivity activity;
    private Context ctx;

    public ScannerViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_scanner_view, container, false);
        // Register ourselves as a handler for scan results.
        mScannerView = new ZXingScannerView(getActivity());   // Programmatically initialize the scanner view
        root = mScannerView;
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mScannerView != null)
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(final Result rawResult) {
        // Do something with the result here
        Log.e("handler", rawResult.getText()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)

        // show the scanner result into dialog box.
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.scan_result));
        builder.setMessage(rawResult.getText());
        builder.setPositiveButton(getString(R.string.text_close_new), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity
                dialog.dismiss();
                // If you would like to resume scanning, call this method below:
                mScannerView.resumeCameraPreview(ScannerViewFragment.this);
            }
        });
        if (rawResult.getText().contains("http") || rawResult.getText().contains("www")) {
            builder.setNegativeButton(getString(R.string.text_open), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // if this button is clicked, close
                    // current activity
                    dialog.dismiss();
                    Intent intent = new Intent(getActivity(), ViewWebActivity.class);
                    intent.putExtra("url", rawResult.getText().toString());
                    getActivity().startActivity(intent);
                    // If you would like to resume scanning, call this method below:
                    mScannerView.resumeCameraPreview(ScannerViewFragment.this);
                }
            });
        }
        android.app.AlertDialog alert1 = builder.create();
        alert1.show();
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(ScannerViewFragment.class);
                }
            }
        });
        return anim;
    }
}
