package kfc.vietnam.main_screen.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by WIKI on 12/12/2017.
 */

public class PolicyOnInformationFragment extends Fragment{
    private Context ctx;
    private MainActivity activity;
    private WebView wv;
    private ProgressBar mProgressBar;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_terms_and_conditions, container, false);

        mProgressBar = (ProgressBar) root.findViewById(R.id.progressbar);
        wv = (WebView) root.findViewById(R.id.wv);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setSupportZoom(true);
        wv.getSettings().setBuiltInZoomControls(false);
        //improve webView performance
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        wv.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        wv.getSettings().setAppCacheEnabled(true);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSettings.setUseWideViewPort(true);
        webSettings.setSavePassword(true);
        webSettings.setSaveFormData(true);
        webSettings.setEnableSmoothTransition(true);

//        wv.loadUrl("javascript:(function() { $('header').remove(); $('footer').remove(); $('body').css('padding-top',0);})()");

        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wv.loadUrl("javascript:(function() { $('.banner, .nav-header, .nav-fix, .wap-sec-tin-tuc, footer').remove();$('body').attr('style','padding-top: 0px !important');})()");
                        wv.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);
                    }
                }, 500);
                super.onPageFinished(view, url);
            }
        });
        SharedPreferences preferencesLanguage = ctx.getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), ctx.MODE_PRIVATE);
        String currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
        if (currentLanguageCode.equals("vn")){
//            wv.loadUrl(ApiClient.ENDPOINT + "vn/chinh-sach-bao-mat.html");
            wv.loadUrl("https://kfcvietnam.com.vn/vn/chinh-sach-bao-mat.html");
        } else {
//            wv.loadUrl(ApiClient.ENDPOINT + "en/chinh-sach-bao-mat.html");
            wv.loadUrl("https://kfcvietnam.com.vn/en/chinh-sach-bao-mat.html");
        }
        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(PolicyOnInformationFragment.class);
                }
            }
        });

        return anim;
    }
}
