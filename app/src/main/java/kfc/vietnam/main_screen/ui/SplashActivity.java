package kfc.vietnam.main_screen.ui;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import kfc.vietnam.BuildConfig;
import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.ConfigureDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                checkVersion();
                //Không check version:
                SplashActivity.this.startActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class));
                finish();
            }
        }, 1000);
    }

    private void checkVersion() {
        final ProgressDialog mProgressDialog = new ProgressDialog(SplashActivity.this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        final String packageName = getApplicationContext().getPackageName();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
        } else {
            Call<ConfigureDataWrapper> call1 = ApiClient.getJsonClient().getConfiguration("en");
            call1.enqueue(new Callback<ConfigureDataWrapper>() {

                @Override
                public void onFailure(Call<ConfigureDataWrapper> arg0, Throwable arg1) {
                    arg1.printStackTrace();
                    mProgressDialog.dismiss();
                }

                @Override
                public void onResponse(Call<ConfigureDataWrapper> arg0, Response<ConfigureDataWrapper> arg1) {
                    mProgressDialog.dismiss();
                    if (arg1.body().result) {
                        ConfigureDataWrapper obj = arg1.body();
                        String versionName = obj.data.version.version_android_app;
//                        String versionName = "4.0.0"; //Test
                        if (versionName.equals(BuildConfig.VERSION_NAME)) { //Don't have new version
                            SplashActivity.this.startActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class));
                            finish();
                        } else {
                            if (obj.data.version.update_required_android) { //Must be update, if not: user can't use app
                                //bắt buộc hiện dialog update
                                DialogFactory.showDialogCheckVersionNotification(SplashActivity.this, true, versionName, new DialogFactory.OnDialogButtonClickListener() {
                                    @Override
                                    public void onDownloadNowButtonClicked() {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                                        } catch (Exception anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                                        }
                                    }

                                    @Override
                                    public void onRemindMeButtonClicked() {
                                        Toast.makeText(SplashActivity.this, getString(R.string.message_must_update), Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onIgnoreButtonClicked() {
                                        Toast.makeText(SplashActivity.this, getString(R.string.message_must_update), Toast.LENGTH_SHORT).show();
                                    }
                                });

//                                AlertDialog.Builder alert = new AlertDialog.Builder(SplashActivity.this);
//                                alert.setTitle(getResources().getString(R.string.alert_title_info));
//                                alert.setCancelable(false);
//                                alert.setMessage(getResources().getString(R.string.new_app_version_available));
//                                alert.setNegativeButton(getResources().getString(R.string.text_cancel),
//                                        new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//
//                                            }
//                                        });
//                                alert.setPositiveButton(getResources().getString(R.string.text_update), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        try {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
//                                        } catch (Exception anfe) {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
//                                        }
//                                        // Exit application
//                                        System.exit(0);
//                                    }
//                                });
//                                if (!isFinishing()) {
//                                    alert.create().show();
//                                }
                            } else { //Can ignore update, user still use app normal
                                SharedPreferences pref = getSharedPreferences(Const.SHARED_PREF, 0);
                                boolean isIgnore = pref.getBoolean(SharedPreferenceKeyType.CHECK_VERSION.toString(), false);
                                if (isIgnore) { //Không hiện dialog vì đã chọn ignore trước đó
                                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class));
                                    finish();
                                } else {
                                    DialogFactory.showDialogCheckVersionNotification(SplashActivity.this, false, versionName, new DialogFactory.OnDialogButtonClickListener() {
                                        @Override
                                        public void onDownloadNowButtonClicked() {
                                            try {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                                            } catch (Exception anfe) {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                                            }
                                        }

                                        @Override
                                        public void onRemindMeButtonClicked() {
                                            SplashActivity.this.startActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class));
                                            finish();
                                        }

                                        @Override
                                        public void onIgnoreButtonClicked() {
                                            SharedPreferences pref = getSharedPreferences(Const.SHARED_PREF, 0);
                                            SharedPreferences.Editor prefEditor = pref.edit();
                                            prefEditor.putBoolean(SharedPreferenceKeyType.CHECK_VERSION.toString(), true); //Không nhắc update nữa
                                            prefEditor.apply();

                                            SplashActivity.this.startActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class));
                                            finish();
                                        }
                                    });
                                }

//                                AlertDialog.Builder alert = new AlertDialog.Builder(SplashActivity.this);
//                                alert.setTitle(getResources().getString(R.string.alert_title_info));
//                                alert.setCancelable(false);
//                                alert.setMessage(getResources().getString(R.string.new_app_version_available));
//                                alert.setNegativeButton(getResources().getString(R.string.text_ignore),
//                                        new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                SplashActivity.this.startActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class));
//                                                finish();
//                                            }
//                                        });
//                                alert.setPositiveButton(getResources().getString(R.string.text_update), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        try {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
//                                        } catch (Exception anfe) {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
//                                        }
//                                        // Exit application
//                                        System.exit(0);
//                                    }
//                                });
//                                if (!isFinishing()) {
//                                    alert.create().show();
//                                }
                            }
                        }
                    }
                }
            });
        }
    }
}
