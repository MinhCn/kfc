package kfc.vietnam.main_screen.ui;

import android.Manifest;
import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dk.animation.circle.CircleAnimationUtil;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import kfc.vietnam.R;
import kfc.vietnam.account_screen.fragment.AccountFragment;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.common.enums.CategoryType;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.LoginType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.AddLikeDataWrapper;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.ConfigureDataWrapper;
import kfc.vietnam.common.object.ContactDataWrapper;
import kfc.vietnam.common.object.DeleteFoodFavoriteDataWrapper;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.FoodFavoriteDataWrapper;
import kfc.vietnam.common.object.GetAddressDataWrapper;
import kfc.vietnam.common.object.IpDataWrapper;
import kfc.vietnam.common.object.LikeNewsDataWrapper;
import kfc.vietnam.common.object.LoginDataWrapper;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.NotificationData;
import kfc.vietnam.common.object.ProductCategoryWrapper;
import kfc.vietnam.common.object.ProductChangeWrapper;
import kfc.vietnam.common.object.ProductDataWrapper;
import kfc.vietnam.common.object.ProductDetailDataWrapper;
import kfc.vietnam.common.object.ProductNoBuyDataWrapper;
import kfc.vietnam.common.object.ProductWrapper001;
import kfc.vietnam.common.object.ProductWrapper002;
import kfc.vietnam.common.object.ProductWrapper003;
import kfc.vietnam.common.object.ProvinceDataWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.ShoppingCartObject;
import kfc.vietnam.common.object.ShowTimeDataWrapper;
import kfc.vietnam.common.object.StoreDeviceTokenDataWrapper;
import kfc.vietnam.common.utility.DialogProvince;
import kfc.vietnam.common.utility.DialogSelectProvince;
import kfc.vietnam.common.utility.FontText;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.utility.ResultListener;
import kfc.vietnam.common.utility.SharedPreferenceUtils;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.fragment.ChangeDeliveryAddressFragment;
import kfc.vietnam.food_menu_screen.fragment.DetailComboOrProductSelectedFragment;
import kfc.vietnam.food_menu_screen.fragment.DetailsMenuFragment;
import kfc.vietnam.food_menu_screen.fragment.MenuFragment;
import kfc.vietnam.food_menu_screen.fragment.OrderHistoryDetailFragment;
import kfc.vietnam.food_menu_screen.fragment.OrderHistoryFragment;
import kfc.vietnam.food_menu_screen.fragment.OrderStatusFragment;
import kfc.vietnam.food_menu_screen.fragment.PaymentFragment;
import kfc.vietnam.food_menu_screen.fragment.PaymentWithATMFragment;
import kfc.vietnam.food_menu_screen.fragment.ShoppingCartFragment;
import kfc.vietnam.food_menu_screen.fragment.StepOneFragment;
import kfc.vietnam.food_menu_screen.fragment.StepThreeFragment;
import kfc.vietnam.food_menu_screen.fragment.StepTwoFragment;
import kfc.vietnam.internet_off_screen.NoInternetFragment;
import kfc.vietnam.main_screen.fragment.ContactKFCFragment;
import kfc.vietnam.main_screen.fragment.OperationPolicyFragment;
import kfc.vietnam.main_screen.fragment.PolicyOnInformationFragment;
import kfc.vietnam.main_screen.fragment.ScannerViewFragment;
import kfc.vietnam.main_screen.fragment.SettingsApplication;
import kfc.vietnam.main_screen.fragment.TermsAndConditionsFragment;
import kfc.vietnam.main_screen.fragment.WhyChooseKFC_Fragment;
import kfc.vietnam.map_screen.fragment.RestaurantFragment;
import kfc.vietnam.promotion_screen.fragment.DetailsPromotionFragment;
import kfc.vietnam.promotion_screen.fragment.PromotionFragment;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 124;
    private static final String NUMBER_PHONE_CONTACT_KFC = "19006886";
    public static GoogleApiClient mGoogleApiClient;
    public static boolean isCombo = false;
    public String currentLanguageCode = "";
    public MenuFragment menuFragment;
    public PromotionFragment promotionFragment;
    public RestaurantFragment restaurantFragment;
    public AccountFragment accountFragment;
    public FragmentManager fragmentManager;
    public FragmentTransaction fragmentTransaction;
    public TextView tvAmountShoppingCart, tvLikeNumber;
    public ArrayList<ProvinceWrapper> lstProvince = new ArrayList<>();
    public ArrayList<DistrictWrapper> lstDistrict;
    public ProvinceWrapper currentProvinceSelected;
    public ConfigureDataWrapper.ConfigureWrapper conf;
    public LoginWrapper userInfo;
    public AddressDataWrapper addressToShip;
    public TextView tvPromotionDetail, tvPaymentURL, tvTitleDetailCombo;
    public RelativeLayout rlMenuPromotion;
    public LinearLayout llScoreContainer;
    public ImageView imgBackPromotion;
    public DialogSelectProvince dialogSelectProvince;
    public DrawerLayout mDrawerLayout;
    public boolean isShoppingCard;
    public boolean isShoppingCard3;
    public LinearLayout llMenuBar;
    public LinearLayout llLogout;
    public LinearLayout llCall, llProvince, llShoppingCart, llLike, llViewProfile;
    public RelativeLayout rlMenu, rlMenu1, rlMenu2, rlMenu3, rlMenu4, rlMenu5, rlMenu6, rlMenu7, rlMenu8, rlMenu9, rlMenu10,
            rlPromotion, rlCoupons, rlRestaurant, rlAccount, rlNeedLogin, rlLogged, rlMain, rlTopMenu_RestaurantFragment, rlContact,
            rlTermsAndConditions, rlOperationPolicy, rlPolicyOnInformation, rlWhyChooseKFC, rlScanQRCode, rlAppSetting; //add by @ManhNguyen
    public ImageView imgShowNavigation, imgShowNavigation2, imgShoppingCart, imgBack2, imgBack3, imgBack4, imgBack5, imgBack6,
            imgBack7, imgBack8, imgBack9, imgMenu, imgPromotion, imgCoupons, imgRestaurant, imgAccount, imgLike,
            imgBackTermsAndConditions, imgBackOperationPolicy, imgBackPolicyOnInformation, imgBackWhyChooseKFC, imgBackScanQRCode, imgBackAppSetting; //add by @ManhNguyen
    public FontText tvMenu, tvPromotion, tvCoupons, tvRestaurant, tvAccount, tvScoreMain;
    public GoogleSignInOptions gso;
    private TextView tvDangKi, tvProvince, tvDisplayName;
    private ImageView imgAvatar, imgBlurAvatar, imgEnglish, imgVietNam;
    private SharedPreferences preferencesUserInfo;
    private boolean isAnimating = false;
    public int checkProcessOrder = 1;
    private MyListenerForLeftMenu listenerForLeftMenu;
    private SharedPreferences preferencesLanguage;
    public static String sProvineId = "";
    private NotificationData notificationData;
    public String customerIp;
    public Double totalPrice = -1d;

    private boolean mFlag_IsVisiable_BottomBar = true;

    private ProgressDialog mProgressDialog;

    private boolean isChangeAddressStep3 = false;

    public boolean isBackFromStep3NotLogin = false;

    private TextView tvTitleStep1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressDialog = new ProgressDialog(MainActivity.this, ProgressDialog.THEME_HOLO_DARK);

        saveToken();
        FacebookSdk.sdkInitialize(this);
        try {
            Intent intent = getIntent();
            if (intent != null && intent.getExtras().containsKey("user_info")) {
                userInfo = (LoginWrapper) intent.getExtras().getSerializable("user_info");
            }
            preferencesUserInfo = getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE);
            if (preferencesUserInfo.contains(SharedPreferenceKeyType.USER_INFO.toString()))
                if (userInfo == null)
                    userInfo = (LoginWrapper) ObjectSerializer.deserialize(preferencesUserInfo.getString(SharedPreferenceKeyType.USER_INFO.toString(), null));
        } catch (Exception e) {
            e.printStackTrace();
        }


        preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
        changeLanguageCart();
        init();
        showToolbar();
        restaurantFragment = new RestaurantFragment();
        promotionFragment = new PromotionFragment();
        accountFragment = new AccountFragment();

        fragmentManager = getSupportFragmentManager();
        //fragmentManager.addOnBackStackChangedListener(getListener());

        resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);

        if (gso == null) {
            // Google plus: Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
        }
        if (mGoogleApiClient == null) {
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        IntentFilter filter = new IntentFilter("CHANGE_LAYOUT_FOOD_FAVORITE");
        registerReceiver(broadcastReceiver, filter);

        Intent intent = getIntent();
        String data = "";
        boolean inApp = false;
        if (intent != null) {
            data = intent.getStringExtra("data");
            if (!TextUtils.isEmpty(data)) {
                notificationData = new Gson().fromJson(data, NotificationData.class);
                inApp = intent.getBooleanExtra("in_app", false);
                if (!inApp) {
                    showNotification(notificationData);
                }

            }
        }
        getProvince(currentLanguageCode);
        getIp(currentLanguageCode);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            processChangeTopMenu(AccountFragment.class);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStart() {
        super.onStart();
//        if (mGoogleApiClient != null)
        mGoogleApiClient.connect();
        if (userInfo != null) {
            String pw = getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).getString(SharedPreferenceKeyType.USER_PASSWORD.toString(), "");
            if (TextUtils.isEmpty(pw))
                return;
            if (userInfo.usertype.equals(LoginType.KFC.getValue())) {
                login(userInfo.email, pw);
            } else {
                String id = userInfo.usertype.equals(LoginType.GOOGLE_PLUS.getValue()) ? userInfo.google : userInfo.facebook;
                loginByFacebookOrGooglePlus(userInfo.usertype, id, userInfo.email, userInfo.name, userInfo.phone, userInfo.avatar);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }


    @Override
    protected void onResume() {
        super.onResume();
//        Log.e("LoginActivity", LoginActivity.lg + "");
//        if (LoginActivity.lg == true) {
//            if (menuFragment != null) {
//                changeFragment(menuFragment);
//            }
//            resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
//        }

        //add by @ManhNguyen
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
            String backStackName = backStackEntry.getName();

            if (backStackName.equals(MenuFragment.class.getName()) || backStackName.equals(PromotionFragment.class.getName()) ||
                    backStackName.equals(AccountFragment.class.getName()) || backStackName.equals(RestaurantFragment.class.getName())) {

                if (!mFlag_IsVisiable_BottomBar){
                    llMenuBar.setVisibility(View.VISIBLE);
                    mFlag_IsVisiable_BottomBar = true;
                }
            }
        }

        mGoogleApiClient.connect();

        //add by @ManhNguyen
        Fragment f1 = fragmentManager.findFragmentByTag(StepOneFragment.class.getName());
        if (f1 instanceof StepOneFragment) {
            preferencesUserInfo = getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE);
            if (preferencesUserInfo.contains(SharedPreferenceKeyType.USER_INFO.toString()))
                try {
                    userInfo = (LoginWrapper) ObjectSerializer.deserialize(preferencesUserInfo.getString(SharedPreferenceKeyType.USER_INFO.toString(), null));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if (userInfo != null){
                changeFragment(new StepTwoFragment());
            }
        }
    }

    public void changeFragment(final Fragment fragment) {
        if (fragment == null)
            return;
        String tagName = fragment.getClass().getName();
        if (fragmentManager == null)
            fragmentManager = getSupportFragmentManager();

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(R.id.mainframe, fragment, tagName);
        fragmentTransaction.addToBackStack(tagName);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void showNoInternetFragment() {
        changeFragment(new NoInternetFragment());
    }

    @Override
    public void onBackPressed() {
        //Handle hide navigation menu
        if (mDrawerLayout != null)
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        if (isShoppingCard3) {
            isShoppingCard3 = false;
            changeFragment(new StepThreeFragment());
            return;
        }
        if (isShoppingCard) {
            isShoppingCard = false;
            llShoppingCart.callOnClick();
            return;
        }

        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
            String backStackName = backStackEntry.getName();

            if (userInfo != null && (backStackName.equals(StepOneFragment.class.getName()) || backStackName.equals(StepTwoFragment.class.getName())
                    || backStackName.equals(StepThreeFragment.class.getName()))) {
                backStackName = checkProcessOrder == 1 ? StepOneFragment.class.getName() : StepTwoFragment.class.getName();
                fragmentManager.popBackStack(backStackName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                return;
            }

            if (userInfo == null && backStackName.equals(StepThreeFragment.class.getName())) {
                fragmentManager.popBackStack(backStackName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                isBackFromStep3NotLogin = true;
                return;
            }

            if (backStackName.equals(MenuFragment.class.getName()) || backStackName.equals(PromotionFragment.class.getName()) ||
                    backStackName.equals(AccountFragment.class.getName()) || backStackName.equals(RestaurantFragment.class.getName())) {
                return;
            }

            //add by @ManhNguyen
            Fragment f1 = fragmentManager.findFragmentByTag(AccountFragment.class.getName());
            if (f1 instanceof AccountFragment) {
                preferencesUserInfo = getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE);
                if (preferencesUserInfo.contains(SharedPreferenceKeyType.USER_INFO.toString()))
                    try {
                        userInfo = (LoginWrapper) ObjectSerializer.deserialize(preferencesUserInfo.getString(SharedPreferenceKeyType.USER_INFO.toString(), null));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

            fragmentManager.popBackStack();

            //add by @ManhNguyen
            if (!mFlag_IsVisiable_BottomBar){
                llMenuBar.setVisibility(View.VISIBLE);
                mFlag_IsVisiable_BottomBar = true;
            }

            //clear flag change address at step 3
            if (getChangeAddressStep3()){
                clearChangeAddressStep3();
            }

        } else {
            super.onBackPressed();
        }
    }

    public Fragment getTopFragmentOfBackStack() {
        //Get top of stack
        FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
        String backStackName = backStackEntry.getName();
        Fragment fragment = fragmentManager.findFragmentByTag(backStackName);

        return fragment;
    }

    public Fragment getSecondFragmentOfBackStack() {
        int count = fragmentManager.getBackStackEntryCount();
        if (count <= 1) {
            count = count - 1;
        } else if (count > 1) {
            count = count - 2;
        }

        //Get second of stack
        FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(count);
        String backStackName = backStackEntry.getName();
        Fragment fragment = fragmentManager.findFragmentByTag(backStackName);

        return fragment;
    }

    public void backToMainMenu() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            for (int i = fragmentManager.getBackStackEntryCount() - 1; i >= 0; i--) {
                if (i == 0)
                    break;
                fragmentManager.popBackStack();
            }
        }

        hideProgressDialog();

        if (!mFlag_IsVisiable_BottomBar){
            llMenuBar.setVisibility(View.VISIBLE);
            mFlag_IsVisiable_BottomBar = true;
        }

    }

    public void backToPreviousScreen() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
            String backStackName = backStackEntry.getName();
            if (userInfo != null && (backStackName.equals(StepOneFragment.class.getName()) || backStackName.equals(StepTwoFragment.class.getName())
                    || backStackName.equals(StepThreeFragment.class.getName()))) {
                backStackName = checkProcessOrder == 1 ? StepOneFragment.class.getName() : StepTwoFragment.class.getName();
                fragmentManager.popBackStack(backStackName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                return;
            }
            fragmentManager.popBackStack();

            //add by @ManhNguyen: hide bottombar, hide keyboard
            if (!mFlag_IsVisiable_BottomBar){
                llMenuBar.setVisibility(View.VISIBLE);
                mFlag_IsVisiable_BottomBar = true;
            }

            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mDrawerLayout.getWindowToken(), 0);
        }
    }

    public Fragment getFragmentByTag(String tagName) {
        return (Fragment) fragmentManager.findFragmentByTag(tagName);
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                if (fragmentManager != null) {
                    int backStackEntryCount = fragmentManager.getBackStackEntryCount();
                    if (backStackEntryCount == 0) {
                        finish();
                    }

                    Fragment fragment = fragmentManager.findFragmentById(R.id.mainframe);
                    fragment.onResume();
                }
            }
        };
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //Handle hide navigation menu
        if (mDrawerLayout != null)
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_language) {

        } else if (id == R.id.nav_select_province) {

        } else if (id == R.id.nav_scan_qr_code) {

        } else if (id == R.id.nav_setting_app) {

        } else if (id == R.id.nav_why_chose_kfc) {

        } else if (id == R.id.nav_operational_policy) {

        } else if (id == R.id.nav_contact_kfc) {

        } else if (id == R.id.nav_kfc_app_info) {

        } else if (id == R.id.nav_logout) {

        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Xử lý login khi click add like sản phẩm ở giỏ hàng (Shopping Cart)
        if (requestCode == RequestResultCodeType.REQUEST_CODE_LOGIN_IN_SHOPPING_CART.getValue() && resultCode == RequestResultCodeType.RESULT_CODE_LOGIN_IN_SHOPPING_CART.getValue()) {

//            if (!mFlag_IsVisiable_BottomBar){
//                llMenuBar.setVisibility(View.VISIBLE);
//                mFlag_IsVisiable_BottomBar = true;
//            }

            Fragment f = getFragmentByTag(ShoppingCartFragment.class.getName());
            if (f != null) {
                f.onActivityResult(requestCode, resultCode, data);
            }
//            userInfo = (LoginWrapper) data.getExtras().getSerializable("user_info");
//            showUserInfoInNavigation(true);
        }
        //Phần xử lý login on navigation
        else if (requestCode == RequestResultCodeType.REQUEST_CODE_LOGIN_ON_NAVIGATION.getValue()) {
            if (resultCode == RequestResultCodeType.RESULT_CODE_LOGIN_ON_NAVIGATION.getValue()) {
                userInfo = (LoginWrapper) data.getExtras().getSerializable("user_info");
                showUserInfoInNavigation(true);

            } else if (resultCode == RequestResultCodeType.RESULT_OPEN_LOGIN.getValue()) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                i.putExtra("login_area", LoginAreaType.LOGIN_ON_NAVIGATION.toString());
                startActivityForResult(i, RequestResultCodeType.REQUEST_LOGIN.getValue());

            } else if (resultCode == RequestResultCodeType.RESULT_OPEN_SIGN_UP.getValue()) {
                Intent in = new Intent(MainActivity.this, RegisterActivity.class);
                in.putExtra("login_area", LoginAreaType.LOGIN_ON_NAVIGATION.toString());
                startActivityForResult(in, RequestResultCodeType.REQUEST_LOGIN.getValue());

            } else if (resultCode == RequestResultCodeType.RESULT_SIGN_UP.getValue()) {
                String type = data.getExtras().getString("type");
                String id = data.getExtras().getString("id");
                String email = data.getExtras().getString("email");
                String urlAvatar = data.getExtras().getString("urlAvatar");
                String name = data.getExtras().getString("name");
                Intent in = new Intent(MainActivity.this, RegisterActivity.class);
                in.putExtra("login_area", LoginAreaType.LOGIN_ON_NAVIGATION.toString());
                in.putExtra("type", type);
                in.putExtra("id", id);
                in.putExtra("email", email);
                in.putExtra("name", name);
                in.putExtra("urlAvatar", urlAvatar);
                startActivityForResult(in, RequestResultCodeType.REQUEST_CODE_LOGIN_ON_NAVIGATION.getValue());
            }
        }
        //Phần xử lý thay đổi món ăn
        else if (requestCode == RequestResultCodeType.REQUEST_CHANGE_DISH.getValue() && resultCode == RequestResultCodeType.RESULT_CHANGE_DISH.getValue()) {
            Fragment f = getFragmentByTag(DetailComboOrProductSelectedFragment.class.getName());
            if (f != null)
                f.onActivityResult(requestCode, resultCode, data);
        }
        //Phần xử lý register account tại màn hình bước 1 của chọn món ăn
        else if (requestCode == RequestResultCodeType.REQUEST_CODE_REGISTER_IN_STEP_ONE.getValue() && resultCode == RequestResultCodeType.RESULT_CODE_REGISTER_IN_STEP_ONE.getValue()) {
            Fragment f = getFragmentByTag(StepOneFragment.class.getName());
            if (f != null)
                f.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == RequestResultCodeType.REQUEST_LOGIN.getValue()) {
            if (resultCode == RequestResultCodeType.RESULT_SIGN_UP.getValue()) {
                String type = data.getExtras().getString("type");
                String id = data.getExtras().getString("id");
                String email = data.getExtras().getString("email");
                String urlAvatar = data.getExtras().getString("urlAvatar");
                String name = data.getExtras().getString("name");
                Intent in = new Intent(MainActivity.this, RegisterActivity.class);
                in.putExtra("type", type);
                in.putExtra("id", id);
                in.putExtra("email", email);
                in.putExtra("name", name);
                in.putExtra("urlAvatar", urlAvatar);
                startActivityForResult(in, RequestResultCodeType.REQUEST_LOGIN.getValue());
            } else if (resultCode == RequestResultCodeType.RESULT_OPEN_LOGIN.getValue()) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivityForResult(i, RequestResultCodeType.REQUEST_LOGIN.getValue());

            } else if (resultCode == RequestResultCodeType.RESULT_OPEN_SIGN_UP.getValue()) {
                Intent in = new Intent(MainActivity.this, RegisterActivity.class);
                startActivityForResult(in, RequestResultCodeType.REQUEST_LOGIN.getValue());
            } else {
                if (LoginActivity.getEx) {
                    if (data.getExtras().getSerializable("user_info") != null){
                        userInfo = (LoginWrapper) data.getExtras().getSerializable("user_info");
                    }
                    showUserInfoInNavigation(true);
                    changeFragment(accountFragment);
                } else {
                    Log.e("getExtras", "null");
                }
            }
        }
    }

    public void gotoMenu(boolean isShopping) {
        isCombo = true;
        isShoppingCard = true;
        resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
        changeFragment(menuFragment);
    }

    public void gotoMenu3() {
        isCombo = true;
        isShoppingCard3 = true;
        resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
        changeFragment(menuFragment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlMenu:
                if (menuFragment == null)
                    menuFragment = new MenuFragment();
                resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);

                changeFragment(menuFragment);
                break;

            case R.id.rlPromotion:
                if (promotionFragment == null)
                    promotionFragment = new PromotionFragment();
                resetColor(rlPromotion, imgPromotion, tvPromotion, R.drawable.ic_promotion_tabbar_bottom_2);
                changeFragment(promotionFragment);
                break;

            case R.id.rlCoupons:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setMessage(R.string.function_will_develop_later);

                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });


                AlertDialog dialog = alertDialog.create();
                dialog.show();
                break;

            case R.id.rlRestaurant:
//                if (restaurantFragment == null)
                restaurantFragment = new RestaurantFragment();
                changeFragment(restaurantFragment);
                resetColor(rlRestaurant, imgRestaurant, tvRestaurant, R.drawable.ic_restaurent_tabbar_bottom_2);
                processChangeTopMenu(RestaurantFragment.class);

//                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
//                        == PackageManager.PERMISSION_GRANTED) {
//                    if (restaurantFragment == null)
//                        restaurantFragment = new RestaurantFragment();
//                    changeFragment(restaurantFragment);
//                    resetColor(rlRestaurant, imgRestaurant, tvRestaurant, R.drawable.ic_restaurent_tabbar_bottom_2);
//                    processChangeTopMenu(RestaurantFragment.class);
//                } else {
//                    // Request permission
//                    ActivityCompat.requestPermissions(MainActivity.this,
//                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
//                }
                break;

            case R.id.rlAccount:

                if (userInfo != null) {
                    resetColor(rlAccount, imgAccount, tvAccount, R.drawable.ic_account_tabbar_bottom_2);
                    if (accountFragment == null)
                        accountFragment = new AccountFragment();
                    changeFragment(accountFragment);
                } else {
                    startActivityForResult(new Intent(this, LoginActivity.class), RequestResultCodeType.REQUEST_LOGIN.getValue());
                }
                break;

            case R.id.rlLogin:
                if (mDrawerLayout != null)
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                i.putExtra("login_area", LoginAreaType.LOGIN_ON_NAVIGATION.toString());
                startActivityForResult(i, RequestResultCodeType.REQUEST_CODE_LOGIN_ON_NAVIGATION.getValue());
                break;

            case R.id.llViewProfile:
                if (accountFragment == null)
                    accountFragment = new AccountFragment();
                changeFragment(accountFragment);
                resetColor(rlAccount, imgAccount, tvAccount, R.drawable.ic_account_tabbar_bottom_2);
                if (mDrawerLayout != null)
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                break;

            case R.id.imgShowNavigation:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;

            case R.id.imgShowNavigation2:
                imgShowNavigation.performClick();
                break;

            case R.id.llProvince:
                showDialogChooseProvince();
                break;

            case R.id.llCall:
                //Edit by @ManhNguyen: Old phone number: (08) 38489828
                AlertDialog.Builder alertDialogBuilderCall1 = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilderCall1
                        .setMessage(getString(R.string.call_hotline) + " " + NUMBER_PHONE_CONTACT_KFC + "?")
                        .setCancelable(false)
                        .setNegativeButton(getString(R.string.cancel_call), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton(getString(R.string.button_call), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callKFC();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialogCall1 = alertDialogBuilderCall1.create();
                // show it
                alertDialogCall1.show();
                break;

            case R.id.llShoppingCart:
//                changeFragment(new ShoppingCartFragment());
                checkTime(currentLanguageCode);
                break;

            case R.id.tvDangKi:
                if (mDrawerLayout != null)
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                Intent in = new Intent(MainActivity.this, RegisterActivity.class);
                in.putExtra("login_area", LoginAreaType.LOGIN_ON_NAVIGATION.toString());
                startActivityForResult(in, RequestResultCodeType.REQUEST_CODE_LOGIN_ON_NAVIGATION.getValue());
//                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                break;

            case R.id.imgBack2:
            case R.id.imgBack3:
            case R.id.imgBack4:
            case R.id.imgBack5:
            case R.id.imgBack6:
            case R.id.imgBack7:
            case R.id.imgBack8:
            case R.id.imgBack9:
            case R.id.imgBackContact:
            case R.id.imgBackPromotion:
                //add by @ManhNguyen
            case R.id.imgBackTermsAndConditions:
            case R.id.imgBackOperationPolicy:
            case R.id.imgBackPolicyOnInformation:
            case R.id.imgBackWhyChooseKFC:
            case R.id.imgBackScanQRCode:
            case R.id.imgBackAppSetting:
//                backToPreviousScreen();
                onBackPressed();
                break;

            case R.id.llLike:
                Fragment f = getTopFragmentOfBackStack();
                if (f instanceof DetailComboOrProductSelectedFragment) {
                    ProductWrapper003 p = ((DetailComboOrProductSelectedFragment) f).productSelected;
                    if (!p.checkLike) {
                        checkAddLike(p);
                    } else {
                        Toast.makeText(this, R.string.info_like, Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.callContact:
                //Edit by @ManhNguyen: Old phone number: (08) 38489828
                AlertDialog.Builder alertDialogBuilderCall = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilderCall
                        .setMessage(getString(R.string.call_hotline) + " " + NUMBER_PHONE_CONTACT_KFC + "?")
                        .setCancelable(false)
                        .setNegativeButton(getString(R.string.cancel_call), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton(getString(R.string.button_call), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callKFC();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialogCall = alertDialogBuilderCall.create();
                // show it
                alertDialogCall.show();
                break;
        }
    }

    private void checkTime(String lang) {
        llShoppingCart.setEnabled(false);
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    llShoppingCart.setEnabled(true);
                }
            }, 2000);

        } else {
            Call<ResponseBody> call = ApiClient.getJsonClient().checkTime(lang);
            call.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    hideProgressDialog();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            llShoppingCart.setEnabled(true);
                        }
                    }, 2000);

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getBoolean("result")) {
                            Toast.makeText(MainActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        } else {
                            changeFragment(new ShoppingCartFragment());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                    throwable.printStackTrace();
                    hideProgressDialog();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            llShoppingCart.setEnabled(true);
                        }
                    }, 2000);

                }
            });
        }
    }

    public void callKFC() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + NUMBER_PHONE_CONTACT_KFC));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            // public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            return;
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
        } else {
            startActivity(callIntent);
        }
    }

    private void init() {
        imgShowNavigation = (ImageView) findViewById(R.id.imgShowNavigation);
        imgShowNavigation.setOnClickListener(this);

        imgShowNavigation2 = (ImageView) findViewById(R.id.imgShowNavigation2);
        imgShowNavigation2.setOnClickListener(this);

        rlMenu = (RelativeLayout) findViewById(R.id.rlMenu);
        imgMenu = (ImageView) findViewById(R.id.imgMenu1);
        tvMenu = (FontText) findViewById(R.id.tvMenu1);
        rlMenu.setOnClickListener(this);

        rlMain = (RelativeLayout) findViewById(R.id.rlMain);
        rlMenu1 = (RelativeLayout) findViewById(R.id.rlMenu1);
        rlMenu2 = (RelativeLayout) findViewById(R.id.rlMenu2);
        rlMenu3 = (RelativeLayout) findViewById(R.id.rlMenu3);
        rlMenu4 = (RelativeLayout) findViewById(R.id.rlMenu4);
        rlMenu5 = (RelativeLayout) findViewById(R.id.rlMenu5);
        rlMenu6 = (RelativeLayout) findViewById(R.id.rlMenu6);
        rlMenu7 = (RelativeLayout) findViewById(R.id.rlMenu7);
        rlMenu8 = (RelativeLayout) findViewById(R.id.rlMenu8);
        rlMenu9 = (RelativeLayout) findViewById(R.id.rlMenu9);
        rlMenu10 = (RelativeLayout) findViewById(R.id.rlMenu10);
        rlContact = (RelativeLayout) findViewById(R.id.rlContact);

        //add by @ManhNguyen
        rlTermsAndConditions = (RelativeLayout) findViewById(R.id.rlTermsAndConditions);
        rlOperationPolicy = (RelativeLayout) findViewById(R.id.rlOperationPolicy);
        rlPolicyOnInformation = (RelativeLayout) findViewById(R.id.rlPolicyOnInformation);
        rlWhyChooseKFC = (RelativeLayout) findViewById(R.id.rlWhyChooseKFC);
        rlScanQRCode = (RelativeLayout) findViewById(R.id.rlScanQRCode);
        rlAppSetting = (RelativeLayout) findViewById(R.id.rlAppSetting);

        rlTopMenu_RestaurantFragment = (RelativeLayout) findViewById(R.id.rlTopMenu_RestaurantFragment);
        rlMenuPromotion = (RelativeLayout) findViewById(R.id.rlMenuPromotion);
        imgBack2 = (ImageView) findViewById(R.id.imgBack2);
        imgBack3 = (ImageView) findViewById(R.id.imgBack3);
        imgBack4 = (ImageView) findViewById(R.id.imgBack4);
        imgBack5 = (ImageView) findViewById(R.id.imgBack5);
        imgBack6 = (ImageView) findViewById(R.id.imgBack6);
        imgBack7 = (ImageView) findViewById(R.id.imgBack7);
        imgBack8 = (ImageView) findViewById(R.id.imgBack8);
        imgBack9 = (ImageView) findViewById(R.id.imgBack9);
        imgBackPromotion = (ImageView) findViewById(R.id.imgBackPromotion);
        tvTitleStep1 = (TextView) findViewById(R.id.tvTitleStep1);

        //add by @ManhNguyen
        imgBackTermsAndConditions = (ImageView) findViewById(R.id.imgBackTermsAndConditions);
        imgBackOperationPolicy = (ImageView) findViewById(R.id.imgBackOperationPolicy);
        imgBackPolicyOnInformation = (ImageView) findViewById(R.id.imgBackPolicyOnInformation);
        imgBackWhyChooseKFC = (ImageView) findViewById(R.id.imgBackWhyChooseKFC);
        imgBackScanQRCode = (ImageView) findViewById(R.id.imgBackScanQRCode);
        imgBackAppSetting = (ImageView) findViewById(R.id.imgBackAppSetting);

        imgBack2.setOnClickListener(this);
        imgBack3.setOnClickListener(this);
        imgBack4.setOnClickListener(this);
        imgBack5.setOnClickListener(this);
        imgBack6.setOnClickListener(this);
        imgBack7.setOnClickListener(this);
        imgBack8.setOnClickListener(this);
        imgBack9.setOnClickListener(this);

        //add by @ManhNguyen
        imgBackTermsAndConditions.setOnClickListener(this);
        imgBackOperationPolicy.setOnClickListener(this);
        imgBackPolicyOnInformation.setOnClickListener(this);
        imgBackWhyChooseKFC.setOnClickListener(this);
        imgBackScanQRCode.setOnClickListener(this);
        imgBackAppSetting.setOnClickListener(this);

        findViewById(R.id.imgBackContact).setOnClickListener(this);
        findViewById(R.id.callContact).setOnClickListener(this);

        imgBackPromotion.setOnClickListener(this);
        tvPaymentURL = (TextView) findViewById(R.id.tvPaymentURL);
        tvTitleDetailCombo = (TextView) findViewById(R.id.tvTitleDetailCombo);

        llLike = (LinearLayout) findViewById(R.id.llLike);
        llLike.setOnClickListener(this);
        imgLike = (ImageView) findViewById(R.id.imgLike);
        tvLikeNumber = (TextView) findViewById(R.id.tvLikeNumber);
        tvPromotionDetail = (TextView) findViewById(R.id.tvPromotionDetail);

        rlPromotion = (RelativeLayout) findViewById(R.id.rlPromotion);
        imgPromotion = (ImageView) findViewById(R.id.imgPromotion1);
        tvPromotion = (FontText) findViewById(R.id.tvPromotion1);
        rlPromotion.setOnClickListener(this);

        rlCoupons = (RelativeLayout) findViewById(R.id.rlCoupons);
        imgCoupons = (ImageView) findViewById(R.id.imgCoupon1);
        tvCoupons = (FontText) findViewById(R.id.tvCoupon1);
        rlCoupons.setOnClickListener(this);

        rlRestaurant = (RelativeLayout) findViewById(R.id.rlRestaurant);
        imgRestaurant = (ImageView) findViewById(R.id.imgRestaurant1);
        tvRestaurant = (FontText) findViewById(R.id.tvRestaurant1);
        rlRestaurant.setOnClickListener(this);

        rlAccount = (RelativeLayout) findViewById(R.id.rlAccount);
        imgAccount = (ImageView) findViewById(R.id.imgAccount1);
        tvAccount = (FontText) findViewById(R.id.tvAccount1);
        rlAccount.setOnClickListener(this);

        tvProvince = (TextView) findViewById(R.id.tvProvince);
        llProvince = (LinearLayout) findViewById(R.id.llProvince);
        llProvince.setOnClickListener(this);

        llCall = (LinearLayout) findViewById(R.id.llCall);
        llCall.setOnClickListener(this);

        llShoppingCart = (LinearLayout) findViewById(R.id.llShoppingCart);
        llShoppingCart.setOnClickListener(this);

        imgShoppingCart = (ImageView) findViewById(R.id.imgShoppingCart);
        tvAmountShoppingCart = (TextView) findViewById(R.id.tvAmountShoppingCart);

        llMenuBar = (LinearLayout) findViewById(R.id.llMenuBar);

        try {
            ArrayList<ShoppingCartObject> lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE).getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null));
            int count_cart = 0;
            if (lstShoppingCard != null) {
                for (int i = 0; i < lstShoppingCard.size(); i++) {
                    count_cart += Integer.parseInt(lstShoppingCard.get(i).amount);
                }

                tvAmountShoppingCart.setText(count_cart + "");
                //tvAmountShoppingCart.setText(lstShoppingCard.size() + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drl_left_menu_activity_main);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

                Log.i(Tag.LOG.getValue(), "onDrawerClosed");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (userInfo != null)
                    showUserInfoInNavigation(true);

                Log.i(Tag.LOG.getValue(), "onDrawerOpened");
            }
        };
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        listenerForLeftMenu = new MyListenerForLeftMenu();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_left_menu_activity_main);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        imgEnglish = (ImageView) headerView.findViewById(R.id.imgEnglish);
        imgVietNam = (ImageView) headerView.findViewById(R.id.imgVietNam);
        if (currentLanguageCode.equals("vn")) {
            imgVietNam.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_border_red)); //edit by @ManhNguyen(chang color)
        } else {
            imgEnglish.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_border_red));
        }
        imgVietNam.setOnClickListener(listenerForLeftMenu);
        imgEnglish.setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llSelectProvince).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llScanQRcode).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llSettingApplication).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llWhyChooseKFC).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llOperationalPolicy).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llContactKFC).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llKFCinfo).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llLogout).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llTermsAndConditions).setOnClickListener(listenerForLeftMenu);
        headerView.findViewById(R.id.llPolicyInformation).setOnClickListener(listenerForLeftMenu);
        rlNeedLogin = (RelativeLayout) headerView.findViewById(R.id.rlNeedLogin);
        rlLogged = (RelativeLayout) headerView.findViewById(R.id.rlLogged);
        llLogout = (LinearLayout) headerView.findViewById(R.id.llLogout);
        imgAvatar = (ImageView) headerView.findViewById(R.id.imgAvatar);
        imgBlurAvatar = (ImageView) headerView.findViewById(R.id.imgBlurAvatar);
        tvDisplayName = (TextView) headerView.findViewById(R.id.tvDisplayName);
        tvScoreMain = (FontText) headerView.findViewById(R.id.tvScoreMain);
        llScoreContainer = (LinearLayout) headerView.findViewById(R.id.llScoreContainer);
        if (userInfo != null) {
            showUserInfoInNavigation(true);
        } else {
            llLogout.setVisibility(View.GONE);
        }

        RelativeLayout rlLogin = (RelativeLayout) navigationView.getHeaderView(0).findViewById(R.id.rlLogin);
        rlLogin.setOnClickListener(this);
        llViewProfile = (LinearLayout) navigationView.getHeaderView(0).findViewById(R.id.llViewProfile);
        llViewProfile.setOnClickListener(this);
        tvDangKi = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvDangKi);
        tvDangKi.setOnClickListener(this);
    }

    public void resetColor(RelativeLayout relativeLayout, final ImageView imageView, TextView textView, int icon) {
        rlMenu.setBackgroundColor(getResources().getColor(R.color.colorMenuBarHome));
        imgMenu.setImageResource(R.drawable.ic_menu_tabbar_bottom_1);
        tvMenu.setTextColor(getResources().getColor(R.color.colorTextMenuBar1));

        rlPromotion.setBackgroundColor(getResources().getColor(R.color.colorMenuBarHome));
        imgPromotion.setImageResource(R.drawable.ic_promotion_tabbar_bottom_1);
        tvPromotion.setTextColor(getResources().getColor(R.color.colorTextMenuBar1));

        rlCoupons.setBackgroundColor(getResources().getColor(R.color.colorMenuBarHome));
        imgCoupons.setImageResource(R.drawable.ic_coupons_tabbar_bottom_1);
        tvCoupons.setTextColor(getResources().getColor(R.color.colorTextMenuBar1));

        rlRestaurant.setBackgroundColor(getResources().getColor(R.color.colorMenuBarHome));
        imgRestaurant.setImageResource(R.drawable.ic_restaurent_tabbar_bottom_1);
        tvRestaurant.setTextColor(getResources().getColor(R.color.colorTextMenuBar1));

        rlAccount.setBackgroundColor(getResources().getColor(R.color.colorMenuBarHome));
        imgAccount.setImageResource(R.drawable.ic_account_tabbar_bottom_1);
        tvAccount.setTextColor(getResources().getColor(R.color.colorTextMenuBar1));

        relativeLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        imageView.setImageResource(icon);
        textView.setTextColor(getResources().getColor(R.color.colorTextMenuBar2));
    }

    public void processChangeTopMenu(final Class<?> cls) {
        Log.i(Tag.LOG.getValue(), "Current class: " + cls.getName());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rlMain.setVisibility(View.GONE);
                rlMenu1.setVisibility(View.GONE);
                rlMenu2.setVisibility(View.GONE);
                rlMenu3.setVisibility(View.GONE);
                rlMenu4.setVisibility(View.GONE);
                rlMenu5.setVisibility(View.GONE);
                rlMenu6.setVisibility(View.GONE);
                rlMenu7.setVisibility(View.GONE);
                rlMenu8.setVisibility(View.GONE);
                rlMenu9.setVisibility(View.GONE);
                rlMenu10.setVisibility(View.GONE);
                llShoppingCart.setVisibility(View.GONE);
                rlMenuPromotion.setVisibility(View.GONE);
                rlContact.setVisibility(View.GONE);

                //add by @ManhNguyen
                rlTermsAndConditions.setVisibility(View.GONE);
                rlOperationPolicy.setVisibility(View.GONE);
                rlPolicyOnInformation.setVisibility(View.GONE);
                rlWhyChooseKFC.setVisibility(View.GONE);
                rlScanQRCode.setVisibility(View.GONE);
                rlAppSetting.setVisibility(View.GONE);

                rlTopMenu_RestaurantFragment.setVisibility(View.GONE);

                if (cls == DetailComboOrProductSelectedFragment.class) { //edit by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    llShoppingCart.setVisibility(View.VISIBLE);
                    rlMenu2.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == ShoppingCartFragment.class) { //edit by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu3.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == ContactKFCFragment.class) { //add by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlContact.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == WhyChooseKFC_Fragment.class) { //add by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlWhyChooseKFC.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == TermsAndConditionsFragment.class) { //add by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlTermsAndConditions.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == OperationPolicyFragment.class) { //add by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlOperationPolicy.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == PolicyOnInformationFragment.class) { //add by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlPolicyOnInformation.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == ScannerViewFragment.class) { //add by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlScanQRCode.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == SettingsApplication.class) { //add by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlAppSetting.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == StepOneFragment.class) { //edit by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu4.setVisibility(View.VISIBLE);

                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;

                    //add by @ManhNguyen
//                    if (getChangeAddressStep3()) {
//                        tvTitleStep1.setText(getString(R.string.TopMenu_003));
//                    } else {
//                        tvTitleStep1.setText(getString(R.string.TopMenu_001));
//                    }

//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == StepTwoFragment.class) { //edit by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu5.setVisibility(View.VISIBLE);

                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;

//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == StepThreeFragment.class || cls == ChangeDeliveryAddressFragment.class) { //edit by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu6.setVisibility(View.VISIBLE);

                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;

//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == OrderHistoryFragment.class) {
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu7.setVisibility(View.VISIBLE);
                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == OrderHistoryDetailFragment.class) { //edit by @ManhNguyen
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu8.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == DetailsPromotionFragment.class) {
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenuPromotion.setVisibility(View.VISIBLE);
                    resetColor(rlPromotion, imgPromotion, tvPromotion, R.drawable.ic_promotion_tabbar_bottom_2);
                } else if (cls == PaymentFragment.class) {
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu9.setVisibility(View.VISIBLE);
                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == OrderStatusFragment.class) { //edit by @ManhNguyen
                    tvAmountShoppingCart.setText(0 + "");
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu10.setVisibility(View.VISIBLE);
                    llMenuBar.setVisibility(View.GONE);
                    mFlag_IsVisiable_BottomBar = false;
//                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == PaymentWithATMFragment.class) {
                    rlMain.setVisibility(View.VISIBLE);
                    rlMenu9.setVisibility(View.VISIBLE);
                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                } else if (cls == AccountFragment.class) {
                    rlMenu10.setVisibility(View.VISIBLE);
                    resetColor(rlAccount, imgAccount, tvAccount, R.drawable.ic_account_tabbar_bottom_2);
                } else if (cls == RestaurantFragment.class) {
                    rlMain.setVisibility(View.VISIBLE);
                    rlTopMenu_RestaurantFragment.setVisibility(View.VISIBLE);
                    resetColor(rlRestaurant, imgRestaurant, tvRestaurant, R.drawable.ic_restaurent_tabbar_bottom_2);
                } else {
                    rlMain.setVisibility(View.VISIBLE);
                    llShoppingCart.setVisibility(View.VISIBLE);
                    rlMenu1.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void getProvince(String lang) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        final Call<ProvinceDataWrapper> call = ApiClient.getJsonClient().getProvince(lang);
        call.enqueue(new Callback<ProvinceDataWrapper>() {

            @Override
            public void onFailure(Call<ProvinceDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<ProvinceDataWrapper> arg0, Response<ProvinceDataWrapper> arg1) {
                hideProgressDialog();

                if (arg1.body().data != null) {
                    lstProvince = arg1.body().data;
                    lstDistrict = lstProvince.get(0).district;

                    menuFragment = new MenuFragment();
                    changeFragment(menuFragment);

                    dialogSelectProvince = new DialogSelectProvince(MainActivity.this);
                    dialogSelectProvince.show();

                    getConfiguration();
                }
            }
        });
    }

    public void showAndAnimationShoppingCart(final String numCart, ImageView srcView) {
        if (!isAnimating) {
            isAnimating = true;
            //runAnimation1(numCart, srcView, imgShoppingCart);
            //runAnimation2(numCart, srcView, imgShoppingCart);
            runAnimation3(numCart, srcView, imgShoppingCart);
        }
    }

    private void runAnimation1(final String numCart, @NonNull ImageView srcView, final View destView) {
        try {
            new CircleAnimationUtil()
                    .attachActivity(MainActivity.this)
                    .setTargetView(srcView)
                    .setDestView(destView)
                    .setMoveDuration(500)
                    .setAnimationListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            zoomAnimation(numCart, destView);
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    })
                    .startAnimation();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void runAnimation2(final String numCart, @NonNull ImageView srcView, final View destView) {
        try {
            final View v = createAnimationView(srcView);
            if (v == null)
                return;

            //SOLUTION TO FIX: java.lang.IllegalArgumentException: width and height must be > 0
            v.post(new Runnable() {
                @Override
                public void run() {
                    new CircleAnimationUtil()
                            .attachActivity(MainActivity.this)
                            .setTargetView(v)
                            .setDestView(destView)
                            .setMoveDuration(500)
                            .setAnimationListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    getRootView().removeView(v);
                                    zoomAnimation(numCart, destView);
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            })
                            .startAnimation();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void runAnimation3(final String numCart, @NonNull ImageView srcView, final View destView) {
        try {
            final View v = createAnimationView(srcView);
            if (v == null)
                return;

            int fromLocation[] = new int[2];
            srcView.getLocationOnScreen(fromLocation);

            int[] destLocation = new int[2];
            destView.getLocationOnScreen(destLocation);

            int i = destLocation[0];
            int j = fromLocation[0];
            int k = destLocation[1];
            int m = fromLocation[1];

            Log.d("location", "i:" + i + " j:" + j + " k:" + k + " m" + m);

            //Animation animTranslate = new ArcTranslateAnimation(-400, i - j, -400, k - m);
            Animation animTranslate = new ArcTranslateAnimation(-300, i, -300, k);
            animTranslate.setDuration(1000);

            float f = destView.getHeight() / srcView.getHeight();
            Animation animScale = new ScaleAnimation(1.0F, f, 1.0F, f);
            animScale.setDuration(1000);

            // Animation animAlpha = new AlphaAnimation(1.0F, 0.5F);

            Animation.AnimationListener animListener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    getRootView().removeView(v);
                    zoomAnimation(numCart, destView);
                }
            };

            AnimationSet localAnimationSet = new AnimationSet(true);
            localAnimationSet.addAnimation(animScale);
            localAnimationSet.addAnimation(animTranslate);
            //    localAnimationSet.addAnimation(animAlpha);
            localAnimationSet.setAnimationListener(animListener);
            v.startAnimation(localAnimationSet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void zoomAnimation(final String numCart, View targetView) {
        ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
        scale.setDuration(500);
        scale.setInterpolator(new OvershootInterpolator());
        scale.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isAnimating = false;
                tvAmountShoppingCart.setText(numCart);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        targetView.startAnimation(scale);
    }

    private View createAnimationView(ImageView srcView) {
        try {
            int fromLocation[] = new int[2];
            srcView.getLocationOnScreen(fromLocation);

            ImageView img = new ImageView(this);
            img.setImageBitmap(getClip((BitmapDrawable) srcView.getDrawable()));

            /*LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(srcView.getWidth(), srcView.getHeight());
            img.setLayoutParams(layoutParams);*/

            /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(srcView.getWidth(), srcView.getHeight());
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            img.setLayoutParams(params);*/

            getRootView().addView(img);
            return img;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private Bitmap getClip(BitmapDrawable paramBitmapDrawable) {
        int i = paramBitmapDrawable.getBitmap().getWidth();
        int j = paramBitmapDrawable.getBitmap().getHeight();
        float f1 = i / 2.0F;
        float f2 = j / 2.0F;
        float f3 = i / 2.0F;
        Object localObject1 = Shader.TileMode.CLAMP;
        Object localObject2 = new RadialGradient(f1, f2, f3, new int[]{-1, -1, -1, 16777215}, null, (Shader.TileMode) localObject1);
        localObject1 = new Paint();
        ((Paint) localObject1).setDither(true);
        ((Paint) localObject1).setAntiAlias(true);
        //  ((Paint) localObject1).setShader((Shader) localObject2);
        localObject2 = new Rect(0, 0, i, j);
        Bitmap localBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap);
        localCanvas.drawARGB(0, 0, 0, 0);
        RectF rectf = new RectF(0, 0, 500, 250);
        localCanvas.drawRect(rectf, (Paint) localObject1);
        ((Paint) localObject1).setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        localCanvas.drawBitmap(paramBitmapDrawable.getBitmap(), (Rect) localObject2, (Rect) localObject2, (Paint) localObject1);
        return localBitmap;
    }

    private ViewGroup getRootView() {
        return (ViewGroup) this.getWindow().getDecorView();
    }

    private int getScreenWidth() {
        return this.getResources().getDisplayMetrics().widthPixels;
    }

    private int getTitleBarHeight() {
        Resources res = this.getResources();
        int i = res.getIdentifier("status_bar_height", "dimen", "android");
        if (i > 0) {
            return res.getDimensionPixelSize(i);
        }
        return (int) (res.getDisplayMetrics().density * 32.0F);
    }

    private void getShipAddress(String userID, String addressID) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        Call<GetAddressDataWrapper> call1 = ApiClient.getJsonClient().getShipAddress(userID, new String(Hex.encodeHex(DigestUtils.md5(userID))), addressID);
        call1.enqueue(new Callback<GetAddressDataWrapper>() {

            @Override
            public void onFailure(Call<GetAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<GetAddressDataWrapper> arg0, Response<GetAddressDataWrapper> arg1) {
                hideProgressDialog();

                System.out.println(arg1.body().toString());
            }
        });
    }

    private void getConfiguration() {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        Call<ConfigureDataWrapper> call1 = ApiClient.getJsonClient().getConfiguration(currentLanguageCode);
        call1.enqueue(new Callback<ConfigureDataWrapper>() {

            @Override
            public void onFailure(Call<ConfigureDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<ConfigureDataWrapper> arg0, Response<ConfigureDataWrapper> arg1) {
                hideProgressDialog();

                ConfigureDataWrapper obj = arg1.body();
                conf = obj.data;
            }
        });
    }

    private void sendContact(String fullname,
                             String address,
                             String phone,
                             String email,
                             String message) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        Call<ContactDataWrapper> call1 = ApiClient.getJsonClient().sendContact(fullname,
                address,
                phone,
                email,
                message);
        call1.enqueue(new Callback<ContactDataWrapper>() {

            @Override
            public void onFailure(Call<ContactDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<ContactDataWrapper> arg0, Response<ContactDataWrapper> arg1) {
                hideProgressDialog();

                ContactDataWrapper obj = arg1.body();
            }
        });
    }

    private void getFoodFavorite(String userID,
                                 String lang,
                                 String city) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        Call<FoodFavoriteDataWrapper> call1 = ApiClient.getJsonClient().getFoodFavorite(userID,
                new String(Hex.encodeHex(DigestUtils.md5(userID))),
                lang,
                city);
        call1.enqueue(new Callback<FoodFavoriteDataWrapper>() {

            @Override
            public void onFailure(Call<FoodFavoriteDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<FoodFavoriteDataWrapper> arg0, Response<FoodFavoriteDataWrapper> arg1) {
                hideProgressDialog();

                FoodFavoriteDataWrapper obj = arg1.body();
                ArrayList<FoodFavoriteDataWrapper.FoodFavoriteWrapper> lst = obj.data;
            }
        });
    }

    private void likeNews(String newsID) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        Call<LikeNewsDataWrapper> call1 = ApiClient.getJsonClient().likeNews(newsID);
        call1.enqueue(new Callback<LikeNewsDataWrapper>() {

            @Override
            public void onFailure(Call<LikeNewsDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<LikeNewsDataWrapper> arg0, Response<LikeNewsDataWrapper> arg1) {
                hideProgressDialog();

                LikeNewsDataWrapper obj = arg1.body();
            }
        });
    }


    private void deleteFoodFavorite(String foodID, String userID) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        Call<DeleteFoodFavoriteDataWrapper> call1 = ApiClient.getJsonClient().deleteFoodFavorite(foodID, userID);
        call1.enqueue(new Callback<DeleteFoodFavoriteDataWrapper>() {

            @Override
            public void onFailure(Call<DeleteFoodFavoriteDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<DeleteFoodFavoriteDataWrapper> arg0, Response<DeleteFoodFavoriteDataWrapper> arg1) {
                hideProgressDialog();

                DeleteFoodFavoriteDataWrapper obj = arg1.body();
            }
        });
    }


    public void checkAddLike(ProductWrapper003 productSelected) {
        if (userInfo != null) {
            addLike(userInfo.id, productSelected);
        } else {
            Toast.makeText(MainActivity.this, getString(R.string.ShoppingCartAdapter_003), Toast.LENGTH_SHORT).show();
        }
    }

    private void addLike(String userID, final ProductWrapper003 productSelected) {
        showProgressDialog();

        String productID = String.valueOf(productSelected.id), type = productSelected.type;

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        Call<AddLikeDataWrapper> call1 = ApiClient.getJsonClient().addLike(userID, type, productID);
        Log.e("user_id", userID);
        Log.e("type", type);
        Log.e("productID", productID);
        call1.enqueue(new Callback<AddLikeDataWrapper>() {

            @Override
            public void onFailure(Call<AddLikeDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<AddLikeDataWrapper> arg0, Response<AddLikeDataWrapper> arg1) {
                hideProgressDialog();
                if (arg1.body().result) {
                    System.out.println(arg1.body().toString());
                    imgLike.setBackgroundResource(R.drawable.ic_favorite_food_2);
//                    tvLikeNumber.setText(String.valueOf(arg1.body().data));
                    productSelected.checkLike = true;
                    tvLikeNumber.setText((productSelected.like + 1) + "");
                } else {
                    Toast.makeText(MainActivity.this, arg1.body().message, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public int calculatePoint(double totalPrice) {
        try {
            //1 - 1000
            //x - totalPrice
            //=> x=(1*totalPrice)/1000

            return (int) Math.floor((Integer.parseInt(conf.point.point) * totalPrice) / Integer.parseInt(conf.point.price));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public String getFormatCurrentDateTime() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Calendar cal = Calendar.getInstance();
            return sdf.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean isValidatePhoneNumber(String phoneNumber) {
        String regexStr = "^[+]?[0-9]{10,13}$";
        return Pattern.matches(regexStr, phoneNumber);
    }

    public boolean patternMail(String mail) {
        String mau = ".*@.*\\.com.*";
        Pattern pattern = Pattern.compile(mau);
        Matcher matcher = pattern.matcher(mail);
        boolean isMatch = matcher.matches();
        return isMatch;
    }

    public void showDialogChooseProvince() {
        DialogProvince dialogProvince = new DialogProvince(MainActivity.this, lstProvince, tvProvince, new DialogProvince.onChooseProvinceListener() {
            @Override
            public void onChooseProvinceClick() {
                getComboAndProduct(currentLanguageCode, currentProvinceSelected.id);
                try {
                    DetailsMenuFragment frg = (DetailsMenuFragment) getSupportFragmentManager().findFragmentByTag(DetailsMenuFragment.class.getName());
                    frg.onChooseProvinceClick();
                } catch (Exception e) {
                }
            }
        });
        dialogProvince.show();
    }

    public void showUserInfoInNavigation(boolean isShow) {
        if (isShow) {
            rlNeedLogin.setVisibility(View.GONE);
            llLogout.setVisibility(View.VISIBLE);
            rlLogged.setVisibility(View.VISIBLE);
            tvDisplayName.setText(userInfo.name);
            tvScoreMain.setText(userInfo.point);
            if (conf != null && conf.point != null && conf.point.checkPoint) {
                llScoreContainer.setVisibility(View.VISIBLE);
            } else {
                llScoreContainer.setVisibility(View.GONE);
            }

            if (userInfo.avatar != null && !userInfo.avatar.isEmpty()) {

                //userInfo.avatar = "http://aaacac.com/vaa.png";
                Glide.with(this)
                        .load(userInfo.avatar)
                        .placeholder(userInfo.gender.equals("1") ? R.drawable.ic_nam : R.drawable.ic_nu)
                        .error(userInfo.gender.equals("1") ? R.drawable.ic_nam : R.drawable.ic_nu)
                        .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .fitCenter()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Picasso.with(MainActivity.this)
                                                .load(R.drawable.avatar_blur)
                                                .error(userInfo.gender.equals("1") ? R.drawable.ic_nam : R.drawable.ic_nu)
                                                .transform(new BlurTransformation(MainActivity.this, 50, 1))
                                                .into(imgBlurAvatar);
                                    }
                                });
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                //progressBar.setVisibility(View.GONE);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Picasso.with(MainActivity.this)
                                                .load(userInfo.avatar)
                                                .error(userInfo.gender.equals("1") ? R.drawable.ic_nam : R.drawable.ic_nu)
                                                .transform(new BlurTransformation(MainActivity.this, 50, 1))
                                                .into(imgBlurAvatar);
                                    }
                                });
                                return false;
                            }
                        })
                        .bitmapTransform(new CropCircleTransformation(this))
                        .into(imgAvatar);
            } else {
                Picasso.with(this)
                        .load(R.drawable.avatar_blur)
                        .transform(new BlurTransformation(this, 50, 1))
                        .error(userInfo.gender.equals("1") ? R.drawable.ic_nam : R.drawable.ic_nu)
                        .into(imgBlurAvatar);
                if (userInfo.gender.equals("1")) {
                    Picasso.with(this)
                            .load(R.drawable.ic_nam)
                            .into(imgAvatar);
                } else {
                    Picasso.with(this)
                            .load(R.drawable.ic_nu)
                            .into(imgAvatar);
                }
            }
        } else {
            rlNeedLogin.setVisibility(View.VISIBLE);
            llLogout.setVisibility(View.GONE);
            rlLogged.setVisibility(View.GONE);
            tvDisplayName.setText("");
//            Glide.with(this)
//                    .load(R.drawable.default_avatar)
//                    .placeholder(R.drawable.loading)
//                    .error(userInfo.gender.equals("1")?R.drawable.ic_nam:R.drawable.ic_nu)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE) // NONE if you load from sdcard
//                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
//                    .fitCenter()
//                    .bitmapTransform(new CropCircleTransformation(MainActivity.this))
//                    .into(imgAvatar);
//
//            Picasso.with(this)
//                    .load(R.drawable.ic_avatar_kfc)
//                    .error(userInfo.gender.equals("1")?R.drawable.ic_nam:R.drawable.ic_nu)
//                    .transform(new BlurTransformation(this, 50, 1))
//                    .into(imgBlurAvatar);
        }
    }

    private void showAlertComingSoon() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setMessage(getString(R.string.function_will_develop_later));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA:
                checkPermission(true);
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callKFC();
                } else {

                }
                break;
        }
    }

    private void signOutGG() {
        try {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {

                        }
                    });
        } catch (Exception e) {
            Log.e("Error", e.toString());
            mGoogleApiClient.reconnect();
            Log.e("mGoogleApiClient", mGoogleApiClient.isConnected() + "");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.e("Error", "onConnectionFailed:" + connectionResult);
    }


    private class MyOnItemSelectedListener implements android.widget.AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class ArcTranslateAnimation extends Animation {
        private PointF mControl;
        private PointF mEnd;
        private int mFromXType = 0;
        private float mFromXValue = 0.0F;
        private int mFromYType = 0;
        private float mFromYValue = 0.0F;
        private PointF mStart;
        private int mToXType = 0;
        private float mToXValue = 0.0F;
        private int mToYType = 0;
        private float mToYValue = 0.0F;

        public ArcTranslateAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
            this.mFromXValue = paramFloat1;
            this.mToXValue = paramFloat2;
            this.mFromYValue = paramFloat3;
            this.mToYValue = paramFloat4;
            this.mFromXType = 0;
            this.mToXType = 0;
            this.mFromYType = 0;
            this.mToYType = 0;
        }

        private long calcBezier(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
            return Math.round(Math.pow(1.0F - paramFloat1, 2.0D) * paramFloat2 + 2.0F * (1.0F - paramFloat1) * paramFloat1 * paramFloat3 + Math.pow(paramFloat1, 2.0D) * paramFloat4);
        }

        protected void applyTransformation(float paramFloat, Transformation paramTransformation) {
            float f = (float) calcBezier(paramFloat, this.mStart.x, this.mControl.x, this.mEnd.x);
            paramFloat = (float) calcBezier(paramFloat, this.mStart.y, this.mControl.y, this.mEnd.y);
            paramTransformation.getMatrix().setTranslate(f, paramFloat);
        }

        public void initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
            super.initialize(paramInt1, paramInt2, paramInt3, paramInt4);
            float f1 = resolveSize(this.mFromXType, this.mFromXValue, paramInt1, paramInt3);
            float f2 = resolveSize(this.mToXType, this.mToXValue, paramInt1, paramInt3);
            float f3 = resolveSize(this.mFromYType, this.mFromYValue, paramInt2, paramInt4);
            float f4 = resolveSize(this.mToYType, this.mToYValue, paramInt2, paramInt4);
            this.mStart = new PointF(f1, f3);
            this.mEnd = new PointF(f2, f4);
            this.mControl = new PointF(f1, f4);
        }

    }

    // block click tab
    public void disableTab(boolean enable) {
        llMenuBar.setEnabled(enable);
    }

    class MyListenerForLeftMenu implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            // Handle navigation view item clicks here.
            int id = view.getId();
            if (id == R.id.imgVietNam) {
                getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "vn").apply();
                SharedPreferenceUtils.firstchooseVN(MainActivity.this);
                currentLanguageCode = "vn";
                changeLanguageApp(currentLanguageCode);
            } else if (id == R.id.imgEnglish) {
                getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en").apply();
                SharedPreferenceUtils.firstchooseEN(MainActivity.this);
                currentLanguageCode = "en";
                changeLanguageApp(currentLanguageCode);
            } else if (id == R.id.llSelectProvince) {
                showDialogChooseProvince();
            } else if (id == R.id.llScanQRcode) {
                checkPermission(false);
//                Intent intent = new Intent(MainActivity.this, ScannerViewActivity.class);
//                startActivity(intent);
            } else if (id == R.id.llSettingApplication) {
                changeFragment(new SettingsApplication());
            } else if (id == R.id.llWhyChooseKFC) {
                changeFragment(new WhyChooseKFC_Fragment());
            } else if (id == R.id.llOperationalPolicy) {
                changeFragment(new OperationPolicyFragment());
            } else if (id == R.id.llTermsAndConditions) {
                changeFragment(new TermsAndConditionsFragment());
            } else if (id == R.id.llPolicyInformation) {
                changeFragment(new PolicyOnInformationFragment());
            } else if (id == R.id.llContactKFC) {
                changeFragment(new ContactKFCFragment());
//                showAlertComingSoon();
            } else if (id == R.id.llKFCinfo) {
                //changeFragment(new KFC_Info_Fragment());
                String version = getString(R.string.text_version) + ": " + conf.version.version_android_app;
                String copyright = getString(R.string.text_copyright) + " " + conf.year.year + " " + getString(R.string.text_kfc_vietnam);
                String mess = version + "\n" + copyright;
//                DialogFactory.showSimpleDialog(MainActivity.this, getString(R.string.msg_info_app), getString(R.string.title_info_app));
                DialogFactory.showSimpleDialog(MainActivity.this, mess, getString(R.string.title_info_app));
            } else if (id == R.id.llLogout) {
                if (userInfo != null) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage(R.string.contentLogout)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().clear().commit();
                                    userInfo = null;
                                    showUserInfoInNavigation(false);
                                    LoginManager.getInstance().logOut(); //Log out from facebook account
                                    signOutGG(); //Log out from google account
                                    menuFragment = new MenuFragment();
                                    changeFragment(menuFragment);
                                    resetColor(rlMenu, imgMenu, tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                                    Toast.makeText(MainActivity.this, getString(R.string.MainActivity_006), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                    Toast.makeText(MainActivity.this, R.string.textLogout, Toast.LENGTH_LONG).show();
                }

            }

            if (mDrawerLayout != null)
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                    mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void changeLanguageApp(String lang) {
        Locale appLoc;
        Configuration appConfig;
        Context ctx = getBaseContext();
        if (lang.equalsIgnoreCase("en")) {
            appLoc = new Locale("en");
            Locale.setDefault(appLoc);
            appConfig = new Configuration();
            appConfig.locale = appLoc;

            ctx.getResources().updateConfiguration(appConfig, ctx.getResources().getDisplayMetrics());
        } else if (lang.equalsIgnoreCase("vn")) {
            appLoc = new Locale("vi");
            Locale.setDefault(appLoc);
            appConfig = new Configuration();
            appConfig.locale = appLoc;

            ctx.getResources().updateConfiguration(appConfig, ctx.getResources().getDisplayMetrics());
        }
        finish();
        Intent intent = getIntent();
        Bundle bundle = new Bundle();
        bundle.putSerializable("user_info", userInfo);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void checkFirtRunApp() {
        SharedPreferences pref = getSharedPreferences(Const.SHARED_PREF, 0);
        boolean isFirst = pref.getBoolean(SharedPreferenceKeyType.FIRST_MENU.toString(), false);
        if (!isFirst) {
            DialogFactory.showDialogFirstRunApp(this, currentLanguageCode.equals("vn") ? R.drawable.bg_first_menu_vn : R.drawable.bg_first_menu_en);
            SharedPreferences.Editor prefEditor = pref.edit();
            prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_MENU.toString(), true);
            prefEditor.apply();
        }
    }

    //    public boolean checkOpen(AddressDataWrapper addressToShip) {
//        for (ProvinceWrapper provine : lstProvince) {
//            if (provine.id.equals(addressToShip.city)) {
//                for (DistrictWrapper ditric : provine.district) {
//                    if (ditric.id.equals(addressToShip.district)) {
//                        for (DistrictWrapper.WardWapper ward : ditric.lstWard) {
//                            if (ward.id.equals(addressToShip.ward)) {
//                                String openTime = conf.timeOpen.open;
//                                String closeTime = conf.timeOpen.close;
//                                if (!TextUtils.isEmpty(ward.open_time)) {
//                                    openTime = ward.open_time;
//                                }
//                                if (!TextUtils.isEmpty(ward.close_time)) {
//                                    closeTime = ward.close_time;
//                                }
//
//                                    String sOpen = openTime + " " + DateFormatUtils.getCurrentDate(DateFormatUtils.DD_MM_YYYY);
//                                    Date open = DateFormatUtils.str2Date(sOpen, DateFormatUtils.HH_MM_DD_MM_YYYY);
//                                    String sClose = closeTime + " " + DateFormatUtils.getCurrentDate(DateFormatUtils.DD_MM_YYYY);
//                                    Date close = DateFormatUtils.str2Date(sClose, DateFormatUtils.HH_MM_DD_MM_YYYY);
//                                    Date current = DateFormatUtils.getCurrentDate();
//                                    if (current.getTime() > open.getTime() && current.getTime() < close.getTime()) {
//                                        return true;
//                                    } else {
//                                        return false;
//                                    }
//
//
//
//                            }
//
//                        }
//                    }
//
//                }
//
//            }
//
//        }
//        return true;
//
//    }
    public void checkOpen(String sProvineId, String wradIDS, final ResultListener<ShowTimeDataWrapper> callBack) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            callBack.onFail("");
            return;
        }

        Call<ShowTimeDataWrapper> call1 = ApiClient.getJsonClient().getCheckDeliveryTime(sProvineId,
                wradIDS,
                currentLanguageCode);
        call1.enqueue(new Callback<ShowTimeDataWrapper>() {

            @Override
            public void onFailure(Call<ShowTimeDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
                callBack.onFail("");
            }

            @Override
            public void onResponse(Call<ShowTimeDataWrapper> arg0, Response<ShowTimeDataWrapper> arg1) {
                hideProgressDialog();
                ShowTimeDataWrapper obj = arg1.body();
                if (TextUtils.isEmpty(obj.getData().getMinTime())) {
                    obj.getData().setMinTime(conf.timeOpen.close);
                }
                callBack.onSucceed(obj);

            }
        });
    }

    private void checkPermission(boolean resust) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> listRequest = new ArrayList<String>();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                listRequest.add(Manifest.permission.CAMERA);
            }
            if (listRequest.size() == 0) {
                changeFragment(new ScannerViewFragment());
            } else {
                if (!resust) {
                    String[] list = new String[listRequest.size()];
                    for (int i = 0; i < listRequest.size(); i++) {
                        list[i] = listRequest.get(i);
                    }
                    requestPermissions(list, MY_PERMISSIONS_REQUEST_CAMERA);
                } else {

                }
            }
        } else {
            changeFragment(new ScannerViewFragment());
        }
    }


    public void saveToken() {
        SharedPreferences sharedpreferences = getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        String keydevice = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        String device_token = sharedpreferences.getString("device_token", "");
        if (!TextUtils.isEmpty(device_token)) {
            saveDeviceToken(device_token, keydevice, "android");
        }
    }


    private void getIp(String lang) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity) this).showNoInternetFragment();
            return;
        }

        Call<IpDataWrapper> call = ApiClient.getJsonClient().getIp(lang);
        call.enqueue(new Callback<IpDataWrapper>() {

            @Override
            public void onFailure(Call<IpDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<IpDataWrapper> arg0, Response<IpDataWrapper> arg1) {
                hideProgressDialog();
                customerIp = arg1.body().data;
            }
        });
    }

    private void saveDeviceToken(String token, String keydevice,
                                 String platform) {

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            return;
        }

        Call<StoreDeviceTokenDataWrapper> call1 = ApiClient.getJsonClient().saveDeviceToken(token,
                keydevice, platform);
        call1.enqueue(new Callback<StoreDeviceTokenDataWrapper>() {

            @Override
            public void onFailure(Call<StoreDeviceTokenDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

            }

            @Override
            public void onResponse(Call<StoreDeviceTokenDataWrapper> arg0, Response<StoreDeviceTokenDataWrapper> arg1) {
                StoreDeviceTokenDataWrapper obj = arg1.body();
            }
        });
    }

    private void getComboAndProduct(String lang, String city) {
        showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            hideProgressDialog();
            Toast.makeText(this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity) this).showNoInternetFragment();
            return;
        }

        Call<ProductDataWrapper> call = ApiClient.getJsonClient().getComboAndProduct(lang, city);
        call.enqueue(new Callback<ProductDataWrapper>() {

            @Override
            public void onFailure(Call<ProductDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                hideProgressDialog();
            }

            @Override
            public void onResponse(Call<ProductDataWrapper> arg0, Response<ProductDataWrapper> arg1) {
                hideProgressDialog();
                SharedPreferences preferencesShoppingCard = MainActivity.this.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);

                try {
                    ArrayList<ShoppingCartObject> lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null));

                    if (lstShoppingCard != null) {
                        Collections.sort(lstShoppingCard, new Comparator<ShoppingCartObject>() {
                            @Override
                            public int compare(ShoppingCartObject shoppingCartObject, ShoppingCartObject t1) {
                                return shoppingCartObject.idCart.compareToIgnoreCase(t1.idCart);
                            }
                        });
                        for (int i = 0; i < lstShoppingCard.size(); i++) {
                            for (int j = 0; j < arg1.body().products.size(); j++) {
                                ProductCategoryWrapper productCategoryWrapper = arg1.body().products.get(j);
                                if (lstShoppingCard.get(i).categoryType.equals(productCategoryWrapper.type)) {
                                    for (int x = 0; x < productCategoryWrapper.product.size(); x++) {
                                        ProductWrapper001 productWrapper001 = productCategoryWrapper.product.get(x);
                                        if (lstShoppingCard.get(i).dishSelected.id.equals(productWrapper001.id)) {
                                            lstShoppingCard.get(i).dishSelected.price = productWrapper001.price;
                                        }
                                    }
                                }
                            }
                        }
                        try {
                            preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).commit();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private ArrayList<ShoppingCartObject> lstShop = new ArrayList<>();

    public void saveCartChange() {
        try {
            if (lstShop != null && lstShop.size() > 0) {
                getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShop)).commit();
                lstShop = new ArrayList<>();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeLanguageCart() {
        SharedPreferences preferencesShoppingCard = getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
        try {
            ArrayList<ShoppingCartObject> lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null));
            if (lstShoppingCard != null && lstShoppingCard.size() > 0) {
                String userID;
                if (userInfo == null || TextUtils.isEmpty(userInfo.id)) {
                    userID = "0";
                } else {
                    userID = userInfo.id;
                }
                for (final ShoppingCartObject cartObject : lstShoppingCard) {
                    if (!(NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1)) {
                        Call<ProductDetailDataWrapper> call = ApiClient.getJsonClient().getComboOrProductDetail(String.valueOf(cartObject.dishSelected.id), userID, currentLanguageCode, null, cartObject.categoryType);
                        call.enqueue(new Callback<ProductDetailDataWrapper>() {
                            @Override
                            public void onResponse(Call<ProductDetailDataWrapper> call, Response<ProductDetailDataWrapper> response) {
                                if (response.body() != null){
                                    ProductDetailDataWrapper dataWrapper = response.body();
                                    cartObject.productSelected.nameProduct = dataWrapper.data.product.nameProduct;
                                    cartObject.dishSelected.name = dataWrapper.data.product.nameProduct;
                                    if (cartObject.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                                        for (ProductWrapper002 obj : cartObject.lstProductDefault) {
                                            for (ProductWrapper002 obj1 : dataWrapper.data.productCombo) {
                                                if (cartObject.combo1 != null && cartObject.combo1.id == obj1.id) {
                                                    cartObject.combo1.nameProduct = obj1.nameProduct;
                                                } else if (cartObject.combo2 != null && cartObject.combo2.id == obj1.id) {
                                                    cartObject.combo2.nameProduct = obj1.nameProduct;
                                                }
                                                if (obj1.id == obj.id) {
                                                    obj.nameProduct = obj1.nameProduct;
                                                }
                                            }
                                        }

                                        if (dataWrapper.data.productChange != null){
                                            if (cartObject.lstProductDefault.size() > 0 && dataWrapper.data.productChange.size() > 0)
                                                for (ProductChangeWrapper productChange : dataWrapper.data.productChange) {
                                                    if (productChange.product.size() > 0) {
                                                        for (ProductWrapper002 obj : cartObject.lstProductDefault) {
                                                            for (ProductWrapper002 obj1 : productChange.product) {
                                                                if (obj1.id == obj.id) {
                                                                    obj.nameProduct = obj1.nameProduct;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }


                                            if (dataWrapper.data.productChange.size() > 0)
                                                for (ProductChangeWrapper productChange : dataWrapper.data.productChange) {
                                                    if (productChange.product.size() > 0) {
                                                        for (ProductWrapper002 obj1 : productChange.product) {
                                                            if (cartObject.combo1 != null && cartObject.combo1.id == obj1.id) {
                                                                cartObject.combo1.nameProduct = obj1.nameProduct;
                                                            } else if (cartObject.combo2 != null && cartObject.combo2.id == obj1.id) {
                                                                cartObject.combo2.nameProduct = obj1.nameProduct;
                                                            }
                                                        }
                                                    }
                                                }


                                            if (cartObject.lstSnacksAndDessertSelected != null && cartObject.lstSnacksAndDessertSelected.size() > 0 && dataWrapper.data.productChange != null && dataWrapper.data.productChange.size() > 0)
                                                for (ProductChangeWrapper productChange : dataWrapper.data.productChange) {
                                                    if (productChange.product.size() > 0) {
                                                        for (ProductWrapper002 obj : cartObject.lstSnacksAndDessertSelected) {
                                                            for (ProductWrapper002 obj1 : productChange.product) {
                                                                if (obj1.id == obj.id) {
                                                                    obj.nameProduct = obj1.nameProduct;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }


                                            if (cartObject.lstDrinkSelected != null && cartObject.lstDrinkSelected.size() > 0 && dataWrapper.data.productChange != null && dataWrapper.data.productChange.size() > 0)
                                                for (ProductChangeWrapper productChange : dataWrapper.data.productChange) {
                                                    if (productChange.product.size() > 0) {
                                                        for (ProductWrapper002 obj : cartObject.lstDrinkSelected) {
                                                            for (ProductWrapper002 obj1 : productChange.product) {
                                                                if (obj1.id == obj.id) {
                                                                    obj.nameProduct = obj1.nameProduct;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            //add by @ManhNguyen
                                            if (cartObject.lstGaRanSelected != null && cartObject.lstGaRanSelected.size() > 0 && dataWrapper.data.productChange != null && dataWrapper.data.productChange.size() > 0)
                                                for (ProductChangeWrapper productChange : dataWrapper.data.productChange) {
                                                    if (productChange.product.size() > 0) {
                                                        for (ProductWrapper002 obj : cartObject.lstGaRanSelected) {
                                                            for (ProductWrapper002 obj1 : productChange.product) {
                                                                if (obj1.id == obj.id) {
                                                                    obj.nameProduct = obj1.nameProduct;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            if (cartObject.lstBurgerComSelected != null && cartObject.lstBurgerComSelected.size() > 0 && dataWrapper.data.productChange != null && dataWrapper.data.productChange.size() > 0)
                                                for (ProductChangeWrapper productChange : dataWrapper.data.productChange) {
                                                    if (productChange.product.size() > 0) {
                                                        for (ProductWrapper002 obj : cartObject.lstBurgerComSelected) {
                                                            for (ProductWrapper002 obj1 : productChange.product) {
                                                                if (obj1.id == obj.id) {
                                                                    obj.nameProduct = obj1.nameProduct;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        }
                                    }

                                    lstShop.add(cartObject);
                                }
                            }

                            @Override
                            public void onFailure(Call<ProductDetailDataWrapper> call, Throwable throwable) {

                            }
                        });

                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void login(String email, String pwd) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            return;
        }

        SharedPreferences sharedpreferences = this.getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("email", email);
        maps.put("password", pwd);
        maps.put("token", sharedpreferences.getString("device_token", ""));
        Call<LoginDataWrapper> call1 = ApiClient.getJsonClient().login(maps);
        call1.enqueue(new Callback<LoginDataWrapper>() {

            @Override
            public void onFailure(Call<LoginDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
            }

            @Override
            public void onResponse(Call<LoginDataWrapper> arg0, Response<LoginDataWrapper> arg1) {

                LoginDataWrapper obj = arg1.body();
                if (obj.result) {
                    try {
                        LoginWrapper mUserInfo = obj.data;
                        userInfo = mUserInfo;
                        getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(mUserInfo)).commit();
                        if (tvScoreMain != null)
                            tvScoreMain.setText(userInfo.point);
                        if (getTopFragmentOfBackStack() instanceof AccountFragment) {
                            accountFragment.setScoreUser();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void loginByFacebookOrGooglePlus(String loginType,
                                             String id,
                                             String email,
                                             String name, String phone,
                                             String urlAvatar) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            return;
        }
        SharedPreferences sharedpreferences = this.getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        String token = sharedpreferences.getString("device_token", "");

        Call<LoginDataWrapper> call1 = ApiClient.getJsonClient().loginByFacebookOrGooglePlus(loginType,
                id,
                email,
                name, phone, "0",
                urlAvatar, token);
        call1.enqueue(new Callback<LoginDataWrapper>() {

            @Override
            public void onFailure(Call<LoginDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
            }

            @Override
            public void onResponse(Call<LoginDataWrapper> arg0, Response<LoginDataWrapper> arg1) {
                LoginDataWrapper obj = arg1.body();
                if (obj.result) {
                    try {
                        LoginWrapper mUserInfo = obj.data;
                        userInfo = mUserInfo;
                        getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(mUserInfo)).commit();
                        if (tvScoreMain != null)
                            tvScoreMain.setText(userInfo.point);
                        if (getTopFragmentOfBackStack() instanceof AccountFragment) {
                            accountFragment.setScoreUser();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    public void checkProductShoppingCart(final ArrayList<ShoppingCartObject> lstShoppingCard, final ResultListener<ArrayList<ShoppingCartObject>> callBack) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            Toast.makeText(MainActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            showNoInternetFragment();
            return;
        }

        if (lstShoppingCard != null) {
            if (lstShoppingCard.size() > 0){
                showProgressDialog();
                final int size = lstShoppingCard.size();
                final int[] count = {0};
                for (final ShoppingCartObject obj : lstShoppingCard) {
                    checkProductNoBuy(sProvineId, obj.dishSelected.id, obj.dishSelected.type, currentLanguageCode, new ResultListener<ProductNoBuyDataWrapper>() {
                        @Override
                        public void onSucceed(ProductNoBuyDataWrapper result) {
                            if (result != null) {
                                count[0]++;
                                if (result.getData()) {
                                    lstShoppingCard.remove(obj);
                                }
                                if (count[0] == size) {
                                    hideProgressDialog();
                                    callBack.onSucceed(lstShoppingCard);
                                }
                            }

//                            int count_cart = 0;
//                            for (ShoppingCartObject cart : lstShoppingCard) {
//                                count_cart += Integer.parseInt(cart.amount);
//                            }
//                            tvAmountShoppingCart.setText(count_cart + "");
                        }

                        @Override
                        public void onFail(String reason) {
                            count[0]++;
                            if (count[0] == size) {
                                hideProgressDialog();
                                callBack.onSucceed(lstShoppingCard);
                            }
                        }
                    });

                }
            }
        }
    }

    public void checkProductNoBuy(String sProvineId, String productId, String type, String language, final ResultListener<ProductNoBuyDataWrapper> callBack) {
        Call<ProductNoBuyDataWrapper> call1 = ApiClient.getJsonClient().checkProductNoBuy(sProvineId,
                productId,
                type,
                language);
        call1.enqueue(new Callback<ProductNoBuyDataWrapper>() {

            @Override
            public void onFailure(Call<ProductNoBuyDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                callBack.onFail("");
            }

            @Override
            public void onResponse(Call<ProductNoBuyDataWrapper> arg0, Response<ProductNoBuyDataWrapper> arg1) {
                ProductNoBuyDataWrapper obj = arg1.body();
                callBack.onSucceed(obj);

            }
        });
    }

    public void showProgressDialog(){
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void hideProgressDialog(){
        mProgressDialog.dismiss();
    }

    public void setChangeAddressStep3() {
        isChangeAddressStep3 = true;
    }

    public boolean getChangeAddressStep3() {
        return isChangeAddressStep3;
    }

    public void clearChangeAddressStep3() {
        isChangeAddressStep3 = false;
    }
}
