package kfc.vietnam.main_screen.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class RegisterSexActivity extends BaseActivity {
    private CheckBox cbMale, cbFemale;
    private TextView mTvTitle;
    int gender = 1;
    String userName = "",sPhone = "", sDay = "", sMonth = "", sYear = "";
    boolean isLogin = false;
    public static String USERNAME = "user_name";
    public static String PHONE = "phone_number";
    public static String DAY_BIRTH = "day";
    public static String MONTH_BIRTH = "month";
    public static String YEAR_BIRTH = "year";
    public static String LOGIN = "login";
    public static String GENDER = "gender";
    private TextView tvName;

    public static int REQUESTFIST = 0123;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent() != null){
            isLogin = getIntent().getBooleanExtra(LOGIN,false);
            userName = getIntent().getStringExtra(USERNAME);
            sPhone = getIntent().getStringExtra(PHONE);
            sDay = getIntent().getStringExtra(DAY_BIRTH);
            sMonth = getIntent().getStringExtra(MONTH_BIRTH);
            sYear = getIntent().getStringExtra(YEAR_BIRTH);
        }
        setContentView(R.layout.layout_register_activity_3);
        initView();
    }
    private void initView(){
        tvName = (TextView) findViewById(R.id.tv);
        int id = R.string.register_username;
        int end = 8 + userName.length() + 1;
        String name = getString(id, userName);
        final SpannableStringBuilder str = new SpannableStringBuilder(name);

        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 8, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.setSpan(new RelativeSizeSpan(1.3f), 8, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvName.setText(str);
        mTvTitle = (TextView) findViewById(R.id.tvTitle);
        if(isLogin){
            mTvTitle.setText(R.string.login_cap);
        }else {
            mTvTitle.setText(R.string.register_cap);
        }
        cbMale = (CheckBox) findViewById(R.id.cbMale);
        cbFemale = (CheckBox) findViewById(R.id.cbFeMale);
        cbMale.setChecked(true);
        cbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbMale.isChecked()) {
                    cbFemale.setChecked(false);
                    gender = 1;
                    cbMale.setEnabled(false);
                    cbFemale.setEnabled(true);
                }
            }
        });

        cbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbFemale.isChecked()) {
                    cbMale.setChecked(false);
                    gender = 0;
                    cbFemale.setEnabled(false);
                    cbMale.setEnabled(true);
                }
            }
        });
        findViewById(R.id.llFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(USERNAME,userName);
                intent.putExtra(PHONE,sPhone);
                intent.putExtra(DAY_BIRTH,sDay);
                intent.putExtra(MONTH_BIRTH,sMonth);
                intent.putExtra(YEAR_BIRTH,sYear);
                intent.putExtra(GENDER,gender);
                setResult(RESULT_OK,intent);
                Log.d("phone", sPhone);
                finish();
            }
        });
        findViewById(R.id.llBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
