package kfc.vietnam.main_screen.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;
import kfc.vietnam.BuildConfig;
import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.utility.WebHelper;

import static kfc.vietnam.R.id.tvAllow;
import static kfc.vietnam.R.id.tvDontAllow;

/**
 * Created by hdadmin on 11/19/2016.
 */
public class DialogFactory {
    public static void showDialogLinkAccountFacebook(final Context context, String email, final ListenerLinkAccount listener) {
        final Dialog dialog = new Dialog(context, R.style.ThemePopup);
        dialog.setContentView(R.layout.dialog_link_account);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        TextView txtContent = (TextView) dialog.findViewById(R.id.txtContent);
        TextView btnAgree = (TextView) dialog.findViewById(R.id.btnAgree);
        ImageView imgBack = (ImageView) dialog.findViewById(R.id.imgBack);
        final EditText inputEmail = (EditText) dialog.findViewById(R.id.input_email);

        txtContent.setText(Html.fromHtml(context.getString(R.string.mgs_link_account_facebook, email)));
        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(context, "Please , Enter email !", Toast.LENGTH_LONG).show();
                } else {
                    listener.clickAgree(email, dialog);
                }

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.close();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void showDialogLinkAccountGoogle(final Context context, String email, final ListenerLinkAccount listener) {
        final Dialog dialog = new Dialog(context, R.style.ThemePopup);
        dialog.setContentView(R.layout.dialog_link_account);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        TextView txtContent = (TextView) dialog.findViewById(R.id.txtContent);
        TextView btnAgree = (TextView) dialog.findViewById(R.id.btnAgree);
        ImageView imgBack = (ImageView) dialog.findViewById(R.id.imgBack);
        final EditText inputEmail = (EditText) dialog.findViewById(R.id.input_email);

        txtContent.setText(Html.fromHtml(context.getString(R.string.mgs_link_account_google, email)));
        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(context, "Please , Enter email !", Toast.LENGTH_LONG).show();
                } else {
                    listener.clickAgree(email, dialog);
                }

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.close();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void showDialogFirstRunApp(final Context context, int background) {
        final Dialog dialog = new Dialog(context, R.style.ThemePopup);
        dialog.setContentView(R.layout.layout_dialog_first_run_app);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        ImageView ivFirst = (ImageView) dialog.findViewById(R.id.ivFirst);
        Picasso.with(context).load(background).into(ivFirst);
        ivFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public static void showDialogInputEmail(final Context context, final ListenerLinkAccount listener) {
        final Dialog dialog = new Dialog(context, R.style.ThemePopup);
        dialog.setContentView(R.layout.dialog_input_email);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        TextView btnAgree = (TextView) dialog.findViewById(R.id.btnAgree);
        ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClose);
        final EditText inputEmail = (EditText) dialog.findViewById(R.id.input_email);

        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(context, "Please , Enter email !", Toast.LENGTH_LONG).show();
                } else {
                    listener.clickAgree(email, dialog);
                }

            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.close();
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public static void showSimpleDialog(final Activity context, String message, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.text_app_reviews, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String url = Const.DOMAIN_GOOGLE_PLAY + BuildConfig.APPLICATION_ID;
                WebHelper.openUrlByExternalBrowser(context, url);
            }
        });
        builder.setNegativeButton(R.string.text_close, null);
        builder.show();
    }


    public static void showDialogAllowNotification(final Activity context) {
        final Dialog dialog = new Dialog(context, R.style.ThemePopup);
        dialog.setContentView(R.layout.layout_dialog_allow);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvDontAllow = (TextView) dialog.findViewById(R.id.tvDontAllow);
        TextView tvAllow = (TextView) dialog.findViewById(R.id.tvAllow);

        tvDontAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = context.getSharedPreferences(Const.SHARED_PREF, 0);
                SharedPreferences.Editor prefEditor = pref.edit();
                prefEditor.putBoolean(SharedPreferenceKeyType.BLOCK_NOTIFICATION.toString(), false);
                prefEditor.commit();
                dialog.dismiss();
            }
        });

        tvAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = context.getSharedPreferences(Const.SHARED_PREF, 0);
                SharedPreferences.Editor prefEditor = pref.edit();
                prefEditor.putBoolean(SharedPreferenceKeyType.BLOCK_NOTIFICATION.toString(), true);
                prefEditor.commit();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showDialogCheckVersionNotification(final Activity context, boolean mustUpdate, String version, final OnDialogButtonClickListener listener) {
        final Dialog dialog = new Dialog(context, R.style.ThemePopup);
        dialog.setContentView(R.layout.layout_dialog_check_version);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvVersion = (TextView) dialog.findViewById(R.id.tvVersion);
        TextView tvDownNow = (TextView) dialog.findViewById(R.id.tvDownNow);
        TextView tvRemindLater = (TextView) dialog.findViewById(R.id.tvRemindLater);
        TextView tvIgnore = (TextView) dialog.findViewById(R.id.tvIgnore);
        LinearLayout layout_ignore = (LinearLayout) dialog.findViewById(R.id.layout_ignore);

        tvVersion.setText(context.getString(R.string.text_version) + " " + version);

        if (mustUpdate){
            layout_ignore.setVisibility(View.GONE);
        }

        tvDownNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDownloadNowButtonClicked();
            }
        });

        tvRemindLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemindMeButtonClicked();
            }
        });

        tvIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onIgnoreButtonClicked();
            }
        });

        dialog.show();
    }

    public static SimpleTooltip showTipLeftTop(Activity context, View viewLeft) {
        return new SimpleTooltip.Builder(context)
                .anchorView(viewLeft)
                .contentView(R.layout.layout_tip_content_combo_product, R.id.tvContent)
                .text(R.string.content_tip_left_top_detail_combo_product)
                .gravity(Gravity.TOP)
                .arrowColor(context.getResources().getColor(R.color.black))
                .dismissOnOutsideTouch(false)
                .arrowWidth(30f)
                .padding(10f)
                .animated(true)
                .transparentOverlay(true)
                .build();
    }

    public static SimpleTooltip showTipRightTop(Activity context, View viewRight) {
        return new SimpleTooltip.Builder(context)
                .anchorView(viewRight)
                .contentView(R.layout.layout_tip_content_combo_product, R.id.tvContent)
                .text(R.string.content_tip_right_top_detail_combo_product)
                .gravity(Gravity.TOP)
                .arrowColor(context.getResources().getColor(R.color.black))
                .dismissOnOutsideTouch(true)
                .arrowWidth(30f)
                .padding(10f)
                .animated(true)
                .transparentOverlay(false)
                .build();
    }

    public static void showDialog(Context context, String title, String message) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.dialog_ok_button_title), null)
                .setCancelable(true)
                .create();
        dialog.show();
    }

    public interface ListenerLinkAccount {
        void clickAgree(String email, Dialog dialog);

        void close();
    }

    public interface OnDialogButtonClickListener {
        void onDownloadNowButtonClicked();

        void onRemindMeButtonClicked();

        void onIgnoreButtonClicked();
    }
}
