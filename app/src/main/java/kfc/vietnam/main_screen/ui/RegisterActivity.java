package kfc.vietnam.main_screen.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.LoginType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.LoginDataWrapper;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.fragment.TermsAndConditionsFragment_Register;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private EditText edtEmail, edtPassword, edtKFCUserName, edtPhoneNumber;
    private CheckBox cbAccept;
    private TextView tvTermsAndConditions;
    private LinearLayout llKFCLoginEmail, llKFCLoginFacebook, llKFCLoginGoogle, llContinue, llFinish, llBack,
            llSignInKFCAccount;
    private LinearLayout login1, login2, login3;

    private String name, email, password, phone, id, urlAvatar, type, mDay, mMonth, mYear;
    private int gender;

    private ImageView imgShowPass;

    private Boolean isShowPass = false;

    private int checkView = 0;

    private CallbackManager callbackManager;
    private LoginButton btnLoginFacebook;

    public FragmentManager fragmentManager;
    public FragmentTransaction fragmentTransaction;
    public boolean isTermsAndConditionsVisible;
    public String fragmentTag;

    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private int RC_SIGN_IN = 123;
    private String currentLanguageCode;
    private SharedPreferences preferencesLanguage;
    private TextView tvName;
    private String loginArea = "";

    private static String loginType_check = "";
    private static String id_check = "";
    private static String email_check = "";
    private static String name_check = "";
    private static String urlAvatar_check = "";

    private TextView tvTitleSignUp;
    private LinearLayout layout_mask;
    private ProgressBar mProgressBar;
    private RelativeLayout actionbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the Facebook SDK before executing any other operations,
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(this);

        if (gso == null) {
            // Google plus: Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
        }
        if (mGoogleApiClient == null) {
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");

        setContentView(R.layout.layout_register_activity);
        init();

        loginArea = getIntent().getStringExtra("login_area");
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }

    private void init() {
        tvName = (TextView) findViewById(R.id.tv);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtKFCUserName = (EditText) findViewById(R.id.edtKFCUserName);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);

        imgShowPass = (ImageView) findViewById(R.id.imgShowPass);
        llBack = (LinearLayout) findViewById(R.id.llBack);
        llBack.setOnClickListener(this);

        llContinue = (LinearLayout) findViewById(R.id.llContinue);
        llFinish = (LinearLayout) findViewById(R.id.llFinish);
        llKFCLoginEmail = (LinearLayout) findViewById(R.id.llKFCLoginEmail);
        llKFCLoginFacebook = (LinearLayout) findViewById(R.id.llKFCLoginFacebook);
        llKFCLoginGoogle = (LinearLayout) findViewById(R.id.llKFCLoginGoogle);
        llSignInKFCAccount = (LinearLayout) findViewById(R.id.llSignInKFCAccount);
        tvTermsAndConditions = (TextView) findViewById(R.id.text_term_conditions);
        tvTitleSignUp = (TextView) findViewById(R.id.tvTitleSignUp);
        layout_mask = (LinearLayout) findViewById(R.id.layout_mask);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        actionbar = (RelativeLayout) findViewById(R.id.actionbar);

        llKFCLoginEmail.setOnClickListener(this);
        llKFCLoginFacebook.setOnClickListener(this);
        llKFCLoginGoogle.setOnClickListener(this);
        llSignInKFCAccount.setOnClickListener(this);
        llContinue.setOnClickListener(this);
        llFinish.setOnClickListener(this);
        imgShowPass.setOnClickListener(this);
        tvTermsAndConditions.setOnClickListener(this);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        CheckBox cbMale = (CheckBox)findViewById(R.id.cbMale);
        CheckBox cbFeMale = (CheckBox)findViewById(R.id.cbFeMale);
        cbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    gender = 1;
            }
        });

        cbFeMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    gender = 2;
            }
        });

        login1 = (LinearLayout) findViewById(R.id.login1);
        login2 = (LinearLayout) findViewById(R.id.login2);
        login3 = (LinearLayout) findViewById(R.id.login3);

        cbAccept = (CheckBox) findViewById(R.id.cbAccept);
        tvTermsAndConditions.setText(Html.fromHtml(getString(R.string.I_have_read)));

        gender = 1;
        Intent intent = getIntent();
        login1.setVisibility(View.VISIBLE);
        login2.setVisibility(View.GONE);
        login3.setVisibility(View.GONE);
        if (intent != null) {
            type = intent.getStringExtra("type");
            id = intent.getStringExtra("id");
            email = intent.getStringExtra("email");
            name = intent.getStringExtra("name");
            urlAvatar = intent.getStringExtra("urlAvatar");

            if (type != null && !type.equals(LoginType.KFC.getValue())) {
                login1.setVisibility(View.GONE);
                login2.setVisibility(View.VISIBLE);
                edtKFCUserName.setText(name);
                login3.setVisibility(View.GONE);
            } else {
                login1.setVisibility(View.VISIBLE);
                login2.setVisibility(View.GONE);
                login3.setVisibility(View.GONE);
            }
        }


        btnLoginFacebook = (LoginButton) findViewById(R.id.btnLoginFacebook);
        btnLoginFacebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        // If using in a fragment
        //btnLoginFacebook.setFragment(this);
        // Callback registration
        btnLoginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(Tag.LOG.getValue(), "LoginButton -> onSuccess() -> " + loginResult.toString());
                Toast.makeText(RegisterActivity.this, getString(R.string.login_facebook_successful), Toast.LENGTH_SHORT).show();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginButton", response.toString());

                                try {
                                    final String id = object.getString("id");
                                    final String name = object.getString("name");
                                    String email = object.optString("email");
//                                    String birthday = object.getString("birthday");
//                                    final String avatar = "https://graph.facebook.com/" + id + "/picture";
                                    final String avatar = "https://graph.facebook.com/" + id + "/picture?type=large";
                                    if (TextUtils.isEmpty(email)) {
                                        DialogFactory.showDialogInputEmail(RegisterActivity.this, new DialogFactory.ListenerLinkAccount() {
                                            @Override
                                            public void clickAgree(String email, Dialog dialog) {
                                                checkLinkedAccountFacebook(currentLanguageCode, LoginType.FACEBOOK.getValue(), id, email, name, avatar, true);
                                            }

                                            @Override
                                            public void close() {
                                            }
                                        });
                                        return;
                                    }
                                    checkLinkedAccountFacebook(currentLanguageCode, LoginType.FACEBOOK.getValue(), id, email, name, avatar, false);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(Tag.LOG.getValue(), "LoginButton -> onCancel()");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(Tag.LOG.getValue(), "LoginButton -> onError() -> " + exception.getMessage());
                Toast.makeText(RegisterActivity.this, getString(R.string.login_facebook_fail), Toast.LENGTH_SHORT).show();
            }
        });
        isTermsAndConditionsVisible = false;
    }

    public void checkEmailRegister(String lang, final String email, String type) {
        final ProgressDialog mProgressDialog = new ProgressDialog(RegisterActivity.this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(RegisterActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, type, null);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    mProgressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    if (object != null) {
                        Toast.makeText(RegisterActivity.this, getString(R.string.mgs_email_already), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(RegisterActivity.this, RegisterUserNameActivity.class);
                        intent.putExtra(RegisterSexActivity.USERNAME, "");
                        intent.putExtra(RegisterSexActivity.LOGIN, false);
                        startActivityForResult(intent, RegisterSexActivity.REQUESTFIST);
                    }

                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                login1.setVisibility(View.GONE);
                login2.setVisibility(View.VISIBLE);
                checkView = 1;
                mProgressDialog.dismiss();
            }
        });
    }

    public void checkLinkedAccountFacebook(final String lang, final String loginType,
                                           final String id,
                                           final String email,
                                           final String name,
                                           final String urlAvatar, boolean isNull) {
        final ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, LoginType.KFC.getValue(), id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    boolean isNew = false;
                    if (object != null) {
                        String facebookID = object.optString("facebook");
                        String googleID = object.optString("google");
                        if (TextUtils.isEmpty(facebookID) && TextUtils.isEmpty(googleID)) {
                            isNew = true;
                        }
                    }
                    String idUser = "";
                    if (loginType == LoginType.FACEBOOK.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("facebook"))) {

                        idUser = object.optString("facebook");
                    } else if (loginType == LoginType.GOOGLE_PLUS.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("google"))) {

                        idUser = object.optString("google");
                    }
                    if (object != null && (idUser.equals("") || idUser.equals("null"))) {
                        LoginWrapper userInfo = mapperRegisterData(jsonObject.optJSONObject("data"));
                        sendOTPFacebook(lang, email, userInfo.id, loginType, id, name, urlAvatar, mProgressDialog);
                    } else if (object == null || id.equals(idUser)) {
                        if (isNew) {
                            if (mProgressDialog != null) {
                                mProgressDialog.dismiss();
                            }
                            id_check = id;
                            loginType_check = loginType;
                            email_check = email;
                            name_check = name;
                            urlAvatar_check = urlAvatar;
                            Intent intent = new Intent(RegisterActivity.this, RegisterUserNameActivity.class);
                            intent.putExtra(RegisterSexActivity.USERNAME, name);
                            intent.putExtra(RegisterSexActivity.LOGIN, true);
                            startActivityForResult(intent, RegisterSexActivity.REQUESTFIST);
                        } else
                            loginByFacebookOrGooglePlus(loginType, id, email, name, phone, urlAvatar, mProgressDialog);

                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                mProgressDialog.dismiss();
            }
        });
    }

    public void checkLinkedAccountGoogle(final String lang, final String loginType,
                                           final String id,
                                           final String email,
                                           final String name,
                                           final String urlAvatar, boolean isNull) {
        final ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, LoginType.KFC.getValue(), id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    boolean isNew = false;
                    if (object != null) {
                        String facebookID = object.optString("facebook");
                        String googleID = object.optString("google");
                        if (TextUtils.isEmpty(facebookID) && TextUtils.isEmpty(googleID)) {
                            isNew = true;
                        }
                    }
                    String idUser = "";
                    if (loginType == LoginType.FACEBOOK.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("facebook"))) {

                        idUser = object.optString("facebook");
                    } else if (loginType == LoginType.GOOGLE_PLUS.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("google"))) {

                        idUser = object.optString("google");
                    }
                    if (object != null && (idUser.equals("") || idUser.equals("null"))) {
                        LoginWrapper userInfo = mapperRegisterData(jsonObject.optJSONObject("data"));
                        sendOTPGoogle(lang, email, userInfo.id, loginType, id, name, urlAvatar, mProgressDialog);
                    } else if (object == null || id.equals(idUser)) {
                        if (isNew) {
                            if (mProgressDialog != null) {
                                mProgressDialog.dismiss();
                            }
                            id_check = id;
                            loginType_check = loginType;
                            email_check = email;
                            name_check = name;
                            urlAvatar_check = urlAvatar;
                            Intent intent = new Intent(RegisterActivity.this, RegisterUserNameActivity.class);
                            intent.putExtra(RegisterSexActivity.USERNAME, name);
                            intent.putExtra(RegisterSexActivity.LOGIN, true);
                            startActivityForResult(intent, RegisterSexActivity.REQUESTFIST);
                        } else
                            loginByFacebookOrGooglePlus(loginType, id, email, name, phone, urlAvatar, mProgressDialog);

                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                mProgressDialog.dismiss();
            }
        });
    }

    public void sendOTPFacebook(final String lang, final String email, final String id_user, final String loginType,
                        final String id,
                        final String name,
                        final String urlAvatar, final ProgressDialog mProgressDialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(RegisterActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().sendOTP(lang, email, id_user);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        DialogFactory.showDialogLinkAccountFacebook(RegisterActivity.this, email, new DialogFactory.ListenerLinkAccount() {
                            @Override
                            public void clickAgree(String email, Dialog dialog) {
                                mProgressDialog.dismiss();
                                checkOTP(lang, email, id_user, loginType, id, name, urlAvatar, mProgressDialog, dialog);
                            }

                            @Override
                            public void close() {
                                mProgressDialog.dismiss();
                            }
                        });
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                mProgressDialog.dismiss();
            }
        });
    }

    public void sendOTPGoogle(final String lang, final String email, final String id_user, final String loginType,
                                final String id,
                                final String name,
                                final String urlAvatar, final ProgressDialog mProgressDialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(RegisterActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().sendOTP(lang, email, id_user);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        DialogFactory.showDialogLinkAccountGoogle(RegisterActivity.this, email, new DialogFactory.ListenerLinkAccount() {
                            @Override
                            public void clickAgree(String email, Dialog dialog) {
                                mProgressDialog.dismiss();
                                checkOTP(lang, email, id_user, loginType, id, name, urlAvatar, mProgressDialog, dialog);
                            }

                            @Override
                            public void close() {
                                mProgressDialog.dismiss();
                            }
                        });
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                mProgressDialog.dismiss();
            }
        });
    }

    public boolean isValidatePhoneNumber(String phoneNumber) {
        String regexStr = "^[+]?[0-9]{10,13}$";
        return Pattern.matches(regexStr, phoneNumber);
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void register(String name,
                          String email,
                          final String pwd,
                          final int gender,
                          String phone,
                          String day,
                          String month,
                          String year) {
        final ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(RegisterActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call1 = ApiClient.getJsonClient().register(name, email, pwd, gender, phone, day, month, year);
        call1.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> arg0, Throwable arg1) {
                arg1.printStackTrace();
                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ResponseBody> arg0, Response<ResponseBody> arg1) {
                try {
                    JSONObject jsonObject = new JSONObject(arg1.body().string());
                    if (jsonObject.getBoolean("result")) {
                        LoginWrapper userInfo = mapperRegisterData(jsonObject.optJSONObject("data"));
                        getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(),
                                Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(),
                                ObjectSerializer.serialize(userInfo)).commit();
                        getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_PASSWORD.toString(), pwd).apply();
                        processNextStepAfterLoginSuccess(userInfo);
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
                mProgressDialog.dismiss();
                System.out.println(arg1.body().toString());
            }
        });
    }

    public void checkOTP(final String lang, final String email, final String id_user, final String loginType,
                         final String id,
                         final String name,
                         final String urlAvatar, final ProgressDialog mProgressDialog, final Dialog dialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(RegisterActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().checkOTP(lang, email, id_user, id, loginType);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        dialog.dismiss();
                        loginByFacebookOrGooglePlus(loginType, id, email, name, phone, urlAvatar, mProgressDialog);
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    dialog.dismiss();
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(RegisterActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public LoginWrapper mapperRegisterData(JSONObject object) throws JSONException {
        LoginWrapper data = new LoginWrapper();
        data.id = object.getString("id");
        data.password = object.optString("password");
        data.name = object.optString("name");
        data.gender = object.optString("gender");
        data.email = object.optString("email");
        data.phone = object.optString("phone");
        data.dt_create = object.optString("dt_create");
        data.usertype = object.optString("usertype");
        data.bl_active = object.optString("bl_active");
        return data;
    }

    private void loginByFacebookOrGooglePlus(String loginType,
                                             String id,
                                             final String email,
                                             String name, String phone,
                                             String urlAvatar, final ProgressDialog mProgressDialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(RegisterActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        SharedPreferences sharedpreferences = this.getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);

        Call<LoginDataWrapper> call1 = ApiClient.getJsonClient().loginByFacebookOrGooglePlus(loginType,
                id,
                email,
                name, phone != null ? phone : "", String.valueOf(gender),
                urlAvatar, sharedpreferences.getString("device_token", ""));
        call1.enqueue(new Callback<LoginDataWrapper>() {

            @Override
            public void onFailure(Call<LoginDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
                Log.e("Error", arg1.toString());
            }

            @Override
            public void onResponse(Call<LoginDataWrapper> arg0, Response<LoginDataWrapper> arg1) {
                mProgressDialog.dismiss();

                LoginDataWrapper obj = arg1.body();
                if (obj.result) {
                    try {
                        LoginWrapper mUserInfo = obj.data;
                        getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(mUserInfo)).commit();
                        Log.e("RLV", mUserInfo.toString());
                        processNextStepAfterLoginSuccess(mUserInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    Toast.makeText(RegisterActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void processNextStepAfterLoginSuccess(LoginWrapper userInfo) throws IOException {
        Intent i = getIntent();
        Bundle b = new Bundle();

        if (loginArea == null) {
            b.putSerializable("user_info", userInfo);
            i.putExtras(b);
            setResult(RequestResultCodeType.REQUEST_LOGIN.getValue(), i);
            LoginActivity.getEx = true;
            finish();
            return;
        }

        if (loginArea.equalsIgnoreCase(LoginAreaType.LOGIN_IN_SHOPPING_CART.toString())) {
            b.putSerializable("user_info", userInfo);
            i.putExtras(b);
            setResult(RequestResultCodeType.RESULT_CODE_LOGIN_IN_SHOPPING_CART.getValue(), i);
        } else if (loginArea.equalsIgnoreCase(LoginAreaType.LOGIN_ON_NAVIGATION.toString())) {
            b.putSerializable("user_info", userInfo);
            i.putExtras(b);
            setResult(RequestResultCodeType.RESULT_CODE_LOGIN_ON_NAVIGATION.getValue(), i);
        } else if (loginArea.equalsIgnoreCase(LoginAreaType.REGISTER_IN_STEP_ONE.toString())) {
            b.putString("email", email);
            b.putString("pwd", password);
            i.putExtras(b);
            setResult(RequestResultCodeType.RESULT_CODE_REGISTER_IN_STEP_ONE.getValue(), i);
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        onBack();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContinue:
                name = edtKFCUserName.getText().toString().trim();
                phone = edtPhoneNumber.getText().toString().trim();
                if (name.length() == 0) {
                    edtKFCUserName.setError("Vui lòng nhập họ tên");
                } else if (!isValidatePhoneNumber(phone)) {
                    edtPhoneNumber.setError(getString(R.string.StepOneFragment_003));
                } else {
                    login2.setVisibility(View.GONE);
                    login3.setVisibility(View.VISIBLE);
                    checkView = 2;
                    int id = R.string.register_username;
                    String name = getString(id, edtKFCUserName.getText().toString().trim());
                    int end = 8 + edtKFCUserName.getText().toString().trim().length() + 1;
                    final SpannableStringBuilder str = new SpannableStringBuilder(name);
                    str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 8, end,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new RelativeSizeSpan(1.3f), 8, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvName.setText(str);
                }
                break;

            case R.id.llFinish:
                if (TextUtils.isEmpty(type) || type.equals(LoginType.KFC.getValue())) {
                    register(name, email, password, gender, phone, mDay, mMonth, mYear);
                } else if (type.equals(LoginType.FACEBOOK.getValue()) || type.equals(LoginType.GOOGLE_PLUS.getValue())) {
                    ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
                    mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
                    mProgressDialog.setCancelable(true);
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.show();
                    loginByFacebookOrGooglePlus(type, id, email, name, phone, urlAvatar, mProgressDialog);
                }
                break;

            case R.id.imgShowPass:
                if (isShowPass) {
                    edtPassword.setTransformationMethod(new PasswordTransformationMethod());
                    isShowPass = !isShowPass;
                } else {
                    edtPassword.setTransformationMethod(null);
                    isShowPass = !isShowPass;
                }
                break;

            case R.id.llBack:
                onBack();
                break;

            case R.id.llKFCLoginEmail:
                email = edtEmail.getText().toString().trim();
                password = edtPassword.getText().toString().trim();
                if (email.length() == 0){
                    Toast.makeText(RegisterActivity.this, getString(R.string.pls_enter_email), Toast.LENGTH_SHORT).show();
                } else if (password.length() == 0){
                    Toast.makeText(RegisterActivity.this, getString(R.string.pls_enter_password), Toast.LENGTH_SHORT).show();
                } else if (!isEmailValid(email)) {
                    Toast.makeText(RegisterActivity.this, getString(R.string.StepOneFragment_004), Toast.LENGTH_SHORT).show();
                } else if (!cbAccept.isChecked()) {
                    Toast.makeText(RegisterActivity.this, getString(R.string.pls_accecpt_term_kfc), Toast.LENGTH_SHORT).show();
                } else {
                    checkEmailRegister(currentLanguageCode, email, LoginType.KFC.getValue());
                }
                break;

            case R.id.llKFCLoginFacebook:
                LoginManager.getInstance().logOut();
                btnLoginFacebook.performClick();
                break;

            case R.id.llKFCLoginGoogle:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;

            case R.id.llSignInKFCAccount:
                setResult(RequestResultCodeType.RESULT_OPEN_LOGIN.getValue() , new Intent());
                finish();
                break;

            case R.id.text_term_conditions:
                showTermAndConditionsFragment();
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RegisterSexActivity.REQUESTFIST) {
            Log.d("Dung", "Code:" + resultCode);
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    name = data.getStringExtra(RegisterSexActivity.USERNAME);
                    phone = data.getStringExtra(RegisterSexActivity.PHONE);
                    mDay = data.getStringExtra(RegisterSexActivity.DAY_BIRTH);
                    mMonth = data.getStringExtra(RegisterSexActivity.MONTH_BIRTH);
                    mYear = data.getStringExtra(RegisterSexActivity.YEAR_BIRTH);
                    gender = data.getIntExtra(RegisterSexActivity.GENDER, 1);
                }
                if (TextUtils.isEmpty(type) || type.equals(LoginType.KFC.getValue())) {
                    register(name, email, password, gender, phone, mDay, mMonth, mYear);
                } else if (type.equals(LoginType.FACEBOOK.getValue()) || type.equals(LoginType.GOOGLE_PLUS.getValue())) {
                    ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
                    mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
                    mProgressDialog.setCancelable(true);
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.show();
                    loginByFacebookOrGooglePlus(type, id, email, name, phone, urlAvatar, mProgressDialog);
                }
            }

        } else if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(Tag.LOG.getValue(), "Google Plus -> onActivityResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                final GoogleSignInAccount acct = result.getSignInAccount();

                String info = "Id: " + acct.getId() + ", Email: " + acct.getEmail() + ", DisplayName: " + acct.getDisplayName() + ", PhotoUrl: " + acct.getPhotoUrl().toString();
                Log.d(Tag.LOG.getValue(), "INFO: " + info);

                Toast.makeText(RegisterActivity.this, getString(R.string.login_google_plus_successful), Toast.LENGTH_SHORT).show();

                if (TextUtils.isEmpty(acct.getEmail())) {
                    DialogFactory.showDialogInputEmail(RegisterActivity.this, new DialogFactory.ListenerLinkAccount() {
                        @Override
                        public void clickAgree(String email, Dialog dialog) {
                            checkLinkedAccountGoogle(currentLanguageCode, LoginType.GOOGLE_PLUS.getValue(), acct.getId(), email, acct.getDisplayName(), acct.getPhotoUrl().toString(), true);
                        }

                        @Override
                        public void close() {
                        }
                    });
                    return;
                }

                checkLinkedAccountGoogle(currentLanguageCode, LoginType.GOOGLE_PLUS.getValue(), acct.getId(), acct.getEmail(), acct.getDisplayName(), acct.getPhotoUrl().toString(), false);

            } else {
                // Signed out, show unauthenticated UI.
                Toast.makeText(RegisterActivity.this, getString(R.string.login_google_plus_fail), Toast.LENGTH_SHORT).show();
            }
        } else {
            //Facebook
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(Tag.LOG.getValue(), "onConnectionFailed():" + connectionResult.toString());
        Toast.makeText(RegisterActivity.this, getString(R.string.connect_google_plus_fail), Toast.LENGTH_SHORT).show();
    }

    public void showTermAndConditionsFragment() {
        isTermsAndConditionsVisible = true;
        Fragment fragment = new TermsAndConditionsFragment_Register();
        fragmentTag = fragment.getClass().getName();
        if (fragmentManager == null)
            fragmentManager = getSupportFragmentManager();

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(R.id.linearlayout_container, fragment, fragmentTag);
        fragmentTransaction.commitAllowingStateLoss();

        tvTitleSignUp.setText(getString(R.string.terms_and_conditions));
        showLayoutMask();
    }

    public void removeTermAndConditionsFragment() {
        isTermsAndConditionsVisible = false;
        fragmentManager.beginTransaction().
                remove(getSupportFragmentManager().findFragmentByTag(fragmentTag)).commit();
    }

    public void showLayoutMask(){
        layout_mask.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        actionbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }

    public void hideLayoutMask(){
        layout_mask.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
    }

    private void onBack() {
        tvTitleSignUp.setText(getString(R.string.register_cap));
        layout_mask.setVisibility(View.GONE);
        actionbar.setBackgroundColor(getResources().getColor(R.color.colorTransparent));
        mProgressBar.setVisibility(View.GONE);

        if (isTermsAndConditionsVisible) {
            removeTermAndConditionsFragment();
            return;
        }
        switch (checkView) {
            case 0:
                finish();
                break;
            case 1:
                login1.setVisibility(View.VISIBLE);
                login2.setVisibility(View.GONE);
                checkView = 0;
                break;
            case 2:
                login2.setVisibility(View.VISIBLE);
                login3.setVisibility(View.GONE);
                checkView = 1;
                break;
        }
    }
}
