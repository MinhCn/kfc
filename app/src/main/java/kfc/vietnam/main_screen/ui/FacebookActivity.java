package kfc.vietnam.main_screen.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.Arrays;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Tag;

/**
 * Created by AnhDuc on 16/07/2016.
 */
public class FacebookActivity extends AppCompatActivity {
    private CallbackManager callbackManager;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the Facebook SDK before executing any other operations,
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.layout_facebook);

        LoginButton btnLoginFacebook = (LoginButton) findViewById(R.id.btnLoginFacebook);
        btnLoginFacebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        // If using in a fragment
        //btnLoginFacebook.setFragment(this);
        // Callback registration
        btnLoginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(Tag.LOG.getValue(), "LoginButton -> onSuccess() -> " + loginResult.toString());

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginButton", response.toString());

                                try {
                                    String id = object.getString("id");
                                    String name = object.getString("name");
                                    String email = object.getString("email");
                                    String birthday = object.getString("birthday");
                                    String avatar = "https://graph.facebook.com/" + id + "/picture";
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(Tag.LOG.getValue(), "LoginButton -> onCancel()");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(Tag.LOG.getValue(), "LoginButton -> onError() -> " + exception.getMessage());
            }
        });
//        //OR
//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        Log.i(Tag.LOG.getValue(), "LoginManager -> onSuccess() -> " + loginResult.toString());
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        Log.i(Tag.LOG.getValue(), "LoginManager -> onCancel()");
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        Log.e(Tag.LOG.getValue(), "LoginManager -> onError() -> " + exception.getMessage());
//                    }
//                });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Facebook
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
