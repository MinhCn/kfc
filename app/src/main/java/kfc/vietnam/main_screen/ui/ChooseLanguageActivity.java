package kfc.vietnam.main_screen.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.Locale;

import kfc.vietnam.BuildConfig;
import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.ConfigureDataWrapper;
import kfc.vietnam.common.object.NotificationData;
import kfc.vietnam.common.utility.FontText;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.SharedPreferenceUtils;
import kfc.vietnam.common.webservice.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 31/07/2016.
 */
public class ChooseLanguageActivity extends Activity implements View.OnClickListener {

    public static String device_token;
    private String currentLanguageCode = "";
    private String keydevice = ""; //IMEI
    private LinearLayout llVietnamese, llEnglish, llCheckLanguage;
    private FontText tvChooseEnglish, tvChooseVietnamese;
    private SharedPreferences preferencesLanguage;
    private boolean clickSelectLanguageCode = false;
    private boolean check_first_run = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView tvCopyright;
    private Calendar mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);

        tvCopyright = (TextView) findViewById(R.id.tvCopyright);
        mCalendar = Calendar.getInstance();
        loadCopyright();

        // Handle notification messages system tray
        Intent intent = getIntent();
        if (intent != null){
            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.containsKey("data")) {
                if (((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0)) {
                    String data = bundle.getString("data");
                    NotificationData notificationData = new Gson().fromJson(data, NotificationData.class);
                    EventBus.getDefault().post(notificationData);

                }else {
                    intent = new Intent(this, MainActivity.class);
                    intent.putExtra("data", bundle.getString("data"));
                    intent.putExtra("in_app", false);
                    startActivity(intent);
                }
                finish();
                return;
            }
        }

        // fix bug installer OS động gọi screen này khi restart app từ stack hệ thống
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }

        tvChooseEnglish = (FontText) findViewById(R.id.tvChooseEnglish);
        tvChooseVietnamese = (FontText) findViewById(R.id.tvChooseVietnamese);

        llCheckLanguage = (LinearLayout) findViewById(R.id.llCheckLanguage);

        llVietnamese = (LinearLayout) findViewById(R.id.llVietnamese);
        llEnglish = (LinearLayout) findViewById(R.id.llEnglish);
        llVietnamese.setOnClickListener(this);
        llEnglish.setOnClickListener(this);

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Const.SHARED_PREF, 0);
        device_token = pref.getString("device_token", "");

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Const.REGISTRATION_COMPLETE)) {

                    preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
//                    if (!preferencesLanguage.contains(SharedPreferenceKeyType.LANGUAGE_CODE.toString())) {
                        //the app is being launched for first time, do something
                        llCheckLanguage.setVisibility(View.VISIBLE);
//                    } else {
//                        currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
//                        new Handler().postDelayed(new Runnable() {
//
//                            // Using handler with postDelayed called runnable run method
//                            @Override
//                            public void run() {
//                                if (clickSelectLanguageCode){
//                                    return;
//                                }
//                                switchLanguage(currentLanguageCode);
//
//                                Intent i = new Intent(ChooseLanguageActivity.this, MainActivity.class);
//                                i.putExtra("language_code", currentLanguageCode);
//                                startActivity(i);
//
//                                // close this activity
//                                finish();
//                            }
//                        }, 3 * 1000);
//
//                    }

                }
            }
        };

        if (!device_token.isEmpty()) {
//            preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
//            if (!preferencesLanguage.contains(SharedPreferenceKeyType.LANGUAGE_CODE.toString())) {
                //the app is being launched for first time, do something
                llCheckLanguage.setVisibility(View.VISIBLE);
//            } else {
//                currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
//                new Handler().postDelayed(new Runnable() {
//
//                    // Using handler with postDelayed called runnable run method
//
//                    @Override
//                    public void run() {
//                        if (clickSelectLanguageCode){
//                            return;
//                        }
//                        switchLanguage(currentLanguageCode);
//
//                        Intent i = new Intent(ChooseLanguageActivity.this, MainActivity.class);
//                        i.putExtra("language_code", currentLanguageCode);
//                        startActivity(i);
//
//                        // close this activity
//                        finish();
//                    }
//                }, 3 * 1000);
//
//            }
        }else {
            check_first_run = true;
        }


    }


    public void checkFirtRunApp(){
        SharedPreferences pref = getSharedPreferences(Const.SHARED_PREF, 0);
        boolean isFirst = pref.getBoolean(SharedPreferenceKeyType.FIRST_ALLOW_NOTIFI.toString() , false);
        if (!isFirst){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogFactory.showDialogAllowNotification(ChooseLanguageActivity.this);
                }
            });

            SharedPreferences.Editor prefEditor = pref.edit();
            prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_ALLOW_NOTIFI.toString(), true);
            prefEditor.apply();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llVietnamese:
                clickSelectLanguageCode = true;
                currentLanguageCode = "vn";

                llEnglish.setBackground(getResources().getDrawable(R.drawable.shape_background_button_english_home_screen));
                llVietnamese.setBackground(getResources().getDrawable(R.drawable.shape_background_button_vietnam_home_screen));

                switchLanguage(currentLanguageCode);

                SharedPreferences settings = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
                SharedPreferences.Editor prefEditor = settings.edit();
                prefEditor.putString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), currentLanguageCode);
                SharedPreferenceUtils.firstchooseVN(this);
                prefEditor.apply();

//                preferencesLanguage.edit().putString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), currentLanguageCode).commit();
                Intent i = new Intent(ChooseLanguageActivity.this, MainActivity.class);
                i.putExtra("language_code", currentLanguageCode);
                i.putExtra("check_first_run", currentLanguageCode);
                startActivity(i);
                finish();
                break;
            case R.id.llEnglish:
                clickSelectLanguageCode = true;
                currentLanguageCode = "en";

                llVietnamese.setBackground(getResources().getDrawable(R.drawable.shape_background_button_english_home_screen));
                llEnglish.setBackground(getResources().getDrawable(R.drawable.shape_background_button_vietnam_home_screen));

                switchLanguage(currentLanguageCode);
                SharedPreferences settings1 = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
                SharedPreferences.Editor prefEditor1 = settings1.edit();
                prefEditor1.putString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), currentLanguageCode);
                SharedPreferenceUtils.firstchooseEN(this);
                prefEditor1.apply();
                Intent _i = new Intent(ChooseLanguageActivity.this, MainActivity.class);
                _i.putExtra("language_code", currentLanguageCode);
                startActivity(_i);
                finish();
                break;

        }
    }

    private void switchLanguage(String lang) {
        Locale appLoc;
        Configuration appConfig;
        Context ctx = getBaseContext();
        if (lang.equalsIgnoreCase("en")) {
            appLoc = new Locale("en");
            Locale.setDefault(appLoc);
            appConfig = new Configuration();
            appConfig.locale = appLoc;

            ctx.getResources().updateConfiguration(appConfig, ctx.getResources().getDisplayMetrics());
        } else if (lang.equalsIgnoreCase("vn")) {
            appLoc = new Locale("vi");
            Locale.setDefault(appLoc);
            appConfig = new Configuration();
            appConfig.locale = appLoc;

            ctx.getResources().updateConfiguration(appConfig, ctx.getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Const.REGISTRATION_COMPLETE));
        // checkPermission();
        checkFirtRunApp();
    }


    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void loadCopyright() {
        final ProgressDialog mProgressDialog = new ProgressDialog(ChooseLanguageActivity.this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();

            String temp = getString(R.string.text_copyright) + " " + mCalendar.get(Calendar.YEAR) + " " +
                    getString(R.string.text_kfc_vietnam);
            tvCopyright.setText(temp);
        } else {
            Call<ConfigureDataWrapper> call1 = ApiClient.getJsonClient().getConfiguration("vn");
            call1.enqueue(new Callback<ConfigureDataWrapper>() {

                @Override
                public void onFailure(Call<ConfigureDataWrapper> arg0, Throwable arg1) {
                    arg1.printStackTrace();
                    mProgressDialog.dismiss();

                    String temp = getString(R.string.text_copyright) + " " + mCalendar.get(Calendar.YEAR) + " " +
                            getString(R.string.text_kfc_vietnam);
                    tvCopyright.setText(temp);
                }

                @Override
                public void onResponse(Call<ConfigureDataWrapper> arg0, Response<ConfigureDataWrapper> arg1) {
                    mProgressDialog.dismiss();

                    if (arg1.body() != null) {
                        if (arg1.body().result) {
                            String temp = getString(R.string.text_copyright) + " " + arg1.body().data.year.year + " " +
                                    getString(R.string.text_kfc_vietnam);
                            tvCopyright.setText(temp);
                        }
                    } else {
                        String temp = getString(R.string.text_copyright) + " " + mCalendar.get(Calendar.YEAR) + " " +
                                getString(R.string.text_kfc_vietnam);
                        tvCopyright.setText(temp);
                    }
                }
            });
        }
    }
}
