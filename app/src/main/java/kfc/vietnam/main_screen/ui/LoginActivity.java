package kfc.vietnam.main_screen.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.LoginType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.LoginDataWrapper;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.StoreDeviceTokenDataWrapper;
import kfc.vietnam.common.utility.DialogForgotPassword;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kfc.vietnam.main_screen.ui.ChooseLanguageActivity.device_token;


/**
 * Created by VietRuyn on 01/08/2016.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private EditText edtKFCUserName, edtKFCPassword;
    private CheckBox cbAccept;
    private LinearLayout llKFCLogin, llRegisterKFCAccount;
    private ImageView imgShowPass;

    private CallbackManager callbackManager;
    private LoginButton btnLoginFacebook;

    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private int RC_SIGN_IN = 123;

    private String loginArea = "";

    private ImageView imgClose;
    public static boolean lg = false, getEx = false;
    boolean showPass = false;

    private CheckBox chkRememberAccount;
    private String currentLanguageCode;
    private SharedPreferences preferencesLanguage;

    private static String loginType_check = "";
    private static String id_check = "";
    private static String email_check = "";
    private static String name_check = "";
    private static String urlAvatar_check = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the Facebook SDK before executing any other operations,
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(this);

        if (gso == null) {
            // Google plus: Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
        }
        if (mGoogleApiClient == null) {
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }

        preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");

        setContentView(R.layout.layout_login_activity);
        init();

        loginArea = getIntent().getStringExtra("login_area");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        lg = true;
        getEx = false;
    }

    private void init() {
        chkRememberAccount = (CheckBox) findViewById(R.id.chkRememberAccount);

        TextView tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(this);

        llKFCLogin = (LinearLayout) findViewById(R.id.llKFCLogin);
        llKFCLogin.setOnClickListener(this);

        llRegisterKFCAccount = (LinearLayout) findViewById(R.id.llRegisterKFCAccount);
        llRegisterKFCAccount.setOnClickListener(this);

        LinearLayout llFacebookLogin = (LinearLayout) findViewById(R.id.llFacebookLogin);
        llFacebookLogin.setOnClickListener(this);

        LinearLayout llGooglePlusLogin = (LinearLayout) findViewById(R.id.llGooglePlusLogin);
        llGooglePlusLogin.setOnClickListener(this);

        edtKFCUserName = (EditText) findViewById(R.id.edtKFCUserName);
        edtKFCUserName.addTextChangedListener(new TextChangedListener(this, edtKFCUserName));
        edtKFCPassword = (EditText) findViewById(R.id.edtKFCPassword);
        edtKFCPassword.addTextChangedListener(new TextChangedListener(this, edtKFCPassword));

        imgShowPass = (ImageView) findViewById(R.id.imgShowPass);
        imgShowPass.setOnClickListener(this);

        cbAccept = (CheckBox) findViewById(R.id.cbAccept);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

        btnLoginFacebook = (LoginButton) findViewById(R.id.btnLoginFacebook);
        btnLoginFacebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        // Callback registration
        btnLoginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                //save device token
                String keydevice = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                saveDeviceToken(device_token, keydevice, "android");

                Log.i(Tag.LOG.getValue(), "LoginButton -> onSuccess() -> " + loginResult.toString());
                Toast.makeText(LoginActivity.this, getString(R.string.login_facebook_successful), Toast.LENGTH_SHORT).show();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginButton", response.toString());

                                try {
                                    final String id = object.getString("id");
                                    final String name = object.getString("name");
                                    String email = object.optString("email");
//                                    String birthday = object.getString("birthday");
//                                    final String avatar = "https://graph.facebook.com/" + id + "/picture";
                                    final String avatar = "https://graph.facebook.com/" + id + "/picture?type=large";
                                    if (TextUtils.isEmpty(email)) {
                                        DialogFactory.showDialogInputEmail(LoginActivity.this, new DialogFactory.ListenerLinkAccount() {
                                            @Override
                                            public void clickAgree(String email, Dialog dialog) {
                                                checkLinkedAccountFacebook(currentLanguageCode, LoginType.FACEBOOK.getValue(), id, email, name, avatar, true);
                                            }

                                            @Override
                                            public void close() {
                                            }
                                        });
                                        return;
                                    }

                                    checkLinkedAccountFacebook(currentLanguageCode, LoginType.FACEBOOK.getValue(), id, email, name, avatar, false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(Tag.LOG.getValue(), "LoginButton -> onCancel()");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(Tag.LOG.getValue(), "LoginButton -> onError() -> " + exception.getMessage());
                Toast.makeText(LoginActivity.this, getString(R.string.login_facebook_fail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void register(String name,
                          String email,
                          String pwd,
                          int gender,
                          String phone,
                          String day,
                          String month,
                          String year) {
        final ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(LoginActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call1 = ApiClient.getJsonClient().register(name, email, pwd, gender, phone, day, month, year);
        call1.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ResponseBody> arg0, Response<ResponseBody> arg1) {
                mProgressDialog.dismiss();

                System.out.println(arg1.body().toString());
            }
        });
    }

    private void login(String email,final String pwd) {
        final ProgressDialog mProgressDialog = new ProgressDialog(LoginActivity.this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(LoginActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        SharedPreferences sharedpreferences = this.getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("email", email);
        maps.put("password", pwd);
        maps.put("token", sharedpreferences.getString("device_token", ""));
        Call<LoginDataWrapper> call1 = ApiClient.getJsonClient().login(maps);
        call1.enqueue(new Callback<LoginDataWrapper>() {

            @Override
            public void onFailure(Call<LoginDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                DialogFactory.showDialog(LoginActivity.this, getString(R.string.error), getString(R.string.login_kfc_fail));
                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<LoginDataWrapper> arg0, Response<LoginDataWrapper> arg1) {
                mProgressDialog.dismiss();

                LoginDataWrapper obj = arg1.body();
                if (obj.result) {
                    try {

                        //save device token
                        String keydevice = Settings.Secure.getString(getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        saveDeviceToken(device_token, keydevice, "android");

                        LoginWrapper mUserInfo = obj.data;
                        if (chkRememberAccount.isChecked()) {
                            getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(mUserInfo)).commit();
                            getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_PASSWORD.toString(), pwd).apply();
                        }

                        processNextStepAfterLoginSuccess(mUserInfo);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                        DialogFactory.showDialog(LoginActivity.this, getString(R.string.error), getString(R.string.login_kfc_fail));
                    }
                } else {
                    DialogFactory.showDialog(LoginActivity.this, getString(R.string.error), getString(R.string.login_kfc_fail));
                }
            }
        });
    }

    private void loginByFacebookOrGooglePlus(String loginType,
                                             String id,
                                             String email,
                                             String name, String phone,
                                             String urlAvatar, ProgressDialog mProgressDialog) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
            mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
        }

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(LoginActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPreferences sharedpreferences = this.getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        String token = sharedpreferences.getString("device_token", "");

        Call<LoginDataWrapper> call1 = ApiClient.getJsonClient().loginByFacebookOrGooglePlus(loginType,
                id,
                email,
                name, phone, "0",
                urlAvatar, token);
        final ProgressDialog finalMProgressDialog = mProgressDialog;
        call1.enqueue(new Callback<LoginDataWrapper>() {

            @Override
            public void onFailure(Call<LoginDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                finalMProgressDialog.dismiss();
                Log.e("Error", arg1.toString());
            }

            @Override
            public void onResponse(Call<LoginDataWrapper> arg0, Response<LoginDataWrapper> arg1) {
                finalMProgressDialog.dismiss();

                LoginDataWrapper obj = arg1.body();
                if (obj.result) {
                    try {
                        //save device token
                        String keydevice = Settings.Secure.getString(getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        saveDeviceToken(device_token, keydevice, "android");

                        LoginWrapper mUserInfo = obj.data;
                        getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(mUserInfo)).commit();
                        Log.e("RLV", mUserInfo.toString());
                        processNextStepAfterLoginSuccess(mUserInfo);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    Toast.makeText(LoginActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void checkLinkedAccountFacebook(final String lang, final String loginType,
                                           final String id,
                                           final String email,
                                           final String name,
                                           final String urlAvatar, final boolean isNull) {
        final ProgressDialog mProgressDialog = new ProgressDialog(LoginActivity.this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, LoginType.KFC.getValue() , id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    boolean isNew = false;
                    if (object != null) {
                        String facebookID = object.optString("facebook");
                        String googleID = object.optString("google");

                        if (TextUtils.isEmpty(facebookID) && TextUtils.isEmpty(googleID)) {
                            isNew = true;
                        }
                    }

                    String idUser = "";
                    if (loginType == LoginType.FACEBOOK.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("facebook"))) {

                        idUser = object.optString("facebook");
                    } else if (loginType == LoginType.GOOGLE_PLUS.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("google"))) {

                        idUser = object.optString("google");
                    }
                    if (object != null && (idUser.equals("") || idUser.equals("null"))) {
                        LoginWrapper userInfo = mapperRegisterData(jsonObject.optJSONObject("data"));
                        sendOTPFacebook(lang, email, userInfo.id, loginType, id, name, urlAvatar, mProgressDialog);
                    } else if (object == null || id.equals(idUser)) {
                        checkEmailRegisterFacebookOrPlus(lang, loginType, id, email, name, urlAvatar, isNull, mProgressDialog, isNew);
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(LoginActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public void checkLinkedAccountGoogle(final String lang, final String loginType,
                                           final String id,
                                           final String email,
                                           final String name,
                                           final String urlAvatar, final boolean isNull) {
        final ProgressDialog mProgressDialog = new ProgressDialog(LoginActivity.this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, LoginType.KFC.getValue() , id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    boolean isNew = false;
                    if (object != null) {
                        String facebookID = object.optString("facebook");
                        String googleID = object.optString("google");

                        if (TextUtils.isEmpty(facebookID) && TextUtils.isEmpty(googleID)) {
                            isNew = true;
                        }
                    }

                    String idUser = "";
                    if (loginType == LoginType.FACEBOOK.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("facebook"))) {

                        idUser = object.optString("facebook");
                    } else if (loginType == LoginType.GOOGLE_PLUS.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("google"))) {

                        idUser = object.optString("google");
                    }
                    if (object != null && (idUser.equals("") || idUser.equals("null"))) {
                        LoginWrapper userInfo = mapperRegisterData(jsonObject.optJSONObject("data"));
                        sendOTPGoogle(lang, email, userInfo.id, loginType, id, name, urlAvatar, mProgressDialog);
                    } else if (object == null || id.equals(idUser)) {
                        checkEmailRegisterFacebookOrPlus(lang, loginType, id, email, name, urlAvatar, isNull, mProgressDialog, isNew);
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(LoginActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public void sendOTPFacebook(final String lang, final String email, final String id_user, final String loginType,
                        final String id,
                        final String name,
                        final String urlAvatar, final ProgressDialog mProgressDialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(LoginActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().sendOTP(lang, email, id_user);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        DialogFactory.showDialogLinkAccountFacebook(LoginActivity.this, email, new DialogFactory.ListenerLinkAccount() {
                            @Override
                            public void clickAgree(String email, Dialog dialog) {
                                checkOTP(lang, email, id_user, loginType, id, name, urlAvatar, mProgressDialog, dialog);
                            }

                            @Override
                            public void close() {
                                mProgressDialog.dismiss();
                            }
                        });
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(LoginActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public void sendOTPGoogle(final String lang, final String email, final String id_user, final String loginType,
                                final String id,
                                final String name,
                                final String urlAvatar, final ProgressDialog mProgressDialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(LoginActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().sendOTP(lang, email, id_user);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        DialogFactory.showDialogLinkAccountGoogle(LoginActivity.this, email, new DialogFactory.ListenerLinkAccount() {
                            @Override
                            public void clickAgree(String email, Dialog dialog) {
                                checkOTP(lang, email, id_user, loginType, id, name, urlAvatar, mProgressDialog, dialog);
                            }

                            @Override
                            public void close() {
                                mProgressDialog.dismiss();
                            }
                        });
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(LoginActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public void checkOTP(final String lang, final String email, final String id_user, final String loginType,
                         final String id,
                         final String name,
                         final String urlAvatar, final ProgressDialog mProgressDialog, final Dialog dialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(LoginActivity.this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().checkOTP(lang, email, id_user, id, loginType);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        dialog.dismiss();
                        loginByFacebookOrGooglePlus(loginType, id, email, name, "", urlAvatar, mProgressDialog);
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    dialog.dismiss();
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(LoginActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public LoginWrapper mapperRegisterData(JSONObject object) throws JSONException {
        LoginWrapper data = new LoginWrapper();
        data.id = object.optString("id");
        data.password = object.optString("password");
        data.name = object.optString("name");
        data.gender = object.optString("gender");
        data.email = object.optString("email");
        data.phone = object.optString("phone");
        data.dt_create = object.optString("dt_create");
        data.usertype = object.optString("usertype");
        data.bl_active = object.optString("bl_active");
        return data;
    }



    public void checkEmailRegisterFacebookOrPlus(String lang, final String loginType,
                                                 final String id,
                                                 final String email,
                                                 final String name,final String urlAvatar, boolean isNull ,final ProgressDialog mProgressDialog, final boolean isNew) {
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, loginType , id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    if (object == null) {
                        mProgressDialog.dismiss();
                        Intent i = getIntent();
                        Bundle b = new Bundle();
                        b.putString("type", loginType);
                        b.putString("id", id);
                        b.putString("email", email);
                        b.putString("urlAvatar", urlAvatar);
                        b.putString("name", name);
                        i.putExtras(b);
                        setResult(RequestResultCodeType.RESULT_SIGN_UP.getValue(), i);
                        finish();
                    } else {
                        if (!isNew) {
                            loginByFacebookOrGooglePlus(loginType, id, email, name, "", urlAvatar, mProgressDialog);
                        } else {
                            if (mProgressDialog != null) {
                                mProgressDialog.dismiss();
                            }
                            id_check = id;
                            loginType_check = loginType;
                            email_check = email;
                            name_check = name;
                            urlAvatar_check = urlAvatar;
                            Intent intent = new Intent(LoginActivity.this, RegisterUserNameActivity.class);
                            intent.putExtra(RegisterSexActivity.USERNAME, name);
                            intent.putExtra(RegisterSexActivity.LOGIN, true);
                            startActivityForResult(intent, RegisterSexActivity.REQUESTFIST);
                        }
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(LoginActivity.this, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llKFCLogin:
                String user = edtKFCUserName.getText().toString().trim();
                String pwd = edtKFCPassword.getText().toString().trim();
                if (!user.isEmpty()) {
                    if (!pwd.isEmpty()) {
                        login(user, pwd);
                    } else {
                        edtKFCPassword.requestFocus();
                        edtKFCPassword.clearComposingText();
                        edtKFCPassword.setError(getString(R.string.error_message_login_password_kfc));
                    }
                } else {
                    edtKFCUserName.requestFocus();
                    edtKFCUserName.clearComposingText();
                    edtKFCUserName.setError(getString(R.string.error_message_login_username_kfc));
                }
                break;

            case R.id.llFacebookLogin:
                LoginManager.getInstance().logOut();
                btnLoginFacebook.performClick();
                break;

            case R.id.imgClose:
                finish();
                lg = true;
                getEx = false;
                break;

            case R.id.imgShowPass:
                if (showPass) {
                    edtKFCPassword.setTransformationMethod(new PasswordTransformationMethod());
                    showPass = false;
                } else {
                    edtKFCPassword.setTransformationMethod(null);
                    showPass = true;
                }
                break;

            case R.id.llGooglePlusLogin:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;

            case R.id.llRegisterKFCAccount:
                setResult(RequestResultCodeType.RESULT_OPEN_SIGN_UP.getValue() , new Intent());
                finish();
                break;

            case R.id.tvForgotPassword:
                DialogForgotPassword dialogDialogForgotPassword = new DialogForgotPassword(LoginActivity.this);
                dialogDialogForgotPassword.show();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(Tag.LOG.getValue(), "onConnectionFailed():" + connectionResult.toString());
        Toast.makeText(this, getString(R.string.connect_google_plus_fail), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RegisterSexActivity.REQUESTFIST) {

            if (data != null) {
                String sname = data.getStringExtra(RegisterSexActivity.USERNAME);
                if (!TextUtils.isEmpty(sname)) {
                    name_check = sname;
                }
            }
            loginByFacebookOrGooglePlus(loginType_check, id_check, email_check, name_check, "", urlAvatar_check, null);

        } else
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                Log.d(Tag.LOG.getValue(), "Google Plus -> onActivityResult:" + result.isSuccess());
                if (result.isSuccess()) {
                    //save device token
                    String keydevice = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    saveDeviceToken(device_token, keydevice, "android");

                    // Signed in successfully, show authenticated UI.
                    final GoogleSignInAccount acct = result.getSignInAccount();

                    String info = "Id: " + acct.getId() + ", Email: " + acct.getEmail() + ", DisplayName: " + acct.getDisplayName() + ", PhotoUrl: " + (acct.getPhotoUrl() == null ? "" : acct.getPhotoUrl());
                    Log.d(Tag.LOG.getValue(), "INFO: " + info);
                    if (TextUtils.isEmpty(acct.getEmail())) {
                        DialogFactory.showDialogInputEmail(LoginActivity.this, new DialogFactory.ListenerLinkAccount() {
                            @Override
                            public void clickAgree(String email, Dialog dialog) {
                                checkLinkedAccountGoogle(currentLanguageCode, LoginType.GOOGLE_PLUS.getValue(), acct.getId(), email, acct.getDisplayName(), (acct.getPhotoUrl() == null ? null : acct.getPhotoUrl().toString()), true);
                            }

                            @Override
                            public void close() {
                            }
                        });
                        return;
                    }

                    Toast.makeText(LoginActivity.this, getString(R.string.login_google_plus_successful), Toast.LENGTH_SHORT).show();
                    checkLinkedAccountGoogle(currentLanguageCode, LoginType.GOOGLE_PLUS.getValue(), acct.getId(), acct.getEmail(), acct.getDisplayName(), (acct.getPhotoUrl() == null ? null : acct.getPhotoUrl().toString()), false);
                } else {
                    // Signed out, show unauthenticated UI.
                    Toast.makeText(this, getString(R.string.login_google_plus_fail), Toast.LENGTH_SHORT).show();
                }
            } else {
                //Facebook
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
    }

    private void processNextStepAfterLoginSuccess(LoginWrapper userInfo) {
        Intent i = getIntent();
        Bundle b = new Bundle();

        if (loginArea == null) {
            b.putSerializable("user_info", userInfo);
            i.putExtras(b);
            setResult(RequestResultCodeType.REQUEST_LOGIN.getValue(), i);
            getEx = true;
            finish();
            return;
        }

        if (loginArea.equalsIgnoreCase(LoginAreaType.LOGIN_IN_SHOPPING_CART.toString())) {
            b.putSerializable("user_info", userInfo);
            i.putExtras(b);
            setResult(RequestResultCodeType.RESULT_CODE_LOGIN_IN_SHOPPING_CART.getValue(), i);
        } else if (loginArea.equalsIgnoreCase(LoginAreaType.LOGIN_ON_NAVIGATION.toString())) {
            b.putSerializable("user_info", userInfo);
            i.putExtras(b);
            setResult(RequestResultCodeType.RESULT_CODE_LOGIN_ON_NAVIGATION.getValue(), i);
        } else if (loginArea.equalsIgnoreCase(LoginAreaType.LOGIN_IN_PROMOTION.toString())) {
            b.putSerializable("user_info", userInfo);
            i.putExtras(b);
            setResult(RequestResultCodeType.RESULT_LOGIN_IN_PROMOTION.getValue(), i);
        }

        finish();
    }

    private class TextChangedListener implements TextWatcher {
        EditText editText;
        private Context mContext;

        public TextChangedListener(Context context, EditText edt) {
            super();
            this.mContext = context;
            this.editText = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            editText.setError(null, null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    private void saveDeviceToken(String token, String keydevice,
                                 String platform) {

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            return;
        }

        Call<StoreDeviceTokenDataWrapper> call1 = ApiClient.getJsonClient().saveDeviceToken(token,
                keydevice, platform);
        call1.enqueue(new Callback<StoreDeviceTokenDataWrapper>() {

            @Override
            public void onFailure(Call<StoreDeviceTokenDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

            }

            @Override
            public void onResponse(Call<StoreDeviceTokenDataWrapper> arg0, Response<StoreDeviceTokenDataWrapper> arg1) {
                StoreDeviceTokenDataWrapper obj = arg1.body();
            }
        });
    }
}
