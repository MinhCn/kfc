package kfc.vietnam.main_screen.ui;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.regex.Pattern;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class RegisterUserNameActivity extends BaseActivity {
    private EditText edtKFCUserName, edtPhoneNumber, edtBirthday;
    String sName;
    boolean isLogin = false;
    private TextView mTvTitle;
    String userName = "";
    private Calendar mCalendar;
    private String day_birth, month_birth, year_birth;
    private String currentLanguageCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent() != null){
            isLogin = getIntent().getBooleanExtra(RegisterSexActivity.LOGIN,false);
            userName = getIntent().getStringExtra(RegisterSexActivity.USERNAME);
        }
        setContentView(R.layout.layout_register_activity_2);
        init();
    }
    private void init() {
        edtKFCUserName = (EditText) findViewById(R.id.edtKFCUserName);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);
        edtBirthday = (EditText) findViewById(R.id.edtBirthday);
        edtKFCUserName.setText(userName);
        mTvTitle = (TextView) findViewById(R.id.tvTitle);
        if(isLogin){
            mTvTitle.setText(R.string.login_cap);
        }else {
            mTvTitle.setText(R.string.register_cap);
        }
        Log.e("user_name", userName);
        findViewById(R.id.llContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtKFCUserName.getText().toString().trim();
                String phone = edtPhoneNumber.getText().toString().trim();
                String birthday = edtBirthday.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    edtKFCUserName.setError(getString(R.string.StepOneFragment_001));
                } else if (TextUtils.isEmpty(phone)) {
                    edtPhoneNumber.setError(getString(R.string.StepOneFragment_003));
                } else if (TextUtils.isEmpty(birthday)) {
                    edtBirthday.setError(getString(R.string.StepOneFragment_010));
                } else if (!isValidatePhoneNumber(phone)) {
                    edtPhoneNumber.setError(getString(R.string.StepOneFragment_003));
                } else {
                    Intent intent = new Intent(RegisterUserNameActivity.this,RegisterSexActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    intent.putExtra(RegisterSexActivity.USERNAME,name);
                    intent.putExtra(RegisterSexActivity.PHONE,phone);
                    intent.putExtra(RegisterSexActivity.DAY_BIRTH,day_birth);
                    intent.putExtra(RegisterSexActivity.MONTH_BIRTH,month_birth);
                    intent.putExtra(RegisterSexActivity.YEAR_BIRTH,year_birth);
                    intent.putExtra(RegisterSexActivity.LOGIN,isLogin);
                    startActivity(intent);
                    finish();
                }
            }
        });
        findViewById(R.id.llBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        SharedPreferences preferencesLanguage = this.getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), this.MODE_PRIVATE);
        currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");

        mCalendar = Calendar.getInstance();
        edtBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterUserNameActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String day_temp = dayOfMonth + "";
                        if (day_temp.length() == 1){
                            day_temp = "0" + dayOfMonth + "";
                        }
                        if (currentLanguageCode.equals("vn")){
                            edtBirthday.setText(day_temp + "-" + (month + 1) + "-"  + year);
                        } else {
                            edtBirthday.setText(year + "-" + (month + 1) + "-" + day_temp);
                        }
                        day_birth = dayOfMonth + "";
                        month_birth = (month + 1) + "";
                        year_birth = year + "";

                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }

        });
    }

    public boolean isValidatePhoneNumber(String phoneNumber) {
        String regexStr = "^[+]?[0-9]{10,13}$";
        return Pattern.matches(regexStr, phoneNumber);
    }
}
