package kfc.vietnam.food_menu_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.adapter.WardAdapter;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.AddAddressDataWrapper;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.AddressListDataWrapper;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.NonScrollListView;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.AddressAdapter002;
import kfc.vietnam.food_menu_screen.adapter.DistrictAdapter;
import kfc.vietnam.food_menu_screen.adapter.ProvinceAdapter;
import kfc.vietnam.food_menu_screen.ui.DeliveryTimeActivity;
import kfc.vietnam.food_menu_screen.ui.OrderLimitActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class ChangeDeliveryAddressFragment extends Fragment implements View.OnClickListener {
    private String currentLanguageCode = "";
    private Context ctx;

    private LinearLayout llMore, llAddress;
    private RelativeLayout rlMoreDetail;
    private TextView tvMinPrice, tvMinTime, tvMore;
    private boolean isNewAddress = true;
    private EditText edtFirstNameAndLastName, edtAddress, edtPhoneNumber, edtEmail;
    private Spinner spnProvince, spnDistrict, spnWard;
    private NonScrollListView lvwAddress;
    private AddressAdapter002 adapterAddress;
    private ArrayList<AddressDataWrapper> lstAddress;
    private ImageView imgMoreAddress;
    private MainActivity activity;
    private RelativeLayout rlDeliverToThisAddress;
    private DistrictAdapter districtAdapter;
    private WardAdapter wardAdapter;
    private String provinceID;
    private String districtID;
    private String wardID;
    public ArrayList<ProvinceWrapper> lstProvince;
    public ArrayList<DistrictWrapper> lstDistrict;
    public ArrayList<DistrictWrapper.WardWapper> lstWard;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
        this.lstProvince = activity.lstProvince;
        this.lstDistrict = lstProvince.get(0).district;
        this.lstWard = lstDistrict.get(0).lstWard;
        this.provinceID = lstProvince.get(0).id;
        this.districtID = lstDistrict.get(0).id;
        this.wardID = lstWard.get(0).id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_change_delivery_address_fragment, container, false);

        llAddress = (LinearLayout) root.findViewById(R.id.llAddress);

        LinearLayout llMoreAddress = (LinearLayout) root.findViewById(R.id.llMoreAddress);
        llMoreAddress.setOnClickListener(this);

        llMore = (LinearLayout) root.findViewById(R.id.llMore);
        imgMoreAddress = (ImageView) root.findViewById(R.id.imgMoreAddress);

        spnProvince = (Spinner) root.findViewById(R.id.spnProvince);
        ProvinceAdapter provinceAdapter = new ProvinceAdapter(ctx, R.layout.custom_item_province_spinner, lstProvince);
        spnProvince.setAdapter(provinceAdapter);
        spnProvince.setSelection(lstProvince.indexOf(activity.currentProvinceSelected));
        spnProvince.setOnItemSelectedListener(onProvinceSelected);

        spnDistrict = (Spinner) root.findViewById(R.id.spnDistrict);
        districtAdapter = new DistrictAdapter(ctx, R.layout.custom_item_district_spinner, lstDistrict);
        spnDistrict.setAdapter(districtAdapter);
        spnDistrict.setOnItemSelectedListener(onDistrictSelected);

        spnWard = (Spinner) root.findViewById(R.id.spnWard);
        wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
        spnWard.setAdapter(wardAdapter);
        spnWard.setOnItemSelectedListener(onWardSelected);

        edtFirstNameAndLastName = (EditText) root.findViewById(R.id.edtFirstNameAndLastName);
        edtAddress = (EditText) root.findViewById(R.id.edtAddress);
        edtPhoneNumber = (EditText) root.findViewById(R.id.edtPhoneNumber);
        edtEmail = (EditText) root.findViewById(R.id.edtEmail);
        edtFirstNameAndLastName.addTextChangedListener(new TextChangedListener(ctx, edtFirstNameAndLastName));
        edtAddress.addTextChangedListener(new TextChangedListener(ctx, edtAddress));
        edtPhoneNumber.addTextChangedListener(new TextChangedListener(ctx, edtPhoneNumber));
        edtEmail.addTextChangedListener(new TextChangedListener(ctx, edtEmail));
        edtEmail.setImeOptions(EditorInfo.IME_ACTION_DONE);

        rlDeliverToThisAddress = (RelativeLayout) root.findViewById(R.id.rlDeliverToThisAddress);
        rlDeliverToThisAddress.setOnClickListener(this);

        lstAddress = new ArrayList<>();
        lvwAddress = (NonScrollListView) root.findViewById(R.id.lvwAddress);
        adapterAddress = new AddressAdapter002(ctx, R.layout.custom_item_address, lstAddress, currentLanguageCode, lvwAddress);
        lvwAddress.setAdapter(adapterAddress);
        lvwAddress.setOnItemClickListener(adapterAddress);

        rlMoreDetail = (RelativeLayout) root.findViewById(R.id.rlMoreDetail);
        tvMinPrice = (TextView) root.findViewById(R.id.tvMinPrice);
        tvMinTime = (TextView) root.findViewById(R.id.tvMinTime);
        tvMore = (TextView) root.findViewById(R.id.tvMore);
        tvMore.setOnClickListener(this);

        if (activity.userInfo != null)
            getShipAddressList(activity.userInfo.id);
        return root;
    }
    private AdapterView.OnItemSelectedListener onProvinceSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            provinceID = lstProvince.get(i).id;
            lstDistrict = lstProvince.get(i).district;
            districtID = lstDistrict.get(0).id;
            lstWard = lstDistrict.get(0).lstWard;;
            wardID = lstWard.get(0).id;
            if(getActivity()!= null){
                districtAdapter = new DistrictAdapter(getActivity(), R.layout.custom_item_district_spinner, lstDistrict);
                spnDistrict.setAdapter(districtAdapter);
                spnDistrict.setOnItemSelectedListener(onDistrictSelected);
                wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                spnWard.setAdapter(wardAdapter);
                spnWard.setOnItemSelectedListener(onWardSelected);
            }



        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private AdapterView.OnItemSelectedListener onDistrictSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            districtID = lstDistrict.get(i).id;
            lstWard = lstDistrict.get(i).lstWard;
            wardID = lstWard.get(0).id;
            if(getActivity()!= null){
                wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                spnWard.setAdapter(wardAdapter);
                spnWard.setOnItemSelectedListener(onWardSelected);
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private AdapterView.OnItemSelectedListener onWardSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            wardID = lstWard.get(i).id;
            if (lstWard.get(i).price.priceMax == null){
                rlMoreDetail.setVisibility(View.GONE);
            } else {
                rlMoreDetail.setVisibility(View.VISIBLE);
                DecimalFormat myFormatter = new DecimalFormat("###,###");
                String output = myFormatter.format(lstWard.get(i).price.priceMax);
                tvMinPrice.setText(output);
                tvMinTime.setText(lstWard.get(i).price.timeMax);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(ChangeDeliveryAddressFragment.class);
                }
            }
        });

        return anim;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {

            case R.id.tvMore:
                Intent intents = new Intent(ctx, OrderLimitActivity.class);
                intents.putExtra("data", MainActivity.sProvineId);
                startActivity(intents);
                break;

            case R.id.llMoreAddress:
                switchAddress();
                break;

            case R.id.rlDeliverToThisAddress:
                String name = edtFirstNameAndLastName.getText().toString().trim();
                String address = edtAddress.getText().toString().trim();
                String phoneNumber = edtPhoneNumber.getText().toString().trim();
                String email = edtEmail.getText().toString().trim();

                if (!name.isEmpty()) {
                    if (!address.isEmpty()) {
                        if (activity.isValidatePhoneNumber(phoneNumber)) {
                            if (!email.isEmpty()) {
                                if (activity.patternMail(email)) {
                                    DistrictWrapper.WardWapper ward = lstWard.get(spnWard.getSelectedItemPosition());
                                    if (activity.totalPrice == -1d || ward.price.priceMax == null || activity.totalPrice >= ward.price.priceMax) {
                                        addShipAddress(activity.userInfo.id, name, provinceID, districtID,wardID, address, phoneNumber, email);
                                    } else {
                                        DecimalFormat myFormatter = new DecimalFormat("###,###");
                                        String output = myFormatter.format(ward.price.priceMax);
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                                        alertDialog.setMessage(activity.getString(R.string.order_limitation_alert, output, ward.name));

                                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                arg0.dismiss();
                                            }
                                        });

                                        AlertDialog dialog = alertDialog.create();
                                        dialog.show();
                                    }
                                } else {
                                    edtEmail.requestFocus();
                                    edtEmail.setError(getString(R.string.StepOneFragment_006));
                                    rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                }
                            } else {
                                edtEmail.requestFocus();
                                edtEmail.clearComposingText();
                                edtEmail.setError(getString(R.string.StepOneFragment_004));
                                rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                            }
                        } else {
                            edtPhoneNumber.requestFocus();
                            edtPhoneNumber.clearComposingText();
                            edtPhoneNumber.setError(getString(R.string.StepOneFragment_003));
                            rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                        }
                    } else {
                        edtAddress.requestFocus();
                        edtAddress.clearComposingText();
                        edtAddress.setError(getString(R.string.StepOneFragment_002));
                        rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                    }
                } else {
                    edtFirstNameAndLastName.requestFocus();
                    edtFirstNameAndLastName.clearComposingText();
                    edtFirstNameAndLastName.setError(getString(R.string.StepOneFragment_001));
                    rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                }
                break;
        }
    }

    private void showAddressList() {
        llAddress.setVisibility(View.VISIBLE);
        llMore.setVisibility(View.GONE);
        imgMoreAddress.setImageResource(R.drawable.ic_add);
    }

    private void switchAddress() {
        if (!isNewAddress) {
            llAddress.setVisibility(View.GONE);
            llMore.setVisibility(View.VISIBLE);
            imgMoreAddress.setImageResource(R.drawable.ic_minus);
        } else
            showAddressList();
        isNewAddress = !isNewAddress;
    }

    private void getShipAddressList(String userID) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity)ctx).showNoInternetFragment();
            return;
        }

        Call<AddressListDataWrapper> call1 = ApiClient.getJsonClient().getShipAddressList(userID, new String(Hex.encodeHex(DigestUtils.md5(userID))));
        call1.enqueue(new Callback<AddressListDataWrapper>() {

            @Override
            public void onFailure(Call<AddressListDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<AddressListDataWrapper> arg0, Response<AddressListDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    lstAddress.clear();
                    lstAddress.addAll(arg1.body().data);
                    adapterAddress.notifyDataSetChanged();

                    showAddressList();
                }
            }
        });
    }

    private void addShipAddress(String userID, String fullName, String city, String district, String wardID,String address, String phone, String email) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity)ctx).showNoInternetFragment();
            return;
        }

        Call<AddAddressDataWrapper> call1 = ApiClient.getJsonClient().addShipAddress(userID, new String(Hex.encodeHex(DigestUtils.md5(userID))), fullName, city, district, wardID,address, phone, email);
        call1.enqueue(new Callback<AddAddressDataWrapper>() {

            @Override
            public void onFailure(Call<AddAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<AddAddressDataWrapper> arg0, Response<AddAddressDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    AddAddressDataWrapper obj = arg1.body();
                    if (obj.result) {
                        //add address to list address

                        //Hiện tại tạm thời gọi api address để load lại
                        if (activity.userInfo != null)
                            getShipAddressList(activity.userInfo.id);
                    }
                }
            }
        });
    }

    private class TextChangedListener implements TextWatcher {
        private Context mContext;
        EditText editText;

        public TextChangedListener(Context context, EditText edt) {
            super();
            this.mContext = context;
            this.editText = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            editText.setError(null, null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

}
