package kfc.vietnam.food_menu_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.PaymentType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.IpDataWrapper;
import kfc.vietnam.common.object.OrderDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.QueryURL_Utils;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class PaymentFragment extends Fragment {
    private Context ctx;
    private MainActivity activity;
    private WebView wvPayment;

    private SharedPreferences preferencesShoppingCard;
    private SharedPreferences preferencesDeliverAddress;

    private String mOrderID = "";
    private String totalPrice = "0";
    private String mPoint = "0";


    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;

        Bundle b = getArguments();
        mOrderID = b.getString("order_id", "");
        totalPrice = b.getString("total_price", "0");
        mPoint = b.getString("point", "0");

        preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
        preferencesDeliverAddress = ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE);

    }

    private boolean paymentSuccess = false;
    private String paymentLogContent = "";
    private String paymentParams = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_payment, container, false);

        wvPayment = (WebView) root.findViewById(R.id.wvPayment);
        wvPayment.getSettings().setJavaScriptEnabled(true);
        wvPayment.getSettings().setSupportZoom(true);
        wvPayment.getSettings().setBuiltInZoomControls(false);
        wvPayment.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d("anhduc", "shouldOverrideUrlLoading -> URL: " + url);
                activity.tvPaymentURL.setText(url);
                if (url.contains("kfcvietnam")) {
                    wvPayment.loadUrl(url);
                    return true;
                } else if (url.contains("canceled")) {
                    activity.backToPreviousScreen();
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                Log.d("anhduc", "onPageFinished -> URL: " + url);
                if (url.contains("kfcvietnam")) {
                    // Check html content
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        view.evaluateJavascript(
                                "(function() { return ('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>'); })();",
                                new ValueCallback<String>() {
                                    @Override
                                    public void onReceiveValue(String html) {
                                        Log.d("HTML", html);
                                        // code here
                                        String validateCode1 = "name=\\\"decision\\\" id=\\\"decision\\\" value=\\\"ACCEPT\\\"";
                                        String validateCode2 = "name=\\\"reason_code\\\" id=\\\"reason_code\\\" value=\\\"100\\\"";
                                        if (url.contains("success") && html.contains(validateCode1) && html.contains(validateCode2)) {
                                            paymentSuccess = true;
                                            paymentLogContent = paymentParams + "&" + validateCode1 + "&" + validateCode2;
                                            if (activity.userInfo != null)
                                                payment(activity.userInfo.id, activity.currentLanguageCode, activity.currentProvinceSelected.id, PaymentType.PAYMENT_ONLINE.getValue(), "", "", mOrderID, paymentLogContent);
                                            else
                                                payment("", activity.currentLanguageCode, activity.currentProvinceSelected.id, PaymentType.PAYMENT_ONLINE.getValue(), "", "", mOrderID, paymentLogContent);

                                        }
                                        if (!paymentSuccess) {
                                            String message = "id=\\\"message\\\" value=\\\"";
                                            if(html.contains(message)){
                                                int index = html.indexOf(message);
                                                String errorMsg = "";
                                                if(index > 0 && html.indexOf("\\\"", index  + message.length()) < html.length()){
                                                    errorMsg = html.substring(index + message.length(), html.indexOf("\\\"", index  + message.length()));
                                                }
                                                Bundle b = new Bundle();
                                                b.putString("order_id", mOrderID);
                                                b.putString("errorMsg", errorMsg);
                                                OrderStatusFragment f = new OrderStatusFragment();
                                                f.setArguments(b);
                                                activity.changeFragment(f);
                                            }
                                        }
                                    }
                                });
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), R.string.message_require_android_kitkat, Toast.LENGTH_LONG);
                        activity.backToPreviousScreen();
                    }
                }
                super.onPageFinished(view, url);
            }
        });
        getCertificate();

        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(PaymentWithATMFragment.class);
                }
            }
        });

        return anim;
    }

    private boolean processEndPayment() {
        activity.tvAmountShoppingCart.setText("0");
        return preferencesShoppingCard.edit().remove(SharedPreferenceKeyType.SHOPPING_CART.toString()).commit() && preferencesDeliverAddress.edit().remove(SharedPreferenceKeyType.DELIVER_ADDRESS.toString()).commit();
    }

    private void getCertificate() {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        String[] nameArray = activity.userInfo.name.split(" ");
        String firstName = activity.userInfo.name;
        String lastName = activity.userInfo.name;
        if (nameArray.length > 1) {
            // Name: Kiet Nguyen
            firstName = nameArray[0];
            lastName = nameArray[nameArray.length - 1];
        } else {
            firstName = "Mr/Mrs/Ms";
            lastName = activity.userInfo.name;
        }

        final Map<String, String> params = new HashMap<String, String>();
        params.put("signed_field_names", "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_address_line1,bill_to_address_city,bill_to_address_postal_code,bill_to_email,bill_to_phone,bill_to_forename,bill_to_address_country,bill_to_surname,ship_to_address_line1,ship_to_address_city,ship_to_address_postal_code,ship_to_address_country,ship_to_email,ship_to_phone,ship_to_forename,ship_to_surname,customer_ip_address");
        params.put("access_key", activity.conf.cyber.accessKey);
        params.put("profile_id", activity.conf.cyber.profileID);
        params.put("transaction_uuid", UUID.randomUUID().toString());
        params.put("unsigned_field_names", "");
        params.put("signed_date_time", activity.getFormatCurrentDateTime());
        params.put("locale", "vi-VN");
        params.put("transaction_type", "sale");
        params.put("reference_number", mOrderID);
        params.put("amount", totalPrice);
        params.put("currency", "VND");
        params.put("customer_ip_address", activity.customerIp);

        params.put("bill_to_address_line1", activity.addressToShip.address);
        params.put("bill_to_address_city", activity.addressToShip.nameCity);
        params.put("bill_to_address_postal_code", "70000");
        params.put("bill_to_address_country", "VN");
        params.put("bill_to_email", activity.userInfo.email);
        params.put("bill_to_phone", activity.userInfo.phone);

        params.put("bill_to_forename", firstName);
        params.put("bill_to_surname", lastName);

        params.put("ship_to_address_line1", activity.addressToShip.address);
        params.put("ship_to_address_city", activity.addressToShip.nameCity);
        params.put("ship_to_address_postal_code", "70000");
        params.put("ship_to_address_country", "VN");
        params.put("ship_to_email", activity.addressToShip.email);
        params.put("ship_to_phone", activity.addressToShip.phone);

        nameArray = activity.addressToShip.name.split(" ");
        firstName = activity.addressToShip.name;
        lastName = activity.addressToShip.name;
        if (nameArray.length > 1) {
            // Name: Kiet Nguyen
            firstName = nameArray[0];
            lastName = nameArray[nameArray.length - 1];
        } else {
            firstName = "Mr/Mrs/Ms";
            lastName = activity.addressToShip.name;
        }
        params.put("ship_to_forename", firstName);
        params.put("ship_to_surname", lastName);

        final Call<ResponseBody> call = ApiClient.getStringClient().getCertificate(params);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ResponseBody> arg0, Response<ResponseBody> arg1) {
                try {
                    mProgressDialog.dismiss();

                    String certificate = arg1.body().string();
                    System.out.println("Certificate: " + certificate);

                    params.put("signature", certificate);
                    String query = QueryURL_Utils.urlEncodeUTF8(params);
                    paymentParams = query;
                    System.out.println("Params: " + query);

                    wvPayment.postUrl(activity.conf.cyber.linkPay, query.getBytes("utf-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void payment(String user,
                         String lang,
                         String city,
                         String paymentType,
                         String requestId,
                         String encryptedPaymentData,
                         final String orderID, String paymentLog) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<OrderDataWrapper> call1 = ApiClient.getJsonClient().payment(user,
                lang,
                city,
                paymentType,
                requestId,
                encryptedPaymentData,
                orderID, paymentLog);
        call1.enqueue(new Callback<OrderDataWrapper>() {

            @Override
            public void onFailure(Call<OrderDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();

                Toast.makeText(ctx, getString(R.string.StepThreeFragment_014), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<OrderDataWrapper> arg0, Response<OrderDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    OrderDataWrapper obj = arg1.body();
                    if (obj.result) {
                        if (processEndPayment()) {
                            Bundle b = new Bundle();
                            b.putString("order_id", mOrderID);
                            b.putString("point", mPoint);

                            OrderStatusFragment f = new OrderStatusFragment();
                            f.setArguments(b);
                            activity.changeFragment(f);
                        }
                    }
                }
            }
        });
    }
}
