package kfc.vietnam.food_menu_screen.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.ChangeDishPositionType;
import kfc.vietnam.common.utility.VerticalSpaceItemDecoration;
import kfc.vietnam.food_menu_screen.adapter.ChangeBurgerAdapter;
import kfc.vietnam.food_menu_screen.adapter.ChangeSnacksAdapter;
import kfc.vietnam.food_menu_screen.ui.ChangeDishActivity;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class ExchangeBurgerComFragment extends Fragment implements View.OnClickListener {
    private static final int VERTICAL_ITEM_SPACE = 3;
    private boolean isShow = false;
    private ChangeDishActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle b = getArguments();
        this.isShow = b.getBoolean("show_change");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_exchange_burger_com_fragment, container, false);

        FragmentActivity ctx = (FragmentActivity) container.getContext();
        activity = ((ChangeDishActivity) ctx);

        RecyclerView rcvListSnacks = (RecyclerView) root.findViewById(R.id.rcvListSnacks);
        ChangeBurgerAdapter changeDishAdapter = new ChangeBurgerAdapter(activity.lstBurgerComChange, ctx, rcvListSnacks);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        rcvListSnacks.setLayoutManager(mLayoutManager);
        rcvListSnacks.setItemAnimator(new DefaultItemAnimator());
        rcvListSnacks.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        rcvListSnacks.setAdapter(changeDishAdapter);

        RelativeLayout rlCompleted = (RelativeLayout) root.findViewById(R.id.rlCompleted);
        rlCompleted.setOnClickListener(this);
        RelativeLayout rlChangePage = (RelativeLayout) root.findViewById(R.id.rlChangePage);
        rlChangePage.setVisibility(isShow ? View.VISIBLE : View.GONE);
        rlChangePage.setOnClickListener(this);

        TextView tvChangeBurgerCom = (TextView) root.findViewById(R.id.tvChangeBurgerCom);
        ImageView imgChangeBurgerCom = (ImageView) root.findViewById(R.id.imgChangeBurgerCom);

        //Luong: snack -> drink -> ga ran -> burger -> snack
        if (activity.lstSnacksAndDessertChange != null && activity.lstSnacksAndDessertChange.size() > 0) {
            tvChangeBurgerCom.setText(getString(R.string.snacks_exchange));
            imgChangeBurgerCom.setImageResource(R.drawable.ic_change_snacks);
        } else if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) {
            tvChangeBurgerCom.setText(getString(R.string.water_exchange));
            imgChangeBurgerCom.setImageResource(R.drawable.ic_change_water);
        } else if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) {
            tvChangeBurgerCom.setText(getString(R.string.garan_exchange ));
            imgChangeBurgerCom.setImageResource(R.drawable.ic_garan);

        } else {
            tvChangeBurgerCom.setText(getString(R.string.change_disk));
        }

        return root;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.rlCompleted:
                activity.backPreviousScreen(true);
                break;

            case R.id.rlChangePage:
                //Luong: snack -> drink -> ga ran -> burger -> snack
                if (activity.lstSnacksAndDessertChange != null && activity.lstSnacksAndDessertChange.size() > 0) {
                    activity.viewPagerChangeDish.setCurrentItem(0);
                }
                else if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) {
                    activity.viewPagerChangeDish.setCurrentItem(0);
                }
                else if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) {
                    activity.viewPagerChangeDish.setCurrentItem(0);
                }
                break;
        }
    }
}
