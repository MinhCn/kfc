package kfc.vietnam.food_menu_screen.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.adapter.WardAdapter;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.LoginType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.LoginDataWrapper;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.ShowTimeDataWrapper;
import kfc.vietnam.common.utility.DialogForgotPassword;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.utility.ResultListener;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.DistrictAdapter;
import kfc.vietnam.food_menu_screen.adapter.ProvinceAdapter;
import kfc.vietnam.food_menu_screen.ui.OrderLimitActivity;
import kfc.vietnam.main_screen.ui.DialogFactory;
import kfc.vietnam.main_screen.ui.MainActivity;
import kfc.vietnam.main_screen.ui.RegisterActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class StepOneFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private String currentLanguageCode = "";
    private Context ctx;

    private LinearLayout llWithoutAccount, llNeedAccount, llLoginWithKFCAccount, llBuyWithoutAccount, llKFCLogin;
    private RadioButton rdbWithoutAccount, rdbNeedAccount;
    private EditText edtKFCUserName, edtKFCPassword, edtFirstNameAndLastName, edtAddress, edtPhoneNumber, edtEmail;
    private Spinner spnProvince;
    private Spinner spnDistrict;
    private Spinner spnWard;
    private ImageView imgShowPassCart;
    private RelativeLayout rlMoreDetail;
    private TextView tvMinPrice, tvMinTime, tvMore;

    private CallbackManager callbackManager;
    private LoginButton btnLoginFacebook;


    boolean showPass = false;

    //    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private int RC_SIGN_IN = 123;

    private MainActivity activity;

    private RelativeLayout rlDeliverToThisAddress;

    private CheckBox chkRememberAccount;
    private TextView mTvNotice;
    private View llNotice;

    private DistrictAdapter districtAdapter;
    private WardAdapter wardAdapter;
    private String provinceID, provinceName;
    private String districtID, districtName;
    private String wardID, nameWar;
    public ArrayList<ProvinceWrapper> lstProvince = new ArrayList<>();
    public ArrayList<DistrictWrapper> lstDistrict = new ArrayList<>();
    public ArrayList<DistrictWrapper.WardWapper> lstWard = new ArrayList<>();

    private String idDefault = "-1";
    ProvinceWrapper defaultProvince;
    DistrictWrapper defaultDistrict;
    DistrictWrapper.WardWapper defaultWard;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        // Initialize the Facebook SDK before executing any other operations,
        FacebookSdk.sdkInitialize(ctx);
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(ctx);

        if (gso == null) {
            // Google plus: Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
        }
//        if (mGoogleApiClient == null) {
//            // Build a GoogleApiClient with access to the Google Sign-In API and the
//            // options specified by gso.
//            mGoogleApiClient = new GoogleApiClient.Builder(ctx)
//                    .enableAutoManage((FragmentActivity) ctx /* FragmentActivity */, this /* OnConnectionFailedListener */)
//                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                    .build();
//        }

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;

        defaultProvince = new ProvinceWrapper(idDefault, activity.getString(R.string.city_new));
        defaultDistrict = new DistrictWrapper(idDefault, activity.getString(R.string.district));
        defaultWard = new DistrictWrapper.WardWapper(idDefault, activity.getString(R.string.ward));

        this.lstProvince.add(defaultProvince);
        this.lstProvince.addAll(activity.lstProvince);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (MainActivity.mGoogleApiClient != null)
            MainActivity.mGoogleApiClient.connect();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_step_one_fragment, container, false);

        init(root);

        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(StepOneFragment.class);
                }
            }
        });

        return anim;
    }

    private void init(View root) {

        rlDeliverToThisAddress = (RelativeLayout) root.findViewById(R.id.rlDeliverToThisAddress);
        rlDeliverToThisAddress.setOnClickListener(this);

        llWithoutAccount = (LinearLayout) root.findViewById(R.id.llWithoutAccount);
        llNeedAccount = (LinearLayout) root.findViewById(R.id.llNeedAccount);
        llWithoutAccount.setOnClickListener(this);
        llNeedAccount.setOnClickListener(this);

        rdbWithoutAccount = (RadioButton) root.findViewById(R.id.rdbWithoutAccount);
        rdbWithoutAccount.setEnabled(false);
        rdbNeedAccount = (RadioButton) root.findViewById(R.id.rdbNeedAccount);
        rdbNeedAccount.setEnabled(false);

        imgShowPassCart = (ImageView) root.findViewById(R.id.imgShowPassCart);
        imgShowPassCart.setOnClickListener(this);

        llLoginWithKFCAccount = (LinearLayout) root.findViewById(R.id.llLoginWithKFCAccount);
        llBuyWithoutAccount = (LinearLayout) root.findViewById(R.id.llBuyWithoutAccount);
        llLoginWithKFCAccount.setOnClickListener(this);
        llBuyWithoutAccount.setOnClickListener(this);

        TextView tvRegisterKFCAccount = (TextView) root.findViewById(R.id.tvRegisterKFCAccount);
        tvRegisterKFCAccount.setOnClickListener(this);

        TextView tvForgotPassword = (TextView) root.findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(this);

        llKFCLogin = (LinearLayout) root.findViewById(R.id.llKFCLogin);
        llKFCLogin.setOnClickListener(this);

        LinearLayout llFacebookLogin = (LinearLayout) root.findViewById(R.id.llFacebookLogin);
        llFacebookLogin.setOnClickListener(this);

        LinearLayout llGooglePlusLogin = (LinearLayout) root.findViewById(R.id.llGooglePlusLogin);
        llGooglePlusLogin.setOnClickListener(this);

        edtKFCUserName = (EditText) root.findViewById(R.id.edtKFCUserName);
        edtKFCUserName.addTextChangedListener(new TextChangedListener(ctx, edtKFCUserName));
        edtKFCPassword = (EditText) root.findViewById(R.id.edtKFCPassword);
        edtKFCPassword.addTextChangedListener(new TextChangedListener(ctx, edtKFCPassword));

        btnLoginFacebook = (LoginButton) root.findViewById(R.id.btnLoginFacebook);
        btnLoginFacebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        // If using in a fragment
        btnLoginFacebook.setFragment(this);
        // Callback registration
        btnLoginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(Tag.LOG.getValue(), "LoginButton -> onSuccess() -> " + loginResult.toString());
                Toast.makeText(ctx, getString(R.string.login_facebook_successful), Toast.LENGTH_SHORT).show();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginButton", response.toString());

                                try {
                                    final String id = object.getString("id");
                                    final String name = object.getString("name");
                                    final String email = object.optString("email");
//                                    String birthday = object.getString("birthday");
                                    final String avatar = "https://graph.facebook.com/" + id + "/picture";
                                    if (TextUtils.isEmpty(email)) {
                                        DialogFactory.showDialogInputEmail(getContext(), new DialogFactory.ListenerLinkAccount() {
                                            @Override
                                            public void clickAgree(String email, Dialog dialog) {
                                                checkLinkedAccountFacebook(currentLanguageCode, LoginType.FACEBOOK.getValue(), id, email, name, avatar, true);
                                            }

                                            @Override
                                            public void close() {

                                            }
                                        });
                                        return;
                                    }
                                    checkLinkedAccountFacebook(currentLanguageCode, LoginType.FACEBOOK.getValue(), id, email, name, avatar, false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(Tag.LOG.getValue(), "LoginButton -> onCancel()");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(Tag.LOG.getValue(), "LoginButton -> onError() -> " + exception.getMessage());
                Toast.makeText(ctx, getString(R.string.login_facebook_fail), Toast.LENGTH_SHORT).show();
            }
        });

//        //OR
//        /*
//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        Log.i(Tag.LOG.getValue(), "LoginManager -> onSuccess() -> " + loginResult.toString());
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        Log.i(Tag.LOG.getValue(), "LoginManager -> onCancel()");
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        Log.e(Tag.LOG.getValue(), "LoginManager -> onError() -> " + exception.getMessage());
//                    }
//                });
//        */

        spnProvince = (Spinner) root.findViewById(R.id.spnProvince);
        ProvinceAdapter provinceAdapter = new ProvinceAdapter(ctx, R.layout.custom_item_province_spinner, lstProvince);
        spnProvince.setAdapter(provinceAdapter);
        spnProvince.setSelection(lstProvince.indexOf(activity.currentProvinceSelected));
        spnProvince.setOnItemSelectedListener(onProvinceSelected);

        spnDistrict = (Spinner) root.findViewById(R.id.spnDistrict);
        districtAdapter = new DistrictAdapter(ctx, R.layout.custom_item_district_spinner, lstDistrict);
        spnDistrict.setAdapter(districtAdapter);
        spnDistrict.setOnItemSelectedListener(onDistrictSelected);

        spnWard = (Spinner) root.findViewById(R.id.spnWard);
        wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
        spnWard.setAdapter(wardAdapter);
        spnWard.setOnItemSelectedListener(onWardSelected);

        edtFirstNameAndLastName = (EditText) root.findViewById(R.id.edtFirstNameAndLastName);
        edtAddress = (EditText) root.findViewById(R.id.edtAddress);
        edtPhoneNumber = (EditText) root.findViewById(R.id.edtPhoneNumber);
        edtEmail = (EditText) root.findViewById(R.id.edtEmail);
        edtFirstNameAndLastName.addTextChangedListener(new TextChangedListener(ctx, edtFirstNameAndLastName));
        edtAddress.addTextChangedListener(new TextChangedListener(ctx, edtAddress));
        edtPhoneNumber.addTextChangedListener(new TextChangedListener(ctx, edtPhoneNumber));
        edtEmail.addTextChangedListener(new TextChangedListener(ctx, edtEmail));

        chkRememberAccount = (CheckBox) root.findViewById(R.id.chkRememberAccount);

        mTvNotice = (TextView) root.findViewById(R.id.tvNameReceived);
        llNotice = root.findViewById(R.id.llNotice);
        checkOpenDeliver(activity.currentProvinceSelected.id, "0", null);

        rlMoreDetail = (RelativeLayout) root.findViewById(R.id.rlMoreDetail);
        tvMinPrice = (TextView) root.findViewById(R.id.tvMinPrice);
        tvMinTime = (TextView) root.findViewById(R.id.tvMinTime);
        tvMore = (TextView) root.findViewById(R.id.tvMore);
        tvMore.setOnClickListener(this);

        //Add by @ManhNguyen
        //Load data if back from step 3 (order not login)
        if (activity.isBackFromStep3NotLogin) {
            activity.isBackFromStep3NotLogin = false;
            setDataLayoutBuyWithoutAccount();
        }

        //Load data if change address delivery from step 3 (order not login)
        if (activity.getChangeAddressStep3()) {
            setDataLayoutBuyWithoutAccount();
        }
    }

    public void setDataLayoutBuyWithoutAccount() {
        rdbWithoutAccount.setChecked(true);
        rdbNeedAccount.setChecked(false);
        llLoginWithKFCAccount.setVisibility(View.GONE);
        llBuyWithoutAccount.setVisibility(View.VISIBLE);

        SharedPreferences preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE);
        String serializeString = preferencesShoppingCard.getString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), null);
        try {
            AddressDataWrapper addressDataWrapper = (AddressDataWrapper) ObjectSerializer.deserialize(serializeString);

            if (addressDataWrapper != null) {
                edtFirstNameAndLastName.setText(addressDataWrapper.name);
                edtAddress.setText(addressDataWrapper.address);
                edtPhoneNumber.setText(addressDataWrapper.phone);
                edtEmail.setText(addressDataWrapper.email);

                /**
                 obj.name = name;
                 obj.nameCity = provinceName;
                 obj.nameDistrict = districtName;
                 obj.city = provinceID;
                 obj.district = districtID;
                 obj.address = address;
                 obj.phone = phoneNumber;
                 obj.ward = wardID;
                 obj.nameWard = nameWar;
                 obj.email = email;
                 */

                for (ProvinceWrapper p : lstProvince) {
                    if (p.id.equalsIgnoreCase(addressDataWrapper.city)) {
                        provinceID = p.id;
                        provinceName = p.name;

                        ProvinceAdapter adapter = new ProvinceAdapter(ctx, R.layout.custom_item_province_spinner, lstProvince);
                        spnProvince.setAdapter(adapter);
                        spnProvince.setSelection(lstProvince.indexOf(p));
                        spnProvince.setOnItemSelectedListener(onProvinceSelected);

                        Log.d("indexOf(Province)", lstProvince.indexOf(p) + "");

                        ProvinceWrapper t = (ProvinceWrapper) spnProvince.getSelectedItem();
                        Log.d("spnProvince", t.name);

                        lstDistrict.add(defaultDistrict);
                        lstDistrict.addAll(p.district);

                        break;
                    }
                }

                for (final DistrictWrapper p : lstDistrict) {
                    if (p.id.equalsIgnoreCase(addressDataWrapper.district)) {
                        districtID = p.id;
                        districtName = p.name;

                        DistrictAdapter adapter = new DistrictAdapter(ctx, R.layout.custom_item_district_spinner, lstDistrict);
                        spnDistrict.setAdapter(adapter);

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                spnDistrict.setSelection(lstDistrict.indexOf(p));
                            }
                        }, 150);

                        spnDistrict.setOnItemSelectedListener(onDistrictSelected);

                        Log.d("indexOf(District)", lstDistrict.indexOf(p) + "");

                        DistrictWrapper t = (DistrictWrapper) spnDistrict.getSelectedItem();
                        Log.d("spnDistrict", t.name);

                        lstWard.add(defaultWard);
                        lstWard.addAll(p.lstWard);

                        break;
                    }
                }

                for (final DistrictWrapper.WardWapper p : lstWard) {
                    if (p.id.equalsIgnoreCase(addressDataWrapper.ward)) {
                        wardID = p.id;
                        nameWar = p.name;

                        WardAdapter adapter = new WardAdapter(ctx, R.layout.custom_item_ward_spinner, lstWard);
                        spnWard.setAdapter(adapter);

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                spnWard.setSelection(lstWard.indexOf(p));
                            }
                        }, 300);

                        spnWard.setOnItemSelectedListener(onWardSelected);

                        Log.d("indexOf(Ward)", lstWard.indexOf(p) + "");

                        DistrictWrapper.WardWapper t = (DistrictWrapper.WardWapper) spnWard.getSelectedItem();
                        Log.d("spnWard", t.name);

                        break;
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {

            case R.id.tvMore:
                Intent intents = new Intent(ctx, OrderLimitActivity.class);
                intents.putExtra("data", MainActivity.sProvineId);
                ctx.startActivity(intents);
                break;

            case R.id.rlDeliverToThisAddress:
                if (rdbWithoutAccount.isChecked()) {
                    String name = edtFirstNameAndLastName.getText().toString().trim();
                    String address = edtAddress.getText().toString().trim();
                    String phoneNumber = edtPhoneNumber.getText().toString().trim();
                    String email = edtEmail.getText().toString().trim();

                    if (!name.isEmpty()) {
                        if (!address.isEmpty()) {
                            if (!provinceID.equals("") || !provinceName.equals("")) {
                                if (!districtID.equals("") || !districtName.equals("")) {
                                    if (!wardID.equals("") || !nameWar.equals("")) {
                                        if (activity.isValidatePhoneNumber(phoneNumber)) {
                                            if (!email.isEmpty()) {
                                                if (activity.patternMail(email)) {
                                                    DistrictWrapper.WardWapper ward = lstWard.get(spnWard.getSelectedItemPosition());

                                                    AddressDataWrapper obj = new AddressDataWrapper();
                                                    obj.name = name;
                                                    obj.nameCity = provinceName;
                                                    obj.nameDistrict = districtName;
                                                    obj.city = provinceID;
                                                    obj.district = districtID;
                                                    obj.address = address;
                                                    obj.phone = phoneNumber;
                                                    obj.ward = wardID;
                                                    obj.nameWard = nameWar;
                                                    obj.email = email;

                                                    if (activity.currentProvinceSelected.id.equalsIgnoreCase(obj.city + "")) {
                                                        if (activity.totalPrice == -1d || ward.price.priceMax == null || activity.totalPrice >= ward.price.priceMax) {
                                                            checkOpen(obj);
                                                        } else {
                                                            DecimalFormat myFormatter = new DecimalFormat("###,###");
                                                            String output = myFormatter.format(ward.price.priceMax);
                                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                                                            alertDialog.setMessage(activity.getString(R.string.order_limitation_alert, output, ward.name));

                                                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface arg0, int arg1) {
                                                                    arg0.dismiss();
                                                                }
                                                            });

                                                            AlertDialog dialog = alertDialog.create();
                                                            dialog.show();
                                                        }
                                                    } else {
                                                        new AlertDialog.Builder(ctx)
                                                                .setMessage(R.string.StepTwoFragment_004)
                                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int which) {

                                                                    }
                                                                })
                                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                                .show();
                                                    }
                                                } else {
                                                    edtEmail.requestFocus();
                                                    edtEmail.setError(getString(R.string.StepOneFragment_006));
                                                    rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                                }
                                            } else {
                                                edtEmail.requestFocus();
                                                edtEmail.clearComposingText();
                                                edtEmail.setError(getString(R.string.StepOneFragment_004));
                                                rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                            }
                                        } else {
                                            edtPhoneNumber.requestFocus();
                                            edtPhoneNumber.clearComposingText();
                                            edtPhoneNumber.setError(getString(R.string.StepOneFragment_003));
                                            rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                        }
                                    } else {
                                        Toast.makeText(ctx, getString(R.string.StepOneFragment_013), Toast.LENGTH_SHORT).show();
                                        rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                    }
                                } else {
                                    Toast.makeText(ctx, getString(R.string.StepOneFragment_012), Toast.LENGTH_SHORT).show();
                                    rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                }
                            } else {
                                Toast.makeText(ctx, getString(R.string.StepOneFragment_011), Toast.LENGTH_SHORT).show();
                                rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                            }
                        } else {
                            edtAddress.requestFocus();
                            edtAddress.clearComposingText();
                            edtAddress.setError(getString(R.string.StepOneFragment_002));
                            rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                        }
                    } else {
                        edtFirstNameAndLastName.requestFocus();
                        edtFirstNameAndLastName.clearComposingText();
                        edtFirstNameAndLastName.setError(getString(R.string.StepOneFragment_001));
                        rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                    }
                }
                break;

            case R.id.llWithoutAccount:
                rdbWithoutAccount.setChecked(true);
                rdbNeedAccount.setChecked(false);
                llLoginWithKFCAccount.setVisibility(View.GONE);
                llBuyWithoutAccount.setVisibility(View.VISIBLE);
                break;

            case R.id.llNeedAccount:
                rdbWithoutAccount.setChecked(false);
                rdbNeedAccount.setChecked(true);
                llLoginWithKFCAccount.setVisibility(View.VISIBLE);
                llBuyWithoutAccount.setVisibility(View.GONE);
                break;

            case R.id.imgShowPassCart:
                if (showPass) {
                    edtKFCPassword.setTransformationMethod(new PasswordTransformationMethod());
                    showPass = false;
                } else {
                    edtKFCPassword.setTransformationMethod(null);
                    showPass = true;
                }
                break;

            case R.id.llKFCLogin:
                String user = edtKFCUserName.getText().toString().trim();
                String pwd = edtKFCPassword.getText().toString().trim();
                if (!user.isEmpty()) {
                    if (!pwd.isEmpty()) {
                        login(user, pwd);
                    } else {
                        edtKFCPassword.requestFocus();
                        edtKFCPassword.clearComposingText();
                        edtKFCPassword.setError(getString(R.string.error_message_login_password_kfc));
                        llKFCLogin.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                    }
                } else {
                    edtKFCUserName.requestFocus();
                    edtKFCUserName.clearComposingText();
                    edtKFCUserName.setError(getString(R.string.error_message_login_username_kfc));
                    llKFCLogin.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                }
                break;

            case R.id.llFacebookLogin:
                LoginManager.getInstance().logOut();
                btnLoginFacebook.performClick();
                break;

            case R.id.llGooglePlusLogin:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(MainActivity.mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;

            case R.id.tvRegisterKFCAccount:
                Intent in = new Intent(ctx, RegisterActivity.class);
                in.putExtra("login_area", LoginAreaType.REGISTER_IN_STEP_ONE.toString());
                getActivity().startActivityForResult(in, RequestResultCodeType.REQUEST_CODE_REGISTER_IN_STEP_ONE.getValue());
                break;

            case R.id.tvForgotPassword:
                DialogForgotPassword dialogDialogForgotPassword = new DialogForgotPassword(ctx);
                dialogDialogForgotPassword.show();
                break;
        }
    }


    private void checkOpen(final AddressDataWrapper dataWrapper) {
        checkOpenDeliver(dataWrapper.city, dataWrapper.ward, new ResultListener<ShowTimeDataWrapper>() {
            @Override
            public void onSucceed(ShowTimeDataWrapper result) {
                if (!result.isResult()) {
                    try {
                        //save address
                        ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), ObjectSerializer.serialize(dataWrapper)).apply();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (activity.getChangeAddressStep3()) {
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.popBackStack();
                    } else {
                        activity.changeFragment(new StepThreeFragment());
                    }

                }
                if (result.getData() != null) {
                    if (!result.getData().isShowTime()) {
                        try {
                            //save address
                            ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), ObjectSerializer.serialize(dataWrapper)).apply();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (activity.getChangeAddressStep3()) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.popBackStack();
                        } else {
                            activity.changeFragment(new StepThreeFragment());
                        }

                    } else {
                        showAlert(result.getMessage());
                    }
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    private void checkOpenDeliver(String provinceID, final String wardIDs, final ResultListener<ShowTimeDataWrapper> callBack) {
        activity.checkOpen(provinceID, wardIDs, new ResultListener<ShowTimeDataWrapper>() {
            @Override
            public void onSucceed(ShowTimeDataWrapper result) {
                if (result.getData().isShowTime()) {
                    StepTwoFragment.customTextView(getActivity(), mTvNotice, result.getData().getMinTime());
                    llNotice.setVisibility(View.VISIBLE);
                }
                if (callBack != null)
                    callBack.onSucceed(result);


            }

            @Override
            public void onFail(String reason) {

            }
        });

    }


    private void register(String name,
                          String email,
                          String pwd,
                          int gender,
                          String phone,
                          String day,
                          String month,
                          String year) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<ResponseBody> call1 = ApiClient.getJsonClient().register(name, email, pwd, gender, phone, day, month, year);
        call1.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ResponseBody> arg0, Response<ResponseBody> arg1) {
                mProgressDialog.dismiss();

                System.out.println(arg1.body().toString());

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {

                }
            }
        });
    }

    private void login(String email, final String pwd) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getContext(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        Map<String, String> maps = new HashMap<String, String>();
        maps.put("email", email);
        maps.put("password", pwd);
        maps.put("token", sharedpreferences.getString("device_token", ""));
        Call<LoginDataWrapper> call1 = ApiClient.getJsonClient().login(maps);
        call1.enqueue(new Callback<LoginDataWrapper>() {

            @Override
            public void onFailure(Call<LoginDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                edtKFCUserName.requestFocus();
                edtKFCUserName.selectAll();
                DialogFactory.showDialog(getContext(), getString(R.string.error), getString(R.string.login_kfc_fail));

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<LoginDataWrapper> arg0, Response<LoginDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    LoginDataWrapper obj = arg1.body();
                    if (obj.result) {
                        try {
                            LoginWrapper mUserInfo = obj.data;
                            if (chkRememberAccount.isChecked()) {
                                ctx.getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(mUserInfo)).commit();
                                ctx.getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_PASSWORD.toString(), pwd).apply();
                            }

                            processNextStepAfterLoginSuccess(mUserInfo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        DialogFactory.showDialog(getContext(), getString(R.string.error), getString(R.string.login_kfc_fail));
                }
            }
        });
    }


    private void loginByFacebookOrGooglePlus(String loginType,
                                             String id,
                                             String email,
                                             String name, String phone,
                                             String urlAvatar, final ProgressDialog mProgressDialog) {

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);
        Call<LoginDataWrapper> call1 = ApiClient.getJsonClient().loginByFacebookOrGooglePlus(loginType,
                id,
                email,
                name, phone, "0",
                urlAvatar, sharedpreferences.getString("device_token", ""));
        call1.enqueue(new Callback<LoginDataWrapper>() {

            @Override
            public void onFailure(Call<LoginDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<LoginDataWrapper> arg0, Response<LoginDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                LoginDataWrapper obj = arg1.body();
                if (obj.result) {
                    try {
                        LoginWrapper mUserInfo = obj.data;
                        ctx.getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(mUserInfo)).commit();

                        processNextStepAfterLoginSuccess(mUserInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    Toast.makeText(ctx, getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestResultCodeType.REQUEST_CODE_REGISTER_IN_STEP_ONE.getValue() && resultCode == RequestResultCodeType.RESULT_CODE_REGISTER_IN_STEP_ONE.getValue()) {
            Bundle b = data.getExtras();
            edtKFCUserName.setText(b.getString("email", ""));
            edtKFCPassword.setText(b.getString("pwd", ""));
        }
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        else if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(Tag.LOG.getValue(), "Google Plus -> onActivityResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                final GoogleSignInAccount acct = result.getSignInAccount();

                String info = "Id: " + acct.getId() + ", Email: " + acct.getEmail() + ", DisplayName: " + acct.getDisplayName() + ", PhotoUrl: " + acct.getPhotoUrl().toString();
                Log.d(Tag.LOG.getValue(), "INFO: " + info);

                Toast.makeText(ctx, getString(R.string.login_google_plus_successful), Toast.LENGTH_SHORT).show();
                if (TextUtils.isEmpty(acct.getEmail())) {
                    DialogFactory.showDialogInputEmail(getContext(), new DialogFactory.ListenerLinkAccount() {
                        @Override
                        public void clickAgree(String email, Dialog dialog) {
                            checkLinkedAccountGoogle(currentLanguageCode, LoginType.GOOGLE_PLUS.getValue(), acct.getId(), email, acct.getDisplayName(), acct.getPhotoUrl().toString(), true);
                        }

                        @Override
                        public void close() {

                        }
                    });
                    return;
                }
                checkLinkedAccountGoogle(currentLanguageCode, LoginType.GOOGLE_PLUS.getValue(), acct.getId(), acct.getEmail(), acct.getDisplayName(), acct.getPhotoUrl().toString(), false);
            } else {
                // Signed out, show unauthenticated UI.
                Toast.makeText(ctx, getString(R.string.login_google_plus_fail), Toast.LENGTH_SHORT).show();
            }
        } else {
            //Facebook
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(Tag.LOG.getValue(), "onConnectionFailed():" + connectionResult.toString());
        Toast.makeText(ctx, getString(R.string.connect_google_plus_fail), Toast.LENGTH_SHORT).show();
    }

    private void processNextStepAfterLoginSuccess(LoginWrapper userInfo) {
        activity.userInfo = userInfo;
        activity.changeFragment(new StepTwoFragment());
        Intent i = activity.getIntent();
        Bundle b = new Bundle();
        b.putSerializable("user_info", userInfo);
        i.putExtras(b);
        activity.setResult(RequestResultCodeType.RESULT_CODE_LOGIN_IN_SHOPPING_CART.getValue(), i);
        activity.showUserInfoInNavigation(true);
    }

    private class TextChangedListener implements TextWatcher {
        private Context mContext;
        EditText editText;

        public TextChangedListener(Context context, EditText edt) {
            super();
            this.mContext = context;
            this.editText = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            editText.setError(null, null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    private AdapterView.OnItemSelectedListener onProvinceSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i == 0) {
                provinceID = "";
                provinceName = "";

                districtID = "";
                districtName = "";
                lstDistrict.clear();
                lstDistrict.add(defaultDistrict);
                spnDistrict.setSelection(0);

                wardID = "";
                nameWar = "";
                lstWard.clear();
                lstWard.add(defaultWard);
            } else {
                provinceID = lstProvince.get(i).id;
                provinceName = lstProvince.get(i).name;

                lstDistrict.clear();
                lstDistrict.add(defaultDistrict);
                lstDistrict.addAll(lstProvince.get(i).district);

                lstWard.clear();
                lstWard.add(defaultWard);

                MainActivity.sProvineId = lstProvince.get(i).id;

                if (getActivity() != null) {
                    districtAdapter = new DistrictAdapter(getActivity(), R.layout.custom_item_district_spinner, lstDistrict);
                    spnDistrict.setAdapter(districtAdapter);
                    spnDistrict.setOnItemSelectedListener(onDistrictSelected);
                    wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                    spnWard.setAdapter(wardAdapter);
                    spnWard.setOnItemSelectedListener(onWardSelected);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private AdapterView.OnItemSelectedListener onDistrictSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i == 0) {
                districtID = "";
                districtName = "";

                wardID = "";
                nameWar = "";
                lstWard.clear();
                lstWard.add(defaultWard);
                spnWard.setSelection(0);
            } else {
                districtID = lstDistrict.get(i).id;
                districtName = lstDistrict.get(i).name;

                lstWard.clear();
                lstWard.add(defaultWard);
                lstWard.addAll(lstDistrict.get(i).lstWard);

                if (getActivity() != null) {
                    wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                    spnWard.setAdapter(wardAdapter);
                    spnWard.setOnItemSelectedListener(onWardSelected);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private AdapterView.OnItemSelectedListener onWardSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i == 0) {
                wardID = "";
                nameWar = "";
            } else {
                wardID = lstWard.get(i).id;
                nameWar = lstWard.get(i).name;
                if (lstWard.get(i).price.priceMax == null) {
                    rlMoreDetail.setVisibility(View.GONE);
                } else {
                    rlMoreDetail.setVisibility(View.VISIBLE);
                    DecimalFormat myFormatter = new DecimalFormat("###,###");
                    String output = myFormatter.format(lstWard.get(i).price.priceMax);
                    tvMinPrice.setText(output);
                    tvMinTime.setText(lstWard.get(i).price.timeMax);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    public void checkLinkedAccountFacebook(final String lang, final String loginType,
                                           final String id,
                                           final String email,
                                           final String name,
                                           final String urlAvatar, final boolean isNull) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getContext(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, LoginType.KFC.getValue(), id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    String idUser = "";
                    if (loginType == LoginType.FACEBOOK.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("facebook"))) {

                        idUser = object.optString("facebook");
                    } else if (loginType == LoginType.GOOGLE_PLUS.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("google"))) {

                        idUser = object.optString("google");
                    }
                    if (object != null && (idUser.equals("") || idUser.equals("null"))) {
                        LoginWrapper userInfo = mapperRegisterData(jsonObject.optJSONObject("data"));
                        sendOTPFacebook(lang, email, userInfo.id, loginType, id, name, urlAvatar, mProgressDialog);
                    } else if (object == null || id.equals(idUser)) {
                        checkEmailRegisterFacebookOrPlus(lang, loginType, id, email, name, urlAvatar, isNull, mProgressDialog);
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public void checkLinkedAccountGoogle(final String lang, final String loginType,
                                         final String id,
                                         final String email,
                                         final String name,
                                         final String urlAvatar, final boolean isNull) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getContext(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, LoginType.KFC.getValue(), id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    String idUser = "";
                    if (loginType == LoginType.FACEBOOK.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("facebook"))) {

                        idUser = object.optString("facebook");
                    } else if (loginType == LoginType.GOOGLE_PLUS.getValue() && object != null &&
                            !TextUtils.isEmpty(object.optString("google"))) {

                        idUser = object.optString("google");
                    }
                    if (object != null && (idUser.equals("") || idUser.equals("null"))) {
                        LoginWrapper userInfo = mapperRegisterData(jsonObject.optJSONObject("data"));
                        sendOTPGoogle(lang, email, userInfo.id, loginType, id, name, urlAvatar, mProgressDialog);
                    } else if (object == null || id.equals(idUser)) {
                        checkEmailRegisterFacebookOrPlus(lang, loginType, id, email, name, urlAvatar, isNull, mProgressDialog);
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }


    public LoginWrapper mapperRegisterData(JSONObject object) throws JSONException {
        LoginWrapper data = new LoginWrapper();
        data.id = object.optString("id");
        data.password = object.optString("password");
        data.name = object.optString("name");
        data.gender = object.optString("gender");
        data.email = object.optString("email");
        data.phone = object.optString("phone");
        data.dt_create = object.optString("dt_create");
        data.usertype = object.optString("usertype");
        data.bl_active = object.optString("bl_active");
        return data;
    }

    public void checkEmailRegisterFacebookOrPlus(String lang, final String loginType,
                                                 final String id,
                                                 final String email,
                                                 final String name,
                                                 final String urlAvatar, boolean isNull, final ProgressDialog mProgressDialog) {
        Call<ResponseBody> call = ApiClient.getJsonClient().checkEmailRegister(lang, email, loginType, id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject object = jsonObject.optJSONObject("data");
                    if (object == null) {
                        mProgressDialog.dismiss();
                        Intent in = new Intent(ctx, RegisterActivity.class);
                        in.putExtra("login_area", LoginAreaType.REGISTER_IN_STEP_ONE.toString());
                        in.putExtra("type", loginType);
                        in.putExtra("id", id);
                        in.putExtra("email", email);
                        in.putExtra("name", name);
                        in.putExtra("urlAvatar", urlAvatar);
                        startActivityForResult(in, RequestResultCodeType.REQUEST_CODE_REGISTER_IN_STEP_ONE.getValue());
                    } else {
                        loginByFacebookOrGooglePlus(loginType, id, email, name, "", urlAvatar, mProgressDialog);
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }


    public void sendOTPFacebook(final String lang, final String email, final String id_user, final String loginType,
                                final String id,
                                final String name,
                                final String urlAvatar, final ProgressDialog mProgressDialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().sendOTP(lang, email, id_user);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        mProgressDialog.dismiss();
                        DialogFactory.showDialogLinkAccountFacebook(getContext(), email, new DialogFactory.ListenerLinkAccount() {
                            @Override
                            public void clickAgree(String email, Dialog dialog) {
                                checkOTP(lang, email, id_user, loginType, id, name, urlAvatar, mProgressDialog, dialog);
                            }

                            @Override
                            public void close() {
                                mProgressDialog.dismiss();
                            }
                        });
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public void sendOTPGoogle(final String lang, final String email, final String id_user, final String loginType,
                              final String id,
                              final String name,
                              final String urlAvatar, final ProgressDialog mProgressDialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().sendOTP(lang, email, id_user);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        mProgressDialog.dismiss();
                        DialogFactory.showDialogLinkAccountGoogle(getContext(), email, new DialogFactory.ListenerLinkAccount() {
                            @Override
                            public void clickAgree(String email, Dialog dialog) {
                                checkOTP(lang, email, id_user, loginType, id, name, urlAvatar, mProgressDialog, dialog);
                            }

                            @Override
                            public void close() {
                                mProgressDialog.dismiss();
                            }
                        });
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    public void checkOTP(final String lang, final String email, final String id_user, final String loginType,
                         final String id,
                         final String name,
                         final String urlAvatar, final ProgressDialog mProgressDialog, final Dialog dialog) {
        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<ResponseBody> call = ApiClient.getJsonClient().checkOTP(lang, email, id_user, id, loginType);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getBoolean("result")) {
                        dialog.dismiss();
                        loginByFacebookOrGooglePlus(loginType, id, email, name, "", urlAvatar, mProgressDialog);
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), URLDecoder.decode(jsonObject.getString("message"), "UTF-8"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    dialog.dismiss();
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.login_kfc_fail), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    private void showAlert(String message) {
        if (TextUtils.isEmpty(message))
            message = getString(R.string.delivery_warning);
        new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
