package kfc.vietnam.food_menu_screen.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
//import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.CategoryType;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.ConfigureDataWrapper;
import kfc.vietnam.common.object.CouponDataWrapper;
import kfc.vietnam.common.object.CouponPopupWrapper;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.ProductBuyMoreModel;
import kfc.vietnam.common.object.ProductWrapper002;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.ShoppingCartObject;
import kfc.vietnam.common.utility.DividerItemDecoration;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.utility.ResultListener;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.ShoppingCartAdapter;
import kfc.vietnam.main_screen.ui.LoginActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kfc.vietnam.R.id.llShoppingCart;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class ShoppingCartFragment extends Fragment implements View.OnClickListener {

    private static final int VERTICAL_ITEM_SPACE = 48;
    public static CouponDataWrapper.CouponResult lastCouponResult = null;
    public static CouponPopupWrapper popupCouponResult = null;

    private String currentLanguageCode = "";
    private static Context ctx;
    private ArrayList<ProvinceWrapper> lstProvince;
    private ArrayList<ShoppingCartObject> lstShoppingCard = null;
    private ArrayList<ShoppingCartObject> lstShoppingCard_KhuyenMai = null;
    private RecyclerView rcvShoppingCart;
    private SharedPreferences preferencesShoppingCard;
    private TextView tvTotalPrice, tvReceivePoint, tvDiscount;
    private EditText edtCodeForSale;
    private NestedScrollView svCart;
    private MainActivity activity;
    private RelativeLayout rlDiscount;
    private LinearLayout llRemoveDiscount, llCouponContainer;
    private double gTotalPrice = 0;
    private double oldTotalPrice = 0;
    private LinearLayout layoutImEmpty, layoutInfo, llProcessOrder;
    private RelativeLayout layoutEmpty;
    private ShoppingCartAdapter mAdapter;
    //
    private View rlDisCountCode;
    private TextView tvDiscountCode, tvTotalMoney, tvCheckout, tvShippingFee;

    private int suggest = 0;

    private RelativeLayout rlCouponPopup;
    private TextView tvCouponPopup;


    /**
     * Format input amount and show in price amount
     *
     * @param value
     * @param label
     */
    public static void formatPrice(double value, TextView label, boolean isDiscount, String discount) {
        if (value > 0) {
            DecimalFormat myFormatter = new DecimalFormat("#,##0");
            String output = myFormatter.format(value);
            try {
                output = output.replace(".", ",");
            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList<String> lst = new ArrayList(Arrays.asList(output.split(",")));
            String last = ".000";
            if (lst.size() >= 2) {
                last = "." + lst.get(lst.size() - 1);
                lst.remove(lst.size() - 1);
            }
            output = lst.toString().replace("[", "").replace("]", "").replace(" ", "") + last + " " + ctx.getString(R.string.vnd_cap);

            int start = 0;
            int end = output.indexOf(".");
            output = output.replace(",", ".");

            SpannableString spanContent = new SpannableString(output);
            spanContent.setSpan(new AbsoluteSizeSpan(18, true), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set style
            if (isDiscount) {
                spanContent.setSpan(new ForegroundColorSpan(Color.BLACK), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
                label.setText("-" + spanContent + (TextUtils.isEmpty(discount) ? "" : (" (" + discount + "%)")));
            } else {
                spanContent.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
                label.setText(spanContent);
            }

        } else {
            label.setText("0 " + ctx.getString(R.string.vnd_cap));
        }
    }


    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
        this.lstProvince = activity.lstProvince;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_shopping_cart_fragment, container, false);
        setUpLayoutEmptyMessage(root);

        //Edit by @ManhNguyen
        tvShippingFee = (TextView) root.findViewById(R.id.tvShippingFee);
        layoutInfo = (LinearLayout) root.findViewById(R.id.layout_info);
        svCart = (NestedScrollView) root.findViewById(R.id.svCart);
        TextView tvTitle001 = (TextView) root.findViewById(R.id.tvTitle001);
        TextView tvTitle002 = (TextView) root.findViewById(R.id.tvTitle002);
        TextView tvTitle003 = (TextView) root.findViewById(R.id.tvTitle003);
        LinearLayout llBuyMoreProduct = (LinearLayout) root.findViewById(R.id.llBuyMoreProduct);
        edtCodeForSale = (EditText) root.findViewById(R.id.edtCodeForSale);
        LinearLayout llAgree = (LinearLayout) root.findViewById(R.id.llAgree);
        rcvShoppingCart = (RecyclerView) root.findViewById(R.id.rcvShoppingCart);
        llProcessOrder = (LinearLayout) root.findViewById(R.id.llProcessOrder);
        tvTotalPrice = (TextView) root.findViewById(R.id.tvTotalPrice);
        tvTotalMoney = (TextView) root.findViewById(R.id.tvTotal_money);
        tvReceivePoint = (TextView) root.findViewById(R.id.tvReceivePoint);
        llRemoveDiscount = (LinearLayout) root.findViewById(R.id.llRemoveDiscount);
        rlDisCountCode = root.findViewById(R.id.rlDiscount_code);
        rlDiscount = (RelativeLayout) root.findViewById(R.id.rlDiscount);
        tvDiscount = (TextView) root.findViewById(R.id.tvDiscount);
        tvDiscountCode = (TextView) root.findViewById(R.id.tvDiscount_code);
        llCouponContainer = (LinearLayout) root.findViewById(R.id.llCouponContainer);
        TextView tvNameReceived = (TextView) root.findViewById(R.id.tvNameReceived);

        //Added by Nhat Minh
        rlCouponPopup = (RelativeLayout) root.findViewById(R.id.rlCouponPopup);
        tvCouponPopup = (TextView) root.findViewById(R.id.tvCouponPopup);
        checkShowCouponPopup();

        preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);

        try {
            lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null));
            lstShoppingCard_KhuyenMai = new ArrayList<ShoppingCartObject>();
            for (int i = 0; i < lstShoppingCard.size(); i++) {
                ShoppingCartObject shoppingCartObject = lstShoppingCard.get(i);
                if (shoppingCartObject.productSelected.khuyenmai.equals("1")) {
                    lstShoppingCard_KhuyenMai.add(shoppingCartObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        activity.checkProductShoppingCart(lstShoppingCard, new ResultListener<ArrayList<ShoppingCartObject>>() {
            @Override
            public void onSucceed(ArrayList<ShoppingCartObject> result) {
                try {
                    preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                    int count_cart = 0;
                    for (ShoppingCartObject cart : lstShoppingCard) {
                        count_cart += Integer.parseInt(cart.amount);
                    }
                    activity.tvAmountShoppingCart.setText(count_cart + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (lstShoppingCard != null)
                    Collections.sort(lstShoppingCard, new Comparator<ShoppingCartObject>() {
                        @Override
                        public int compare(ShoppingCartObject shoppingCartObject, ShoppingCartObject t1) {
                            return shoppingCartObject.idCart.compareToIgnoreCase(t1.idCart);
                        }
                    });
                if (lstShoppingCard != null) {
                    for (ShoppingCartObject obj : lstShoppingCard) {
                        if (obj.dishSelected != null && obj.price_one_item != null) {
                            if (Double.valueOf(obj.dishSelected.price) != obj.price_one_item) {
                                obj.price_one_item = Double.valueOf(obj.dishSelected.price);

                            }
                        }

                    }
                }
                if (lstShoppingCard != null) {
                    if (lstShoppingCard.size() > 0) {
                        hideLayoutEmptyMess();
                        mAdapter = new ShoppingCartAdapter(ctx, lstShoppingCard, ShoppingCartFragment.this, onclickChangeValue);
                        rcvShoppingCart.setAdapter(mAdapter);
                        updateShowTotalPrice();
                    } else {
                        showLayoutEmptyMess();
                    }
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });


        ConfigureDataWrapper.TimeOpenWrapper timeOpen = activity.conf.timeOpen;

        tvTitle001.setText(activity.getString(R.string.We_accept_orders) + " " + timeOpen.open + " " + activity.getString(R.string.to) + " " + timeOpen.close);

        tvTitle002.setText(activity.getString(R.string.Minimum_Order) + " " + formatMinPrice(Double.parseDouble(activity.conf.point.minPrice)).replace(",", "."));

        if (activity.conf.point.shipPrice.equalsIgnoreCase("0")) {
            tvTitle003.setText(activity.getString(R.string.free_ship));
            tvShippingFee.setText(activity.getString(R.string.free).toUpperCase());
        } else {
            tvTitle003.setText(activity.getString(R.string.shipping_fee) + ": " + formatMinPrice(Double.parseDouble(activity.conf.point.shipPrice)).replace(",", "."));
            tvShippingFee.setText(formatMinPrice(Double.parseDouble(activity.conf.point.shipPrice)).replace(",", "."));
        }


        llBuyMoreProduct.setOnClickListener(this);

        edtCodeForSale.addTextChangedListener(new TextChangedListener(ctx, edtCodeForSale));

        llAgree.setOnClickListener(this);


        LinearLayoutManager layoutManager = new LinearLayoutManager(ctx);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvShoppingCart.setLayoutManager(layoutManager);
        //add ItemDecoration
        //rcvShoppingCart.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //or
        //rcvShoppingCart.addItemDecoration(new DividerItemDecoration(getActivity()));
        //or
        rcvShoppingCart.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider_listview));


        llProcessOrder.setOnClickListener(this);


        rlDisCountCode.setVisibility(View.GONE);

        rlDiscount.setVisibility(View.GONE);


        if (activity.conf != null && activity.conf.coupon != null && activity.conf.coupon.checkCoupon) {
            //Add by Nhat Minh (hide coupon container)
            //llCouponContainer.setVisibility(View.VISIBLE);
            llCouponContainer.setVisibility(View.GONE);
            rlDiscount.setVisibility(View.GONE);
            rlDisCountCode.setVisibility(View.GONE);
        } else {
            llCouponContainer.setVisibility(View.GONE);
            rlDiscount.setVisibility(View.GONE);
            rlDisCountCode.setVisibility(View.GONE);
        }


        ShoppingCartFragment.lastCouponResult = null;
        llRemoveDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastCouponResult = null;
                showTotalPrice(gTotalPrice);
                formatPrice(gTotalPrice, tvTotalPrice, false, "");
                rlDiscount.setVisibility(View.GONE);
                rlDisCountCode.setVisibility(View.GONE);
            }
        });

        updateShowTotalPrice();

        svCart.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if ((oldScrollY - scrollY) > 5) {
                    edtCodeForSale.setError(null);
                    edtCodeForSale.clearFocus();
                }
            }
        });

        //Edit by @ManhNguyen
        tvNameReceived.setOnClickListener(this);
        //layout hiện gợi ý mua thêm
        RelativeLayout layout_suggest = (RelativeLayout) root.findViewById(R.id.layout_suggest);

        if (lstShoppingCard != null) {
            if (lstShoppingCard.size() > 0) {
                layoutEmpty.setVisibility(View.GONE);

                //Add by @ManhNguyen
                boolean hasCombo = false;
//                boolean hasDrink = false;
                boolean hasDrink = true;
                boolean hasDessert = false;

                for (ShoppingCartObject objShoppingCart : lstShoppingCard) {//
                    //Thứ tự ưu tiên gợi ý
                    //1
                    if (objShoppingCart.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                        hasCombo = true;

                        //Theo web hien tai
                        //2
//                        if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0) {
//                            hasDrink = true;
//                        }
                        //3
//                        if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0) {
//                            hasDessert = true;
//                        }
                    }

                    if (objShoppingCart.categoryType.equalsIgnoreCase(CategoryType.PRODUCT.getValue())) {
//                        if (objShoppingCart.productSelected.categoryID.equals("4"))
//                            hasDrink = true;

                        if (objShoppingCart.productSelected.categoryID.equals("5")) {
                            if (objShoppingCart.productSelected.nameProduct.toUpperCase().contains("BÁNH TRỨNG") ||
                                    objShoppingCart.productSelected.nameProduct.toUpperCase().contains("EGG"))
                                hasDessert = true;
                        }
                    }

                }

                //check hiện gợi ý
                if (!hasCombo) {
                    tvNameReceived.setText(getString(R.string.StepThreeFragment_021));
                    suggest = 0;
                } else {
                    if (!hasDrink) {
                        tvNameReceived.setText(getString(R.string.StepThreeFragment_022));
                    } else if (!hasDessert) {
                        tvNameReceived.setText(getString(R.string.StepThreeFragment_023));
                        suggest = 5;
                    } else {
                        layout_suggest.setVisibility(View.GONE);
                    }
                }

            } else {
                layoutEmpty.setVisibility(View.VISIBLE);
            }
        }

        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (enter) {
                    activity.processChangeTopMenu(ShoppingCartFragment.class);
                }
            }
        });

        return anim;
    }

    public void updateShowTotalPrice() {
        //check here

        gTotalPrice = 0;
        int count_cart = 0;
        if (lstShoppingCard != null) {
            for (ShoppingCartObject objShoppingCart : lstShoppingCard) {
//                gTotalPrice += Double.parseDouble(obj.dishSelected.price) * Integer.parseInt(obj.amount);
//                gTotalPrice += obj.price_one_item * Integer.parseInt(obj.amount);
                double price = Double.parseDouble(objShoppingCart.dishSelected.price);
//                double price = objShoppingCart.price_in_total;
                if (objShoppingCart.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                    int countElement = objShoppingCart.count_element;
                    if (countElement == 0) {
                        if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        //add by @ManhNguyen
                        if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                    } else if (countElement == 1) {
                        if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        //add by @ManhNguyen
                        if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }
                    } else if (countElement == 2) {

                        if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        //add by @ManhNguyen
                        if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }
                    }
                }

                if (objShoppingCart.listProductBuyMoreModel != null) {
                    if (objShoppingCart.listProductBuyMoreModel.size() > 0) {
                        for (ProductBuyMoreModel temp : objShoppingCart.listProductBuyMoreModel) {
                            price += Double.parseDouble(temp.price);
                        }
                    }
                }

                gTotalPrice += price * Integer.parseInt(objShoppingCart.amount);
                count_cart += Integer.parseInt(objShoppingCart.amount);
            }
        }

        formatPrice(gTotalPrice, tvTotalPrice, false, "");
        showTotalPrice(gTotalPrice);
        if (oldTotalPrice >= 1000000 && gTotalPrice < 1000000) {
            //remove
            lastCouponResult = null;
            showTotalPrice(gTotalPrice);
            formatPrice(gTotalPrice, tvTotalPrice, false, "");
            rlDiscount.setVisibility(View.GONE);
            rlDisCountCode.setVisibility(View.GONE);
            oldTotalPrice = gTotalPrice;
        } else {
            oldTotalPrice = gTotalPrice;
        }

        activity.tvAmountShoppingCart.setText(count_cart + "");
    }

    private String formatMinPrice(double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(totalPrice).replace(".", ",");
            return output + " " + getString(R.string.vnd_cap);
        } else
            return "0 " + getString(R.string.vnd_cap);
    }

    private void showTotalPrice(double totalPrice) {
        if (totalPrice > 0) {
            double value = gTotalPrice;
            double fee = 0;
            if (lastCouponResult != null) {

                if (lastCouponResult.type == 1) {
                    fee = lastCouponResult.discount;
                } else {
                    fee = (gTotalPrice * lastCouponResult.discount) / 100;
                    if (fee > lastCouponResult.maxdiscount && lastCouponResult.maxdiscount > 0)
                        fee = lastCouponResult.maxdiscount;

                }
            }
//            value = gTotalPrice - fee;
            value = gTotalPrice - fee + Double.parseDouble(activity.conf.point.shipPrice); //edit by @ManhNguyen

            formatPrice(value, tvTotalMoney, false, "");

            tvReceivePoint.setText(activity.calculatePoint(totalPrice) + "");
            if (lastCouponResult != null && lastCouponResult.discount > 0) {
                if (activity.conf != null && activity.conf.coupon != null && activity.conf.coupon.checkCoupon) {
                    rlDiscount.setVisibility(View.VISIBLE);
                    rlDisCountCode.setVisibility(View.VISIBLE);
                } else {
                    rlDiscount.setVisibility(View.GONE);
                    rlDisCountCode.setVisibility(View.GONE);
                }
                String sDisCount = "";
                if (lastCouponResult.type != 1) {
                    if (fee < lastCouponResult.maxdiscount) {
                        DecimalFormat myFormatter = new DecimalFormat("###,###");
                        sDisCount = "" + myFormatter.format(lastCouponResult.discount);
                    }
                }
                formatPrice(fee, tvDiscount, true, sDisCount);
            } else {
                rlDiscount.setVisibility(View.GONE);
                rlDisCountCode.setVisibility(View.GONE);
            }
        } else {
            tvTotalPrice.setText("0 " + getString(R.string.vnd_cap));
            tvReceivePoint.setText("0");
            //TODO remove discount
        }
    }

    //Add by Nhat Minh
    private void checkShowCouponPopup() {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<CouponPopupWrapper> call1 = ApiClient.getJsonClient().checkShowCouponPopup(currentLanguageCode);
        call1.enqueue(new Callback<CouponPopupWrapper>() {
            @Override
            public void onFailure(Call<CouponPopupWrapper> arg0, Throwable arg1) {
                rlCouponPopup.setVisibility(View.GONE);
                mProgressDialog.dismiss();
                Log.d("POPUP COUPON",getString(R.string.ShoppingCartFragment_Request_Error));
            }

            @Override
            public void onResponse(Call<CouponPopupWrapper> arg0, Response<CouponPopupWrapper> arg1) {
                mProgressDialog.dismiss();
                // Success
                if (arg1.isSuccessful()) {
                    popupCouponResult = arg1.body();
                    if (popupCouponResult != null) {
                        if(popupCouponResult.getResult()) {
                            rlCouponPopup.setVisibility(View.VISIBLE);
                            tvCouponPopup.setText(Html.fromHtml(popupCouponResult.getMessage()));
                        }
                    } else {
                        rlCouponPopup.setVisibility(View.GONE);
                    }
                } else {
                    Log.d("POPUP COUPON",getString(R.string.ShoppingCartFragment_Request_Error));
                    rlCouponPopup.setVisibility(View.GONE);
                }
            }
        });
    }

    private void checkTime(String lang) {
        llProcessOrder.setEnabled(false);
        activity.showProgressDialog();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            activity.hideProgressDialog();
            Toast.makeText(getActivity(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    llProcessOrder.setEnabled(true);
                }
            }, 2000);

        } else {
            Call<ResponseBody> call = ApiClient.getJsonClient().checkTime(lang);
            call.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    activity.hideProgressDialog();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            llProcessOrder.setEnabled(true);
                        }
                    }, 2000);

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getBoolean("result")) {
                            Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        } else {
                            if (gTotalPrice >= Double.parseDouble(activity.conf.point.minPrice)) {
                                if (activity.userInfo == null) {// chưa đăng nhập
                                    ((MainActivity) ctx).changeFragment(new StepOneFragment());
                                    ((MainActivity) ctx).checkProcessOrder = 1;
                                    ((MainActivity) ctx).totalPrice = gTotalPrice;
                                } else {// đã đăng nhập
                                    ((MainActivity) ctx).changeFragment(new StepTwoFragment());
                                    ((MainActivity) ctx).checkProcessOrder = 2;
                                    ((MainActivity) ctx).totalPrice = gTotalPrice;
                                }
                            } else {
                                if (gTotalPrice == 0) {
                                    ((MainActivity) ctx).backToMainMenu();
                                } else {
                                    DecimalFormat myFormatter = new DecimalFormat("###,###");
                                    String output = myFormatter.format(Double.parseDouble(activity.conf.point.minPrice)).replace(".", ",");

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                                    alertDialog.setMessage(activity.getString(R.string.Minimum_Order_Alert) + " " + output.replace(",", ".") + " " + getString(R.string.vnd_cap));

                                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            arg0.dismiss();
                                        }
                                    });

                                    AlertDialog dialog = alertDialog.create();
                                    dialog.show();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                    throwable.printStackTrace();
                    activity.hideProgressDialog();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            llProcessOrder.setEnabled(true);
                        }
                    }, 2000);
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llProcessOrder:
//                if (gTotalPrice >= Double.parseDouble(activity.conf.point.minPrice)) {
//                    if (activity.userInfo == null) {// chưa đăng nhập
//                        ((MainActivity) ctx).changeFragment(new StepOneFragment());
//                        ((MainActivity) ctx).checkProcessOrder = 1;
//                        ((MainActivity) ctx).totalPrice = gTotalPrice;
//                    } else {// đã đăng nhập
//                        ((MainActivity) ctx).changeFragment(new StepTwoFragment());
//                        ((MainActivity) ctx).checkProcessOrder = 2;
//                        ((MainActivity) ctx).totalPrice = gTotalPrice;
//                    }
//                } else {
//                    if (gTotalPrice == 0) {
//                        ((MainActivity) ctx).backToMainMenu();
//                    } else {
//                        DecimalFormat myFormatter = new DecimalFormat("###,###");
//                        String output = myFormatter.format(Double.parseDouble(activity.conf.point.minPrice)).replace(".", ",");
//
//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
//                        alertDialog.setMessage(activity.getString(R.string.Minimum_Order_Alert) + " " + output.replace(",", ".") + " " + getString(R.string.vnd_cap));
//
//                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface arg0, int arg1) {
//                                arg0.dismiss();
//                            }
//                        });
//
//                        AlertDialog dialog = alertDialog.create();
//                        dialog.show();
//                    }
//                }

                checkTime(currentLanguageCode);
                break;

            case R.id.llBuyMoreProduct:
                ((MainActivity) ctx).backToMainMenu();
                break;

            case R.id.llAgree:
                if (activity.userInfo != null) {
                    final String code = edtCodeForSale.getText().toString();
                    if (!code.isEmpty()) {
                        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                        double couponAmount = gTotalPrice;
                        if (lastCouponResult != null) couponAmount += lastCouponResult.discount;

                        Log.e("code", code);
                        Log.e("amount", String.valueOf(couponAmount));

                        //Register new discount code
                        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
                        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
                        mProgressDialog.setCancelable(true);
                        mProgressDialog.setCanceledOnTouchOutside(false);
                        mProgressDialog.show();

                        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
                            mProgressDialog.dismiss();
                            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
                            activity.showNoInternetFragment();
                            return;
                        }

                        if (lstShoppingCard_KhuyenMai != null && lstShoppingCard_KhuyenMai.size() > 0) {
                            String nameProduct =getString(R.string.HaveCouponOther);
                            for (int i = 0; i < lstShoppingCard_KhuyenMai.size(); i++) {
                                ShoppingCartObject shoppingCartObject = lstShoppingCard_KhuyenMai.get(i);
                                nameProduct = nameProduct + "\n    - " + shoppingCartObject.productSelected.nameProduct;
                            }

                            AlertDialog.Builder alertDialogBuilderCall = new AlertDialog.Builder(ctx);
                            alertDialogBuilderCall
                                    .setMessage(nameProduct)
                                    .setCancelable(false)
                                    .setNegativeButton(getString(R.string.Yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // if this button is clicked, just close
                                            // the dialog box and do nothing
                                            for (int i = 0; i < lstShoppingCard_KhuyenMai.size(); i++) {
                                                ShoppingCartObject shoppingCartObject = lstShoppingCard_KhuyenMai.get(i);
                                                for (int j = 0; j < lstShoppingCard.size(); j++) {
                                                    ShoppingCartObject shoppingCartObject2 = lstShoppingCard.get(j);
                                                    if (shoppingCartObject.idCart.equals(shoppingCartObject2.idCart)) {
                                                        lstShoppingCard.remove(shoppingCartObject2);
                                                    }
                                                }
                                            }
                                            try {
                                                SharedPreferences preferencesShoppingCart = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
                                                preferencesShoppingCart.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                                                ShoppingCartFragment.this.lstShoppingCard = lstShoppingCard;
                                                mAdapter = new ShoppingCartAdapter(ctx, lstShoppingCard, ShoppingCartFragment.this, onclickChangeValue);
                                                rcvShoppingCart.setAdapter(mAdapter);
                                                mAdapter.notifyDataSetChanged();
                                                updateShowTotalPrice();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            dialog.cancel();
                                        }
                                    })
                                    .setPositiveButton(getString(R.string.No_Thanks), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    });
                            // create alert dialog
                            AlertDialog alertDialogCall = alertDialogBuilderCall.create();
                            // show it
                            alertDialogCall.show();
                            mProgressDialog.dismiss();

                            return;
                        }


                        Call<CouponDataWrapper> call1 = ApiClient.getJsonClient().checkCoupon(code,
                                (activity.userInfo != null) ? activity.userInfo.id : "",
                                (activity.userInfo != null) ? new String(Hex.encodeHex(DigestUtils.md5(activity.userInfo.id))) : "",
                                String.valueOf(couponAmount), currentLanguageCode);
                        call1.enqueue(new Callback<CouponDataWrapper>() {
                            @Override
                            public void onFailure(Call<CouponDataWrapper> arg0, Throwable arg1) {
                                edtCodeForSale.requestFocus();
                                edtCodeForSale.setError(getString(R.string.ShoppingCartFragment_Request_Error));
                                edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                mProgressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call<CouponDataWrapper> arg0, Response<CouponDataWrapper> arg1) {
                                mProgressDialog.dismiss();
                                // Sucess
                                if (arg1.isSuccessful()) {
                                    CouponDataWrapper respone = arg1.body();
                                    if (respone.result) {
                                        lastCouponResult = respone.getData();
                                        tvDiscountCode.setText(code);
                                        showTotalPrice(gTotalPrice);
                                        edtCodeForSale.setText("");
                                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(edtCodeForSale.getWindowToken(), 0);
                                    } else {
                                        edtCodeForSale.requestFocus();
                                        edtCodeForSale.setError(respone.message);
                                        edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                    }
                                } else {
                                    edtCodeForSale.requestFocus();
                                    edtCodeForSale.setError(getString(R.string.ShoppingCartFragment_Request_Error));
                                    edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                }
                            }
                        });
                    } else {
                        edtCodeForSale.requestFocus();
                        edtCodeForSale.setError(getString(R.string.ShoppingCartFragment_001));
                        edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                    }
                } else {
                    //Toast.makeText(ctx, getString(R.string.ShoppingCartAdapter_004), Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ctx, LoginActivity.class);
                    i.putExtra("login_area", LoginAreaType.LOGIN_IN_SHOPPING_CART.toString());
                    ((FragmentActivity) ctx).startActivityForResult(i, RequestResultCodeType.REQUEST_CODE_LOGIN_IN_SHOPPING_CART.getValue());
                }
                break;
            case R.id.tvNameReceived:
//                ((MainActivity) ctx).gotoMenu(true);
                Bundle b = new Bundle();
                //CHỌN TỚI TAB STRIP NÀO QUA id_category
                b.putString("id_category", suggest + "");

                DetailsMenuFragment f = new DetailsMenuFragment();
                f.setArguments(b);
                ((MainActivity) ctx).changeFragment(f);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Xử lý login khi click add like sản phẩm ở giỏ hàng (Shopping Cart)
        if (requestCode == RequestResultCodeType.REQUEST_CODE_LOGIN_IN_SHOPPING_CART.getValue() && resultCode == RequestResultCodeType.RESULT_CODE_LOGIN_IN_SHOPPING_CART.getValue()) {
            activity.userInfo = (LoginWrapper) data.getExtras().getSerializable("user_info");
        }
    }


    private class TextChangedListener implements TextWatcher {
        EditText editText;
        private Context mContext;

        public TextChangedListener(Context context, EditText edt) {
            super();
            this.mContext = context;
            this.editText = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            editText.setError(null, null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

    }

    private ShoppingCartAdapter.OnclickChangeValue onclickChangeValue = new ShoppingCartAdapter.OnclickChangeValue() {
        @Override
        public void valueChange(boolean isPlus, int post) {
            ShoppingCartObject objShoppingCart = lstShoppingCard.get(post);
            int sValue = Integer.valueOf(objShoppingCart.amount);
            if (isPlus) {
                sValue = sValue + 1;
            } else {
                if (sValue == 1) {
                    sValue = 1;
                } else {
                    sValue = sValue - 1;
                }
            }
            objShoppingCart.amount = "" + sValue;
            lstShoppingCard.set(post, objShoppingCart);
            try {
                preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mAdapter.notifyDataSetChanged();
            updateShowTotalPrice();

        }

        @Override
        public void likeValue(boolean isLike, int pos) {

        }

        @Override
        public void refresh(ArrayList<ShoppingCartObject> lstShoppingCard) {
            ShoppingCartFragment.this.lstShoppingCard = lstShoppingCard;

            mAdapter.removeAllItem();
            mAdapter = new ShoppingCartAdapter(ctx, lstShoppingCard, ShoppingCartFragment.this, onclickChangeValue);
            rcvShoppingCart.setAdapter(mAdapter);
        }
    };

    //handle empty layout

    private void setUpLayoutEmptyMessage(View view) {
        tvCheckout = (TextView) view.findViewById(R.id.tvCheckout);
        layoutImEmpty = (LinearLayout) view.findViewById(R.id.layout_im_empty);
        layoutEmpty = (RelativeLayout) view.findViewById(R.id.layout_empty);
//        layoutEmpty.post(new Runnable() {
//            @Override
//            public void run() {
//                ViewGroup.LayoutParams layoutParams = layoutEmpty.getLayoutParams();
//                layoutParams.height = layoutImEmpty.getHeight();
//                layoutEmpty.setLayoutParams(layoutParams);
//            }
//        });
    }

    public void showLayoutEmptyMess() {

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    layoutInfo.setVisibility(View.GONE);
                    tvCheckout.setText(getString(R.string.buy_more_products));
                    layoutEmpty.setVisibility(View.VISIBLE);
//                    layoutEmpty.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                            layoutEmpty.setLayoutParams(layoutParams);
//                        }
//                    });
                }
            });
        }

    }

    private void hideLayoutEmptyMess() {
        tvCheckout.setText(getString(R.string.conducted_order));
        layoutEmpty.setVisibility(View.GONE);
        layoutInfo.setVisibility(View.VISIBLE);
    }

}

