package kfc.vietnam.food_menu_screen.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import kfc.vietnam.R;
import kfc.vietnam.common.object.CategoryWrapper;
import kfc.vietnam.common.object.LayoutItemMenu;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.ListenerLoadImage;
import kfc.vietnam.main_screen.ui.MainActivity;

public class ItemMenuFragment extends Fragment implements View.OnClickListener {
    private static final String POSITON = "position";
    private static final String SCALE = "scale";
    private static final String DRAWABLE_RESOURE = "resource";
    public static TextView textView;
    private String currentLanguageCode = "";
    private Context ctx;
    private ArrayList<ProvinceWrapper> lstProvince;
    private int screenWidth;
    private int screenHeight;
    private ProgressBar progressBar;

    public static Fragment newInstance(Context context, int pos, float scale, CategoryWrapper obj, String lang, ArrayList<ProvinceWrapper> lstProvince) {
        Bundle b = new Bundle();
        b.putInt(POSITON, pos);
        b.putFloat(SCALE, scale);
        b.putSerializable(CategoryWrapper.class.getName(), obj);
        b.putString("language_code", lang);
        b.putSerializable("province_list", lstProvince);

        return Fragment.instantiate(context, ItemMenuFragment.class.getName(), b);
    }

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWidthAndHeight();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null)
            return null;

        Bundle b = this.getArguments();
        final int position = b.getInt(POSITON);
        float scale = b.getFloat(SCALE);
        CategoryWrapper obj = (CategoryWrapper) b.getSerializable(CategoryWrapper.class.getName());
        this.currentLanguageCode = b.getString("language_code");
        this.lstProvince = (ArrayList<ProvinceWrapper>) b.getSerializable("province_list");

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((screenWidth * 3) / (4), (screenHeight * 3) / (5));
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.custom_item_menu_fragment, container, false);

        textView = (TextView) linearLayout.findViewById(R.id.text);
        progressBar = (ProgressBar) linearLayout.findViewById(R.id.pbLoading);
        LayoutItemMenu root = (LayoutItemMenu) linearLayout.findViewById(R.id.root_container);
        final ImageView imageView = (ImageView) linearLayout.findViewById(R.id.pagerImg);

        textView.setText(obj.name.toUpperCase());
        imageView.setLayoutParams(layoutParams);
        imageView.setContentDescription(obj.description);
        Glide.with(this)
                .load(obj.image)
//                .placeholder(R.drawable.loading)
//                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .dontAnimate()
                .fitCenter()
                .listener(new ListenerLoadImage(progressBar , imageView))
                .bitmapTransform(new RoundedCornersTransformation(ctx, 5, 0, RoundedCornersTransformation.CornerType.ALL))
                .into(imageView);
        imageView.setTag(obj.id + "");

        //handling click event
        imageView.setOnClickListener(this);

        root.setScaleBoth(scale);

        return linearLayout;
    }

    /**
     * Get device screen width and height
     */
    private void getWidthAndHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeight = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.pagerImg:
                Bundle b = new Bundle();
                //CHỌN TỚI TAB STRIP NÀO QUA id_category
                b.putString("id_category", view.getTag().toString());

                DetailsMenuFragment f = new DetailsMenuFragment();
                f.setArguments(b);
                ((MainActivity) ctx).changeFragment(f);
                break;
        }
    }
}
