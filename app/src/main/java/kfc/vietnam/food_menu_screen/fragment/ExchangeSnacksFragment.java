package kfc.vietnam.food_menu_screen.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.ChangeDishPositionType;
import kfc.vietnam.common.utility.VerticalSpaceItemDecoration;
import kfc.vietnam.food_menu_screen.adapter.ChangeSnacksAdapter;
import kfc.vietnam.food_menu_screen.ui.ChangeDishActivity;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class ExchangeSnacksFragment extends Fragment implements View.OnClickListener {
    private static final int VERTICAL_ITEM_SPACE = 3;
    private boolean isShow = false;
    private ChangeDishActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle b = getArguments();
        this.isShow = b.getBoolean("show_change");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_exchange_snasks_fragment, container, false);

        FragmentActivity ctx = (FragmentActivity) container.getContext();
        activity = ((ChangeDishActivity) ctx);

        RecyclerView rcvListSnacks = (RecyclerView) root.findViewById(R.id.rcvListSnacks);
        ChangeSnacksAdapter changeDishAdapter = new ChangeSnacksAdapter(activity.lstSnacksAndDessertChange, ctx, rcvListSnacks);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        rcvListSnacks.setLayoutManager(mLayoutManager);
        rcvListSnacks.setItemAnimator(new DefaultItemAnimator());
        rcvListSnacks.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        rcvListSnacks.setAdapter(changeDishAdapter);

        RelativeLayout rlCompleted = (RelativeLayout) root.findViewById(R.id.rlCompleted);
        rlCompleted.setOnClickListener(this);
        RelativeLayout rlChangePage = (RelativeLayout) root.findViewById(R.id.rlChangePage);
        rlChangePage.setVisibility(isShow ? View.VISIBLE : View.GONE);
        rlChangePage.setOnClickListener(this);

        TextView tvChangeSnack = (TextView) root.findViewById(R.id.tvChangeSnack);
        ImageView imgChangeSnack = (ImageView) root.findViewById(R.id.imgChangeSnack);

        //Luong: snack -> drink -> ga ran -> burger -> snack
        if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) {
            tvChangeSnack.setText(getString(R.string.water_exchange));
            imgChangeSnack.setImageResource(R.drawable.ic_change_water);
        } else if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) {
            tvChangeSnack.setText(getString(R.string.garan_exchange));
            imgChangeSnack.setImageResource(R.drawable.ic_garan);
        } else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) {
            tvChangeSnack.setText(getString(R.string.burger_exchange));
            imgChangeSnack.setImageResource(R.drawable.ic_burger);
        } else {
            tvChangeSnack.setText(getString(R.string.change_disk));
        }

        return root;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.rlCompleted:
                activity.backPreviousScreen(true);
                break;
            case R.id.rlChangePage:
                //Luong: snack -> drink -> ga ran -> burger -> snack
                if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) {
                    activity.viewPagerChangeDish.setCurrentItem(1); //luôn luôn ở vị trí 1
                }
                else if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) {
                    activity.viewPagerChangeDish.setCurrentItem(1);
                }
                else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) {
                    activity.viewPagerChangeDish.setCurrentItem(1);
                }
                break;
        }
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser && getView() !=null) {
//            activity.currentViewVisible = ChangeDishPositionType.SNACKS.getValue();
//        }
//    }
}
