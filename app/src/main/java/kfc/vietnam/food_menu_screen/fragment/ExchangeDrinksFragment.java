package kfc.vietnam.food_menu_screen.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.ChangeDishPositionType;
import kfc.vietnam.common.utility.VerticalSpaceItemDecoration;
import kfc.vietnam.food_menu_screen.adapter.ChangeDrinksAdapter;
import kfc.vietnam.food_menu_screen.ui.ChangeDishActivity;

import static kfc.vietnam.R.string.aa;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class ExchangeDrinksFragment extends Fragment implements View.OnClickListener {
    private static final int VERTICAL_ITEM_SPACE = 3;
    private boolean isShow = false;
    private ChangeDishActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle b = getArguments();
        this.isShow = b.getBoolean("show_change");

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_exchange_drinks_fragment, container, false);

        FragmentActivity ctx = (FragmentActivity) container.getContext();
        activity = ((ChangeDishActivity) ctx);

        RecyclerView rcvListDrinks = (RecyclerView) root.findViewById(R.id.rcvListDrinks);
        ChangeDrinksAdapter changeDishAdapter = new ChangeDrinksAdapter(activity.lstDrinkChange, ctx, rcvListDrinks);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        rcvListDrinks.setLayoutManager(mLayoutManager);
        rcvListDrinks.setItemAnimator(new DefaultItemAnimator());
        rcvListDrinks.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        rcvListDrinks.setAdapter(changeDishAdapter);

        RelativeLayout rlCompleted = (RelativeLayout) root.findViewById(R.id.rlCompleted);
        rlCompleted.setOnClickListener(this);
        RelativeLayout rlChangePage = (RelativeLayout) root.findViewById(R.id.rlChangePage);
        rlChangePage.setVisibility(isShow ? View.VISIBLE : View.GONE);
        rlChangePage.setOnClickListener(this);

        TextView tvChangeDrink = (TextView) root.findViewById(R.id.tvChangeDrink);
        ImageView imgChangeDrink = (ImageView) root.findViewById(R.id.imgChangeDrink);

        //Luong: snack -> drink -> ga ran -> burger -> snack
        if (activity.lstSnacksAndDessertChange != null && activity.lstSnacksAndDessertChange.size() > 0) { //Co view doi snack o vi tri 0, view doi drink o vi tri 1
            if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) {
                tvChangeDrink.setText(getString(R.string.garan_exchange));
                imgChangeDrink.setImageResource(R.drawable.ic_garan);
            } else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) { // k0 co doi ga ran nhung co doi burger
                tvChangeDrink.setText(getString(R.string.burger_exchange));
                imgChangeDrink.setImageResource(R.drawable.ic_burger);
            } else { // chi co 2 view doi snack va doi drink
                tvChangeDrink.setText(getString(R.string.snacks_exchange));
                imgChangeDrink.setImageResource(R.drawable.ic_change_snacks);
            }
        }
        else if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) { // k0 co snack -> drink o vi tri 0
            tvChangeDrink.setText(getString(R.string.garan_exchange));
            imgChangeDrink.setImageResource(R.drawable.ic_garan);
        }
        else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) { // k0 co snack, ga ran -> drink o vi tri 0
            tvChangeDrink.setText(getString(R.string.burger_exchange));
            imgChangeDrink.setImageResource(R.drawable.ic_burger);
        } else {
            tvChangeDrink.setText(getString(R.string.change_disk));
        }

        return root;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.rlCompleted:
                activity.backPreviousScreen(true);
                break;

            case R.id.rlChangePage:
                //Luong: snack -> drink -> ga ran -> burger -> snack
                if (activity.lstSnacksAndDessertChange != null && activity.lstSnacksAndDessertChange.size() > 0) { //Co view doi snack o vi tri 0, view doi drink o vi tri 1
                    if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) {
                        activity.viewPagerChangeDish.setCurrentItem(2); // chuyen toi view doi ga ran o vi tri 2
                    } else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) { // k0 co doi ga ran nhung co doi burger
                        activity.viewPagerChangeDish.setCurrentItem(2); // chuyen toi view doi burger o vi tri 2
                    } else { // chi co 2 view doi snack va doi drink
                        activity.viewPagerChangeDish.setCurrentItem(0); //luôn luôn ở vị trí 0
                    }
                }
                else if (activity.lstGaRanChange != null && activity.lstGaRanChange.size() > 0) { // k0 co snack -> drink o vi tri 0
                    activity.viewPagerChangeDish.setCurrentItem(1);
                }
                else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) { // k0 co snack, ga ran -> drink o vi tri 0
                    activity.viewPagerChangeDish.setCurrentItem(1);
                }
                break;
        }
    }
}
