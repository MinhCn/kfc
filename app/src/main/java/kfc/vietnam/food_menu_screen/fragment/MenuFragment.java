package kfc.vietnam.food_menu_screen.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.CategoryDataWrapper;
import kfc.vietnam.common.object.CategoryWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.MenuPagerAdapter;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 17/07/2016.
 */
public class MenuFragment extends Fragment {
    private String currentLanguageCode = "";
    private ArrayList<ProvinceWrapper> lstProvince;
    public final static int LOOPS = 1;
    public MenuPagerAdapter adapter;
    public ViewPager pager;
    public static int count = 0; //ViewPager items size
    /**
     * You shouldn't define first page = 0.
     * Let define firstpage = 'number viewpager size' to make endless carousel
     */
    public static int FIRST_PAGE = 0;
    private List<ImageView> dots;
    private FragmentActivity ctx;
    private LinearLayout dotsLayout;

    private ArrayList<CategoryWrapper> lstCategory;

    @Override
    public void onAttach(Context context) {
        ctx = (FragmentActivity) context;
        super.onAttach(context);

        MainActivity activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
        this.lstProvince = activity.lstProvince;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_menu_fragment, container, false);

        init(view);

        if (lstCategory == null) {
            getCategory(currentLanguageCode);
        } else {
            count = lstCategory.size();
            initViewPage();
            addDots();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    ((MainActivity) getActivity()).processChangeTopMenu(MenuFragment.class);
                }
            }
        });

        return anim;
    }

    private void initViewPage() {
        //set page margin between pages for viewpager
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        int pageMargin = ((metrics.widthPixels / 9) * 2);
        int pageMargin = ((metrics.widthPixels / 7) * 2);
        pager.setPageMargin(-pageMargin);

        adapter = new MenuPagerAdapter(getActivity(), ((MainActivity) getActivity()).menuFragment, getChildFragmentManager(), lstCategory, currentLanguageCode, lstProvince);
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        pager.addOnPageChangeListener(adapter);

        // Set current item to the middle page so we can fling to both
        // directions left and right
        pager.setCurrentItem(FIRST_PAGE);
        pager.setOffscreenPageLimit(3);
        if(MainActivity.isCombo){
            MainActivity.isCombo = false;
            Bundle b = new Bundle();
            b.putString("id_category",lstCategory.get(FIRST_PAGE).id+"");
            DetailsMenuFragment f = new DetailsMenuFragment();
            f.setArguments(b);
            ((MainActivity) ctx).changeFragment(f);
        }
    }

    private void init(View view) {
        pager = (ViewPager) view.findViewById(R.id.myviewpager);
        dotsLayout = (LinearLayout) view.findViewById(R.id.dots);
    }


    public void addDots() {
        dots = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            ImageView dot = new ImageView(getActivity());
            if (i == 0)
                dot.setImageDrawable(getResources().getDrawable(R.drawable.pager_dot_selected));
            else
                dot.setImageDrawable(getResources().getDrawable(R.drawable.pager_dot_not_selected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
//            params.setMargins(2, 0, 2, 10);
            params.setMargins(10, 0, 10, 0);
            dot.setLayoutParams(params);
            dotsLayout.addView(dot, params);
            dots.add(dot);
        }

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectDot(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void selectDot(int idx) {
        Resources res = getResources();
        for (int i = 0; i < count; i++) {
            int drawableId = (i == idx) ? (R.drawable.pager_dot_selected) : (R.drawable.pager_dot_not_selected);
            Drawable drawable = res.getDrawable(drawableId);
            dots.get(i).setImageDrawable(drawable);
        }
    }

    private void getCategory(String lang) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            if (ctx instanceof MainActivity)
                ((MainActivity)ctx).showNoInternetFragment();
            return;
        }

        Call<CategoryDataWrapper> call = ApiClient.getJsonClient().getCategory(lang);
        call.enqueue(new Callback<CategoryDataWrapper>() {

            @Override
            public void onFailure(Call<CategoryDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<CategoryDataWrapper> arg0, Response<CategoryDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    lstCategory = new ArrayList<CategoryWrapper>();
                    lstCategory = arg1.body().data.category;

                    //Phần sắp xếp cho combo nằm giữa list
//                    Comparator<CategoryWrapper> mComparator = new Comparator<CategoryWrapper>() {
//                        @Override
//                        public int compare(CategoryWrapper t0, CategoryWrapper t1) {
//                            return String.valueOf(t0.id).compareToIgnoreCase(String.valueOf(t1.id));
//                        }
//                    };
//                    Collections.sort(lstCategory, mComparator);
//                    CategoryWrapper obj = new CategoryWrapper();
//                    obj.id = 0;
//                    int index = Collections.binarySearch(lstCategory, obj, mComparator);
//                    if (index >= 0) {
//                        int size = lstCategory.size();
//                        int middle = (int) Math.floor(size / 2.0);
//
//                        //set current page
//                        FIRST_PAGE = middle;
//
//                        obj = lstCategory.remove(index);
//                        lstCategory.add(middle, obj);
//                    }

                    count = lstCategory.size();
                    initViewPage();
                    addDots();
                }
            }
        });
    }

}
