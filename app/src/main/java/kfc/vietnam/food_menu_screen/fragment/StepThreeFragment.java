package kfc.vietnam.food_menu_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.CategoryType;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.PaymentType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.CouponDataWrapper;
import kfc.vietnam.common.object.CouponPopupWrapper;
import kfc.vietnam.common.object.OrderDataWrapper;
import kfc.vietnam.common.object.OrderObject;
import kfc.vietnam.common.object.ProductBuyMoreModel;
import kfc.vietnam.common.object.ProductWrapper002;
import kfc.vietnam.common.object.ShoppingCartObject;
import kfc.vietnam.common.utility.DividerItemDecoration;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.ShoppingCartAdapter;
import kfc.vietnam.food_menu_screen.ui.OperationPolicyActivity;
import kfc.vietnam.main_screen.ui.LoginActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kfc.vietnam.R.id.rcvShoppingCart;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class StepThreeFragment extends Fragment implements View.OnClickListener {
    private static final int VERTICAL_ITEM_SPACE = 48;
    private String currentLanguageCode = "";
    private Context ctx;
    private LinearLayout llPaymentOnDelivery, llPaymentWithCard, llPaymentWithATM, llCancelOrder, llIAgreeToOrder;
    private RadioButton rdbPaymentWithCard, rdbPaymentOnDelivery, rdbPaymentWithATM;
    private ArrayList<ShoppingCartObject> lstShoppingCard = null;
    private ArrayList<ShoppingCartObject> lstShoppingCard_KhuyenMai = null;

    private AddressDataWrapper addressToShip;

    private TextView tvTotalPrice, tvReceivePoint, tvDiscount, tvDiscountCode, tvTotalAmount, tvTermsconditions, tvOnlinePaymentNotice;
    private RelativeLayout rlDiscountCode, rlDiscount;
    private ImageView imRemoveDiscountCode;

    private EditText edtCodeForSale;
    private LinearLayout llCouponContainer;

    private String mPoint = "0";
    private String mOrderID = "";

    private SharedPreferences preferencesShoppingCard;
    private SharedPreferences preferencesDeliverAddress;

    private MainActivity activity;

    private CheckBox chkTermsConditions;
    private double mTotalPrice = 0;
    //Added by Nhat Minh
    private double oldTotalPrice = 0;
    private double priceDiscount = 0;
    private double apiTotalPrice = 0;

    private RelativeLayout rlCouponPopup;
    private TextView tvCouponPopup;

    private String paymentLogContent = "";
    private boolean paymentOK = false;
    private ShoppingCartAdapter mAdapter;
    private TextView tvWard;
    private TextView tvShippingFee3;

    private RecyclerView rcvShoppingCart;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;

        preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
        preferencesDeliverAddress = ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE);

        try {
            lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null));
            lstShoppingCard_KhuyenMai = new ArrayList<ShoppingCartObject>();
            for (int i = 0; i < lstShoppingCard.size(); i++) {
                ShoppingCartObject shoppingCartObject = lstShoppingCard.get(i);
                if (shoppingCartObject.productSelected.khuyenmai.equals("1")) {
                    lstShoppingCard_KhuyenMai.add(shoppingCartObject);
                }
            }
            if (lstShoppingCard != null)
                Collections.sort(lstShoppingCard, new Comparator<ShoppingCartObject>() {
                    @Override
                    public int compare(ShoppingCartObject shoppingCartObject, ShoppingCartObject t1) {
                        return shoppingCartObject.idCart.compareToIgnoreCase(t1.idCart);
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (lstShoppingCard != null) {
            for (ShoppingCartObject obj : lstShoppingCard) {
                if (obj.dishSelected != null && obj.price_one_item != null) {
                    if (Double.valueOf(obj.dishSelected.price) != obj.price_one_item) {
                        obj.price_one_item = Double.valueOf(obj.dishSelected.price);

                    }
                }

            }
        }
    }

    private void customTextView(TextView view) {
        String i_agree_with = getContext().getResources().getText(R.string.i_agree_with).toString();
        String terms_conditions = getContext().getResources().getText(R.string.operation_policy_new).toString();
        String KFC_Vietnam_purchases = getContext().getResources().getText(R.string.KFC_Vietnam_purchases).toString();
        SpannableStringBuilder builder = new SpannableStringBuilder();
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(ctx, OperationPolicyActivity.class));

            }
        };

        SpannableString redSpannable = new SpannableString(i_agree_with);
        redSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, i_agree_with.length(), 0);
        builder.append(redSpannable);

        SpannableString whiteSpannable = new SpannableString(terms_conditions);
        whiteSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, terms_conditions.length(), 0);
        builder.append(whiteSpannable);
        builder.setSpan(clickableSpan, i_agree_with.length(), (i_agree_with.length() + terms_conditions.length()), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString blueSpannable = new SpannableString(KFC_Vietnam_purchases);
        blueSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, KFC_Vietnam_purchases.length(), 0);
        builder.append(blueSpannable);

        view.setText(builder, TextView.BufferType.SPANNABLE);
        view.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void customOnlinePaymentNoticeTextView(TextView view) {
        String notice = getContext().getResources().getText(R.string.notice).toString();
        String noticeContent = getContext().getResources().getText(R.string.online_payment_notice).toString();
        String more = getContext().getResources().getText(R.string.online_payment_more).toString();
        SpannableStringBuilder builder = new SpannableStringBuilder();
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(ctx, OperationPolicyActivity.class));

            }
        };

        SpannableString boldSpannable = new SpannableString(notice);
        StyleSpan boldStyleSpan = new StyleSpan(android.graphics.Typeface.BOLD);
        boldSpannable.setSpan(boldStyleSpan, 0, notice.length(), 0);
        builder.append(boldSpannable);

        SpannableString blackSpannable = new SpannableString(noticeContent);
        blackSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, noticeContent.length(), 0);
        builder.append(blackSpannable);

        SpannableString redSpannable = new SpannableString(more);
        redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, more.length(), 0);
        builder.append(redSpannable);
        builder.setSpan(clickableSpan, notice.length() + noticeContent.length(), (notice.length() + noticeContent.length() + more.length()), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        
        view.setText(builder, TextView.BufferType.SPANNABLE);
        view.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_step_three_fragment, container, false);

        try {
            addressToShip = (AddressDataWrapper) ObjectSerializer.deserialize(preferencesDeliverAddress.getString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), null));
        } catch (Exception e) {
            e.printStackTrace();
        }

        init(root);

        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(StepThreeFragment.class);
                }
            }
        });

        return anim;
    }

    public void updateDiscountCode() {
        double value = mTotalPrice;
        double fee = 0;
        if (ShoppingCartFragment.lastCouponResult != null) {
            //Added  by Nhat Minh
            if (activity.conf != null && activity.conf.coupon != null && activity.conf.coupon.checkCoupon) {
                rlDiscount.setVisibility(View.VISIBLE);
                rlDiscountCode.setVisibility(View.VISIBLE);
            } else {
                rlDiscount.setVisibility(View.GONE);
                rlDiscountCode.setVisibility(View.GONE);
            }

            if (ShoppingCartFragment.lastCouponResult.type == 1) {
                fee = ShoppingCartFragment.lastCouponResult.discount;
            } else {
                fee = (mTotalPrice * ShoppingCartFragment.lastCouponResult.discount) / 100;
                if (fee > ShoppingCartFragment.lastCouponResult.maxdiscount && ShoppingCartFragment.lastCouponResult.maxdiscount > 0)
                    fee = ShoppingCartFragment.lastCouponResult.maxdiscount;

            }
//            value = mTotalPrice - fee;
            value = mTotalPrice - fee + Double.parseDouble(activity.conf.point.shipPrice); //edit by @ManhNguyen
            String sDisCount = "";
            if (ShoppingCartFragment.lastCouponResult.type != 1) {
                if (fee < ShoppingCartFragment.lastCouponResult.maxdiscount) {
                    DecimalFormat myFormatter = new DecimalFormat("###,###");
                    sDisCount = "" + myFormatter.format(ShoppingCartFragment.lastCouponResult.discount);
                }
            }
            //Add by Nhat minh
            oldTotalPrice = mTotalPrice;
            priceDiscount = fee;
            ShoppingCartFragment.formatPrice(fee, tvDiscount, true, sDisCount);
            tvDiscountCode.setText(ShoppingCartFragment.lastCouponResult.code);
            // Update price
            apiTotalPrice = value;
            ShoppingCartFragment.formatPrice(value, tvTotalAmount, false, "");
        } else {
            //Added by Nhat Minh
            rlDiscount.setVisibility(View.GONE);
            rlDiscountCode.setVisibility(View.GONE);

            value = mTotalPrice + Double.parseDouble(activity.conf.point.shipPrice); //edit by @ManhNguyen
            apiTotalPrice = value;
            priceDiscount = 0;
            oldTotalPrice = 0;

            ShoppingCartFragment.formatPrice(0, tvDiscount, false, "");
            tvDiscountCode.setText(getResources().getString(R.string.msg_no));
            ShoppingCartFragment.formatPrice(value, tvTotalAmount, false, "");
            //imRemoveDiscountCode.setVisibility(View.GONE);
        }
    }

    private void init(View root) {
        Button btnChangeDeliveryAddress = (Button) root.findViewById(R.id.btnChangeDeliveryAddress);
        btnChangeDeliveryAddress.setOnClickListener(this);

        TextView tvFirstNameAndLastName = (TextView) root.findViewById(R.id.tvFirstNameAndLastName);
        TextView tvAddress = (TextView) root.findViewById(R.id.tvAddress);
        TextView tvPhoneNumber = (TextView) root.findViewById(R.id.tvPhoneNumber);
        TextView tvEmail = (TextView) root.findViewById(R.id.tvEmail);
        tvTermsconditions = (TextView) root.findViewById(R.id.tvTermsconditions);
        customTextView(tvTermsconditions);
        tvOnlinePaymentNotice = (TextView) root.findViewById(R.id.tvOnlinePaymentNotice);
        customOnlinePaymentNoticeTextView(tvOnlinePaymentNotice);

        rlDiscount = (RelativeLayout) root.findViewById(R.id.rlDiscount);
        rlDiscountCode = (RelativeLayout) root.findViewById(R.id.rlDiscountCode);
        tvDiscount = (TextView) root.findViewById(R.id.tvDiscount);
        tvDiscountCode = (TextView) root.findViewById(R.id.tvDiscountCode);
        imRemoveDiscountCode = (ImageView) root.findViewById(R.id.imRemoveDiscountCode);
        tvTotalAmount = (TextView) root.findViewById(R.id.tvTotalAmount);
        tvWard = (TextView) root.findViewById(R.id.tvWard);

        //Added by Nhat Minh
        rlCouponPopup = (RelativeLayout) root.findViewById(R.id.rlCouponPopup);
        tvCouponPopup = (TextView) root.findViewById(R.id.tvCouponPopup);
        if(ShoppingCartFragment.popupCouponResult != null) {
            rlCouponPopup.setVisibility(View.VISIBLE);
            tvCouponPopup.setText(Html.fromHtml(ShoppingCartFragment.popupCouponResult.getMessage()));
        } else {
            rlCouponPopup.setVisibility(View.GONE);
        }

        edtCodeForSale = (EditText) root.findViewById(R.id.edtCodeForSale);
        LinearLayout llAgree = (LinearLayout) root.findViewById(R.id.llAgree);
        llCouponContainer = (LinearLayout) root.findViewById(R.id.llCouponContainer);

        rlDiscountCode.setVisibility(View.GONE);
        rlDiscount.setVisibility(View.GONE);

        if(activity.conf != null && activity.conf.coupon != null && activity.conf.coupon.checkCoupon){
            llCouponContainer.setVisibility(View.VISIBLE);
            //rlDiscount.setVisibility(View.VISIBLE);
            //rlDiscountCode.setVisibility(View.VISIBLE);
        } else {
            llCouponContainer.setVisibility(View.GONE);
            rlDiscount.setVisibility(View.GONE);
            rlDiscountCode.setVisibility(View.GONE);
        }

        imRemoveDiscountCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShoppingCartFragment.lastCouponResult = null;
                updateDiscountCode();
                rlDiscount.setVisibility(View.GONE);
                rlDiscountCode.setVisibility(View.GONE);
            }
        });

        llAgree.setOnClickListener(this);
        //End by Nhat minh

        TextView tvCity = (TextView) root.findViewById(R.id.tvCity);
        TextView tvDistrict = (TextView) root.findViewById(R.id.tvDistrict);

        if (addressToShip != null) {
            tvFirstNameAndLastName.setText(getString(R.string.receiver) + ": " + addressToShip.name);
            tvAddress.setText(getString(R.string.StepOneFragment_005) + ": " + addressToShip.address);
            tvPhoneNumber.setText(getString(R.string.phone_number) + ": " + addressToShip.phone);
            tvEmail.setText(getString(R.string.email) + ": " + addressToShip.email);
            tvCity.setText(getString(R.string.city) + ": " + addressToShip.nameCity);
            tvDistrict.setText(getString(R.string.district) + ": " + addressToShip.nameDistrict);
            if (!TextUtils.isEmpty(addressToShip.nameWard))
                tvWard.setText(getString(R.string.ward) + ": " + addressToShip.nameWard);
        }

        rcvShoppingCart = (RecyclerView) root.findViewById(R.id.rcvShoppingCart);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ctx);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvShoppingCart.setNestedScrollingEnabled(false);
        rcvShoppingCart.setLayoutManager(layoutManager);
        //add ItemDecoration
        //rcvShoppingCart.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //or
        //rcvShoppingCart.addItemDecoration(new DividerItemDecoration(getActivity()));
        //or
        rcvShoppingCart.addItemDecoration(new DividerItemDecoration(ctx, R.drawable.divider_listview));
        if (lstShoppingCard != null) {
            mAdapter = new ShoppingCartAdapter(ctx, lstShoppingCard, null, onclickChangeValue);
            rcvShoppingCart.setAdapter(mAdapter);
        }

        chkTermsConditions = (CheckBox) root.findViewById(R.id.chkTerms_Conditions);

        llIAgreeToOrder = (LinearLayout) root.findViewById(R.id.llIAgreeToOrder);
        llIAgreeToOrder.setOnClickListener(this);

        llPaymentOnDelivery = (LinearLayout) root.findViewById(R.id.llPaymentOnDelivery);
        llPaymentWithCard = (LinearLayout) root.findViewById(R.id.llPaymentWithCard);
        llPaymentWithATM = (LinearLayout) root.findViewById(R.id.llPaymentWithATM);
        llPaymentWithCard.setOnClickListener(this);
        llPaymentOnDelivery.setOnClickListener(this);
        llPaymentWithATM.setOnClickListener(this);

        rdbPaymentWithCard = (RadioButton) root.findViewById(R.id.rdbPaymentWithCard);
        rdbPaymentOnDelivery = (RadioButton) root.findViewById(R.id.rdbPaymentOnDelivery);
        rdbPaymentWithATM = (RadioButton) root.findViewById(R.id.rdbPaymentWithATM);
        rdbPaymentWithCard.setEnabled(false);
        rdbPaymentOnDelivery.setEnabled(false);
        rdbPaymentWithATM.setEnabled(false);

        String onlinePayment = ((MainActivity) getActivity()).currentProvinceSelected.payment;
        if(onlinePayment.equals("1")){
            enableOnlinePayment();
        }

        tvTotalPrice = (TextView) root.findViewById(R.id.tvTotalPrice);
        tvReceivePoint = (TextView) root.findViewById(R.id.tvReceivePoint);
        updateShowTotalPrice();

        llCancelOrder = (LinearLayout) root.findViewById(R.id.llCancelOrder);
        llCancelOrder.setOnClickListener(this);
        root.findViewById(R.id.tvNameReceived).setOnClickListener(this);
        updateDiscountCode();

        //add by @ManhNguyen
        tvShippingFee3 = (TextView) root.findViewById(R.id.tvShippingFee3);
        if (activity.conf.point.shipPrice.equalsIgnoreCase("0")){
            tvShippingFee3.setText(activity.getString(R.string.free).toUpperCase());
        }
        else {
            tvShippingFee3.setText(formatMinPrice(Double.parseDouble(activity.conf.point.shipPrice)).replace(",", "."));
        }

        //Add by Nhat Minh
        //checkShowCouponPopup();
    }

    private String formatMinPrice(double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(totalPrice).replace(".", ",");
            return output + " " + getString(R.string.vnd_cap);
        } else
            return "0 " + getString(R.string.vnd_cap);
    }

    public void enableOnlinePayment() {
        tvOnlinePaymentNotice.setVisibility(View.VISIBLE);
        llPaymentWithCard.setVisibility(View.VISIBLE);
        llPaymentWithATM.setVisibility(View.VISIBLE);
    }

    public void updateShowTotalPrice() {
        double totalPrice = 0;
        int count_cart = 0;
        if (lstShoppingCard != null) {
            for (ShoppingCartObject objShoppingCart : lstShoppingCard) {
//                gTotalPrice += Double.parseDouble(obj.dishSelected.price) * Integer.parseInt(obj.amount);
//                gTotalPrice += obj.price_one_item * Integer.parseInt(obj.amount);
                double price = Double.parseDouble(objShoppingCart.dishSelected.price);
                if (objShoppingCart.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                    int countElement = objShoppingCart.count_element;
                    if (countElement == 0) {
                        if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        //add by @ManhNguyen
                        if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }
                    } else if (countElement == 1) {
                        if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        //add by @ManhNguyen
                        if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }
                    } else if (countElement == 2) {
                        if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        //add by @ManhNguyen
                        if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }

                        if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj.price);
                            }
                    }
                }

                if (objShoppingCart.listProductBuyMoreModel != null){
                    if (objShoppingCart.listProductBuyMoreModel.size() > 0){
                        for (ProductBuyMoreModel temp : objShoppingCart.listProductBuyMoreModel) {
                            price += Double.parseDouble(temp.price);
                        }
                    }
                }

                totalPrice += price * Integer.parseInt(objShoppingCart.amount);
                count_cart += Integer.parseInt(objShoppingCart.amount);
            }
        }
        mTotalPrice = totalPrice;
        showTotalPrice(totalPrice);
        /*if (oldTotalPrice >= 1000000 && mTotalPrice < 1000000) {
            //remove
            ShoppingCartFragment.lastCouponResult = null;
            showTotalPrice(mTotalPrice);
            ShoppingCartFragment.formatPrice(mTotalPrice, tvTotalPrice, false, "");
            rlDiscount.setVisibility(View.GONE);
            rlDiscountCode.setVisibility(View.GONE);
            oldTotalPrice = 0;
        } else {
            oldTotalPrice = mTotalPrice;
        }*/
        activity.tvAmountShoppingCart.setText(count_cart + "");
    }

    private void showTotalPrice(double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("#,##0");
            String output = myFormatter.format(totalPrice);
            try {
                output = output.replace(".", ",");
            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList<String> lst = new ArrayList(Arrays.asList(output.split(",")));
            if (lst.size() >= 2)
                lst.remove(lst.size() - 1);
            output = lst.toString().replace("[", "").replace("]", "").replace(" ", "") + ".000 " + getString(R.string.vnd_cap);

            int start = 0;
            int end = output.indexOf(".");
            output = output.replace(",", ".");

            SpannableString spanContent = new SpannableString(output);
            spanContent.setSpan(new AbsoluteSizeSpan(18, true), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            //spanContent.setSpan(new RelativeSizeSpan(2f), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set style
            spanContent.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
            tvTotalPrice.setText(spanContent);

            mPoint = activity.calculatePoint(totalPrice) + "";
            tvReceivePoint.setText(mPoint);
        } else {
            tvTotalPrice.setText("0 " + getString(R.string.vnd_cap));
            tvReceivePoint.setText("0");
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnChangeDeliveryAddress:
                if (activity.userInfo != null) {
                    activity.changeFragment(new ChangeDeliveryAddressFragment());
                } else {
                    //add by @ManhNguyen
                    activity.setChangeAddressStep3();
                    activity.changeFragment(new StepOneFragment());
                }

                break;

            case R.id.llIAgreeToOrder:
                if (chkTermsConditions.isChecked())
                    if (mTotalPrice >= Double.parseDouble(activity.conf.point.minPrice)) {
                        processStartOrder();
                    } else {
                            DecimalFormat myFormatter = new DecimalFormat("###,###");
                            String output = myFormatter.format(Double.parseDouble(activity.conf.point.minPrice)).replace(".", ",");

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                            alertDialog.setMessage(activity.getString(R.string.Minimum_Order_Alert) + " " + output.replace(",", ".") + " " + getString(R.string.vnd_cap));

                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    arg0.dismiss();
                                }
                            });

                            AlertDialog dialog = alertDialog.create();
                            dialog.show();
                    }
                else
                    Toast.makeText(ctx, ctx.getString(R.string.StepThreeFragment_019), Toast.LENGTH_SHORT).show();
                break;

            case R.id.llPaymentOnDelivery:
                rdbPaymentOnDelivery.setChecked(true);
                rdbPaymentWithCard.setChecked(false);
                rdbPaymentWithATM.setChecked(false);
                break;

            case R.id.llPaymentWithCard:
                rdbPaymentOnDelivery.setChecked(false);
                rdbPaymentWithCard.setChecked(true);
                rdbPaymentWithATM.setChecked(false);
                break;

            case R.id.llPaymentWithATM:
                rdbPaymentOnDelivery.setChecked(false);
                rdbPaymentWithCard.setChecked(false);
                rdbPaymentWithATM.setChecked(true);
                break;

            case R.id.llCancelOrder:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                // set title
                alertDialogBuilder.setTitle(getString(R.string.StepThreeFragment_015));
                // set dialog message
                alertDialogBuilder
                        .setMessage(getString(R.string.StepThreeFragment_018))
                        .setCancelable(false)
                        .setNegativeButton(getString(R.string.StepThreeFragment_017), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton(getString(R.string.StepThreeFragment_016), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked
                                if (processEndPayment())
                                    activity.backToMainMenu();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
                break;
            case R.id.tvNameReceived:
                ((MainActivity) ctx).gotoMenu3();
                break;

            case R.id.llAgree:
                if (activity.userInfo != null) {
                    final String code = edtCodeForSale.getText().toString();
                    if (!code.isEmpty()) {
                        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                        double couponAmount = mTotalPrice;
                        if (ShoppingCartFragment.lastCouponResult != null) couponAmount += ShoppingCartFragment.lastCouponResult.discount;

                        Log.e("code", code);
                        Log.e("amount", String.valueOf(couponAmount));

                        //Register new discount code
                        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
                        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
                        mProgressDialog.setCancelable(true);
                        mProgressDialog.setCanceledOnTouchOutside(false);
                        mProgressDialog.show();

                        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
                            mProgressDialog.dismiss();
                            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
                            activity.showNoInternetFragment();
                            return;
                        }

                        if (lstShoppingCard_KhuyenMai != null && lstShoppingCard_KhuyenMai.size() > 0) {
                            String nameProduct =getString(R.string.HaveCouponOther);
                            for (int i = 0; i < lstShoppingCard_KhuyenMai.size(); i++) {
                                ShoppingCartObject shoppingCartObject = lstShoppingCard_KhuyenMai.get(i);
                                nameProduct = nameProduct + "\n    - " + shoppingCartObject.productSelected.nameProduct;
                            }

                            android.app.AlertDialog.Builder alertDialogBuilderCall = new android.app.AlertDialog.Builder(ctx);
                            alertDialogBuilderCall
                                    .setMessage(nameProduct)
                                    .setCancelable(false)
                                    .setNegativeButton(getString(R.string.Yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // if this button is clicked, just close
                                            // the dialog box and do nothing
                                            for (int i = 0; i < lstShoppingCard_KhuyenMai.size(); i++) {
                                                ShoppingCartObject shoppingCartObject = lstShoppingCard_KhuyenMai.get(i);
                                                for (int j = 0; j < lstShoppingCard.size(); j++) {
                                                    ShoppingCartObject shoppingCartObject2 = lstShoppingCard.get(j);
                                                    if (shoppingCartObject.idCart.equals(shoppingCartObject2.idCart)) {
                                                        lstShoppingCard.remove(shoppingCartObject2);
                                                    }
                                                }
                                            }
                                            try {
                                                SharedPreferences preferencesShoppingCart = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
                                                preferencesShoppingCart.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                                                StepThreeFragment.this.lstShoppingCard = lstShoppingCard;
                                                mAdapter = new ShoppingCartAdapter(ctx, lstShoppingCard, null, onclickChangeValue);
                                                rcvShoppingCart.setAdapter(mAdapter);
                                                mAdapter.notifyDataSetChanged();
                                                updateShowTotalPrice();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            dialog.cancel();
                                        }
                                    })
                                    .setPositiveButton(getString(R.string.No_Thanks), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    });
                            // create alert dialog
                            android.app.AlertDialog alertDialogCall = alertDialogBuilderCall.create();
                            // show it
                            alertDialogCall.show();
                            mProgressDialog.dismiss();

                            return;
                        }


                        Call<CouponDataWrapper> call1 = ApiClient.getJsonClient().checkCoupon(code,
                                (activity.userInfo != null) ? activity.userInfo.id : "",
                                (activity.userInfo != null) ? new String(Hex.encodeHex(DigestUtils.md5(activity.userInfo.id))) : "",
                                String.valueOf(couponAmount), currentLanguageCode);
                        call1.enqueue(new Callback<CouponDataWrapper>() {
                            @Override
                            public void onFailure(Call<CouponDataWrapper> arg0, Throwable arg1) {
                                edtCodeForSale.requestFocus();
                                edtCodeForSale.setError(getString(R.string.ShoppingCartFragment_Request_Error));
                                edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                mProgressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call<CouponDataWrapper> arg0, Response<CouponDataWrapper> arg1) {
                                mProgressDialog.dismiss();
                                // Success
                                if (arg1.isSuccessful()) {
                                    CouponDataWrapper respone = arg1.body();
                                    if (respone.result) {
                                        ShoppingCartFragment.lastCouponResult = respone.getData();
                                        tvDiscountCode.setText(code);
                                        //showTotalPrice(mTotalPrice);
                                        updateDiscountCode();
                                        edtCodeForSale.setText("");
                                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(edtCodeForSale.getWindowToken(), 0);
                                    } else {
                                        edtCodeForSale.requestFocus();
                                        edtCodeForSale.setError(respone.message);
                                        edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                    }
                                } else {
                                    edtCodeForSale.requestFocus();
                                    edtCodeForSale.setError(getString(R.string.ShoppingCartFragment_Request_Error));
                                    edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                }
                            }
                        });
                    } else {
                        edtCodeForSale.requestFocus();
                        edtCodeForSale.setError(getString(R.string.ShoppingCartFragment_001));
                        edtCodeForSale.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                    }
                } else {
                    //Toast.makeText(ctx, getString(R.string.ShoppingCartAdapter_004), Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ctx, LoginActivity.class);
                    i.putExtra("login_area", LoginAreaType.LOGIN_IN_SHOPPING_CART.toString());
                    ((FragmentActivity) ctx).startActivityForResult(i, RequestResultCodeType.REQUEST_CODE_LOGIN_IN_SHOPPING_CART.getValue());
                }
                break;
        }
    }

    private void processStartOrder() {
        if (lstShoppingCard != null && addressToShip != null) {
            Comparator<ProductWrapper002> mComparator = new Comparator<ProductWrapper002>() {
                @Override
                public int compare(ProductWrapper002 productWrapper002, ProductWrapper002 t1) {
                    return String.valueOf(productWrapper002.id).compareToIgnoreCase(String.valueOf(t1.id));
                }
            };
            ArrayList<OrderObject> lstCart = new ArrayList<OrderObject>();
            for (ShoppingCartObject obj : lstShoppingCard) {
                ArrayList<String> lstChangeID = new ArrayList<>();
                ArrayList<String> lstComboProductID = new ArrayList<>();
                ArrayList<String> lstGroupID = new ArrayList<>();

                if (obj.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                    //Phần xử lý món mặc định
                    for (ProductWrapper002 df : obj.lstProductDefault) {
                        lstChangeID.add(df.id + "|" + df.id);
                        lstComboProductID.add(df.combo_product_id + "");
                        lstGroupID.add(df.group_id + "");
                    }

                    //Phần xử lý 2 món đầu tiên
                    if (obj.combo1 != null) {
                        lstChangeID.add(obj.combo1.id + "|" + obj.combo1.id);
                        lstComboProductID.add(obj.combo1.combo_product_id + "");
                        lstGroupID.add(obj.combo1.group_id + "");
                    }
                    if (obj.combo2 != null) {
                        lstChangeID.add(obj.combo2.id + "|" + obj.combo2.id);
                        lstComboProductID.add(obj.combo2.combo_product_id + "");
                        lstGroupID.add(obj.combo2.group_id + "");
                    }

                    //Phần xử lý món ăn nhẹ
                    if (obj.lstSnacksAndDessertSelected != null && obj.lstSnacksAndDessertDefault != null){
                        if (obj.lstSnacksAndDessertSelected.size() == obj.lstSnacksAndDessertDefault.size()) {
                            for (int i = 0; i < obj.lstSnacksAndDessertDefault.size(); i++) {
                                //vì 2 list dưới được sắp xếp các item ở index tương ứng nên ta có thể get cùng index
                                ProductWrapper002 p = obj.lstSnacksAndDessertDefault.get(i);
                                ProductWrapper002 pSelected = obj.lstSnacksAndDessertSelected.get(i);
                                lstChangeID.add(p.id + "|" + pSelected.id);

                                //kiểm tra nếu ko có id thì có nghĩa là đã change sản phẩm vì trong change list server api ko có trả về id
                                if (pSelected.combo_product_id == -1 && pSelected.group_id == -1) {
                                    //get thông tin này từ trong list default: bằng cách lấy parent field đem get trong default list
                                    ProductWrapper002 objSearch = new ProductWrapper002();
                                    objSearch.id = Integer.parseInt(pSelected.parent);
                                    int index = Collections.binarySearch(obj.lstSnacksAndDessertDefault, objSearch, mComparator);
                                    if (index >= 0) {
                                        objSearch = obj.lstSnacksAndDessertDefault.get(index);
                                        lstComboProductID.add(objSearch.combo_product_id + "");
                                        lstGroupID.add(objSearch.group_id + "");
                                    }
                                } else {
                                    lstComboProductID.add(pSelected.combo_product_id + "");
                                    lstGroupID.add(pSelected.group_id + "");
                                }
                            }
                        } else
                            Log.e(Tag.LOG.getValue(), "List Snacks And Dessert Selected != List Snacks And Dessert Default");
                    }

                    //Phần xử lý nước uống
                    if (obj.lstDrinkSelected != null && obj.lstDrinkDefault != null) {
                        if (obj.lstDrinkSelected.size() == obj.lstDrinkDefault.size()) {
                            for (int i = 0; i < obj.lstDrinkDefault.size(); i++) {
                                //vì 2 list dưới được sắp xếp các item ở index tương ứng nên ta có thể get cùng index
                                ProductWrapper002 p = obj.lstDrinkDefault.get(i);
                                ProductWrapper002 pSelected = obj.lstDrinkSelected.get(i);
                                lstChangeID.add(p.id + "|" + pSelected.id);

                                //kiểm tra nếu ko có id thì có nghĩa là đã change sản phẩm vì trong change list server api ko có trả về id
                                if (pSelected.combo_product_id == -1 && pSelected.group_id == -1) {
                                    //get thông tin này từ trong list default: bằng cách lấy parent field đem get trong default list
                                    ProductWrapper002 objSearch = new ProductWrapper002();
                                    objSearch.id = Integer.parseInt(pSelected.parent);
                                    objSearch.combo_product_id = pSelected.combo_product_id;
                                    int index = Collections.binarySearch(obj.lstDrinkDefault, objSearch, mComparator);
                                    if (index >= 0) {
                                        objSearch = obj.lstDrinkDefault.get(index);
                                        lstComboProductID.add(objSearch.combo_product_id + "");
                                        lstGroupID.add(objSearch.group_id + "");
                                    }
                                } else {
                                    lstComboProductID.add(pSelected.combo_product_id + "");
                                    lstGroupID.add(pSelected.group_id + "");
                                }
                            }
                        } else
                            Log.e(Tag.LOG.getValue(), "List Drink Selected != List Drink Default");
                    }

                    //add by @ManhNguyen
                    //Phần xử lý ga ran
                    if (obj.lstGaRanSelected != null && obj.lstGaRanDefault != null) {
                        if (obj.lstGaRanSelected.size() == obj.lstGaRanDefault.size()) {
                            for (int i = 0; i < obj.lstGaRanDefault.size(); i++) {
                                //vì 2 list dưới được sắp xếp các item ở index tương ứng nên ta có thể get cùng index
                                ProductWrapper002 p = obj.lstGaRanDefault.get(i);
                                ProductWrapper002 pSelected = obj.lstGaRanSelected.get(i);
                                lstChangeID.add(p.id + "|" + pSelected.id);

                                //kiểm tra nếu ko có id thì có nghĩa là đã change sản phẩm vì trong change list server api ko có trả về id
                                if (pSelected.combo_product_id == -1 && pSelected.group_id == -1) {
                                    //get thông tin này từ trong list default: bằng cách lấy parent field đem get trong default list
                                    ProductWrapper002 objSearch = new ProductWrapper002();
                                    objSearch.id = Integer.parseInt(pSelected.parent);
                                    objSearch.combo_product_id = pSelected.combo_product_id;
                                    int index = Collections.binarySearch(obj.lstGaRanDefault, objSearch, mComparator);
                                    if (index >= 0) {
                                        objSearch = obj.lstGaRanDefault.get(index);
                                        lstComboProductID.add(objSearch.combo_product_id + "");
                                        lstGroupID.add(objSearch.group_id + "");
                                    }
                                } else {
                                    lstComboProductID.add(pSelected.combo_product_id + "");
                                    lstGroupID.add(pSelected.group_id + "");
                                }
                            }
                        } else
                            Log.e(Tag.LOG.getValue(), "List Ga ran Selected != List ga ran Default");
                    }

                    //Phần xử lý burger
                    if (obj.lstBurgerComSelected != null && obj.lstBurgerComDefault != null) {
                        if (obj.lstBurgerComSelected.size() == obj.lstBurgerComDefault.size()) {
                            for (int i = 0; i < obj.lstBurgerComDefault.size(); i++) {
                                //vì 2 list dưới được sắp xếp các item ở index tương ứng nên ta có thể get cùng index
                                ProductWrapper002 p = obj.lstBurgerComDefault.get(i);
                                ProductWrapper002 pSelected = obj.lstBurgerComSelected.get(i);
                                lstChangeID.add(p.id + "|" + pSelected.id);

                                //kiểm tra nếu ko có id thì có nghĩa là đã change sản phẩm vì trong change list server api ko có trả về id
                                if (pSelected.combo_product_id == -1 && pSelected.group_id == -1) {
                                    //get thông tin này từ trong list default: bằng cách lấy parent field đem get trong default list
                                    ProductWrapper002 objSearch = new ProductWrapper002();
                                    objSearch.id = Integer.parseInt(pSelected.parent);
                                    objSearch.combo_product_id = pSelected.combo_product_id;
                                    int index = Collections.binarySearch(obj.lstBurgerComDefault, objSearch, mComparator);
                                    if (index >= 0) {
                                        objSearch = obj.lstBurgerComDefault.get(index);
                                        lstComboProductID.add(objSearch.combo_product_id + "");
                                        lstGroupID.add(objSearch.group_id + "");
                                    }
                                } else {
                                    lstComboProductID.add(pSelected.combo_product_id + "");
                                    lstGroupID.add(pSelected.group_id + "");
                                }
                            }
                        } else
                            Log.e(Tag.LOG.getValue(), "List burger Selected != List burger Default");
                    }
                }

                ArrayList<String> listProductBuyMore = new ArrayList<>();
                double price_buy_more = 0.0;
                if (obj.listProductBuyMoreModel != null){
                    if (obj.listProductBuyMoreModel.size() > 0){
                        for (ProductBuyMoreModel temp:obj.listProductBuyMoreModel){
                            if (temp.price != null && temp.price != ""){
                                price_buy_more += Double.parseDouble(temp.price);
                            } else {
                                Log.d("pricebuymore_null", "");
                            }
                            listProductBuyMore.add(temp.id + "");
                        }
                    }
                }

                OrderObject objOrder = new OrderObject();
                objOrder.quantity = obj.amount;
                // objOrder.price = obj.dishSelected.price;
                double price = Double.parseDouble(obj.dishSelected.price);
                if (obj.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                    int countElement = obj.count_element;
                    if (countElement == 0) {
                        if (obj.lstDrinkSelected != null && obj.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        if (obj.lstSnacksAndDessertSelected != null && obj.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        //add by @ManhNguyen
                        if (obj.lstGaRanSelected != null && obj.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        if (obj.lstBurgerComSelected != null && obj.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }
                    } else if (countElement == 1) {
                        if (obj.lstDrinkSelected != null && obj.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        if (obj.lstSnacksAndDessertSelected != null && obj.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        //add by @ManhNguyen
                        if (obj.lstGaRanSelected != null && obj.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        if (obj.lstBurgerComSelected != null && obj.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }
                    } else if (countElement == 2) {
                        if (obj.lstDrinkSelected != null && obj.lstDrinkSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstDrinkSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        if (obj.lstSnacksAndDessertSelected != null && obj.lstSnacksAndDessertSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstSnacksAndDessertSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        //add by @ManhNguyen
                        if (obj.lstGaRanSelected != null && obj.lstGaRanSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstGaRanSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }

                        if (obj.lstBurgerComSelected != null && obj.lstBurgerComSelected.size() > 0)
                            for (ProductWrapper002 obj1 : obj.lstBurgerComSelected) {
                                price = price + Double.parseDouble(obj1.price);
                            }
                    }

                }
                //edit by @ManhNguyen
                objOrder.price = String.valueOf(price + price_buy_more);

                objOrder.productBuyMore = listProductBuyMore;

                if (obj.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue()))
                    objOrder.type = "1";//combo
                else
                    objOrder.type = "2";//product
                objOrder.id = obj.dishSelected.id;
                objOrder.change_product = lstChangeID;
                objOrder.combo_product_id = lstComboProductID;
                objOrder.group_id = lstGroupID;

                lstCart.add(objOrder);
            }

            Gson gson = new GsonBuilder().create();
            JsonArray arrCart = gson.toJsonTree(lstCart).getAsJsonArray();
            if (activity.userInfo != null) {
                order(currentLanguageCode, addressToShip.city, activity.userInfo.id, arrCart.toString(), addressToShip.name, addressToShip.address,
                        addressToShip.district, addressToShip.ward, addressToShip.phone, addressToShip.email, " ");
            } else {
                //String userID ="" (không đăng nhâp)
                order(currentLanguageCode, addressToShip.city, "", arrCart.toString(), addressToShip.name, addressToShip.address,
                        addressToShip.district, addressToShip.ward, addressToShip.phone, addressToShip.email, "");
            }

        } else
            Log.e(Tag.LOG.getValue(), "Shopping cart and ship address is null");
    }

    private boolean processEndPayment() {
        activity.tvAmountShoppingCart.setText("0");
        lstShoppingCard.clear();
        return preferencesShoppingCard.edit().remove(SharedPreferenceKeyType.SHOPPING_CART.toString()).commit() && preferencesDeliverAddress.edit().remove(SharedPreferenceKeyType.DELIVER_ADDRESS.toString()).commit();
    }

    private void order(final String lang,
                       final String city,
                       final String userID,
                       String cart,
                       String name,
                       String address,
                       String district,
                       String ward,
                       String phone,
                       String email,
                       String note) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Log.d("OLD_TOTAL: ", String.valueOf(oldTotalPrice));
        Log.d("TOTAL: ", String.valueOf(apiTotalPrice));
        Log.d("TOTAL_DISCOUNT: ", String.valueOf(priceDiscount));


        String discount = ShoppingCartFragment.lastCouponResult != null ? String.valueOf(ShoppingCartFragment.lastCouponResult.discount) : "";
        String maxdiscount = ShoppingCartFragment.lastCouponResult != null ? String.valueOf(ShoppingCartFragment.lastCouponResult.maxdiscount) : "";
        String maxorder = ShoppingCartFragment.lastCouponResult != null ? String.valueOf(ShoppingCartFragment.lastCouponResult.maxorder) : "";
        String minorder = ShoppingCartFragment.lastCouponResult != null ? String.valueOf(ShoppingCartFragment.lastCouponResult.minorder) : "";
        String type = ShoppingCartFragment.lastCouponResult != null ? String.valueOf(ShoppingCartFragment.lastCouponResult.type) : "";
        String code = ShoppingCartFragment.lastCouponResult != null ? String.valueOf(ShoppingCartFragment.lastCouponResult.code) : "";
        String platform = "3";
        String mobile_platform = "2";
        //Add by Nhat Minh
        String oldTotal = String.valueOf(oldTotalPrice);
        String total = String.valueOf(apiTotalPrice);
        String pricediscount = String.valueOf(priceDiscount);

        Call<OrderDataWrapper> call1 = ApiClient.getJsonClient().order(lang,
                city,
                userID,
                cart,
                name,
                address,
                district,
                ward,
                phone,
                email,
                note,
                discount,
                maxdiscount,
                maxorder,
                minorder,
                type,
                code,
                platform,
                mobile_platform,
                city,
                oldTotal,
                total,
                pricediscount
        );
        call1.enqueue(new Callback<OrderDataWrapper>() {

            @Override
            public void onFailure(Call<OrderDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();

                Toast.makeText(ctx, getString(R.string.StepThreeFragment_006), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<OrderDataWrapper> arg0, Response<OrderDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    OrderDataWrapper obj = arg1.body();
//                    Log.i(Tag.LOG.getValue(), "Order: " + obj.toString());
                    if (obj.result) {
                        mOrderID = obj.data.orderID;

                        //payment with credit card
//                        if (rdbPaymentWithCard.isChecked()) {
//                            Bundle b = new Bundle();
//                            b.putString("order_id", mOrderID);
//                            b.putString("total_price", obj.data.priceTotal + "");
//                            b.putString("point", mPoint);
//
//                            PaymentFragment f = new PaymentFragment();
//                            f.setArguments(b);
//                            activity.changeFragment(f);
//
//                        } else if (rdbPaymentWithATM.isChecked())  { //Payment with ATM
//                            Bundle b = new Bundle();
//                            b.putString("order_id", mOrderID);
//                            b.putString("total_price", obj.data.priceTotal + "");
//                            b.putString("point", mPoint);
//
//                            PaymentWithATMFragment f = new PaymentWithATMFragment();
//                            f.setArguments(b);
//                            activity.changeFragment(f);
//                        } else {
                            payment(userID, lang, city, PaymentType.PAYMENT_AT_HOME.getValue(), "", "", mOrderID, paymentLogContent);
//                        }
                        activity.tvAmountShoppingCart.setText(0 + "");
                    } else {
//                        Log.e(Tag.LOG.getValue(), "Order FAIL");
                        Toast.makeText(ctx, getString(R.string.StepThreeFragment_005), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void payment(String user,
                         String lang,
                         String city,
                         String paymentType,
                         String requestId,
                         String encryptedPaymentData,
                         final String orderID, String paymentLog) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<OrderDataWrapper> call1 = ApiClient.getJsonClient().payment(user,
                lang,
                city,
                paymentType,
                requestId,
                encryptedPaymentData,
                orderID, paymentLog);
        call1.enqueue(new Callback<OrderDataWrapper>() {

            @Override
            public void onFailure(Call<OrderDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
                showErrorPayment();
            }

            @Override
            public void onResponse(Call<OrderDataWrapper> arg0, Response<OrderDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    OrderDataWrapper obj = arg1.body();
                    if (obj.result) {
                        if (processEndPayment()) {
                            activity.tvAmountShoppingCart.setText(0 + "");
                            Bundle b = new Bundle();
                            b.putString("order_id", mOrderID);
                            b.putString("point", mPoint);

                            OrderStatusFragment f = new OrderStatusFragment();
                            f.setArguments(b);
                            activity.changeFragment(f);
                        }
                    } else {
                        MainActivity activity = (MainActivity) getActivity();
                        activity.backToMainMenu();
                        activity.processChangeTopMenu(MenuFragment.class);
                        Errorpayment f = new Errorpayment();
                        activity.changeFragment(f);
                    }
                }
            }
        });
    }

    private ShoppingCartAdapter.OnclickChangeValue onclickChangeValue = new ShoppingCartAdapter.OnclickChangeValue() {
        @Override
        public void valueChange(boolean isPlus, int post) {
            ShoppingCartObject objShoppingCart = lstShoppingCard.get(post);
            int sValue = Integer.valueOf(objShoppingCart.amount);
            if (isPlus)
                sValue = sValue + 1;
            else {
                if (sValue == 1)
                    sValue = 1;
                else
                    sValue = sValue - 1;
            }
            objShoppingCart.amount = "" + sValue;
            lstShoppingCard.set(post, objShoppingCart);
            try {
                preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mAdapter.notifyDataSetChanged();
            updateShowTotalPrice();
            updateDiscountCode();

        }

        @Override
        public void likeValue(boolean isLike, int pos) {

        }

        @Override
        public void refresh(ArrayList<ShoppingCartObject> lstShoppingCard) {
            StepThreeFragment.this.lstShoppingCard = lstShoppingCard;

            mAdapter.removeAllItem();

            mAdapter = new ShoppingCartAdapter(ctx, lstShoppingCard, null, onclickChangeValue);
            rcvShoppingCart.setAdapter(mAdapter);
        }
    };

    private void showErrorPayment() {
        MainActivity activity = (MainActivity) getActivity();
        activity.backToMainMenu();
        activity.processChangeTopMenu(MenuFragment.class);
        Errorpayment f = new Errorpayment();
        activity.changeFragment(f);
    }
}
