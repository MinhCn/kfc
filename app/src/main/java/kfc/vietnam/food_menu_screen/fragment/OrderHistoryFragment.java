package kfc.vietnam.food_menu_screen.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.OrderHistoryDataWrapper;
import kfc.vietnam.food_menu_screen.adapter.OrderHistoryAdapter;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class OrderHistoryFragment extends Fragment {
    private Context ctx;
    private MainActivity activity;
    private ArrayList<OrderHistoryDataWrapper.OrderHistoryWrapper> lstData;
    private LinearLayout layoutEmpty, layoutImEmpty;
    private LinearLayout imgBack;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;

        lstData = (ArrayList<OrderHistoryDataWrapper.OrderHistoryWrapper>) getArguments().getSerializable("transaction_history_data");
        if (lstData == null)
            lstData = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_order_history, container, false);
        ListView lvwOrderHistory = (ListView) root.findViewById(R.id.lvwOrderHistory);
        layoutEmpty = (LinearLayout) root.findViewById(R.id.layout_empty);
        if (lstData != null && lstData.size() != 0) {
            OrderHistoryAdapter adapter = new OrderHistoryAdapter(ctx, R.layout.custom_item_order_history, lstData);
            lvwOrderHistory.setAdapter(adapter);
            lvwOrderHistory.setOnItemClickListener(adapter);
        } else {
            lvwOrderHistory.setVisibility(View.GONE);
            layoutEmpty.setVisibility(View.VISIBLE);
        }
        imgBack = (LinearLayout) root.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                Log.d(Tag.LOG.getValue(), "Animation ended.");
//                if (enter) {
//                    activity.processChangeTopMenu(OrderHistoryFragment.class);
//                }
            }
        });

        return anim;
    }


}
