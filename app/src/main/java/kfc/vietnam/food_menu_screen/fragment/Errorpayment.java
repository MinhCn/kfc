package kfc.vietnam.food_menu_screen.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by hdadmin on 11/28/2016.
 */

public class Errorpayment extends Fragment{

    private WebView wvContent;
    private LinearLayout llReturnMenu;
    private String html = "<!DOCTYPE html>" +
            "<html>" +
            "    <head>" +
            "        <style>" +
            "            html, body { text-align:center; }" +
            "        </style>" +
            "    </head>" +
            "    <body>" +
            "      %s"+
            "    </body>" +
            "</html>";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_error_payment , container , false);
        wvContent = (WebView) view.findViewById(R.id.wvContent);
        wvContent.getSettings().setSupportZoom(false);
        wvContent.getSettings().setBuiltInZoomControls(false);
        wvContent.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        wvContent.setWebChromeClient(new WebChromeClient());
        llReturnMenu  = (LinearLayout) view.findViewById(R.id.llReturnMenu);
        llReturnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity activity = (MainActivity) getActivity();
                activity.backToMainMenu();
            }
        });

        wvContent.loadData(String.format(html , getText(R.string.txt_content_error_payment)), "text/html; charset=utf-8", "utf-8");
        return view;
    }

}
