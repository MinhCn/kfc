package kfc.vietnam.food_menu_screen.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.ProductCategoryWrapper;
import kfc.vietnam.common.object.ProductDataWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.ShoppingCartObject;
import kfc.vietnam.common.utility.DialogProvince;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.utility.ResultListener;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.DetailMenuPagerAdapter;
import kfc.vietnam.food_menu_screen.adapter.ShoppingCartAdapter;
import kfc.vietnam.main_screen.ui.DialogFactory;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kfc.vietnam.R.id.rcvShoppingCart;

public class DetailsMenuFragment extends Fragment implements View.OnClickListener, DialogProvince.onChooseProvinceListener {
    private String currentLanguageCode = "";
    private FragmentActivity ctx;
    private ArrayList<ProvinceWrapper> lstProvince;

    private ViewPager pager;

    private ArrayList<ProductCategoryWrapper> lstCategory;
    private String idCategory;

    private MainActivity activity;

    private SharedPreferences preferencesShoppingCard;
    private ArrayList<ShoppingCartObject> lstShoppingCard;

    @Override
    public void onAttach(Context context) {
        ctx = (FragmentActivity) context;
        super.onAttach(context);

        Bundle b = getArguments();
        this.idCategory = b.getString("id_category");

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
        this.lstProvince = activity.lstProvince;

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_menu_tabship, container, false);

        /** Getting a reference to the ViewPager defined the layout file */
        pager = (ViewPager) view.findViewById(R.id.pager);
        PagerTabStrip pager_tab_strip = (PagerTabStrip) view.findViewById(R.id.pager_tab_strip);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ((ViewPager.LayoutParams) pager_tab_strip.getLayoutParams()).isDecor = true;
        checkFirtRunApp();

        if (lstCategory == null) {
            if (lstProvince != null && activity.currentProvinceSelected != null)
                getComboAndProduct(currentLanguageCode, activity.currentProvinceSelected.id);
        } else
            showData();
        activity.resetColor(activity.rlMenu, activity.imgMenu, activity.tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
        return view;
    }

    public void checkFirtRunApp() {
        SharedPreferences pref = getActivity().getSharedPreferences(Const.SHARED_PREF, 0);
        boolean isFirst = pref.getBoolean(SharedPreferenceKeyType.FIRST_MENU_DETAIL.toString(), false);
        if (!isFirst) {
            DialogFactory.showDialogFirstRunApp(getActivity(), currentLanguageCode.equals("vn") ? R.drawable.bg_first_detail_menu_vn : R.drawable.bg_first_detail_menu_en);
            SharedPreferences.Editor prefEditor = pref.edit();
            prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_MENU_DETAIL.toString(), true);
            prefEditor.commit();
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(DetailsMenuFragment.class);
                }
            }
        });

        return anim;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

    }

    private void getComboAndProduct(String lang, String city) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity) ctx).showNoInternetFragment();
            return;
        }

        Call<ProductDataWrapper> call = ApiClient.getJsonClient().getComboAndProduct(lang, city);
        call.enqueue(new Callback<ProductDataWrapper>() {

            @Override
            public void onFailure(Call<ProductDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ProductDataWrapper> arg0, Response<ProductDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    lstCategory = arg1.body().products;
                    //Phần sắp xếp cho combo nằm giữa list
//                    Comparator<ProductCategoryWrapper> mComparator = new Comparator<ProductCategoryWrapper>() {
//                        @Override
//                        public int compare(ProductCategoryWrapper t0, ProductCategoryWrapper t1) {
//                            return String.valueOf(t0.categoryID).compareToIgnoreCase(String.valueOf(t1.categoryID));
//                        }
//                    };
//                    Collections.sort(lstCategory, mComparator);
//                    ProductCategoryWrapper obj = new ProductCategoryWrapper();
//                    obj.categoryID = "0";
//                    int index = Collections.binarySearch(lstCategory, obj, mComparator);
//                    if (index >= 0) {
//                        int size = lstCategory.size();
//                        int middle = (int) Math.floor(size / 2.0);
//
//                        obj = lstCategory.remove(index);
//                        lstCategory.add(middle, obj);
//                    }

                    showData();
                }
            }
        });
    }

    private void showData() {
        int index = 0;
        //get index of id cliked trong list category
        if (lstCategory != null) {
            for (int i = 0; i < lstCategory.size(); i++) {
                ProductCategoryWrapper obj = lstCategory.get(i);
                if (obj.categoryID.equalsIgnoreCase(idCategory)) {
                    index = i;
                    break;
                }
            }
        }

        /** Instantiating FragmentStatePagerAdapter */
        DetailMenuPagerAdapter pagerAdapter = new DetailMenuPagerAdapter(getChildFragmentManager(), getActivity(), lstCategory, currentLanguageCode, lstProvince);

        /** Setting the FragmentStatePagerAdapter to the pager object */
        pager.setAdapter(pagerAdapter);

        pager.setCurrentItem(index);
    }

    @Override
    public void onChooseProvinceClick() {
        if (lstProvince != null)
            getComboAndProduct(currentLanguageCode, activity.currentProvinceSelected.id);

        //edit by @ManhNguyen
        preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
        try {
            lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (lstShoppingCard != null && lstShoppingCard.size() > 0)
            activity.checkProductShoppingCart(lstShoppingCard, new ResultListener<ArrayList<ShoppingCartObject>>() {
                @Override
                public void onSucceed(ArrayList<ShoppingCartObject> result) {
                    try {
                        preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                        int count_cart = 0;
                        for (ShoppingCartObject cart : lstShoppingCard) {
                            count_cart += Integer.parseInt(cart.amount);
                        }
                        activity.tvAmountShoppingCart.setText(count_cart + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFail(String reason) {

                }
            });
    }
}
