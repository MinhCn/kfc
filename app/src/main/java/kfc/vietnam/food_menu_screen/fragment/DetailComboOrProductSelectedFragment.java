package kfc.vietnam.food_menu_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;
import kfc.vietnam.R;
import kfc.vietnam.common.enums.CategoryType;
import kfc.vietnam.common.enums.ChangeDishPositionType;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.ProductChangeType;
import kfc.vietnam.common.enums.ProductComboType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.ChangeProductDataWrapper;
import kfc.vietnam.common.object.GroupContentWrapper001;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.ProductBuyMoreModel;
import kfc.vietnam.common.object.ProductChangeWrapper;
import kfc.vietnam.common.object.ProductDetailDataWrapper;
import kfc.vietnam.common.object.ProductWrapper001;
import kfc.vietnam.common.object.ProductWrapper002;
import kfc.vietnam.common.object.ProductWrapper003;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.ShoppingCartObject;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.NonScrollListView;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.CategoryInfoAdapter;
import kfc.vietnam.food_menu_screen.adapter.ComboAdapter001;
import kfc.vietnam.food_menu_screen.adapter.ComboAdapter002;
import kfc.vietnam.food_menu_screen.adapter.ProductAdapter002;
import kfc.vietnam.food_menu_screen.ui.ChangeDishActivity;
import kfc.vietnam.main_screen.ui.DialogFactory;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class DetailComboOrProductSelectedFragment extends Fragment implements View.OnClickListener {
    public ProductWrapper003 productSelected;
    private String currentLanguageCode = "";
    private Context ctx;
    private ArrayList<ProvinceWrapper> lstProvince;
    private String number[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    private Spinner spnNumber;
    private TextView tvTotalPrice, tvTitleChoose1, tvTitleChoose2, tvTitleChoose3, tvTitleDetailCombo;
    private NonScrollListView lv1, lv2, lv3, lvSnacksAndDessert, lvDrinks, lvGaRan, lvBurgerCom;
    private LinearLayout llChoose1, llChoose2, llChoose3, llSnacksAndDessert, llDrinks, llAddToCart;
    private LinearLayout llGaRan, llBurgerCom;
    private ProductWrapper001 dishSelected;
    private ArrayList<ProductWrapper002> lstProductDefault, lstSnacksAndDessertDefault, lstDrinkDefault, lstSnacksAndDessertSelected, lstDrinkSelected;
    private ArrayList<ProductWrapper002> lstGaRanDefault, lstBurgerComDefault, lstGaRanSelected, lstBurgerComSelected;
    private ArrayList<ProductWrapper002> lstSnacksAndDessertChange, lstDrinkChange, lstSnacksAndDessertChangeDefault, lstDrinkChangeDefault;
    private ArrayList<ProductWrapper002> lstGaRanChange, lstBurgerComChange, lstGaRanChangeDefault, lstBurgerComChangeDefault;
    private String categoryType = "", productID, buy = "";
    private int countElement = 0;
    private NonScrollListView lvCategoryInfo;
    private ArrayList<String> lstCategoryInfo;
    private MainActivity activity;
    private LinearLayout llOverlay;
    private NestedScrollView nvDetail;
    private SimpleTooltip simpleTooltip;

    private ImageView imgCategory1, imgCategory2;
    private String product_name, product_price, product_image, product_like, product_type, product_buy;
    private double addedSnacksPrice = 0;
    private double addedDrinksPrice = 0;
    private double addedGaRanPrice = 0;
    private double addedBurgerComPrice = 0;
    private double tmpPrice = 0;
    private double totalPrice = 0;
    private boolean checkData = false;
    private Button btnChangeSnacksAndDessert;
    private Button btnChangeDrink;
    private Button btnChangeGaRan;
    private Button btnChangeBurgerCom;
    private View bgShape;
    private ProgressBar pbLoading;
    private ComboAdapter001 comboAdapter1;
    private ComboAdapter001 comboAdapter2;
    boolean isFirstChangeFood = false;
    boolean isFirst = false;
    SharedPreferences pref;
    private SharedPreferences preferencesUserInfo;
    public LoginWrapper userInfo;

    private ArrayList<ProductBuyMoreModel> mListProductBuyMore;
    private ArrayList<ProductBuyMoreModel> mListFoodSelected;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
        this.lstProvince = activity.lstProvince;

        Bundle b = getArguments();
        this.productID = b.getString("id");
        this.categoryType = b.getString("category_type");
        this.buy = b.getString("buy");

        lstProductDefault = new ArrayList<>();

        lstSnacksAndDessertDefault = new ArrayList<>();
        lstSnacksAndDessertSelected = new ArrayList<>();
        lstSnacksAndDessertChangeDefault = new ArrayList<>();

        lstDrinkDefault = new ArrayList<>();
        lstDrinkSelected = new ArrayList<>();
        lstDrinkChangeDefault = new ArrayList<>();

        //add by @ManhNguyen
        lstGaRanDefault = new ArrayList<>();
        lstGaRanSelected = new ArrayList<>();
        lstGaRanChangeDefault = new ArrayList<>();

        lstBurgerComDefault = new ArrayList<>();
        lstBurgerComSelected = new ArrayList<>();
        lstBurgerComChangeDefault = new ArrayList<>();

        mListProductBuyMore = new ArrayList<>();
        mListFoodSelected = new ArrayList<>();

        activity.llMenuBar.setVisibility(View.GONE);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_detail_combo_or_product_activity, container, false);
        pref = getActivity().getSharedPreferences(Const.SHARED_PREF, 0);
        isFirstChangeFood = pref.getBoolean(SharedPreferenceKeyType.FIRST_PRODUCT_DETAIL_CHANGE_FOOD.toString(), false);
        isFirst = pref.getBoolean(SharedPreferenceKeyType.FIRST_PRODUCT_DETAIL.toString(), false);
        init(root);
        activity.resetColor(activity.rlMenu, activity.imgMenu, activity.tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
        if (lstProvince != null)
            getComboOrProductDetail(productID, currentLanguageCode, activity.currentProvinceSelected.id, categoryType);
        return root;
    }

    public void checkFirtRunApp(View view) {
        if (!isFirstChangeFood) {
            isFirstChangeFood = true;
            SharedPreferences pref = getActivity().getSharedPreferences(Const.SHARED_PREF, 0);
            simpleTooltip = DialogFactory.showTipRightTop(getActivity(), view);
            simpleTooltip.show();
            SharedPreferences.Editor prefEditor = pref.edit();
            prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_PRODUCT_DETAIL_CHANGE_FOOD.toString(), true);
            prefEditor.apply();
        }
    }


    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (enter) {
                    activity.processChangeTopMenu(DetailComboOrProductSelectedFragment.class);
                }
            }
        });

        return anim;
    }

    private void init(View root) {
        imgCategory1 = (ImageView) root.findViewById(R.id.imgCategory1);
        imgCategory2 = (ImageView) root.findViewById(R.id.imgCategory2);
        bgShape = root.findViewById(R.id.bgShape);
        pbLoading = (ProgressBar) root.findViewById(R.id.pbLoading);

        lstCategoryInfo = new ArrayList<>();
        CategoryInfoAdapter categoryInfoAdapter = new CategoryInfoAdapter(ctx, R.layout.custom_item_category_info_in_detail_combo_or_product, lstCategoryInfo);
        lvCategoryInfo = (NonScrollListView) root.findViewById(R.id.lvCategoryInfo);
        lvCategoryInfo.setAdapter(categoryInfoAdapter);

        llChoose1 = (LinearLayout) root.findViewById(R.id.llChoose1);
        llOverlay = (LinearLayout) root.findViewById(R.id.llOverlay);
        llOverlay.setOnClickListener(this);
        llChoose2 = (LinearLayout) root.findViewById(R.id.llChoose2);
        llChoose3 = (LinearLayout) root.findViewById(R.id.llChoose3);
        tvTitleChoose1 = (TextView) root.findViewById(R.id.tvTitleChoose1);
        tvTitleChoose2 = (TextView) root.findViewById(R.id.tvTitleChoose2);
        tvTitleChoose3 = (TextView) root.findViewById(R.id.tvTitleChoose3);

        lv1 = (NonScrollListView) root.findViewById(R.id.lvComboOrProduct1);
        lv2 = (NonScrollListView) root.findViewById(R.id.lvComboOrProduct2);
        lv3 = (NonScrollListView) root.findViewById(R.id.lvComboOrProduct3);
        lvSnacksAndDessert = (NonScrollListView) root.findViewById(R.id.lvSnacksAndDessert);
        lvDrinks = (NonScrollListView) root.findViewById(R.id.lvDrinks);
        lvGaRan = (NonScrollListView) root.findViewById(R.id.lvGaRan);
        lvBurgerCom = (NonScrollListView) root.findViewById(R.id.lvBurgerCom);

        llSnacksAndDessert = (LinearLayout) root.findViewById(R.id.llSnacksAndDessert);
        btnChangeSnacksAndDessert = (Button) root.findViewById(R.id.btnChangeSnacksAndDessert);
        btnChangeSnacksAndDessert.setOnClickListener(this);

        llDrinks = (LinearLayout) root.findViewById(R.id.llDrinks);
        btnChangeDrink = (Button) root.findViewById(R.id.btnChangeDrink);
        btnChangeDrink.setOnClickListener(this);

        //Add by @ManhNguyen
        llGaRan = (LinearLayout) root.findViewById(R.id.llGaRan);
        btnChangeGaRan = (Button) root.findViewById(R.id.btnChangeGaRan);
        btnChangeGaRan.setOnClickListener(this);

        llBurgerCom = (LinearLayout) root.findViewById(R.id.llBurgerCom);
        btnChangeBurgerCom = (Button) root.findViewById(R.id.btnChangeBurgerCom);
        btnChangeBurgerCom.setOnClickListener(this);

        spnNumber = (Spinner) root.findViewById(R.id.spnNumber);

        llAddToCart = (LinearLayout) root.findViewById(R.id.llAddToCart);
        if (buy == null || buy.isEmpty() || buy.equals("0"))
            llAddToCart.setVisibility(View.GONE);
        else
            llAddToCart.setVisibility(View.VISIBLE);

        llAddToCart.setOnClickListener(this);

        tvTotalPrice = (TextView) root.findViewById(R.id.tvTotalPrice);
        nvDetail = (NestedScrollView) root.findViewById(R.id.nvDetail);
        nvDetail.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));


                if (diff == 0) {
                    if (checkData) {
                        if (llSnacksAndDessert.getVisibility() != View.GONE) {
                            checkFirtRunApp(btnChangeSnacksAndDessert);
                        } else if (llDrinks.getVisibility() != View.GONE) {
                            checkFirtRunApp(btnChangeDrink);
                        } else if (llGaRan.getVisibility() != View.GONE) {
                            checkFirtRunApp(btnChangeGaRan);
                        } else if (llBurgerCom.getVisibility() != View.GONE) {
                            checkFirtRunApp(btnChangeBurgerCom);
                        }
                        checkData = false;
                    }
                }
            }
        });
        try {
            preferencesUserInfo = getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE);
            if (preferencesUserInfo.contains(SharedPreferenceKeyType.USER_INFO.toString()))
                if (userInfo == null)
                    userInfo = (LoginWrapper) ObjectSerializer.deserialize(preferencesUserInfo.getString(SharedPreferenceKeyType.USER_INFO.toString(), null));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setData() {
        Glide.with(ctx)
                .load(product_image)
                .asBitmap()
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String s, Target<Bitmap> target, boolean b) {
                        pbLoading.setVisibility(View.GONE);
                        bgShape.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap bitmap, String s, Target<Bitmap> target, boolean b, boolean b1) {
                        pbLoading.setVisibility(View.GONE);
                        bgShape.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                        imgCategory1.setImageBitmap(bitmap);
                        imgCategory2.setImageBitmap(bitmap);
                    }
                });

        ArrayAdapter adapterNumber = new ArrayAdapter(getActivity(), R.layout.simple_spinner_item, number);
        adapterNumber.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spnNumber.setAdapter(adapterNumber);
        spnNumber.setOnItemSelectedListener(new MyOnItemSelectedListener());

        double totalPrice = Double.parseDouble(product_price);
        showTotalPrice(totalPrice);
        activity.tvTitleDetailCombo.setText(product_name);
    }


    private void showTotalPrice(double _price) {
        totalPrice = _price;
        if (_price > 0) {

            DecimalFormat myFormatter = new DecimalFormat("#,##0");
            String output = myFormatter.format(totalPrice);
            try {
                output = output.replace(".", ",");
            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList<String> lst = new ArrayList(Arrays.asList(output.split(",")));
            if (lst.size() >= 2)
                lst.remove(lst.size() - 1);
            output = lst.toString().replace("[", "").replace("]", "").replace(" ", "") + ".000 " + getString(R.string.vnd_cap);

            int start = 0;
            int end = output.indexOf(".");
            output = output.replace(",", ".");

            SpannableString spanContent = new SpannableString(output);
            spanContent.setSpan(new AbsoluteSizeSpan(18, true), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set style
            spanContent.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
            tvTotalPrice.setText(spanContent);
        } else
            tvTotalPrice.setText("0 " + getString(R.string.vnd_cap));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llAddToCart:
                dishSelected = new ProductWrapper001();
                dishSelected.id = productID;
                dishSelected.name = product_name;
                dishSelected.image = product_image;
                dishSelected.price = product_price;
                dishSelected.type = product_type;
                dishSelected.like = product_like;
                dishSelected.buy = product_buy;

                ShoppingCartObject obj = new ShoppingCartObject();
                obj.listProductBuyMoreModel = mListFoodSelected;
                if (categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                    obj.idCart = UUID.randomUUID().toString();
                    obj.categoryType = categoryType;
                    obj.dishSelected = dishSelected;
                    obj.lstProductDefault = lstProductDefault;

                    obj.lstSnacksAndDessertDefault = lstSnacksAndDessertDefault;
                    obj.lstSnacksAndDessertSelected = lstSnacksAndDessertSelected;

                    obj.lstDrinkDefault = lstDrinkDefault;
                    obj.lstDrinkSelected = lstDrinkSelected;

                    //add by @Manhnguyen
                    obj.lstGaRanDefault = lstGaRanDefault;
                    obj.lstGaRanSelected = lstGaRanSelected;

                    obj.lstBurgerComDefault = lstBurgerComDefault;
                    obj.lstBurgerComSelected = lstBurgerComSelected;

                    obj.amount = spnNumber.getSelectedItem().toString().trim();
                    obj.count_element = countElement;
                    obj.price_one_item = tmpPrice;
                    obj.price_in_total = totalPrice;
                    obj.productSelected = productSelected;

                    if (countElement == 0) {
                        obj.combo1 = null;
                        obj.combo2 = null;
                    } else if (countElement == 1) {
                        GroupContentWrapper001 p1 = ((ComboAdapter001) lv1.getAdapter()).foodSelected;
                        if (p1 != null) {
                            obj.combo1 = p1;
                            obj.combo2 = null;
                        } else {
                            Toast.makeText(ctx, getString(R.string.DetailComboOrProductSelectedFragment_001), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else if (countElement == 2) {
                        GroupContentWrapper001 p1 = ((ComboAdapter001) lv1.getAdapter()).foodSelected;
                        GroupContentWrapper001 p2 = ((ComboAdapter001) lv2.getAdapter()).foodSelected;
                        if (p1 != null && p2 != null) {
                            obj.combo1 = p1;
                            obj.combo2 = p2;
                        } else {
                            Toast.makeText(ctx, getString(R.string.DetailComboOrProductSelectedFragment_001), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                } else {
                    obj.idCart = UUID.randomUUID().toString();
                    obj.categoryType = categoryType;
                    obj.dishSelected = dishSelected;
                    obj.productSelected = productSelected;
                    obj.price_in_total = totalPrice;
                    obj.amount = spnNumber.getSelectedItem().toString().trim();
                }

                ArrayList<ShoppingCartObject> lstShoppingCard = new ArrayList<>();
                SharedPreferences preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
                if (!preferencesShoppingCard.contains(SharedPreferenceKeyType.SHOPPING_CART.toString())) {
                    lstShoppingCard.add(obj);
                    try {
                        preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        String serializeString = preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null);
                        lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(serializeString);
                        lstShoppingCard.add(obj);
                        preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                int count_cart = 0;
                if (lstShoppingCard != null) {
                    for (int i = 0; i < lstShoppingCard.size(); i++) {
                        count_cart += Integer.parseInt(lstShoppingCard.get(i).amount);
                    }
                }
                activity.showAndAnimationShoppingCart(count_cart + "", imgCategory2);
                break;

            case R.id.btnChangeSnacksAndDessert:
                startChangeDishActivity(ChangeDishPositionType.SNACKS.getValue(), RequestResultCodeType.REQUEST_CHANGE_DISH.getValue());
                break;

            case R.id.btnChangeDrink:
                startChangeDishActivity(ChangeDishPositionType.DRINKS.getValue(), RequestResultCodeType.REQUEST_CHANGE_DISH.getValue());
                break;

            case R.id.btnChangeGaRan:
                startChangeDishActivity(ChangeDishPositionType.GARAN.getValue(), RequestResultCodeType.REQUEST_CHANGE_DISH.getValue());
                break;

            case R.id.btnChangeBurgerCom:
                startChangeDishActivity(ChangeDishPositionType.BURGER.getValue(), RequestResultCodeType.REQUEST_CHANGE_DISH.getValue());
                break;

            case R.id.llOverlay:
                llOverlay.setVisibility(View.GONE);
                if (comboAdapter1 != null) {
                    comboAdapter1.closeToolTip();
                } else if (comboAdapter2 != null) {
                    comboAdapter2.closeToolTip();
                }
                break;

        }
    }

    private void startChangeDishActivity(int pos, int req) {
        Intent intent = new Intent(ctx, ChangeDishActivity.class);
        intent.putExtra("list_snacks_and_dessert_change", lstSnacksAndDessertChange);
        intent.putExtra("lst_drink_change", lstDrinkChange);
        intent.putExtra("lst_ga_ran_change", lstGaRanChange);
        intent.putExtra("lst_burger_com_change", lstBurgerComChange);
        if (pos == -1)
            pos = 0;
        intent.putExtra("position_screen", pos);
        startActivityForResult(intent, req);
    }

    private void getComboOrProductDetail(String id, String lang, String city, final String type) {
        countElement = 0;

        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity) ctx).showNoInternetFragment();
            return;
        }

        String userID;
        if (userInfo == null || TextUtils.isEmpty(userInfo.id)) {
            userID = "0";
        } else {
            userID = userInfo.id;
        }

        Call<ProductDetailDataWrapper> call = ApiClient.getJsonClient().getComboOrProductDetail(id, userID, lang, city, type);
        call.enqueue(new Callback<ProductDetailDataWrapper>() {

            @Override
            public void onFailure(Call<ProductDetailDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ProductDetailDataWrapper> arg0, Response<ProductDetailDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    ProductDetailDataWrapper productDetailDataWrapper = arg1.body();

                    product_name = productDetailDataWrapper.data.product.nameProduct;
                    product_price = productDetailDataWrapper.data.product.price;
                    product_image = productDetailDataWrapper.data.product.image;
                    product_like = String.valueOf(productDetailDataWrapper.data.product.like);
                    product_type = productDetailDataWrapper.data.product.type;
                    product_buy = productDetailDataWrapper.data.product.buy;
                    mListProductBuyMore = productDetailDataWrapper.data.product.productBuyMore;
                    setData();

                    lstProductDefault.clear();

                    lstSnacksAndDessertDefault.clear();
                    lstSnacksAndDessertSelected.clear();

                    lstDrinkDefault.clear();
                    lstDrinkSelected.clear();

                    //add by @ManhNguyen
                    lstGaRanDefault.clear();
                    lstGaRanSelected.clear();

                    lstBurgerComDefault.clear();
                    lstBurgerComSelected.clear();

                    addedSnacksPrice = 0;
                    addedDrinksPrice = 0;
                    addedGaRanPrice = 0;
                    addedBurgerComPrice = 0;

                    if (type.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
                        productSelected = productDetailDataWrapper.data.product;
                        //set like number in menu
                        activity.tvLikeNumber.setText(productSelected.like + "");
                        if (productSelected.checkLike)
                            activity.imgLike.setBackgroundResource(R.drawable.ic_favorite_food_2);
                        else
                            activity.imgLike.setBackgroundResource(R.drawable.ic_favorite_food_1);

                        ArrayList<GroupContentWrapper001> lstCombo1 = productDetailDataWrapper.data.groups.group_0;
                        if (lstCombo1 != null && lstCombo1.size() > 0) {
                            llChoose1.setVisibility(View.VISIBLE);
                            checkData = true;
                            countElement++;
                            if (!isFirst) {
                                llOverlay.setVisibility(View.VISIBLE);
                            }
                            comboAdapter1 = new ComboAdapter001(activity, R.layout.custom_item_combo_or_product, lstCombo1, lv1, currentLanguageCode, !isFirst);
                            lv1.setAdapter(comboAdapter1);
                            tvTitleChoose1.setText(Html.fromHtml(getString(R.string.please_select1) + "<b> 1 " + getString(R.string.please_select3) + " " + lstCombo1.size() + "</b> " + getString(R.string.please_select2)));
                        } else
                            llChoose1.setVisibility(View.GONE);


                        ArrayList<GroupContentWrapper001> lstCombo2 = productDetailDataWrapper.data.groups.group_1;
                        if (lstCombo2 != null && lstCombo2.size() > 0) {
                            llChoose2.setVisibility(View.VISIBLE);
                            if (llOverlay.getVisibility() == View.GONE && !isFirst) {
                                llOverlay.setVisibility(View.VISIBLE);
                            }
                            checkData = true;
                            countElement++;

                            comboAdapter2 = new ComboAdapter001(activity, R.layout.custom_item_combo_or_product, lstCombo2, lv2, currentLanguageCode, (!isFirst && (lstCombo1 == null || lstCombo1.size() == 0)));
                            lv2.setAdapter(comboAdapter2);
                            tvTitleChoose2.setText(Html.fromHtml(getString(R.string.please_select1) + "<b> 1 " + getString(R.string.please_select3) + " " + lstCombo2.size() + "</b> " + getString(R.string.please_select2)));
                        } else
                            llChoose2.setVisibility(View.GONE);

                        ArrayList<ProductChangeWrapper> lstProductChange = productDetailDataWrapper.data.productChange;
                        if (lstProductChange != null) {
                            for (ProductChangeWrapper obj : lstProductChange) {
                                //Edit by @ManhNguyen
                                if (obj.id == ProductChangeType.SNACKS_AND_DESSERTS.getValue()) {
                                    //Snacks And Dessert
                                    lstSnacksAndDessertChange = obj.product;
                                } else if (obj.id == ProductChangeType.DRINKS.getValue()) {
                                    //Drink
                                    lstDrinkChange = obj.product;
                                } else if (obj.id == ProductChangeType.GA_RAN.getValue()) {
                                    lstGaRanChange = obj.product;
                                } else if (obj.id == ProductChangeType.BURGER_COM.getValue()) {
                                    lstBurgerComChange = obj.product;
                                }
                            }
                        }
                        Comparator<ProductWrapper002> mComparator = new Comparator<ProductWrapper002>() {
                            @Override
                            public int compare(ProductWrapper002 productWrapper002, ProductWrapper002 t1) {
                                return String.valueOf(productWrapper002.id).compareToIgnoreCase(String.valueOf(t1.id));
                            }
                        };

                        if (lstSnacksAndDessertChange != null)
                            Collections.sort(lstSnacksAndDessertChange, mComparator);

                        if (lstDrinkChange != null)
                            Collections.sort(lstDrinkChange, mComparator);

                        //add by @ManhNguyen
                        if (lstGaRanChange != null)
                            Collections.sort(lstGaRanChange, mComparator);

                        if (lstBurgerComChange != null)
                            Collections.sort(lstBurgerComChange, mComparator);

                        if (lstSnacksAndDessertChange != null) {
                            for (ProductWrapper002 obj : lstSnacksAndDessertChange) {
                                if ((obj.id + "").equalsIgnoreCase(obj.parent)) {
                                    if (productDetailDataWrapper.data.productCombo != null) {
                                        for (ProductWrapper002 product : productDetailDataWrapper.data.productCombo) {
                                            if (obj.id == product.id) {
                                                obj.combo_product_id = product.combo_product_id;
                                                obj.group_id = product.group_id;
                                            }
                                        }
                                    }
                                    lstSnacksAndDessertDefault.add(obj);
                                    obj.isProductSelected = true;
                                }
                            }

                            Collections.sort(lstSnacksAndDessertDefault, mComparator);
                            lstSnacksAndDessertSelected = new ArrayList<>(lstSnacksAndDessertDefault);
                            if (lstSnacksAndDessertSelected.size() > 0) {
                                llSnacksAndDessert.setVisibility(View.VISIBLE);
                                ComboAdapter002 adapter1 = new ComboAdapter002(ctx, R.layout.custom_item_drinks_snacks, lstSnacksAndDessertSelected, lvSnacksAndDessert, currentLanguageCode);
                                lvSnacksAndDessert.setAdapter(adapter1);
                                if (!checkData) {
                                    checkFirtRunApp(btnChangeSnacksAndDessert);
                                }
                                checkData = true;
                            }
                            lstSnacksAndDessertChangeDefault.addAll(new ArrayList<>(lstSnacksAndDessertChange));
                        }

                        if (lstDrinkChange != null) {
                            for (ProductWrapper002 obj : lstDrinkChange) {
                                if ((obj.id + "").equalsIgnoreCase(obj.parent)) {
                                    if (productDetailDataWrapper.data.productCombo != null) {
                                        for (ProductWrapper002 product : productDetailDataWrapper.data.productCombo) {
                                            if (obj.id == product.id) {
                                                obj.combo_product_id = product.combo_product_id;
                                                obj.group_id = product.group_id;
                                            }
                                        }
                                    }
                                    lstDrinkDefault.add(obj);
                                    obj.isProductSelected = true;
                                }
                            }
                            Collections.sort(lstDrinkDefault, mComparator);
                            lstDrinkSelected = new ArrayList<>(lstDrinkDefault);
                            if (lstDrinkSelected.size() > 0) {
                                llDrinks.setVisibility(View.VISIBLE);
                                ComboAdapter002 adapter2 = new ComboAdapter002(ctx, R.layout.custom_item_drinks_snacks, lstDrinkSelected, lvDrinks, currentLanguageCode);
                                lvDrinks.setAdapter(adapter2);
                                if (lstSnacksAndDessertSelected != null) {
                                    if (!checkData && lstSnacksAndDessertSelected.size() == 0) {
                                        checkFirtRunApp(btnChangeDrink);
                                        checkData = true;
                                    }
                                }
                            }
                            lstDrinkChangeDefault.addAll(new ArrayList<>(lstDrinkChange));
                        }

                        //add by @ManhNguyen
                        if (lstGaRanChange != null) {
                            for (ProductWrapper002 obj : lstGaRanChange) {
                                if ((obj.id + "").equalsIgnoreCase(obj.parent)) {
                                    if (productDetailDataWrapper.data.productCombo != null) {
                                        for (ProductWrapper002 product : productDetailDataWrapper.data.productCombo) {
                                            if (obj.id == product.id) {
                                                obj.combo_product_id = product.combo_product_id;
                                                obj.group_id = product.group_id;
                                            }
                                        }
                                    }
                                    lstGaRanDefault.add(obj);
                                    obj.isProductSelected = true;
                                }
                            }
                            Collections.sort(lstGaRanDefault, mComparator);
                            lstGaRanSelected = new ArrayList<>(lstGaRanDefault);
                            if (lstGaRanSelected.size() > 0) {
                                llGaRan.setVisibility(View.VISIBLE);
                                ComboAdapter002 adapter3 = new ComboAdapter002(ctx, R.layout.custom_item_drinks_snacks, lstGaRanSelected, lvGaRan, currentLanguageCode);
                                lvGaRan.setAdapter(adapter3);
                                if (lstDrinkSelected != null && lstSnacksAndDessertSelected != null) {
                                    if (!checkData && lstSnacksAndDessertSelected.size() == 0 && lstDrinkSelected.size() == 0) {
                                        checkFirtRunApp(btnChangeGaRan);
                                        checkData = true;
                                    }
                                }
                            }
                            lstGaRanChangeDefault.addAll(new ArrayList<>(lstGaRanChange));
                        }

                        //add by @ManhNguyen
                        if (lstBurgerComChange != null) {
                            for (ProductWrapper002 obj : lstBurgerComChange) {
                                if ((obj.id + "").equalsIgnoreCase(obj.parent)) {
                                    if (productDetailDataWrapper.data.productCombo != null) {
                                        for (ProductWrapper002 product : productDetailDataWrapper.data.productCombo) {
                                            if (obj.id == product.id) {
                                                obj.combo_product_id = product.combo_product_id;
                                                obj.group_id = product.group_id;
                                            }
                                        }
                                    }
                                    lstBurgerComDefault.add(obj);
                                    obj.isProductSelected = true;
                                }
                            }
                            Collections.sort(lstBurgerComDefault, mComparator);
                            lstBurgerComSelected = new ArrayList<>(lstBurgerComDefault);
                            if (lstBurgerComSelected.size() > 0) {
                                llBurgerCom.setVisibility(View.VISIBLE);
                                ComboAdapter002 adapter4 = new ComboAdapter002(ctx, R.layout.custom_item_drinks_snacks, lstBurgerComSelected, lvBurgerCom, currentLanguageCode);
                                lvBurgerCom.setAdapter(adapter4);
                                if (lstSnacksAndDessertSelected != null && lstDrinkSelected != null && lstGaRanSelected != null) {
                                    if (!checkData && lstSnacksAndDessertSelected.size() == 0 && lstDrinkSelected.size() == 0 && lstGaRanSelected.size() == 0) {
                                        checkFirtRunApp(btnChangeBurgerCom);
                                        checkData = true;
                                    }
                                }
                            }
                            lstBurgerComChangeDefault.addAll(new ArrayList<>(lstBurgerComChange));
                        }

                        //phần get product còn lại trong product combo nếu những product này không có trong product change
                        ArrayList<ProductWrapper002> lstProductComboDefault = productDetailDataWrapper.data.productCombo;
                        for (ProductWrapper002 obj : lstProductComboDefault) {
                            int index = Collections.binarySearch(lstSnacksAndDessertDefault, obj, mComparator);
                            if (index < 0) {
                                index = Collections.binarySearch(lstDrinkDefault, obj, mComparator);
                                if (index < 0) {
                                    //@Edit by @ManhNguyen
                                    index = Collections.binarySearch(lstGaRanDefault, obj, mComparator);
                                    if (index < 0) {
                                        index = Collections.binarySearch(lstBurgerComDefault, obj, mComparator);
                                        if (index < 0) {
                                            lstProductDefault.add(obj);
                                        }
                                    }
                                }
                            }
                        }

                        updateCategoryInfo();
                    } else {
                        productSelected = productDetailDataWrapper.data.product;
                        activity.tvLikeNumber.setText(productSelected.like + "");
                        if (productSelected.checkLike)
                            activity.imgLike.setBackgroundResource(R.drawable.ic_favorite_food_2);
                        else
                            activity.imgLike.setBackgroundResource(R.drawable.ic_favorite_food_1);

                        ProductAdapter002 productAdapter1 = new ProductAdapter002(ctx, R.layout.custom_item_buymore, mListProductBuyMore);
                        lv3.setAdapter(productAdapter1);

                        //update info category list
                        if (mListProductBuyMore != null && mListProductBuyMore.size() > 0) {
                            llChoose3.setVisibility(View.VISIBLE);
                        }
                        updateCategoryInfo();
                    }
                }
            }
        });
    }

    public void processProductComboDefault() {
        //reset value
        llSnacksAndDessert.setVisibility(View.GONE);
        llDrinks.setVisibility(View.GONE);
        llGaRan.setVisibility(View.GONE);
        llBurgerCom.setVisibility(View.GONE);

        lstSnacksAndDessertSelected.clear();
        lstSnacksAndDessertSelected.addAll(new ArrayList<>(lstSnacksAndDessertDefault));

        lstDrinkSelected.clear();
        lstDrinkSelected.addAll(new ArrayList<>(lstDrinkDefault));

        //Edit by @ManhNguyen
        lstGaRanSelected.clear();
        lstGaRanSelected.addAll(new ArrayList<>(lstGaRanDefault));

        lstBurgerComSelected.clear();
        lstBurgerComSelected.addAll(new ArrayList<>(lstBurgerComDefault));

        lstSnacksAndDessertChange = lstSnacksAndDessertChangeDefault;
        lstDrinkChange = lstDrinkChangeDefault;

        lstGaRanChange = lstGaRanChangeDefault;
        lstBurgerComChange = lstBurgerComChangeDefault;

        for (ProductWrapper002 obj : lstSnacksAndDessertChange)
            obj.isProductSelected = false;
        if (lstSnacksAndDessertSelected != null && lstSnacksAndDessertSelected.size() > 0) {
            llSnacksAndDessert.setVisibility(View.VISIBLE);
            updateProductComboOnScreen(ProductComboType.SNACKS_AND_DESSERTS);
        }

        for (ProductWrapper002 obj : lstDrinkChange)
            obj.isProductSelected = false;
        if (lstDrinkSelected != null && lstDrinkSelected.size() > 0) {
            llDrinks.setVisibility(View.VISIBLE);
            updateProductComboOnScreen(ProductComboType.DRINKS);
        }

        //add by @ManhNguyen
        for (ProductWrapper002 obj : lstGaRanChange)
            obj.isProductSelected = false;
        if (lstGaRanSelected != null && lstGaRanSelected.size() > 0) {
            llGaRan.setVisibility(View.VISIBLE);
            updateProductComboOnScreen(ProductComboType.GA_RAN);
        }

        for (ProductWrapper002 obj : lstBurgerComChange)
            obj.isProductSelected = false;
        if (lstBurgerComSelected != null && lstBurgerComSelected.size() > 0) {
            llBurgerCom.setVisibility(View.VISIBLE);
            updateProductComboOnScreen(ProductComboType.BURGER_COM);
        }

        addedSnacksPrice = 0;
        addedDrinksPrice = 0;
        addedGaRanPrice = 0;
        addedBurgerComPrice = 0;

        int count = Integer.parseInt(spnNumber.getSelectedItem().toString().trim());
        tmpPrice = Double.parseDouble(product_price) + addedSnacksPrice + addedDrinksPrice +
                addedGaRanPrice + addedBurgerComPrice;
        showTotalPrice(tmpPrice * count);
    }

    private void updateProductComboOnScreen(ProductComboType type) {
        if (categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
            if (type == ProductComboType.SNACKS_AND_DESSERTS) {
                if (lstSnacksAndDessertSelected != null && lstSnacksAndDessertSelected.size() > 0)
                    lstSnacksAndDessertSelected.get(0).isProductSelected = true;
                ComboAdapter002 adapter1 = new ComboAdapter002(ctx, R.layout.custom_item_drinks_snacks, lstSnacksAndDessertSelected, lvSnacksAndDessert, currentLanguageCode);
                lvSnacksAndDessert.setAdapter(adapter1);
            } else if (type == ProductComboType.DRINKS) {
                if (lstDrinkSelected != null && lstDrinkSelected.size() > 0)
                    lstDrinkSelected.get(0).isProductSelected = true;
                ((ComboAdapter002) lvDrinks.getAdapter()).notifyDataSetChanged();
            } else if (type == ProductComboType.GA_RAN) {
                if (lstGaRanSelected != null && lstGaRanSelected.size() > 0)
                    lstGaRanSelected.get(0).isProductSelected = true;
                ((ComboAdapter002) lvGaRan.getAdapter()).notifyDataSetChanged();
            } else if (type == ProductComboType.BURGER_COM) {
                if (lstBurgerComSelected != null && lstBurgerComSelected.size() > 0)
                    lstBurgerComSelected.get(0).isProductSelected = true;
                ((ComboAdapter002) lvBurgerCom.getAdapter()).notifyDataSetChanged();
            }
        } else {
//            Toast.makeText(ctx, "Coming soon", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Kiểm tra nếu sản phẩm đang chọn đã có trong new change list thì giữ nguyên ko đổi, ngược lại thì chọn sản phẩm default trong new change list để hiển thị
     *
     * @param changeProduct
     */
    public void changeData(ChangeProductDataWrapper changeProduct) {
        Comparator<ProductWrapper002> mComparator = new Comparator<ProductWrapper002>() {
            @Override
            public int compare(ProductWrapper002 productWrapper002, ProductWrapper002 t1) {
                return String.valueOf(productWrapper002.id).compareToIgnoreCase(String.valueOf(t1.id));
            }
        };

        if (categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
            ArrayList<ProductChangeWrapper> lstProductChange = changeProduct.productChange;
            for (ProductChangeWrapper obj : lstProductChange) {
                if (obj.id == ProductChangeType.SNACKS_AND_DESSERTS.getValue()) {
                    //Snacks And Dessert
                    lstSnacksAndDessertChange = obj.product;
                    Collections.sort(lstSnacksAndDessertChange, mComparator);
                } else if (obj.id == ProductChangeType.DRINKS.getValue()) {
                    //Drink
                    lstDrinkChange = obj.product;
                    Collections.sort(lstDrinkChange, mComparator);
                } else if (obj.id == ProductChangeType.GA_RAN.getValue()) { //add by @ManhNguyen
                    lstGaRanChange = obj.product;
                    Collections.sort(lstGaRanChange, mComparator);
                } else if (obj.id == ProductChangeType.BURGER_COM.getValue()) {
                    lstBurgerComChange = obj.product;
                    Collections.sort(lstBurgerComChange, mComparator);
                }
            }

            //check Snacks And Dessert
            if (lstSnacksAndDessertSelected != null && lstSnacksAndDessertSelected.size() > 0) {
                //Phần xử lý khi mà Snacks And Dessert đã có dữ liệu mặc định trước khi change
                if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0) {
                    lstSnacksAndDessertSelected.clear();
                    for (ProductWrapper002 obj : lstSnacksAndDessertChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstSnacksAndDessertSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.SNACKS_AND_DESSERTS);
                    llSnacksAndDessert.setVisibility(View.VISIBLE);
                } else {
                    //hide control in screen
                    lstSnacksAndDessertSelected.clear();
                    llSnacksAndDessert.setVisibility(View.GONE);
                }
            } else {
                //Phần xử lý khi mà Snacks And Dessert không có dữ liệu mặc định trước khi change
                if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0) {
                    for (ProductWrapper002 obj : lstSnacksAndDessertChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstSnacksAndDessertSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.SNACKS_AND_DESSERTS);
                    llSnacksAndDessert.setVisibility(View.VISIBLE);
                }
            }

            //check Drinks
            if (lstDrinkSelected != null && lstDrinkSelected.size() > 0) {
                //Phần xử lý khi mà Drink đã có dữ liệu mặc định trước khi change
                if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
                    lstDrinkSelected.clear();
                    for (ProductWrapper002 obj : lstDrinkChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstDrinkSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.DRINKS);
                    llDrinks.setVisibility(View.VISIBLE);
                } else {
                    //hide control in screen
                    lstDrinkSelected.clear();
                    llDrinks.setVisibility(View.GONE);
                }
            } else {
                //Phần xử lý khi mà Drink không có dữ liệu mặc định trước khi change
                if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
                    for (ProductWrapper002 obj : lstDrinkChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstDrinkSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.DRINKS);
                    llDrinks.setVisibility(View.VISIBLE);
                }
            }

            //add by @ManhNguyen
            //check Ga ran
            if (lstGaRanSelected != null && lstGaRanSelected.size() > 0) {
                //Phần xử lý khi mà Drink đã có dữ liệu mặc định trước khi change
                if (lstGaRanChange != null && lstGaRanChange.size() > 0) {
                    lstGaRanSelected.clear();
                    for (ProductWrapper002 obj : lstGaRanChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstGaRanSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.GA_RAN);
                    llGaRan.setVisibility(View.VISIBLE);
                } else {
                    //hide control in screen
                    lstGaRanSelected.clear();
                    llGaRan.setVisibility(View.GONE);
                }
            } else {
                //Phần xử lý khi mà Drink không có dữ liệu mặc định trước khi change
                if (lstGaRanChange != null && lstGaRanChange.size() > 0) {
                    for (ProductWrapper002 obj : lstGaRanChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstGaRanSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.GA_RAN);
                    llGaRan.setVisibility(View.VISIBLE);
                }
            }

            //check Burger com
            if (lstBurgerComSelected != null && lstBurgerComSelected.size() > 0) {
                //Phần xử lý khi mà Drink đã có dữ liệu mặc định trước khi change
                if (lstBurgerComChange != null && lstBurgerComChange.size() > 0) {
                    lstBurgerComSelected.clear();
                    for (ProductWrapper002 obj : lstBurgerComChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstBurgerComSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.BURGER_COM);
                    llBurgerCom.setVisibility(View.VISIBLE);
                } else {
                    //hide control in screen
                    lstBurgerComSelected.clear();
                    llBurgerCom.setVisibility(View.GONE);
                }
            } else {
                //Phần xử lý khi mà Drink không có dữ liệu mặc định trước khi change
                if (lstBurgerComChange != null && lstBurgerComChange.size() > 0) {
                    for (ProductWrapper002 obj : lstBurgerComChange)
                        if ((obj.id + "").equalsIgnoreCase(obj.parent))
                            lstBurgerComSelected.add(obj);

                    updateProductComboOnScreen(ProductComboType.BURGER_COM);
                    llBurgerCom.setVisibility(View.VISIBLE);
                }
            }

        } else {
//            Toast.makeText(ctx, "Coming soon", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Phần xử lý thay đổi món ăn
        if (requestCode == RequestResultCodeType.REQUEST_CHANGE_DISH.getValue() && resultCode == RequestResultCodeType.RESULT_CHANGE_DISH.getValue()) {

            addedSnacksPrice = 0;
            addedDrinksPrice = 0;
            addedGaRanPrice = 0;
            addedBurgerComPrice = 0;

            if (data.getBooleanExtra("is_change", false)) {
                ListAdapter adapter1 = lvSnacksAndDessert.getAdapter();
                if (adapter1 != null) {
                    lstSnacksAndDessertSelected.clear();
                    lstSnacksAndDessertSelected.addAll((ArrayList<ProductWrapper002>) data.getSerializableExtra("list_snacks_selected"));
                    ((ComboAdapter002) adapter1).notifyDataSetChanged();
                }

                ListAdapter adapter2 = lvDrinks.getAdapter();
                if (adapter2 != null) {
                    lstDrinkSelected.clear();
                    lstDrinkSelected.addAll((ArrayList<ProductWrapper002>) data.getSerializableExtra("list_drinks_selected"));
                    ((ComboAdapter002) adapter2).notifyDataSetChanged();
                }

                //add by @ManhNguyen
                ListAdapter adapter3 = lvGaRan.getAdapter();
                if (adapter3 != null) {
                    lstGaRanSelected.clear();
                    lstGaRanSelected.addAll((ArrayList<ProductWrapper002>) data.getSerializableExtra("list_ga_ran_selected"));
                    ((ComboAdapter002) adapter3).notifyDataSetChanged();
                }

                ListAdapter adapter4 = lvBurgerCom.getAdapter();
                if (adapter4 != null) {
                    lstBurgerComSelected.clear();
                    lstBurgerComSelected.addAll((ArrayList<ProductWrapper002>) data.getSerializableExtra("list_burger_com_selected"));
                    ((ComboAdapter002) adapter4).notifyDataSetChanged();
                }

                //update change list
                Comparator<ProductWrapper002> mComparator = new Comparator<ProductWrapper002>() {
                    @Override
                    public int compare(ProductWrapper002 productWrapper002, ProductWrapper002 t1) {
                        return String.valueOf(productWrapper002.id).compareToIgnoreCase(String.valueOf(t1.id));
                    }
                };

                if (lstSnacksAndDessertChange != null) {
                    for (ProductWrapper002 obj : lstSnacksAndDessertChange)
                        obj.isProductSelected = false;
                    for (ProductWrapper002 obj : lstSnacksAndDessertSelected) {
                        int index = Collections.binarySearch(lstSnacksAndDessertChange, obj, mComparator);
                        if (index >= 0) {
                            lstSnacksAndDessertChange.get(index).isProductSelected = true;
                            addedSnacksPrice += Double.parseDouble(lstSnacksAndDessertChange.get(index).price);
                            Log.e("addedSnacksPrice", addedSnacksPrice + "");
                        }
                    }
                }

                if (lstDrinkChange != null) {
                    for (ProductWrapper002 obj : lstDrinkChange)
                        obj.isProductSelected = false;
                    for (ProductWrapper002 obj : lstDrinkSelected) {
                        int index = Collections.binarySearch(lstDrinkChange, obj, mComparator);
                        if (index >= 0) {
                            lstDrinkChange.get(index).isProductSelected = true;
                            addedDrinksPrice += Double.parseDouble(lstDrinkChange.get(index).price);
                            Log.e("addedDrinksPrice", addedDrinksPrice + "");
                        }
                    }
                }

                //add by @ManhNguyen
                if (lstGaRanChange != null) {
                    for (ProductWrapper002 obj : lstGaRanChange)
                        obj.isProductSelected = false;
                    for (ProductWrapper002 obj : lstGaRanSelected) {
                        int index = Collections.binarySearch(lstGaRanChange, obj, mComparator);
                        if (index >= 0) {
                            lstGaRanChange.get(index).isProductSelected = true;
                            addedGaRanPrice += Double.parseDouble(lstGaRanChange.get(index).price);
                            Log.e("addedGaRanPrice", addedGaRanPrice + "");
                        }
                    }
                }

                if (lstBurgerComChange != null) {
                    for (ProductWrapper002 obj : lstBurgerComChange)
                        obj.isProductSelected = false;
                    for (ProductWrapper002 obj : lstBurgerComSelected) {
                        int index = Collections.binarySearch(lstBurgerComChange, obj, mComparator);
                        if (index >= 0) {
                            lstBurgerComChange.get(index).isProductSelected = true;
                            addedBurgerComPrice += Double.parseDouble(lstBurgerComChange.get(index).price);
                            Log.e("addedBurgerComPrice", addedBurgerComPrice + "");
                        }
                    }
                }

                updateCategoryInfo();
            }
        }
    }

    public void updateCategoryInfo() {
        if (categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
            lstCategoryInfo.clear();


            for (ProductWrapper002 obj : lstProductDefault)
                lstCategoryInfo.add(obj.nameProduct);

            if (countElement == 1) {
                GroupContentWrapper001 p1 = ((ComboAdapter001) lv1.getAdapter()).foodSelected;
                if (p1 != null) {
                    lstCategoryInfo.add(p1.nameProduct);
                }
            } else if (countElement == 2) {
                GroupContentWrapper001 p1 = ((ComboAdapter001) lv1.getAdapter()).foodSelected;
                GroupContentWrapper001 p2 = ((ComboAdapter001) lv2.getAdapter()).foodSelected;
                if (p1 != null)
                    lstCategoryInfo.add(p1.nameProduct);
                if (p2 != null)
                    lstCategoryInfo.add(p2.nameProduct);
            }

            if (lstSnacksAndDessertSelected != null && lstSnacksAndDessertSelected.size() > 0)
                for (ProductWrapper002 obj : lstSnacksAndDessertSelected)
                    lstCategoryInfo.add(obj.nameProduct);

            if (lstDrinkSelected != null && lstDrinkSelected.size() > 0)
                for (ProductWrapper002 obj : lstDrinkSelected)
                    lstCategoryInfo.add(obj.nameProduct);

            //add by @ManhNguyen
            if (lstGaRanSelected != null && lstGaRanSelected.size() > 0)
                for (ProductWrapper002 obj : lstGaRanSelected)
                    lstCategoryInfo.add(obj.nameProduct);

            if (lstBurgerComSelected != null && lstBurgerComSelected.size() > 0)
                for (ProductWrapper002 obj : lstBurgerComSelected)
                    lstCategoryInfo.add(obj.nameProduct);

            //send action to re-load category info list on screen
            ((CategoryInfoAdapter) lvCategoryInfo.getAdapter()).notifyDataSetChanged();

            int count = Integer.parseInt(spnNumber.getSelectedItem().toString().trim());
            tmpPrice = Double.parseDouble(product_price) + addedSnacksPrice + addedDrinksPrice +
                    addedGaRanPrice + addedBurgerComPrice;
            showTotalPrice(tmpPrice * count);
        } else { //add by @ManhNguyen: type = product
            mListFoodSelected = ((ProductAdapter002) lv3.getAdapter()).foodSelected;
            double sumPriceBuyMore = 0;
            int count = Integer.parseInt(spnNumber.getSelectedItem().toString().trim());
            lstCategoryInfo.clear();
            lstCategoryInfo.add(product_name);
            if (mListFoodSelected != null && mListFoodSelected.size() > 0) {
                for (ProductBuyMoreModel productBuyMore : mListFoodSelected) {
                    lstCategoryInfo.add(productBuyMore.name);
                    sumPriceBuyMore += Double.parseDouble(productBuyMore.price);
                }
                tmpPrice = Double.parseDouble(product_price) + sumPriceBuyMore;
                showTotalPrice(tmpPrice * count);
            } else {
                tmpPrice = Double.parseDouble(product_price);
                showTotalPrice(tmpPrice * count);
            }

            //send action to re-load category info list on screen
            ((CategoryInfoAdapter) lvCategoryInfo.getAdapter()).notifyDataSetChanged();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.llMenuBar.setVisibility(View.VISIBLE);
    }

    private class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            int id = adapterView.getId();
            if (id == R.id.spnNumber) {
                if (view != null) {
                    int number = Integer.parseInt(((AppCompatTextView) view).getText().toString().trim());

//                    double tmpPrice = Double.parseDouble(dishSelected.price) * number;
                    double _price = (tmpPrice == 0.0 ? Double.parseDouble(product_price) : tmpPrice) * number;
                    showTotalPrice(_price);

                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (comboAdapter1 != null) {
            comboAdapter1.closeToolTip();
        } else if (comboAdapter2 != null) {
            comboAdapter2.closeToolTip();
        }
        if (simpleTooltip != null) {
            simpleTooltip.dismiss();
        }
    }
}
