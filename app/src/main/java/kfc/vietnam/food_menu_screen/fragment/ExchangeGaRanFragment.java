package kfc.vietnam.food_menu_screen.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.ChangeDishPositionType;
import kfc.vietnam.common.utility.VerticalSpaceItemDecoration;
import kfc.vietnam.food_menu_screen.adapter.ChangeDrinksAdapter;
import kfc.vietnam.food_menu_screen.adapter.ChangeGaRanAdapter;
import kfc.vietnam.food_menu_screen.ui.ChangeDishActivity;

import static android.R.attr.type;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class ExchangeGaRanFragment extends Fragment implements View.OnClickListener {
    private static final int VERTICAL_ITEM_SPACE = 3;
    private boolean isShow = false;
    private ChangeDishActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle b = getArguments();
        this.isShow = b.getBoolean("show_change");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_exchange_ga_ran_fragment, container, false);

        FragmentActivity ctx = (FragmentActivity) container.getContext();
        activity = ((ChangeDishActivity) ctx);

        RecyclerView rcvListDrinks = (RecyclerView) root.findViewById(R.id.rcvListDrinks);
        ChangeGaRanAdapter changeDishAdapter = new ChangeGaRanAdapter(activity.lstGaRanChange, ctx, rcvListDrinks);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        rcvListDrinks.setLayoutManager(mLayoutManager);
        rcvListDrinks.setItemAnimator(new DefaultItemAnimator());
        rcvListDrinks.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        rcvListDrinks.setAdapter(changeDishAdapter);

        RelativeLayout rlCompleted = (RelativeLayout) root.findViewById(R.id.rlCompleted);
        rlCompleted.setOnClickListener(this);
        RelativeLayout rlChangePage = (RelativeLayout) root.findViewById(R.id.rlChangePage);
        rlChangePage.setVisibility(isShow ? View.VISIBLE : View.GONE);
        rlChangePage.setOnClickListener(this);

        TextView tvChangeGaRan = (TextView) root.findViewById(R.id.tvChangeGaRan);
        ImageView imgChangeGaRan = (ImageView) root.findViewById(R.id.imgChangeGaRan);

        //Luong: snack -> drink -> ga ran -> burger -> snack
        if (activity.lstSnacksAndDessertChange != null && activity.lstSnacksAndDessertChange.size() > 0) {
            if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) {
                if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) {
                    tvChangeGaRan.setText(getString(R.string.burger_exchange)); // toi burger
                    imgChangeGaRan.setImageResource(R.drawable.ic_burger);
                } else {
                    tvChangeGaRan.setText(getString(R.string.snacks_exchange)); //quay ve snack
                    imgChangeGaRan.setImageResource(R.drawable.ic_change_snacks);
                }
            }
        }
        else if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) { // k0 co snack
            if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) {
                tvChangeGaRan.setText(getString(R.string.burger_exchange)); // toi burger
                imgChangeGaRan.setImageResource(R.drawable.ic_burger);
            } else {
                tvChangeGaRan.setText(getString(R.string.water_exchange)); //toi drink
                imgChangeGaRan.setImageResource(R.drawable.ic_change_water);
            }
        }
        else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) { //k0 snack, drink
            tvChangeGaRan.setText(getString(R.string.burger_exchange)); //toi burger
            imgChangeGaRan.setImageResource(R.drawable.ic_burger);
        } else {
            tvChangeGaRan.setText(getString(R.string.change_disk)); //luc nay da bi an
        }

        return root;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.rlCompleted:
                activity.backPreviousScreen(true);
                break;

            case R.id.rlChangePage:
                //Luong: snack -> drink -> ga ran -> burger -> snack
                if (activity.lstSnacksAndDessertChange != null && activity.lstSnacksAndDessertChange.size() > 0) {
                    if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) {
                        if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) {
                            activity.viewPagerChangeDish.setCurrentItem(3); //chuyen toi burger
                        } else {
                            activity.viewPagerChangeDish.setCurrentItem(ChangeDishPositionType.SNACKS.getValue()); //luôn luôn ở vị trí 0
                        }
                    }
                }
                else if (activity.lstDrinkChange != null && activity.lstDrinkChange.size() > 0) {
                    if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) {
                        activity.viewPagerChangeDish.setCurrentItem(2); //chuyen toi burger
                    } else {
                        activity.viewPagerChangeDish.setCurrentItem(0); //chuyen toi drink
                    }
                }
                else if (activity.lstBurgerComChange != null && activity.lstBurgerComChange.size() > 0) {
                    activity.viewPagerChangeDish.setCurrentItem(1);
                }
                break;
        }
    }
}
