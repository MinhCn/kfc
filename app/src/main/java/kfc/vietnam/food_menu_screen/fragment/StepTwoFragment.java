package kfc.vietnam.food_menu_screen.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.adapter.WardAdapter;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.AddAddressDataWrapper;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.AddressListDataWrapper;
import kfc.vietnam.common.object.DeleteShipAddressDataWrapper;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.ShowTimeDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.NonScrollListView;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.utility.ResultListener;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.AddressAdapter001;
import kfc.vietnam.food_menu_screen.adapter.DistrictAdapter;
import kfc.vietnam.food_menu_screen.adapter.ProvinceAdapter;
import kfc.vietnam.food_menu_screen.ui.DeliveryTimeActivity;
import kfc.vietnam.food_menu_screen.ui.OrderLimitActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class StepTwoFragment extends Fragment implements View.OnClickListener, AddressAdapter001.Listener {
    private String currentLanguageCode = "";
    private Context ctx;

    private LinearLayout llMore, llAddress;
    private RelativeLayout rlMoreDetail;
    private TextView tvMinPrice, tvMinTime, tvMore;
    private boolean isNewAddress = false;
    private EditText edtFirstNameAndLastName, edtAddress, edtPhoneNumber, edtEmail;
    private Spinner spnProvince, spnDistrict, spnWard;
    private NonScrollListView lvwAddress;
    private AddressAdapter001 adapterAddress;
    private ArrayList<AddressDataWrapper> lstAddress;
    private ImageView imgMoreAddress;
    private MainActivity activity;
    private RelativeLayout rlDeliverToThisAddress;
    private String provinceID;
    private DistrictAdapter districtAdapter;
    private WardAdapter wardAdapter;
    private String districtID;
    private String wardID;
    public ArrayList<ProvinceWrapper> lstProvince = new ArrayList<>();
    public ArrayList<DistrictWrapper> lstDistrict = new ArrayList<>();
    public ArrayList<DistrictWrapper.WardWapper> lstWard = new ArrayList<>();
    private TextView mTvNotice;
    private View llNotice;

    private String idDefault = "-1";
    ProvinceWrapper defaultProvince;
    DistrictWrapper defaultDistrict;
    DistrictWrapper.WardWapper defaultWard;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;

        defaultProvince = new ProvinceWrapper(idDefault, activity.getString(R.string.city_new));
        defaultDistrict = new DistrictWrapper(idDefault, activity.getString(R.string.district));
        defaultWard = new DistrictWrapper.WardWapper(idDefault, activity.getString(R.string.ward));

        this.lstProvince.add(defaultProvince);
        this.lstProvince.addAll(activity.lstProvince);

//        this.lstProvince = activity.lstProvince;
//        this.lstDistrict = lstProvince.get(0).district;
//        this.lstWard = lstDistrict.get(0).lstWard;
//        this.provinceID = lstProvince.get(0).id;
//        this.districtID = lstDistrict.get(0).id;
//        this.wardID = lstWard.get(0).id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_step_two_fragment, container, false);

        llAddress = (LinearLayout) root.findViewById(R.id.llAddress);

        LinearLayout llMoreAddress = (LinearLayout) root.findViewById(R.id.llMoreAddress);
        llMoreAddress.setOnClickListener(this);

        llMore = (LinearLayout) root.findViewById(R.id.llMore);
        imgMoreAddress = (ImageView) root.findViewById(R.id.imgMoreAddress);

        spnProvince = (Spinner) root.findViewById(R.id.spnProvince);
        ProvinceAdapter provinceAdapter = new ProvinceAdapter(ctx, R.layout.custom_item_province_spinner, lstProvince);
        spnProvince.setAdapter(provinceAdapter);
        spnProvince.setSelection(lstProvince.indexOf(activity.currentProvinceSelected));
        spnProvince.setOnItemSelectedListener(onProvinceSelected);

        spnDistrict = (Spinner) root.findViewById(R.id.spnDistrict);
        districtAdapter = new DistrictAdapter(ctx, R.layout.custom_item_district_spinner, lstDistrict);
        spnDistrict.setAdapter(districtAdapter);
        spnDistrict.setOnItemSelectedListener(onDistrictSelected);

        spnWard = (Spinner) root.findViewById(R.id.spnWard);
        wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
        spnWard.setAdapter(wardAdapter);
        spnWard.setOnItemSelectedListener(onWardSelected);


        edtFirstNameAndLastName = (EditText) root.findViewById(R.id.edtFirstNameAndLastName);
        edtAddress = (EditText) root.findViewById(R.id.edtAddress);
        edtPhoneNumber = (EditText) root.findViewById(R.id.edtPhoneNumber);
        edtEmail = (EditText) root.findViewById(R.id.edtEmail);
        edtFirstNameAndLastName.addTextChangedListener(new TextChangedListener(ctx, edtFirstNameAndLastName));
        edtAddress.addTextChangedListener(new TextChangedListener(ctx, edtAddress));
        edtPhoneNumber.addTextChangedListener(new TextChangedListener(ctx, edtPhoneNumber));
        edtEmail.addTextChangedListener(new TextChangedListener(ctx, edtEmail));
        edtEmail.setImeOptions(EditorInfo.IME_ACTION_DONE);

        rlDeliverToThisAddress = (RelativeLayout) root.findViewById(R.id.rlDeliverToThisAddress);
        rlDeliverToThisAddress.setOnClickListener(this);

        lstAddress = new ArrayList<>();
        lvwAddress = (NonScrollListView) root.findViewById(R.id.lvwAddress);
        adapterAddress = new AddressAdapter001(ctx, R.layout.custom_item_address, lstAddress, currentLanguageCode, lvwAddress, this);
        lvwAddress.setAdapter(adapterAddress);
        lvwAddress.setOnItemClickListener(adapterAddress);
        mTvNotice = (TextView) root.findViewById(R.id.tvNameReceived);
        llNotice = root.findViewById(R.id.llNotice);
        checkOpenDeliver(activity.currentProvinceSelected.id, "0", null);

        rlMoreDetail = (RelativeLayout) root.findViewById(R.id.rlMoreDetail);
        tvMinPrice = (TextView) root.findViewById(R.id.tvMinPrice);
        tvMinTime = (TextView) root.findViewById(R.id.tvMinTime);
        tvMore = (TextView) root.findViewById(R.id.tvMore);
        tvMore.setOnClickListener(this);

        if (activity.userInfo != null) {
            getShipAddressList(activity.userInfo.id);
            edtFirstNameAndLastName.setText(activity.userInfo.name + "");
            edtEmail.setText(activity.userInfo.email + "");
        }

        return root;
    }


    private AdapterView.OnItemSelectedListener onProvinceSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i == 0) {
                provinceID = "";

                districtID = "";
                lstDistrict.clear();
                lstDistrict.add(defaultDistrict);
                spnDistrict.setSelection(0);

                wardID = "";
                lstWard.clear();
                lstWard.add(defaultWard);
            } else {
                provinceID = lstProvince.get(i).id;

//                lstDistrict = lstProvince.get(i).district;
                lstDistrict.clear();
                lstDistrict.add(defaultDistrict);
                lstDistrict.addAll(lstProvince.get(i).district);

//                districtID = lstDistrict.get(0).id;

//                lstWard = lstDistrict.get(0).lstWard;
                lstWard.clear();
                lstWard.add(defaultWard);

//                wardID = lstWard.get(0).id;
                if (getActivity() != null) {
                    districtAdapter = new DistrictAdapter(getActivity(), R.layout.custom_item_district_spinner, lstDistrict);
                    spnDistrict.setAdapter(districtAdapter);
                    spnDistrict.setOnItemSelectedListener(onDistrictSelected);
                    wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                    spnWard.setAdapter(wardAdapter);
                    spnWard.setOnItemSelectedListener(onWardSelected);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private AdapterView.OnItemSelectedListener onDistrictSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i == 0) {
                districtID = "";

                wardID = "";
                lstWard.clear();
                lstWard.add(defaultWard);
                spnWard.setSelection(0);
            } else {
                districtID = lstDistrict.get(i).id;

//                lstWard = lstDistrict.get(i).lstWard;
                lstWard.clear();
                lstWard.add(defaultWard);
                lstWard.addAll(lstDistrict.get(i).lstWard);

//                wardID = lstWard.get(0).id;
                if (getActivity() != null) {
                    wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                    spnWard.setAdapter(wardAdapter);
                    spnWard.setOnItemSelectedListener(onWardSelected);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private AdapterView.OnItemSelectedListener onWardSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i == 0) {
                wardID = "";
            } else {
                wardID = lstWard.get(i).id;
                if (lstWard.get(i).price.priceMax == null) {
                    rlMoreDetail.setVisibility(View.GONE);
                } else {
                    rlMoreDetail.setVisibility(View.VISIBLE);
                    DecimalFormat myFormatter = new DecimalFormat("###,###");
                    String output = myFormatter.format(lstWard.get(i).price.priceMax);
                    tvMinPrice.setText(output);
                    tvMinTime.setText(lstWard.get(i).price.timeMax);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(StepTwoFragment.class);
                }
            }
        });

        return anim;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvMore:
                Intent intents = new Intent(ctx, OrderLimitActivity.class);
                intents.putExtra("data", MainActivity.sProvineId);
                ctx.startActivity(intents);
                break;

            case R.id.llMoreAddress:
                switchAddress();
                break;

            case R.id.rlDeliverToThisAddress:
                String name = edtFirstNameAndLastName.getText().toString().trim();
                String address = edtAddress.getText().toString().trim();
                String phoneNumber = edtPhoneNumber.getText().toString().trim();
                String email = edtEmail.getText().toString().trim();

                if (!name.isEmpty()) {
                    if (!address.isEmpty()) { //&& address.length() >= 10
                        if (!provinceID.equals("")) {
                            if (!districtID.equals("")) {
                                if (!wardID.equals("")) {
                                    if (activity.isValidatePhoneNumber(phoneNumber)) {
                                        if (!email.isEmpty()) {
                                            if (activity.patternMail(email)) {
                                                DistrictWrapper.WardWapper ward = lstWard.get(spnWard.getSelectedItemPosition());
                                                if (activity.totalPrice == -1d || ward.price.priceMax == null || activity.totalPrice >= ward.price.priceMax) {
                                                    addShipAddress(activity.userInfo.id, name, provinceID, districtID, wardID, address, phoneNumber, email);
                                                } else {
                                                    DecimalFormat myFormatter = new DecimalFormat("###,###");
                                                    String output = myFormatter.format(ward.price.priceMax);
                                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                                                    alertDialog.setMessage(activity.getString(R.string.order_limitation_alert, output, ward.name));

                                                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface arg0, int arg1) {
                                                            arg0.dismiss();
                                                        }
                                                    });

                                                    AlertDialog dialog = alertDialog.create();
                                                    dialog.show();
                                                }
                                            } else {
                                                edtEmail.requestFocus();
                                                edtEmail.setError(getString(R.string.StepOneFragment_006));
                                                rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                            }
                                        } else {
                                            edtEmail.requestFocus();
                                            edtEmail.clearComposingText();
                                            edtEmail.setError(getString(R.string.StepOneFragment_004));
                                            rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                        }
                                    } else {
                                        edtPhoneNumber.requestFocus();
                                        edtPhoneNumber.clearComposingText();
                                        edtPhoneNumber.setError(getString(R.string.StepOneFragment_003));
                                        rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                    }
                                } else {
                                    Toast.makeText(ctx, getString(R.string.StepOneFragment_013), Toast.LENGTH_SHORT).show();
                                    rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                                }
                            } else {
                                Toast.makeText(ctx, getString(R.string.StepOneFragment_012), Toast.LENGTH_SHORT).show();
                                rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                            }
                        } else {
                            Toast.makeText(ctx, getString(R.string.StepOneFragment_011), Toast.LENGTH_SHORT).show();
                            rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                        }
                    } else {
                        edtAddress.requestFocus();
                        edtAddress.clearComposingText();
                        edtAddress.setError(getString(R.string.StepOneFragment_002));
                        rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                    }
                } else {
                    edtFirstNameAndLastName.requestFocus();
                    edtFirstNameAndLastName.clearComposingText();
                    edtFirstNameAndLastName.setError(getString(R.string.StepOneFragment_001));
                    rlDeliverToThisAddress.startAnimation(AnimationUtils.loadAnimation(ctx, R.anim.shake_error));
                }
                break;
        }
    }

    private void showAddressList() {
        if (lstAddress != null && lstAddress.size() > 0) {
            llAddress.setVisibility(View.VISIBLE);
        }
        llMore.setVisibility(View.GONE);
        imgMoreAddress.setImageResource(R.drawable.ic_add);
    }

    private void switchAddress() {
        if (!isNewAddress) {
            llAddress.setVisibility(View.GONE);
            llMore.setVisibility(View.VISIBLE);
            imgMoreAddress.setImageResource(R.drawable.ic_minus);
        } else
            showAddressList();
        isNewAddress = !isNewAddress;
    }

    private void getShipAddressList(String userID) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<AddressListDataWrapper> call1 = ApiClient.getJsonClient().getShipAddressList(userID, new String(Hex.encodeHex(DigestUtils.md5(userID))));
        call1.enqueue(new Callback<AddressListDataWrapper>() {

            @Override
            public void onFailure(Call<AddressListDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<AddressListDataWrapper> arg0, Response<AddressListDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    lstAddress.clear();
                    lstAddress.addAll(arg1.body().data);
                    adapterAddress.notifyDataSetChanged();

                    if (lstAddress != null && lstAddress.size() > 0)
                        showAddressList();
                    else {
                        isNewAddress = false;
                        switchAddress();
                    }
                }
            }
        });
    }

    private void addShipAddress(String userID, String fullName, String city, String district, String wardID, String address, String phone, String email) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<AddAddressDataWrapper> call1 = ApiClient.getJsonClient().addShipAddress(userID, new String(Hex.encodeHex(DigestUtils.md5(userID))), fullName, city, district, wardID, address, phone, email);
        call1.enqueue(new Callback<AddAddressDataWrapper>() {

            @Override
            public void onFailure(Call<AddAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<AddAddressDataWrapper> arg0, Response<AddAddressDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    AddAddressDataWrapper obj = arg1.body();
                    if (obj.result) {
                        //add address to list address
                        isNewAddress = false;
                        //Hiện tại tạm thời gọi api address để load lại
                        if (activity.userInfo != null)
                            getShipAddressList(activity.userInfo.id);
                    }
                }
            }
        });
    }

    @Override
    public void deleteAddress(String addressID, int positionToDelete) {
        deleteShipAddress(addressID, positionToDelete);
    }

    @Override
    public void DeliverToThisAddress(final AddressDataWrapper addressToShip) {
        checkOpenDeliver(addressToShip.city, addressToShip.ward, new ResultListener<ShowTimeDataWrapper>() {
            @Override
            public void onSucceed(ShowTimeDataWrapper result) {
                if (!result.isResult()) {
                    try {
                        //save address
                        ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), ObjectSerializer.serialize(addressToShip)).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    activity.addressToShip = addressToShip;
                    activity.changeFragment(new StepThreeFragment());

                } else if (result.getData() != null) {
                    if (!result.getData().isShowTime()) {
                        try {
                            //save address
                            ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), ObjectSerializer.serialize(addressToShip)).commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        activity.addressToShip = addressToShip;
                        activity.changeFragment(new StepThreeFragment());
                    } else {
                        showAlert(result.getMessage());
                    }
                }

            }

            @Override
            public void onFail(String reason) {

            }
        });


    }

    private void checkOpenDeliver(String provinceID, final String wardIDs, final ResultListener<ShowTimeDataWrapper> callBack) {
        activity.checkOpen(provinceID, wardIDs, new ResultListener<ShowTimeDataWrapper>() {
            @Override
            public void onSucceed(ShowTimeDataWrapper result) {
                if (result.getData().isShowTime()) {
                    customTextView(getActivity(), mTvNotice, result.getData().getMinTime());
                    llNotice.setVisibility(View.VISIBLE);
                }
                if (callBack != null)
                    callBack.onSucceed(result);


            }

            @Override
            public void onFail(String reason) {

            }
        });

    }

    private void showAlert(String message) {
        if (TextUtils.isEmpty(message))
            message = getString(R.string.delivery_warning);
        new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void deleteShipAddress(String addressID, final int positionToDelete) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, ctx.getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<DeleteShipAddressDataWrapper> call1 = ApiClient.getJsonClient().deleteShipAddress(addressID);
        call1.enqueue(new Callback<DeleteShipAddressDataWrapper>() {

            @Override
            public void onFailure(Call<DeleteShipAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();

                Toast.makeText(ctx, ctx.getString(R.string.StepTwoFragment_003), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<DeleteShipAddressDataWrapper> arg0, Response<DeleteShipAddressDataWrapper> arg1) {
                mProgressDialog.dismiss();

                lstAddress.remove(positionToDelete);
                adapterAddress.notifyDataSetChanged();
                if (lstAddress != null && lstAddress.size() == 0) {
                    isNewAddress = false;
                    llAddress.setVisibility(View.GONE);
                }
                Toast.makeText(ctx, ctx.getString(R.string.StepTwoFragment_002), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void customTextView(final Activity mActivity, TextView view, String time) {
        String sNotice = mActivity.getResources().getText(R.string.delivery_time_notice).toString();
        String sAfter = mActivity.getString(R.string.delivery_time_after, time).toString();
        String sContent = mActivity.getResources().getText(R.string.delivery_time_content).toString();
        String sList = mActivity.getResources().getText(R.string.delivery_time_list).toString();

        SpannableStringBuilder builder = new SpannableStringBuilder();
        NonUnderlinedClickableSpan clickableSpan = new NonUnderlinedClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intents = new Intent(mActivity, DeliveryTimeActivity.class);
                intents.putExtra("data", MainActivity.sProvineId);
                mActivity.startActivityForResult(intents, 1024);

            }
        };
        SpannableString redSpannable = new SpannableString(sNotice);
        redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, sNotice.length(), 0);
        redSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, sNotice.length(), 0);
        builder.append(redSpannable);

        SpannableString balckSpannable = new SpannableString(sAfter);
        balckSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, sAfter.length(), 0);
        balckSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, sAfter.length(), 0);
        builder.append(balckSpannable);

        SpannableString whiteSpannable = new SpannableString(sContent);
        whiteSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, sContent.length(), 0);
        builder.append(whiteSpannable);
        SpannableString space = new SpannableString(" ");
        builder.append(space);

        SpannableString redSmallSpannable = new SpannableString(sList);
        redSmallSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, sList.length(), 0);
        redSmallSpannable.setSpan(new StyleSpan(Typeface.NORMAL), 0, sList.length(), 0);
        builder.append(redSmallSpannable);
        int lengFist = sNotice.length() + sAfter.length() + sContent.length() + 1;
        builder.setSpan(clickableSpan, lengFist, (lengFist + sList.length()), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        view.setText(builder, TextView.BufferType.SPANNABLE);
        view.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private class TextChangedListener implements TextWatcher {
        EditText editText;
        private Context mContext;

        public TextChangedListener(Context context, EditText edt) {
            super();
            this.mContext = context;
            this.editText = edt;
        }

        @Override
        public void afterTextChanged(Editable s) {
            editText.setError(null, null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    public static class NonUnderlinedClickableSpan extends ClickableSpan {

        @Override
        public void updateDrawState(TextPaint ds) {

            ds.setColor(Color.RED);
            ds.setUnderlineText(false);

        }

        @Override
        public void onClick(View v) {

        }
    }
}
