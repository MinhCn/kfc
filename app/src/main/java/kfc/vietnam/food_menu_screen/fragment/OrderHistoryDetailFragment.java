package kfc.vietnam.food_menu_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.PaymentType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.DeatilOrderHistoryWrapper;
import kfc.vietnam.common.object.OrderHistoryDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.NonScrollListView;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.OrderHistoryDetailAdapter;
import kfc.vietnam.main_screen.ui.MainActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kfc.vietnam.R.id.tvShippingFee3;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class OrderHistoryDetailFragment extends Fragment {
    private Context ctx;
    private MainActivity activity;
    TextView tvTongDonHang, tvDonHang, tvWard;
    private NonScrollListView lvwOrderHistoryDetail;
    private TextView tvNguoiNhan, tvDiaChiNguoiNhan, tvTPNguoiNhan, tvQuanNguoiNhan, tvDienThoai, tvEmail, tvPhiVanChuyen, tvPhuongThucThanhToan;
    private TextView tvDiemTichLuy, tvGiamGia;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_order_history_detail, container, false);

        OrderHistoryDataWrapper.OrderHistoryWrapper obj = (OrderHistoryDataWrapper.OrderHistoryWrapper) getArguments().getSerializable("order_info");

//        TextView tvMaDonHang = (TextView) root.findViewById(R.id.tvMaDonHang);
//        tvMaDonHang.setText(obj.orderID + "");
//
//        TextView tvNgayDatHang = (TextView) root.findViewById(R.id.tvNgayDatHang);
//        tvNgayDatHang.setText(obj.date);
//
//        TextView tvTrangThaiDonHang = (TextView) root.findViewById(R.id.tvTrangThaiDonHang);
//        tvTrangThaiDonHang.setText(obj.deliveryStatus);

        tvNguoiNhan = (TextView) root.findViewById(R.id.tvNguoiNhan);
        tvDiaChiNguoiNhan = (TextView) root.findViewById(R.id.tvDiaChiNguoiNhan);
        tvTPNguoiNhan = (TextView) root.findViewById(R.id.tvTPNguoiNhan);
        tvQuanNguoiNhan = (TextView) root.findViewById(R.id.tvQuanNguoiNhan);
        tvDienThoai = (TextView) root.findViewById(R.id.tvDienThoai);
        tvEmail = (TextView) root.findViewById(R.id.tvEmail);
        tvPhiVanChuyen = (TextView) root.findViewById(R.id.tvPhiVanChuyen);
        tvPhuongThucThanhToan = (TextView) root.findViewById(R.id.tvPhuongThucThanhToan);
        lvwOrderHistoryDetail = (NonScrollListView) root.findViewById(R.id.lvwOrderHistoryDetail);
        tvDiemTichLuy = (TextView) root.findViewById(R.id.tvDiemTichLuy);
        tvGiamGia = (TextView) root.findViewById(R.id.tvGiamGia);
        tvDonHang = (TextView) root.findViewById(R.id.tvDonHang);
        tvTongDonHang = (TextView) root.findViewById(R.id.tvTongDonHang);
        root.findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        tvWard = (TextView) root.findViewById(R.id.tvWard);

        if (obj != null)
            getOrderHistory(obj.orderID, activity.currentLanguageCode);

        return root;
    }


    private void getOrderHistory(String orderID,
                                 String lang) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<DeatilOrderHistoryWrapper> call1 = ApiClient.getJsonClient().getDetailOrderHistory(orderID,
                lang);
        call1.enqueue(new Callback<DeatilOrderHistoryWrapper>() {

            @Override
            public void onFailure(Call<DeatilOrderHistoryWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<DeatilOrderHistoryWrapper> arg0, Response<DeatilOrderHistoryWrapper> arg1) {
                mProgressDialog.dismiss();
                try {
                    DeatilOrderHistoryWrapper wrapper = arg1.body();

                    if (wrapper != null && wrapper.data != null) {
                        showData(wrapper.data);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showData(OrderHistoryDataWrapper.OrderHistoryWrapper obj) {
        tvNguoiNhan.setText(obj.order_name);
        tvDiaChiNguoiNhan.setText(obj.order_address);
        tvTPNguoiNhan.setText(obj.order_city);
        tvQuanNguoiNhan.setText(obj.order_district);

        tvDienThoai.setText(obj.order_phone);
        tvEmail.setText(obj.order_email);
        if (!TextUtils.isEmpty(obj.order_ward)) {
            tvWard.setText(getString(R.string.ward) + ": " + obj.order_ward);
        }


        if (obj.payment_type.equalsIgnoreCase(PaymentType.PAYMENT_ONLINE.getValue()))
            tvPhuongThucThanhToan.setText(activity.getString(R.string.OrderHistoryFragment_008));
        else if (obj.payment_type.equalsIgnoreCase(PaymentType.PAYMENT_ATM.getValue()))
            tvPhuongThucThanhToan.setText(activity.getString(R.string.OrderHistoryFragment_010));
        else
            tvPhuongThucThanhToan.setText(activity.getString(R.string.OrderHistoryFragment_007));

        try {
//            showPrice(Double.parseDouble(obj.price) + Double.parseDouble(obj.price_discount) , 2);
            //Edit by @ManhNguyen: "tổng đơn hàng": obj.price: giá all in, trừ ship - giá giá mới ra giá này:
            //double tongDonHang = Double.parseDouble(obj.price) - Double.parseDouble(obj.price_discount) - Double.parseDouble(activity.conf.point.shipPrice);
            //Edit by Nhat Minh
            double tongDonHang = Double.parseDouble(obj.price) + Double.parseDouble(obj.price_discount) - Double.parseDouble(activity.conf.point.shipPrice);
            showPrice(tongDonHang, 2);

            tvDiemTichLuy.setText("+" + activity.calculatePoint(Double.parseDouble(obj.price) + Double.parseDouble(obj.price_discount))
                    + " " + activity.getString(R.string.OrderHistoryFragment_009));
            showPrice(Double.parseDouble(obj.price), 1);

            if (Double.parseDouble(obj.price_discount) > 0) {

                showPrice(Double.parseDouble(obj.price_discount), 3);

            } else {

                tvGiamGia.setText("0 " + getString(R.string.vnd_cap));

            }


        } catch (Exception e) {

        }

//        tvPhiVanChuyen.setText(activity.getString(R.string.OrderHistoryDetailFragment_017));
        ArrayList<OrderHistoryDataWrapper.ProductWrapper> lst = obj.product;
        OrderHistoryDetailAdapter adapter = new OrderHistoryDetailAdapter(ctx, R.layout.custom_item_order_history_detail, lst);
        lvwOrderHistoryDetail.setAdapter(adapter);

        if (activity.conf.point.shipPrice.equalsIgnoreCase("0")) {
            tvPhiVanChuyen.setText(activity.getString(R.string.free).toUpperCase());
        } else {
            tvPhiVanChuyen.setText(formatMinPrice(Double.parseDouble(activity.conf.point.shipPrice)).replace(",", "."));
        }
    }

    private String formatMinPrice(double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(totalPrice).replace(".", ",");
            return output + " " + getString(R.string.vnd_cap);
        } else
            return "0 " + getString(R.string.vnd_cap);
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
//                if (enter) {
//                    activity.processChangeTopMenu(OrderHistoryDetailFragment.class);
//                }
            }
        });

        return anim;
    }

    private void showPrice(double totalPrice, int type) {
        DecimalFormat myFormatter = new DecimalFormat("#,##0");
        String output = myFormatter.format(totalPrice);
        try {
            output = output.replace(".", ",");
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<String> lst = new ArrayList(Arrays.asList(output.split(",")));
        if (lst.size() >= 2)
            lst.remove(lst.size() - 1);
        output = lst.toString().replace("[", "").replace("]", "").replace(" ", "") + ".000 " + getString(R.string.vnd_cap);

        int start = 0;
        int end = output.indexOf(".");
        output = output.replace(",", ".");
        if (type == 3) {
            tvGiamGia.setText(output);
            return;
        }
        SpannableString spanContent = new SpannableString(output);
        spanContent.setSpan(new AbsoluteSizeSpan(18, true), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
        //spanContent.setSpan(new RelativeSizeSpan(2f), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
        spanContent.setSpan(new ForegroundColorSpan(Color.BLACK), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
        if (type == 1) {
            spanContent.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// s
            tvTongDonHang.setText(spanContent);
        } else if (type == 2) {
            spanContent.setSpan(new ForegroundColorSpan(Color.BLACK), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// s
            tvDonHang.setText(spanContent);
        }
    }

}
