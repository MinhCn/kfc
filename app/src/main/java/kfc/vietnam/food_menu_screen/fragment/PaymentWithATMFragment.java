package kfc.vietnam.food_menu_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Locale;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.PaymentType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.OrderDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class PaymentWithATMFragment extends Fragment {
    private Context ctx;
    private MainActivity activity;
    private WebView wvPayment;

    private SharedPreferences preferencesShoppingCard;
    private SharedPreferences preferencesDeliverAddress;

    private String mOrderID = "";
    private String totalPrice = "0";
    private String mPoint = "0";


    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;

        Bundle b = getArguments();
        mOrderID = b.getString("order_id", "");
        totalPrice = b.getString("total_price", "0");
        mPoint = b.getString("point", "0");

        preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
        preferencesDeliverAddress = ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE);

        activity.llMenuBar.setVisibility(View.GONE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.llMenuBar.setVisibility(View.VISIBLE);
    }

    private boolean paymentSuccess = false;
    private String paymentLogContent = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_payment, container, false);

        wvPayment = (WebView) root.findViewById(R.id.wvPayment);
        wvPayment.getSettings().setJavaScriptEnabled(true);
        wvPayment.getSettings().setLoadWithOverviewMode(true);
        wvPayment.getSettings().setUseWideViewPort(true);
        wvPayment.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d("anhduc", "shouldOverrideUrlLoading -> URL: " + url);
                activity.tvPaymentURL.setText(url);
                if (url.contains("vpc_ResponseCode=0")) {
                    paymentSuccess = true;
                    paymentLogContent = url;
                } else if (url.contains("kfcvietnam")) {
                    if (paymentSuccess) {
                        if (activity.userInfo != null)
                            payment(activity.userInfo.id, activity.currentLanguageCode, activity.currentProvinceSelected.id, PaymentType.PAYMENT_ONLINE.getValue(), "", "", mOrderID, paymentLogContent);
                        else
                            payment("", activity.currentLanguageCode, activity.currentProvinceSelected.id, PaymentType.PAYMENT_ONLINE.getValue(), "", "", mOrderID, paymentLogContent);
                        return true;
                    } else {
                        Bundle b = new Bundle();
                        b.putString("order_id", mOrderID);
                        b.putBoolean("fail", true);
                        OrderStatusFragment f = new OrderStatusFragment();
                        f.setArguments(b);
                        activity.changeFragment(f);
                        return true;
                    }
                } else if (url.contains("canceled")) {
                    activity.backToPreviousScreen();
                    return true;
                }

                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d("anhduc", "onPageFinished -> URL: " + url);
                super.onPageFinished(view, url);
            }
        });
        getCertificate();

        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(PaymentWithATMFragment.class);
                }
            }
        });

        return anim;
    }

    private boolean processEndPayment() {
        activity.tvAmountShoppingCart.setText("0");
        return preferencesShoppingCard.edit().remove(SharedPreferenceKeyType.SHOPPING_CART.toString()).commit() && preferencesDeliverAddress.edit().remove(SharedPreferenceKeyType.DELIVER_ADDRESS.toString()).commit();
    }

    private void getCertificate() {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        String SecureHash = "198BE3F2E8C75A53F38C1C4A5B6DBA27";
        String AccessCode = "ECAFAB";
        Float _price = Float.parseFloat(totalPrice);
        _price = _price * 100;
        String Amount = "0";
        if (_price % 1 == 0) {
            Amount = String.format(Locale.US, "%.0f", _price);
        } else {
            Amount = String.format(Locale.US, "%.1f", _price);
        }
        String BackURL = ApiClient.ENDPOINT + "payment/callback";
        String CardType = "payVNDvn";
        String MerchTxnRef = "KFC_" + mOrderID;
        String MerchantID = "SMLTEST";
        String OrderID = mOrderID;
        String ATM = "ATM";
        String ReturnURL = ApiClient.ENDPOINT + "payment/successNapas";
        String ip = "::12.0";

        String Hash = SecureHash + AccessCode + Amount + BackURL + CardType + MerchTxnRef + MerchantID + OrderID +
                ATM + ReturnURL + ip;

        String md5Hex = new String(Hex.encodeHex(DigestUtils.md5(Hash)));

        String post = "vpc_Version=2.0&vpc_Command=pay&vpc_AccessCode=ECAFAB&vpc_MerchTxnRef=" + MerchTxnRef
                + "&vpc_Merchant=SMLTEST&vpc_OrderInfo=" + mOrderID + "&vpc_Amount=" + Amount
                + "&vpc_ReturnURL=" + ApiClient.ENDPOINT +"payment/successNapas&vpc_BackURL=" + ApiClient.ENDPOINT +"payment/callback&vpc_Locale=vn&vpc_CurrencyCode=VND&vpc_TicketNo=::1" +
                "&vpc_PaymentGateway=ATM&vpc_CardType=&vpc_SecureHash=" + md5Hex.toUpperCase();


        wvPayment.loadUrl("https://sandbox.napas.com.vn/gateway/vpcpay.do?" + post);

        mProgressDialog.dismiss();
    }

    private void payment(String user,
                         String lang,
                         String city,
                         String paymentType,
                         String requestId,
                         String encryptedPaymentData,
                         final String orderID, String pamentLog) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<OrderDataWrapper> call1 = ApiClient.getJsonClient().payment(user,
                lang,
                city,
                paymentType,
                requestId,
                encryptedPaymentData,
                orderID, pamentLog);
        call1.enqueue(new Callback<OrderDataWrapper>() {

            @Override
            public void onFailure(Call<OrderDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();

                Toast.makeText(ctx, getString(R.string.StepThreeFragment_014), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<OrderDataWrapper> arg0, Response<OrderDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    OrderDataWrapper obj = arg1.body();
                    if (obj.result) {
                        if (processEndPayment()) {
                            Bundle b = new Bundle();
                            b.putString("order_id", mOrderID);
                            b.putString("point", mPoint);

                            OrderStatusFragment f = new OrderStatusFragment();
                            f.setArguments(b);
                            activity.changeFragment(f);
                        }
                    }
                }
            }
        });
    }
}
