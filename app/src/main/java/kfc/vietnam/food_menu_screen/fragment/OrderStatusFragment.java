package kfc.vietnam.food_menu_screen.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.DeatilOrderHistoryWrapper;
import kfc.vietnam.common.object.OrderHistoryDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 7/28/2016.
 */
public class OrderStatusFragment extends Fragment implements View.OnClickListener {
    private Context ctx;
    private String orderID;
    private String point = "0";
    private MainActivity activity;
    private WebView webView;
    private String errorMsg = "";

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);
        activity = (MainActivity) ctx;

        Bundle b = getArguments();
        this.orderID = b.getString("order_id");
        this.point = b.getString("point");
        errorMsg = b.getString("errorMsg");

        activity.llMenuBar.setVisibility(View.GONE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.llMenuBar.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_order_status, container, false);

        initWebView(root);

        TextView tvOrderCode = (TextView) root.findViewById(R.id.tvOrderCode);
        tvOrderCode.setText(orderID);

        TextView tvReceivePoint = (TextView) root.findViewById(R.id.tvReceivePoint);
        tvReceivePoint.setText(point);

        TextView tvTitleTransaction = (TextView) root.findViewById(R.id.tvTitleTransaction);
        tvTitleTransaction.setPaintFlags(tvTitleTransaction.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvTitleTransaction.setOnClickListener(this);

        LinearLayout llBackOnTheMenu = (LinearLayout) root.findViewById(R.id.llBackOnTheMenu);
        llBackOnTheMenu.setOnClickListener(this);

        if(activity.userInfo == null){
            ((LinearLayout) root.findViewById(R.id.llOrderHistory)).setVisibility(View.GONE);
            ((LinearLayout) root.findViewById(R.id.llPointTotal)).setVisibility(View.GONE);
        }

        TextView tvErrorMsg = (TextView) root.findViewById(R.id.tvErrorMsg);
        LinearLayout llError = (LinearLayout) root.findViewById(R.id.llError);
        LinearLayout llThanks = (LinearLayout) root.findViewById(R.id.llThanks);

        if(!TextUtils.isEmpty(errorMsg)) {
            llThanks.setVisibility(View.GONE);
            llError.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(errorMsg);
        }

        return root;
    }
    
    public void initWebView(View root){
        webView = (WebView) root.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(false);
        //improve webView performance
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.getSettings().setAppCacheEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSettings.setUseWideViewPort(true);
        webSettings.setSavePassword(true);
        webSettings.setSaveFormData(true);
        webSettings.setEnableSmoothTransition(true);
        webView.loadUrl(ApiClient.ENDPOINT +  "api/socket");
    }

    @Override
    public void onResume() {
        super.onResume();

        //Process key event in fragment
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    backToMainMenu();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(OrderStatusFragment.class);
                }
            }
        });

        return anim;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llBackOnTheMenu:
                backToMainMenu();
                break;

            case R.id.tvTitleTransaction:
                getOrderHistory(orderID,  activity.currentLanguageCode);
                break;
        }
    }

    private void backToMainMenu() {
        activity.backToMainMenu();
    }

    private void getOrderHistory(String orderID,
                                 String lang) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<DeatilOrderHistoryWrapper> call1 = ApiClient.getJsonClient().getDetailOrderHistory(orderID,
                lang);
        call1.enqueue(new Callback<DeatilOrderHistoryWrapper>() {

            @Override
            public void onFailure(Call<DeatilOrderHistoryWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<DeatilOrderHistoryWrapper> arg0, Response<DeatilOrderHistoryWrapper> arg1) {
                mProgressDialog.dismiss();
                DeatilOrderHistoryWrapper obj = arg1.body();
                OrderHistoryDataWrapper.OrderHistoryWrapper historyWrapper = obj.data;
                Bundle b = new Bundle();
                b.putSerializable("order_info", historyWrapper);
                OrderHistoryDetailFragment f = new OrderHistoryDetailFragment();
                f.setArguments(b);
                activity.changeFragment(f);
            }
        });
    }

}
