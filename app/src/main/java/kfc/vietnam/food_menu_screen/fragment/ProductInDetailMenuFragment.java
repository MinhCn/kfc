package kfc.vietnam.food_menu_screen.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.ProductCategoryWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.food_menu_screen.adapter.ProductDetailAdapter;


public class ProductInDetailMenuFragment extends Fragment {
    private String currentLanguageCode = "";
    private ArrayList<ProvinceWrapper> lstProvince;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_detail_menu_fragment, container, false);

        Bundle b = this.getArguments();
        ProductCategoryWrapper category = (ProductCategoryWrapper) b.getSerializable("category");
        this.currentLanguageCode = b.getString("language_code", "en");
        this.lstProvince = (ArrayList<ProvinceWrapper>) b.getSerializable("province_list");

        initRecyclerViews(v, category);
        return v;
    }

    private void initRecyclerViews(View v, ProductCategoryWrapper category) {
        RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.rcvListCombo);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider_listview));

        ProductDetailAdapter mAdapter = new ProductDetailAdapter(getActivity(), category, currentLanguageCode, lstProvince);
        mRecyclerView.setAdapter(mAdapter);
    }

}
