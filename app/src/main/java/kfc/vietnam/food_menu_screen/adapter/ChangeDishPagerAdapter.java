package kfc.vietnam.food_menu_screen.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import kfc.vietnam.common.enums.ChangeDishPositionType;
import kfc.vietnam.food_menu_screen.fragment.ExchangeBurgerComFragment;
import kfc.vietnam.food_menu_screen.fragment.ExchangeDrinksFragment;
import kfc.vietnam.food_menu_screen.fragment.ExchangeGaRanFragment;
import kfc.vietnam.food_menu_screen.fragment.ExchangeSnacksFragment;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class ChangeDishPagerAdapter extends FragmentPagerAdapter {
    private int NUM_ITEMS;
    private ArrayList<Integer> type;
    private Fragment fragment;
    private boolean isHasDefault;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();


    public ChangeDishPagerAdapter(FragmentManager fm, int size, ArrayList<Integer> type, int currentItem) {
        super(fm);
        NUM_ITEMS = size;
        isHasDefault = false;

        //Dựa vào current item người dụng chọn để "sort" item đó lên đầu list type (danh sách các product có thế đổi), SNACK == 0 luôn ở đầu list nên không xét
        this.type = new ArrayList<>();
        if (size > 1) {
            if (currentItem == ChangeDishPositionType.DRINKS.getValue()){
                for (Integer t:type) {
                    if (t == currentItem) {
                        this.type.add(t);
                        type.remove(t);
                        break;
                    }
                }
                this.type.addAll(type);
            } else if (currentItem == ChangeDishPositionType.GARAN.getValue()){
                for (Integer t:type) {
                    if (t == currentItem) {
                        this.type.add(t);
                        type.remove(t);
                        break;
                    }
                }
                this.type.addAll(type);
            } else if (currentItem == ChangeDishPositionType.BURGER.getValue()){
                for (Integer t:type) {
                    if (t == currentItem) {
                        this.type.add(t);
                        type.remove(t);
                        break;
                    }
                }
                this.type.addAll(type);
            } else {
                this.type = type;
            }
        } else {
            this.type = type;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
                for (Integer t : type) {
                    if (t == ChangeDishPositionType.SNACKS.getValue()) {
                        ExchangeSnacksFragment snacksFragment = new ExchangeSnacksFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("show_change", NUM_ITEMS > 1);
                        snacksFragment.setArguments(bundle);
                        type.remove(t);
                        if (!isHasDefault) {
                            fragment = snacksFragment;
                            isHasDefault = true;
                        }
                        return snacksFragment;
                    } else if (t == ChangeDishPositionType.DRINKS.getValue()) {
                        ExchangeDrinksFragment drinksFragment = new ExchangeDrinksFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("show_change", NUM_ITEMS > 1);
                        drinksFragment.setArguments(bundle);
                        type.remove(t);
                        if (!isHasDefault) {
                            fragment = drinksFragment;
                            isHasDefault = true;
                        }
                        return drinksFragment;
                    } else if (t == ChangeDishPositionType.GARAN.getValue()) {
                        ExchangeGaRanFragment gaRanFragment = new ExchangeGaRanFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("show_change", NUM_ITEMS > 1);
                        gaRanFragment.setArguments(bundle);
                        type.remove(t);
                        if (!isHasDefault) {
                            fragment = gaRanFragment;
                            isHasDefault = true;
                        }
                        return gaRanFragment;
                    } else {//t == ChangeDishPositionType.BURGER.getValue()
                        ExchangeBurgerComFragment burgerComFragment = new ExchangeBurgerComFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("show_change", NUM_ITEMS > 1);
                        burgerComFragment.setArguments(bundle);
                        type.remove(t);
                        if (!isHasDefault) {
                            fragment = burgerComFragment;
                            isHasDefault = true;
                        }
                        return burgerComFragment;
                    }
                }
            default:
                return fragment;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

}
