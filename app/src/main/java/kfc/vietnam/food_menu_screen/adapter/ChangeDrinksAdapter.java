package kfc.vietnam.food_menu_screen.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.ProductWrapper002;

/**
 * Created by VietRuyn on 02/08/2016.
 */
public class ChangeDrinksAdapter extends RecyclerView.Adapter<ChangeDrinksAdapter.MyViewHolder> {
    private final FragmentActivity ctx;
    private ArrayList<ProductWrapper002> lstData;
    private RecyclerView rcv;

    public ChangeDrinksAdapter(ArrayList<ProductWrapper002> lstData, FragmentActivity ctx, RecyclerView rcv) {
        this.lstData = lstData;
        Log.e("lstDataDrink", "" + lstData.size());
        this.ctx = ctx;
        this.rcv = rcv;
    }

    @Override
    public ChangeDrinksAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_exchange, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductWrapper002 obj = lstData.get(position);

        holder.tvName.setText(obj.nameProduct);

        Glide.with(ctx)
                .load(obj.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.img.setImageDrawable(resource);
                    }
                });

        //Check to set check item in product combo list
        if (obj.isProductSelected) {
            holder.rdb.setChecked(true);
        }
        showPrice(holder.tvPrice, Double.parseDouble(obj.price));


        //Phần xử lý thay đổi item được chọn
        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeSelect(obj, holder.rdb);
            }
        });
        holder.rdb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeSelect(obj, holder.rdb);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lstData.size();
    }

    private int getNumberOfParent() {
        if (lstData == null || lstData.size() == 0)
            return 0;
        else {
            ArrayList<String> listParent = new ArrayList<>();
            for (int i = 0; i < lstData.size(); i++){
                if (!listParent.contains(lstData.get(i).parent))
                    listParent.add(lstData.get(i).parent);
            }
            return listParent.size();
        }
    }

    private void changeSelect(ProductWrapper002 currentObj, RadioButton rdb) {
        Log.e("parent_size", getNumberOfParent() + "");
        RadioGroup[] radioGroupList = new RadioGroup[getNumberOfParent()];
        //Duyệt qua tất cả các item của list, nếu item nào có cùng parent thì clear check
        for (int i = 0; i < lstData.size(); i++) {
            ProductWrapper002 obj = lstData.get(i);
            if (obj.parent.equalsIgnoreCase(currentObj.parent)) {
                Log.e("check", "check=" + currentObj.parent + "-i=" + i);
                View v = rcv.getChildAt(i);
                if (v != null) {
                    //set un-checked for all item
                    LinearLayout llRoot = (LinearLayout) v;
                    RadioButton rb = (RadioButton) llRoot.findViewById(R.id.rdb);
                    rb.setChecked(false);
                    Log.e("false", "false");

                    //set to check return value
                    obj.isProductSelected = false;
                } else {
                    Log.e("false", "view null");
                }
            }
        }



        //set checked for this item clicked
        rdb.setChecked(true);

        //set to check return value
        currentObj.isProductSelected = true;
    }

    private void showPrice(TextView tvPrice, double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(totalPrice).replace(".", ",");
            tvPrice.setText("+" + output + " " + ctx.getString(R.string.vnd));
        } else
            tvPrice.setText(ctx.getString(R.string.ChangeDishAdapter_002));
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llParent;
        private TextView tvName, tvPrice;
        private RadioButton rdb;
        private ImageView img;

        public MyViewHolder(View view) {
            super(view);
            llParent = (LinearLayout) view.findViewById(R.id.llParent);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvPrice = (TextView) view.findViewById(R.id.tvPrice);
            rdb = (RadioButton) view.findViewById(R.id.rdb);
            img = (ImageView) view.findViewById(R.id.img);
        }
    }
}
