package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import kfc.vietnam.R;
import kfc.vietnam.common.object.ProductCategoryWrapper;
import kfc.vietnam.common.object.ProductWrapper001;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.ListenerLoadImage;
import kfc.vietnam.food_menu_screen.fragment.DetailComboOrProductSelectedFragment;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by VietRuyn on 19/07/2016.
 */
public class ProductDetailAdapter extends RecyclerView.Adapter<ProductDetailAdapter.ViewHolder> {
    private String currentLanguageCode = "";
    private Context ctx;
    private ArrayList<ProvinceWrapper> lstProvince;
    private ProductCategoryWrapper category;
    private ArrayList<ProductWrapper001> lstData;

    public ProductDetailAdapter(Context context, ProductCategoryWrapper category, String lang, ArrayList<ProvinceWrapper> lstProvince) {
        this.ctx = context;
        this.category = category;
        this.lstData = category.product;
        this.currentLanguageCode = lang;
        this.lstProvince = lstProvince;
    }

    @Override
    public void onBindViewHolder(final ProductDetailAdapter.ViewHolder holder, int i) {
        final ProductWrapper001 obj = lstData.get(i);
        holder.tvName.setText(obj.name);

        double totalPrice = Double.parseDouble(obj.price);
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###.000");
            String output = myFormatter.format(totalPrice);
            output = output.substring(0, output.indexOf("."));
            ArrayList<String> lst = new ArrayList(Arrays.asList(output.split(",")));
            if (lst.size() >= 2)
                lst.remove(lst.size() - 1);
            output = lst.toString().replace("[", "").replace("]", "").replace(" ", "") + ".000 " + ctx.getString(R.string.vnd_cap);

            int start = 0;
            int end = output.indexOf(".");

            SpannableString spanContent = new SpannableString(output);
            spanContent.setSpan(new AbsoluteSizeSpan(15, true), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            //spanContent.setSpan(new RelativeSizeSpan(2f), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set style
            spanContent.setSpan(new ForegroundColorSpan(Color.parseColor("#ffff4444")), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
            holder.tvPrice.setText(spanContent);
        } else
            holder.tvPrice.setText("0 " + ctx.getString(R.string.vnd_cap));

        Glide.with(ctx)
                .load(obj.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .centerCrop()
                .listener(new ListenerLoadImage(holder.progressBar , holder.imgAvatar))
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.imgAvatar.setImageDrawable(resource);
                    }
                });

        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("id", obj.id);
                b.putString("category_type", category.type);
                b.putString("buy", obj.buy);
                DetailComboOrProductSelectedFragment f = new DetailComboOrProductSelectedFragment();
                f.setArguments(b);
                ((MainActivity) ctx).changeFragment(f);
            }
        });
    }

    @Override
    public ProductDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup vGroup, int i) {
        View view = LayoutInflater.from(vGroup.getContext()).inflate(R.layout.custom_item_product_detail, vGroup, false);

        return new ViewHolder(view);
    }


    @Override
    public int getItemCount() {
        return lstData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llParent;
        private TextView tvName;
        private TextView tvPrice;
        private ImageView imgAvatar;
        private ProgressBar progressBar;

        public ViewHolder(View v) {
            super(v);

            llParent = (LinearLayout) v.findViewById(R.id.llParent);
            tvName = (TextView) v.findViewById(R.id.tvNameItem1);
            tvPrice = (TextView) v.findViewById(R.id.tvPriceItem1);
            imgAvatar = (ImageView) v.findViewById(R.id.imgBackgroundItem1);
            progressBar = (ProgressBar) v.findViewById(R.id.pbLoading);
        }
    }

}