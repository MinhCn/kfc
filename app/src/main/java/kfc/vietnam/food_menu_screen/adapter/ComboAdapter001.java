package kfc.vietnam.food_menu_screen.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;
import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.ChangeProductDataWrapper;
import kfc.vietnam.common.object.GroupContentWrapper001;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.NonScrollListView;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.fragment.DetailComboOrProductSelectedFragment;
import kfc.vietnam.main_screen.ui.DialogFactory;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class ComboAdapter001 extends ArrayAdapter implements View.OnClickListener {
    private String currentLanguageCode = "";
    private Activity ctx;
    private LayoutInflater inflater;
    private ArrayList<GroupContentWrapper001> lstData;
    private int resource;
    private int previousSelectedIndex = -1;
    private NonScrollListView lvw;
    private SimpleTooltip tooltip;
    public GroupContentWrapper001 foodSelected;
    private boolean showToolTip = false;

    public ComboAdapter001(Activity context, int resource, ArrayList<GroupContentWrapper001> lstData, NonScrollListView lvw, String lang , boolean showToolTip) {
        super(context, resource, lstData);
        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        this.lvw = lvw;
        this.currentLanguageCode = lang;
        this.showToolTip = showToolTip;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.llParent = (LinearLayout) convertView.findViewById(R.id.llParent);
            holder.rdb = (RadioButton) convertView.findViewById(R.id.rdb);
            holder.img = (ImageView) convertView.findViewById(R.id.img);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == 0 && showToolTip){
            SharedPreferences pref = ctx.getSharedPreferences(Const.SHARED_PREF, 0);
            boolean isFirst = pref.getBoolean(SharedPreferenceKeyType.FIRST_PRODUCT_DETAIL.toString() , false);
            if (!isFirst){
                tooltip = DialogFactory.showTipLeftTop(ctx , holder.rdb);
                tooltip.show();
                SharedPreferences.Editor prefEditor = pref.edit();
                prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_PRODUCT_DETAIL.toString(), true);
                prefEditor.apply();
            }
        }

        GroupContentWrapper001 obj = lstData.get(position);
        holder.llParent.setOnClickListener(this);
        holder.rdb.setOnClickListener(this);
        holder.rdb.setTag(obj);

        Glide.with(ctx)
                .load(obj.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.img.setImageDrawable(resource);
                    }
                });
        holder.tvName.setText(obj.nameProduct);

        return convertView;
    }

    @Override
    public void onClick(View view) {
        if (previousSelectedIndex == -1) {
            previousSelectedIndex = this.lvw.getPositionForView(view);
        } else {
            //set un-checked for previous item
            LinearLayout llRoot = (LinearLayout) this.lvw.getChildAt(previousSelectedIndex);
            RadioButton rb = (RadioButton) llRoot.findViewById(R.id.rdb);
            rb.setChecked(false);

            previousSelectedIndex = this.lvw.getPositionForView(view);
        }

        //Click on parent layout
        if (view instanceof LinearLayout) {
            //set checked for this item clicked
            RadioButton rb = (RadioButton) view.findViewById(R.id.rdb);
            rb.setChecked(true);

            foodSelected = (GroupContentWrapper001) rb.getTag();
        }
        //click on radio button
        else if (view instanceof RadioButton) {
            //set checked for this item clicked
            RadioButton rb = (RadioButton) view;
            rb.setChecked(true);

            foodSelected = (GroupContentWrapper001) rb.getTag();
        }

        Fragment f = ((MainActivity) ctx).getTopFragmentOfBackStack();

        //update category info
        if (f instanceof DetailComboOrProductSelectedFragment) {
//            ((DetailComboOrProductSelectedFragment) f).processProductComboDefault();
            ((DetailComboOrProductSelectedFragment) f).updateCategoryInfo();
        }

        if (foodSelected.change.equalsIgnoreCase("1"))
            //thay lai code ngon ngu
            changeProduct(this.currentLanguageCode, ((MainActivity) ctx).currentProvinceSelected.id, foodSelected.combo_id + "", foodSelected.group_id + "", foodSelected.combo_product_id + "");
        else
        {
            //set value from product combo default
//            if (f instanceof DetailComboOrProductSelectedFragment)
//                ((DetailComboOrProductSelectedFragment) f).processProductComboDefault();
        }
    }

    static class ViewHolder {
        private LinearLayout llParent;
        private RadioButton rdb;
        private ImageView img;
        private TextView tvName;
    }

    private void changeProduct(String lang, String city, String comboID, String groupID, String productID) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, ctx.getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity)ctx).showNoInternetFragment();
            return;
        }

        Call<ChangeProductDataWrapper> call = ApiClient.getJsonClient().changeProduct(productID, lang, city, comboID, groupID, productID);
        call.enqueue(new Callback<ChangeProductDataWrapper>() {

            @Override
            public void onFailure(Call<ChangeProductDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<ChangeProductDataWrapper> arg0, Response<ChangeProductDataWrapper> arg1) {
                mProgressDialog.dismiss();

                ChangeProductDataWrapper changeProduct = arg1.body();
                Fragment f = ((MainActivity) ctx).getTopFragmentOfBackStack();
                if (f instanceof DetailComboOrProductSelectedFragment)
                    ((DetailComboOrProductSelectedFragment) f).changeData(changeProduct);
            }
        });
    }

    public void closeToolTip(){
        if (tooltip != null && tooltip.isShowing()){
            tooltip.dismiss();
        }
    }
}
