package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.utility.FontText;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class CategoryInfoAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    private ArrayList<String> lstData;
    private int resource;

    public CategoryInfoAdapter(Context context, int resource, ArrayList<String> lstData) {
        super(context, resource, lstData);

        this.resource = resource;
        this.lstData = lstData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.tvName = (FontText) convertView.findViewById(R.id.tvName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String obj = lstData.get(position);
        holder.tvName.setText(obj);

        return convertView;
    }

    static class ViewHolder {
        private FontText tvName;
    }
}
