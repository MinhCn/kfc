package kfc.vietnam.food_menu_screen.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.CategoryType;
import kfc.vietnam.common.enums.LoginAreaType;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.AddLikeDataWrapper;
import kfc.vietnam.common.object.GroupContentWrapper001;
import kfc.vietnam.common.object.ProductBuyMoreModel;
import kfc.vietnam.common.object.ProductWrapper001;
import kfc.vietnam.common.object.ProductWrapper002;
import kfc.vietnam.common.object.ShoppingCartObject;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.NonScrollListView;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.fragment.ShoppingCartFragment;
import kfc.vietnam.food_menu_screen.fragment.StepThreeFragment;
import kfc.vietnam.main_screen.ui.LoginActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by VietRuyn on 19/07/2016.
 */
public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<ShoppingCartObject> lstDataShopping;

    private Context ctx;
    private MainActivity activity;
    private ShoppingCartFragment shoppingCartFragment;
    private OnclickChangeValue onclickChangeValue;


    public ShoppingCartAdapter(Context context, ArrayList<ShoppingCartObject> lstData, ShoppingCartFragment shoppingCartFragment, OnclickChangeValue changeValue) {
        this.ctx = context;
        this.lstDataShopping = lstData;
        this.onclickChangeValue = changeValue;
        this.shoppingCartFragment = shoppingCartFragment;
        activity = (MainActivity) ctx;

        Collections.sort(lstDataShopping, new Comparator<ShoppingCartObject>() {
            @Override
            public int compare(ShoppingCartObject t1, ShoppingCartObject t2) {
                int s1 = Integer.parseInt(t1.dishSelected.id);
                int s2 = Integer.parseInt(t2.dishSelected.id);

                Log.d("sort", "s1: " + s1 + " - " + "s2: " + s2 + " = " + (s1 - s2));

                return s1 - s2; //Ascending
            }
        });
    }

    @Override
    public void onBindViewHolder(final ShoppingCartAdapter.ViewHolder holder, final int position) {
        final ShoppingCartObject objShoppingCart = lstDataShopping.get(position);

        ProductWrapper001 dishSelected = objShoppingCart.dishSelected;

        double price = Double.parseDouble(dishSelected.price);

        Glide.with(ctx)
                .load(dishSelected.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.imgBackground.setImageDrawable(resource);
                    }
                });

        //Add by Nhat Minh (hind plus, minus, favorite, delete on Step Three)
        if(shoppingCartFragment == null) {
            holder.imgFavorite.setVisibility(View.GONE);
            holder.imgDelete.setVisibility(View.GONE);
            holder.lnPlusMinus.setVisibility(View.GONE);
        }

        holder.imgFavorite.setTag(objShoppingCart.dishSelected.id + "--" + objShoppingCart.dishSelected.type);
        holder.imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (objShoppingCart.productSelected != null) {
                    String[] _tag = view.getTag().toString().split("--");
                    final String productID = _tag[0];
                    String productType = _tag[1];
                    if (activity.userInfo != null) {
                        if (!objShoppingCart.productSelected.checkLike) {

                            final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
                            mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
                            mProgressDialog.setCancelable(false);
                            mProgressDialog.setCanceledOnTouchOutside(false);
                            mProgressDialog.show();

                            if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
                                mProgressDialog.dismiss();
                                Toast.makeText(ctx, ctx.getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
                                ((MainActivity) ctx).showNoInternetFragment();
                                return;
                            }

                            Call<AddLikeDataWrapper> call1 = ApiClient.getJsonClient().addLike(activity.userInfo.id, productType, productID);
                            call1.enqueue(new Callback<AddLikeDataWrapper>() {

                                @Override
                                public void onFailure(Call<AddLikeDataWrapper> arg0, Throwable arg1) {
                                    arg1.printStackTrace();
                                    Toast.makeText(activity.getBaseContext(), R.string.pls_try_again, Toast.LENGTH_SHORT).show();
                                    mProgressDialog.dismiss();

                                }

                                @Override
                                public void onResponse(Call<AddLikeDataWrapper> arg0, Response<AddLikeDataWrapper> arg1) {
                                    mProgressDialog.dismiss();

                                    holder.imgFavorite.setImageResource(R.drawable.ic_favorite_food_4);
                                    objShoppingCart.productSelected.checkLike = true;
                                    SharedPreferences preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
                                    String serializeString = preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null);
                                    try {
                                        ArrayList<ShoppingCartObject> lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(serializeString);
                                        for (int i = 0; i < lstShoppingCard.size(); i++) {
                                            ShoppingCartObject shoppingCartObject = lstShoppingCard.get(i);
                                            if (shoppingCartObject.dishSelected.id.equals(productID)) {
                                                shoppingCartObject.productSelected.checkLike = true;
                                            }
                                            lstShoppingCard.set(i, shoppingCartObject);
                                        }

                                        //Refresh lại list để care trường hợp add nhiều sản phẩm cùng loại
                                        //vì logic app không cho gộp
                                        onclickChangeValue.refresh(lstShoppingCard);

                                        preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                            });

                        } else {
                            Toast.makeText(activity.getBaseContext(), R.string.info_like, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Intent i = new Intent(ctx, LoginActivity.class);
                        i.putExtra("login_area", LoginAreaType.LOGIN_IN_SHOPPING_CART.toString());
                        ((FragmentActivity) ctx).startActivityForResult(i, RequestResultCodeType.REQUEST_CODE_LOGIN_IN_SHOPPING_CART.getValue());
                    }
                }
            }
        });

        if (objShoppingCart.productSelected.checkLike)
            holder.imgFavorite.setImageResource(R.drawable.ic_favorite_food_4);
        else
            holder.imgFavorite.setImageResource(R.drawable.ic_favorite_food_3);

        holder.imgDelete.setOnClickListener(this);
        holder.imgDelete.setTag(objShoppingCart.idCart + "--" + objShoppingCart.categoryType);

        holder.tvTitle.setText(dishSelected.name);

        ArrayList<String> lst = new ArrayList<>();
        if (objShoppingCart.categoryType.equalsIgnoreCase(CategoryType.COMBO.getValue())) {
            for (ProductWrapper002 obj : objShoppingCart.lstProductDefault)
                lst.add(obj.nameProduct);

            int countElement = objShoppingCart.count_element;
            if (countElement == 0) {
                if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                //add by @ManhNguyen
                if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

            } else if (countElement == 1) {
                GroupContentWrapper001 p1 = objShoppingCart.combo1;
                lst.add(p1.nameProduct);
                if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                //add by @ManhNguyen
                if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

            } else if (countElement == 2) {
                GroupContentWrapper001 p1 = objShoppingCart.combo1, p2 = objShoppingCart.combo2;
                lst.add(p1.nameProduct);
                lst.add(p2.nameProduct);

                if (objShoppingCart.lstDrinkSelected != null && objShoppingCart.lstDrinkSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstDrinkSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                if (objShoppingCart.lstSnacksAndDessertSelected != null && objShoppingCart.lstSnacksAndDessertSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstSnacksAndDessertSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                //add by @ManhNguyen
                if (objShoppingCart.lstGaRanSelected != null && objShoppingCart.lstGaRanSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstGaRanSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }

                if (objShoppingCart.lstBurgerComSelected != null && objShoppingCart.lstBurgerComSelected.size() > 0)
                    for (ProductWrapper002 obj : objShoppingCart.lstBurgerComSelected) {
                        price = price + Double.parseDouble(obj.price);
                        lst.add(obj.nameProduct);
                    }
            }
        } else { //product
            lst.add(objShoppingCart.productSelected.nameProduct);
        }

        if (objShoppingCart.listProductBuyMoreModel != null) {
            if (objShoppingCart.listProductBuyMoreModel.size() > 0) {
                for (ProductBuyMoreModel obj : objShoppingCart.listProductBuyMoreModel) {
                    price = price + Double.parseDouble(obj.price);
                    lst.add(obj.name);
                }
            }
        }

        CategoryInfoAdapter categoryInfoAdapter = new CategoryInfoAdapter(ctx, R.layout.custom_item_category_info_in_cart, lst);
        holder.lvCategoryInfo.setAdapter(categoryInfoAdapter);
        //Nhat Minh custom
        if (shoppingCartFragment == null) holder.tvValue.setText("x" +objShoppingCart.amount);
        else holder.tvValue.setText(objShoppingCart.amount);

        holder.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onclickChangeValue != null) {
                    onclickChangeValue.valueChange(true, position);
                }

            }
        });

        holder.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onclickChangeValue != null) {
                    onclickChangeValue.valueChange(false, position);
                }
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    lstDataShopping.remove(objShoppingCart);
                    notifyDataSetChanged();

                    try {
                        SharedPreferences preferencesShoppingCart = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
                        preferencesShoppingCart.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstDataShopping)).apply();

                        //update total price of all item on screen
                        Fragment f = ((MainActivity) ctx).getTopFragmentOfBackStack();
                        if (f instanceof ShoppingCartFragment) {
                            ((ShoppingCartFragment) f).updateShowTotalPrice();
                        } else if (f instanceof StepThreeFragment) {
                            ((StepThreeFragment) f).updateShowTotalPrice();
                            ((StepThreeFragment) f).updateDiscountCode();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    MainActivity activity = ((MainActivity) ctx);
                    int count_cart = 0;
                    if (lstDataShopping.size() == 0) {
                        count_cart = 0;
                        if (shoppingCartFragment != null)
                            shoppingCartFragment.showLayoutEmptyMess();
                    } else {
                        for (ShoppingCartObject cart : lstDataShopping) {
                            count_cart += Integer.parseInt(cart.amount);
                        }
                    }
                    activity.tvAmountShoppingCart.setText(count_cart + "");

            }
        });

        showTotalPrice(price, holder.tvPrice);
    }

    @Override
    public ShoppingCartAdapter.ViewHolder onCreateViewHolder(ViewGroup vGroup, int i) {
        View view = LayoutInflater.from(vGroup.getContext()).inflate(R.layout.custom_item_shopping_cart, vGroup, false);

        return new ViewHolder(view);
    }

    private void showTotalPrice(double totalPrice, TextView tvTotalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###.000");
            String output = myFormatter.format(totalPrice);
            output = output.substring(0, output.indexOf("."));
            ArrayList<String> lst = new ArrayList(Arrays.asList(output.split(",")));
            if (lst.size() >= 2)
                lst.remove(lst.size() - 1);
            output = lst.toString().replace("[", "").replace("]", "").replace(" ", "") + ".000 " + ctx.getString(R.string.vnd_cap);

            int start = 0;
            int end = output.indexOf(".");

            SpannableString spanContent = new SpannableString(output);
            spanContent.setSpan(new AbsoluteSizeSpan(15, true), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            //spanContent.setSpan(new RelativeSizeSpan(2f), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set style
            spanContent.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
            tvTotalPrice.setText(spanContent);

        } else
            tvTotalPrice.setText("0 " + ctx.getString(R.string.vnd_cap));
    }

    @Override
    public int getItemCount() {
        return lstDataShopping.size();
    }

    public void removeAllItem() {
        lstDataShopping.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgBackground;
        private ImageView imgFavorite;
        private ImageView imgDelete;
        private TextView tvTitle;
        private NonScrollListView lvCategoryInfo;
        private TextView tvPrice, tvMinus, tvPlus, tvValue;
        private LinearLayout lnPlusMinus;

        public ViewHolder(View v) {
            super(v);

            imgBackground = (ImageView) v.findViewById(R.id.imgBackground);
            imgFavorite = (ImageView) v.findViewById(R.id.imgFavorite);
            imgDelete = (ImageView) v.findViewById(R.id.imgDelete);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            lvCategoryInfo = (NonScrollListView) v.findViewById(R.id.lvCategoryInfo);
            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            tvMinus = (TextView) v.findViewById(R.id.text_minus);
            tvPlus = (TextView) v.findViewById(R.id.text_plus);
            tvValue = (TextView) v.findViewById(R.id.tv_value);
            lnPlusMinus = (LinearLayout) v.findViewById(R.id.ll_price_1);
        }
    }

    public interface OnclickChangeValue {
        void valueChange(boolean isPlus, int pos);

        void likeValue(boolean isLike, int pos);

        void refresh(ArrayList<ShoppingCartObject> lstShoppingCard);
    }
}