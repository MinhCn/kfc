package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import kfc.vietnam.R;
import kfc.vietnam.common.object.OrderHistoryDataWrapper;
import kfc.vietnam.common.utility.NonScrollListView;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class OrderHistoryDetailAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    private static Context ctx;
    private ArrayList<OrderHistoryDataWrapper.ProductWrapper> lstData;
    private int resource;

    public OrderHistoryDetailAdapter(Context context, int resource, ArrayList<OrderHistoryDataWrapper.ProductWrapper> lstData) {
        super(context, resource, lstData);
        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lstData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.tvTenSanPham = (TextView) convertView.findViewById(R.id.tvTenSanPham);
            holder.lvCategoryInfo = (NonScrollListView) convertView.findViewById(R.id.lvCategoryInfo);
            holder.tvSoLuong = (TextView) convertView.findViewById(R.id.tvSoLuong);
            holder.tvThanhTien = (TextView) convertView.findViewById(R.id.tvThanhTien);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        OrderHistoryDataWrapper.ProductWrapper obj = lstData.get(position);

        holder.tvTenSanPham.setText(obj.name);

        ArrayList<String> lst = new ArrayList<>();
        for (OrderHistoryDataWrapper.ElementWrapper element : obj.thanhphan)
            lst.add(element.name);
        CategoryInfoAdapter categoryInfoAdapter = new CategoryInfoAdapter(parent.getContext(), R.layout.custom_item_category_info_in_order_history_detail, lst);
        holder.lvCategoryInfo.setAdapter(categoryInfoAdapter);

        holder.tvSoLuong.setText(obj.quantity);
        double thanhtien = Double.valueOf(obj.quantity) * Double.valueOf(obj.price);
//        holder.tvThanhTien.setText(String.valueOf(thanhtien));
        showPrice(holder.tvThanhTien, thanhtien);

        return convertView;
    }

    private void showPrice(TextView tv, double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("#,##0");
            String output = myFormatter.format(totalPrice);
            try {
                output = output.replace(".", ",");
            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList<String> lst = new ArrayList(Arrays.asList(output.split(",")));
            if (lst.size() >= 2)
                lst.remove(lst.size() - 1);
            output = lst.toString().replace("[", "").replace("]", "").replace(" ", "") + ".000 " + ctx.getString(R.string.vnd_cap);

            int start = 0;
            int end = output.indexOf(".");
            output = output.replace(",", ".");

            SpannableString spanContent = new SpannableString(output);
            spanContent.setSpan(new AbsoluteSizeSpan(18, true), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            //spanContent.setSpan(new RelativeSizeSpan(2f), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set size
            spanContent.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set style
            spanContent.setSpan(new ForegroundColorSpan(Color.BLACK), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// set color
            tv.setText(spanContent);

        } else
            tv.setText("0 " + ctx.getString(R.string.vnd_cap));
    }

    static class ViewHolder {
        private NonScrollListView lvCategoryInfo;
        private TextView tvTenSanPham, tvSoLuong, tvThanhTien;
    }

}
