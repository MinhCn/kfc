package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.GroupContentWrapper001;
import kfc.vietnam.common.object.ProductWrapper002;
import kfc.vietnam.common.utility.NonScrollListView;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class ComboAdapter002 extends ArrayAdapter {
    public GroupContentWrapper001 foodSelected;
    private String currentLanguageCode = "";
    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<ProductWrapper002> lstData;
    private int resource;
    private int previousSelectedIndex = -1;
    private NonScrollListView lvw;

    public ComboAdapter002(Context context, int resource, ArrayList<ProductWrapper002> lstData, NonScrollListView lvw, String lang) {
        super(context, resource, lstData);
        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        this.lvw = lvw;
        this.currentLanguageCode = lang;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.llParent = (LinearLayout) convertView.findViewById(R.id.llParent);
            holder.rdb = (RadioButton) convertView.findViewById(R.id.rdb);
            holder.img = (ImageView) convertView.findViewById(R.id.img);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ProductWrapper002 obj = lstData.get(position);
        holder.rdb.setChecked(true);
        holder.rdb.setTag(obj);
        Glide.with(ctx)
                .load(obj.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.img.setImageDrawable(resource);
                    }
                });
        holder.tvName.setText(obj.nameProduct);
        if (String.valueOf(obj.id).equalsIgnoreCase(obj.parent)) {
            holder.tvPrice.setText(ctx.getString(R.string.ChangeDishAdapter_002));
        } else
            showPrice(holder.tvPrice, Double.parseDouble(obj.price));

        return convertView;
    }

    private void showPrice(TextView tvPrice, double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(totalPrice).replace(".", ",");
            tvPrice.setText("+" + output + " " + ctx.getString(R.string.vnd));
        } else
            tvPrice.setText("0 " + ctx.getString(R.string.vnd));
    }

    static class ViewHolder {
        private LinearLayout llParent;
        private RadioButton rdb;
        private ImageView img;
        private TextView tvName, tvPrice;
    }
}
