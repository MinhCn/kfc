package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.GroupContentWrapper001;
import kfc.vietnam.common.object.ProductBuyMoreModel;
import kfc.vietnam.common.object.ProductWrapper003;
import kfc.vietnam.food_menu_screen.fragment.DetailComboOrProductSelectedFragment;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class ProductAdapter002 extends ArrayAdapter {
    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<ProductBuyMoreModel> lstData;
    private int resource;
    public ArrayList<ProductBuyMoreModel> foodSelected;
    private boolean isChecked = false;


    public ProductAdapter002(Context context, int resource, ArrayList<ProductBuyMoreModel> lstData) {
        super(context, resource, lstData);
        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        foodSelected = new ArrayList<>();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        final ProductBuyMoreModel obj = lstData.get(position);
        final ViewHolder holder = new ViewHolder();
        convertView = inflater.inflate(resource, parent, false);
        holder.parentLayout = (RelativeLayout) convertView.findViewById(R.id.parentLayout);
        holder.rdb = (RadioButton) convertView.findViewById(R.id.rdb);
        holder.img = (ImageView) convertView.findViewById(R.id.img);
        holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
        holder.tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);

        holder.rdb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChecked = !isChecked;
                holder.rdb.setChecked(isChecked);

                if (holder.rdb.isChecked()) {
                    boolean isExist = false;
                    if (foodSelected.size() > 0) {
                        for (ProductBuyMoreModel temp : foodSelected) {
                            if (temp.id == obj.id) {
                                isExist = true;
                            }
                        }
                    }
                    if (isExist == false) {
                        foodSelected.add(obj);
                    }
                } else {
                    foodSelected.remove(obj);
                }

                Fragment f = ((MainActivity) ctx).getTopFragmentOfBackStack();
                //update category info
                if (f instanceof DetailComboOrProductSelectedFragment) {
                    ((DetailComboOrProductSelectedFragment) f).updateCategoryInfo();
                }
            }
        });

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChecked = !isChecked;
                holder.rdb.setChecked(isChecked);

                if (holder.rdb.isChecked()) {
                    boolean isExist = false;
                    if (foodSelected.size() > 0) {
                        for (ProductBuyMoreModel temp : foodSelected) {
                            if (temp.id == obj.id) {
                                isExist = true;
                            }
                        }
                    }
                    if (isExist == false) {
                        foodSelected.add(obj);
                    }
                } else {
                    foodSelected.remove(obj);
                }

                Fragment f = ((MainActivity) ctx).getTopFragmentOfBackStack();
                //update category info
                if (f instanceof DetailComboOrProductSelectedFragment) {
                    ((DetailComboOrProductSelectedFragment) f).updateCategoryInfo();
                }
            }
        });

        Glide.with(ctx)
                .load(obj.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.img.setImageDrawable(resource);
                    }
                });

        holder.tvName.setText(obj.name);
        showPrice(holder.tvPrice, Double.parseDouble(obj.price));

        return convertView;
    }

    public static class ViewHolder {
        private RelativeLayout parentLayout;
        private RadioButton rdb;
        private ImageView img;
        private TextView tvName;
        private TextView tvPrice;
    }

    private void showPrice(TextView tvPrice, double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(totalPrice).replace(".", ",");
            tvPrice.setText("+" + output + " " + ctx.getString(R.string.vnd_cap));
        } else
            tvPrice.setText("");
    }
}
