package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kfc.vietnam.R;
import kfc.vietnam.common.object.CategoryWrapper;
import kfc.vietnam.common.object.LayoutItemMenu;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.food_menu_screen.fragment.ItemMenuFragment;
import kfc.vietnam.food_menu_screen.fragment.MenuFragment;

public class MenuPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {
    private String currentLanguageCode = "";
    private Context ctx;
    private ArrayList<ProvinceWrapper> lstProvince;

    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.9f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    private float scale;
    private MenuFragment menuFragment;
    private static Map<Integer, Fragment> mapItem;
    private ArrayList<CategoryWrapper> lstCategory;


    public MenuPagerAdapter(Context context, MenuFragment menuFragment, FragmentManager childFragmentManager, ArrayList<CategoryWrapper> lstCategory, String lang, ArrayList<ProvinceWrapper> lstProvince) {
        super(childFragmentManager);
        this.ctx = context;
        this.menuFragment = menuFragment;
        this.lstCategory = lstCategory;
        this.currentLanguageCode = lang;
        this.lstProvince = lstProvince;
        if (mapItem == null)
            mapItem = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        // make the first pager bigger than others
        try {
            if (position == MenuFragment.FIRST_PAGE)
                scale = BIG_SCALE;
            else
                scale = SMALL_SCALE;

            position = position % MenuFragment.count;

        } catch (Exception e) {
            e.printStackTrace();
        }

        Fragment item = ItemMenuFragment.newInstance(ctx, position, scale, lstCategory.get(position), currentLanguageCode, lstProvince);
        mapItem.put(position, item);
        return item;
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = MenuFragment.count * MenuFragment.LOOPS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        try {
            Log.i("anhduc", "onPageScrolled: position=" + position + ", positionOffset=" + positionOffset + ", positionOffset=" + positionOffsetPixels);
            if (positionOffset >= 0f && positionOffset <= 1f) {
                LayoutItemMenu currentPage = getRootView(position);
                LayoutItemMenu nextPage = getRootView(position + 1);

                currentPage.setScaleBoth(BIG_SCALE - DIFF_SCALE * positionOffset);
                nextPage.setScaleBoth(SMALL_SCALE + DIFF_SCALE * positionOffset);

                /*//Phan set color for title
                TextView tvCurrentTitle = (TextView) currentPage.findViewById(R.id.text);
                tvCurrentTitle.setTextColor(Color.parseColor("#ed1c24"));
                TextView tvNextTitle = (TextView) nextPage.findViewById(R.id.text);
                tvNextTitle.setTextColor(Color.parseColor("#000000"));*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageSelected(int position) {
        Log.i("anhduc", "onPageSelected");
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.i("anhduc", "onPageScrollStateChanged: " + state);
        if (state == ViewPager.SCROLL_STATE_IDLE) {

        }
    }

    @SuppressWarnings("ConstantConditions")
    private LayoutItemMenu getRootView(int position) {
        LayoutItemMenu layoutItemMenu = (LayoutItemMenu) (mapItem.get(position).getView());
        return layoutItemMenu;
    }

}