package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import kfc.vietnam.common.object.ProductCategoryWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.food_menu_screen.fragment.ProductInDetailMenuFragment;

public class DetailMenuPagerAdapter extends FragmentStatePagerAdapter {
    private String currentLanguageCode = "";
    private Context mContext;
    private static ArrayList<ProductCategoryWrapper> lstCategory;
    private ArrayList<ProvinceWrapper> lstProvince;

    /**
     * Constructor of the class
     */
    public DetailMenuPagerAdapter(FragmentManager fm, Context mContext, ArrayList<ProductCategoryWrapper> lstCategory, String lang, ArrayList<ProvinceWrapper> lstProvince) {
        super(fm);
        this.mContext = mContext;
        this.lstCategory = lstCategory;
        this.currentLanguageCode = lang;
        this.lstProvince = lstProvince;
    }

    /**
     * This method will be invoked when a page is requested to create
     */
    @Override
    public Fragment getItem(int position) {
        Bundle b = new Bundle();
        b.putInt("position_detail_page", position);
        b.putSerializable("category", lstCategory.get(position));
        b.putString("language_code", currentLanguageCode);
        b.putSerializable("province_list", lstProvince);

        ProductInDetailMenuFragment productInDetailMenuFragment = new ProductInDetailMenuFragment();
        productInDetailMenuFragment.setArguments(b);
        return productInDetailMenuFragment;
    }

    /**
     * Returns the number of pages
     */
    @Override
    public int getCount() {
        return lstCategory.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return lstCategory.get(position).categoryName.toUpperCase();
    }


}
