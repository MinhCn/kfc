package kfc.vietnam.food_menu_screen.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.DeleteShipAddressDataWrapper;
import kfc.vietnam.common.object.ShowTimeDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.NonScrollListView;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.utility.ResultListener;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.fragment.StepThreeFragment;
import kfc.vietnam.food_menu_screen.fragment.StepTwoFragment;
import kfc.vietnam.food_menu_screen.ui.OrderLimitActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class AddressAdapter002 extends ArrayAdapter implements AdapterView.OnItemClickListener, View.OnClickListener {
    private String currentLanguageCode = "";
    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<AddressDataWrapper> lstData;
    private int resource, currentPositionSelected = -1, positionToDelete = -1;
    private NonScrollListView lvw;
    private MainActivity activity;


    public AddressAdapter002(Context context, int resource, ArrayList<AddressDataWrapper> lstData, String currentLanguageCode, NonScrollListView lvw) {
        super(context, resource, lstData);
        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        this.currentLanguageCode = currentLanguageCode;
        this.lvw = lvw;
        this.activity = (MainActivity) ctx;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.llParent = (LinearLayout) convertView.findViewById(R.id.llParent);
            holder.llDeliverToThisAddress = (LinearLayout) convertView.findViewById(R.id.llDeliverToThisAddress);
            holder.llDeleteAddress = (LinearLayout) convertView.findViewById(R.id.llDeleteAddress);
            holder.tvFirstNameAndLastName = (TextView) convertView.findViewById(R.id.tvFirstNameAndLastName);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
            holder.tvPhoneNumber = (TextView) convertView.findViewById(R.id.tvPhoneNumber);
            holder.tvEmail = (TextView) convertView.findViewById(R.id.tvEmail);
            holder.tvCity = (TextView) convertView.findViewById(R.id.tvCity);
            holder.tvDistrict = (TextView) convertView.findViewById(R.id.tvDistrict);
            holder.tvWard = (TextView) convertView.findViewById(R.id.tvWard);
            holder.tvMinTime = (TextView) convertView.findViewById(R.id.tvMinTime);
            holder.tvMinPrice = (TextView) convertView.findViewById(R.id.tvMinPrice);
            holder.tvMore = (TextView) convertView.findViewById(R.id.tvMore);
            holder.llMore = (LinearLayout) convertView.findViewById(R.id.llMore);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AddressDataWrapper obj = lstData.get(position);

        //Change background color of item selected on method onItemClick() when receive command: notifyDataSetChanged()
        if (position == currentPositionSelected)
            holder.llParent.setBackgroundColor(ctx.getResources().getColor(R.color.gray_efecec));
        else
            holder.llParent.setBackgroundColor(ctx.getResources().getColor(android.R.color.white));

        holder.llDeliverToThisAddress.setOnClickListener(this);
        holder.llDeliverToThisAddress.setTag(obj);
        holder.llDeleteAddress.setOnClickListener(this);
        holder.llDeleteAddress.setTag(obj);

        holder.tvFirstNameAndLastName.setText(ctx.getString(R.string.receiver) + ": " + obj.name);
        holder.tvAddress.setText(ctx.getString(R.string.StepOneFragment_005) + ": " + obj.address);
        holder.tvPhoneNumber.setText(ctx.getString(R.string.phone_number) + ": " + obj.phone);
        holder.tvEmail.setText(ctx.getString(R.string.email) + ": " + obj.email);
        if (obj.nameCity != null)
            holder.tvCity.setText(ctx.getString(R.string.city) + ": " + obj.nameCity);
        if (obj.nameDistrict != null)
            holder.tvDistrict.setText(ctx.getString(R.string.district) + ": " + obj.nameDistrict);
        if (obj.nameWard != null)
            holder.tvWard.setText(ctx.getString(R.string.ward) + ": " + obj.nameWard);

        if (obj.price.priceMax == null){
            holder.llMore.setVisibility(View.GONE);
        } else {
            holder.llMore.setVisibility(View.VISIBLE);
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(obj.price.priceMax);
            holder.tvMinPrice.setText(output);
            holder.tvMinTime.setText(obj.price.timeMax);
        }
        holder.tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intents = new Intent(ctx, OrderLimitActivity.class);
                intents.putExtra("data", MainActivity.sProvineId);
                ctx.startActivity(intents);
            }
        });

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        currentPositionSelected = position;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llDeliverToThisAddress:
                AddressDataWrapper addressToShip = (AddressDataWrapper) view.getTag();
                if (addressToShip != null) {
                    try {
                        if (activity.currentProvinceSelected.id.equalsIgnoreCase(addressToShip.city + "")){
                            if (activity.totalPrice == -1d || addressToShip.price.priceMax == null || activity.totalPrice >= addressToShip.price.priceMax) {
                                //save address
                                checkOpen(addressToShip);
                            } else {
                                DecimalFormat myFormatter = new DecimalFormat("###,###");
                                String output = myFormatter.format(addressToShip.price.priceMax);
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                                alertDialog.setMessage(activity.getString(R.string.order_limitation_alert, output, addressToShip.nameWard));

                                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        arg0.dismiss();
                                    }
                                });

                                AlertDialog dialog = alertDialog.create();
                                dialog.show();
                            }
                    } else {
                        new AlertDialog.Builder(ctx)
                                .setMessage(R.string.StepTwoFragment_004)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    Toast.makeText(ctx, ctx.getString(R.string.StepTwoFragment_001), Toast.LENGTH_SHORT).show();
                break;

            case R.id.llDeleteAddress:
                positionToDelete = lvw.getPositionForView(view);

                AddressDataWrapper addressToDelete = (AddressDataWrapper) view.getTag();
                deleteShipAddress(addressToDelete.id + "");
                break;
        }
    }

    static class ViewHolder {
        private LinearLayout llParent, llDeliverToThisAddress, llDeleteAddress, llMore;
        private TextView tvFirstNameAndLastName, tvAddress, tvPhoneNumber, tvEmail, tvCity, tvDistrict, tvWard, tvMinTime, tvMore, tvMinPrice;
    }

    private void deleteShipAddress(String addressID) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, ctx.getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            ((MainActivity)ctx).showNoInternetFragment();
            return;
        }

        Call<DeleteShipAddressDataWrapper> call1 = ApiClient.getJsonClient().deleteShipAddress(addressID);
        call1.enqueue(new Callback<DeleteShipAddressDataWrapper>() {

            @Override
            public void onFailure(Call<DeleteShipAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();

                Toast.makeText(ctx, ctx.getString(R.string.StepTwoFragment_003), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<DeleteShipAddressDataWrapper> arg0, Response<DeleteShipAddressDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //remove in list and update screen
                lstData.remove(positionToDelete);
                notifyDataSetChanged();

                Toast.makeText(ctx, ctx.getString(R.string.StepTwoFragment_002), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void checkOpen(final AddressDataWrapper dataWrapper) {
        checkOpenDeliver(dataWrapper.city, dataWrapper.ward, new ResultListener<ShowTimeDataWrapper>() {
            @Override
            public void onSucceed(ShowTimeDataWrapper result) {
                if (!result.isResult()) {
                    try {
                        //save address
                        ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), ObjectSerializer.serialize(dataWrapper)).commit();

                        MainActivity activity = ((MainActivity) ctx);
                        activity.addressToShip = dataWrapper;
                        activity.backToPreviousScreen();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    activity.changeFragment(new StepThreeFragment());

                }
                if (result.getData() != null) {
                    if (!result.getData().isShowTime()) {
                        try {
                            //save address
                            ctx.getSharedPreferences(SharedPreferenceKeyType.DELIVER_ADDRESS_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.DELIVER_ADDRESS.toString(), ObjectSerializer.serialize(dataWrapper)).commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        activity.changeFragment(new StepThreeFragment());
                    } else {
                        showAlert(result.getMessage());
                    }
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    private void checkOpenDeliver(String provinceID, final String wardIDs, final ResultListener<ShowTimeDataWrapper> callBack) {
        activity.checkOpen(provinceID, wardIDs, new ResultListener<ShowTimeDataWrapper>() {
            @Override
            public void onSucceed(ShowTimeDataWrapper result) {
                if (result.getData().isShowTime()) {
                }
                if (callBack != null)
                    callBack.onSucceed(result);


            }

            @Override
            public void onFail(String reason) {

            }
        });

    }

    private void showAlert(String message) {
        if (TextUtils.isEmpty(message))
            message = ctx.getString(R.string.delivery_warning);
        new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

}
