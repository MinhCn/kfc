package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.ProductWrapper003;
import kfc.vietnam.main_screen.ui.DialogFactory;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class ProductAdapter001 extends ArrayAdapter {
    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<ProductWrapper003> lstData;
    private int resource;


    public ProductAdapter001(Context context, int resource, ArrayList<ProductWrapper003> lstData) {
        super(context, resource, lstData);
        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.llParent = (LinearLayout) convertView.findViewById(R.id.llParent);
            holder.rdb = (RadioButton) convertView.findViewById(R.id.rdb);
            holder.img = (ImageView) convertView.findViewById(R.id.img);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ProductWrapper003 obj = lstData.get(position);
        holder.rdb.setChecked(true);
        holder.rdb.setTag(obj);
        Glide.with(ctx)
                .load(obj.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.img.setImageDrawable(resource);
                    }
                });
        holder.tvName.setText(obj.nameProduct);

        return convertView;
    }

    static class ViewHolder {
        private LinearLayout llParent;
        private RadioButton rdb;
        private ImageView img;
        private TextView tvName;
    }

}
