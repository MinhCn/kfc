package kfc.vietnam.food_menu_screen.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.PaymentType;
import kfc.vietnam.common.object.OrderHistoryDataWrapper;
import kfc.vietnam.food_menu_screen.fragment.OrderHistoryDetailFragment;
import kfc.vietnam.main_screen.ui.MainActivity;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class OrderHistoryAdapter extends ArrayAdapter implements AdapterView.OnItemClickListener {
    private LayoutInflater inflater;
    private Context ctx;
    private ArrayList<OrderHistoryDataWrapper.OrderHistoryWrapper> lstData;
    private int resource;

    public OrderHistoryAdapter(Context context, int resource, ArrayList<OrderHistoryDataWrapper.OrderHistoryWrapper> lstData) {
        super(context, resource, lstData);
        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lstData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.tvMaDonHang = (TextView) convertView.findViewById(R.id.tvMaDonHang);
            holder.tvNgayDatHang = (TextView) convertView.findViewById(R.id.tvNgayDatHang);
            holder.tvTongTien = (TextView) convertView.findViewById(R.id.tvTongTien);
            holder.tvThanhToan = (TextView) convertView.findViewById(R.id.tvThanhToan);
            holder.tvTrangThaiDonHang = (TextView) convertView.findViewById(R.id.tvTrangThaiDonHang);
            holder.tvDiemTichLuy = (TextView) convertView.findViewById(R.id.tvDiemTichLuy);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        OrderHistoryDataWrapper.OrderHistoryWrapper obj = lstData.get(position);
        holder.tvMaDonHang.setTag(obj);
        holder.tvMaDonHang.setText(obj.orderID + "");
        holder.tvNgayDatHang.setText(obj.date);
        holder.tvTongTien.setText(showPrice(Double.parseDouble(obj.price)));
        if (obj.payment_type.equalsIgnoreCase(PaymentType.PAYMENT_ONLINE.getValue()))
            holder.tvThanhToan.setText(parent.getContext().getString(R.string.OrderHistoryFragment_008));
        else if (obj.payment_type.equalsIgnoreCase(PaymentType.PAYMENT_ATM.getValue()))
            holder.tvThanhToan.setText(parent.getContext().getString(R.string.OrderHistoryFragment_010));
        else
            holder.tvThanhToan.setText(parent.getContext().getString(R.string.OrderHistoryFragment_007));
        holder.tvTrangThaiDonHang.setText(obj.deliveryStatus);
        holder.tvDiemTichLuy.setText("+" + ((MainActivity) parent.getContext()).calculatePoint(Double.parseDouble(obj.price) + Double.parseDouble(obj.price_discount)) + " " + parent.getContext().getString(R.string.OrderHistoryFragment_009));
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        OrderHistoryDataWrapper.OrderHistoryWrapper obj = (OrderHistoryDataWrapper.OrderHistoryWrapper) view.findViewById(R.id.tvMaDonHang).getTag();
        Bundle b = new Bundle();
        b.putSerializable("order_info", lstData.get(i));
        OrderHistoryDetailFragment f = new OrderHistoryDetailFragment();
        f.setArguments(b);

        MainActivity activity = (MainActivity) adapterView.getContext();
        activity.changeFragment(f);
    }

    private String showPrice(double totalPrice) {
        if (totalPrice > 0) {
            DecimalFormat myFormatter = new DecimalFormat("###,###");
            String output = myFormatter.format(totalPrice).replace(",", ".") + " " + ctx.getString(R.string.vnd);
            return output;
        } else
            return "0 " + ctx.getString(R.string.vnd);
    }

    static class ViewHolder {
        private TextView tvMaDonHang, tvNgayDatHang, tvTongTien, tvThanhToan, tvTrangThaiDonHang, tvDiemTichLuy;
    }

}
