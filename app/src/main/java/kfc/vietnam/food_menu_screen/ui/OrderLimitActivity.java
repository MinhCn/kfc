package kfc.vietnam.food_menu_screen.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;

/**
 * Created by Dung on 1/6/17.
 */

public class OrderLimitActivity extends BaseActivity {
    private WebView tv;
    private int sWith, lengtPercent;
    private String sCity = "1";
    private static final String NUMBER_PHONE_CONTACT_KFC = "19006886";
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 124;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            sCity = getIntent().getStringExtra("data");
        }
        if(TextUtils.isEmpty(sCity))
            sCity = "1";
        setContentView(R.layout.activity_order_limit);
        initView();
    }

    private void initView() {
        tv = (WebView) findViewById(R.id.tv);
        tv.getSettings().setSupportZoom(false);
        tv.getSettings().setBuiltInZoomControls(false);
        tv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        tv.getSettings().setJavaScriptEnabled(true);
        findViewById(R.id.llCall).setOnClickListener(onClickListener);
        findViewById(R.id.llBack).setOnClickListener(onClickListener);
        Display display = getWindowManager().getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {

            Point size = new Point();
            display.getSize(size);
            sWith = size.x;
        } else {
            sWith = display.getWidth();
        }
        getTime(sCity);
    }

    private void getTime(String city) {
        SharedPreferences preferencesLanguage;
        preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        String currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
        final ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        String surl = ApiClient.ENDPOINT + "api/listWardPrice/" + city + "/?language=" + currentLanguageCode;
        tv.loadUrl(surl);

        tv.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                lengtPercent = (progress * sWith) / 100;
                if (lengtPercent == sWith) {
                    mProgressDialog.dismiss();
                    view.scrollTo(0, 0);
                }
            }

        });
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.llBack) {
                finish();

            } else if (view.getId() == R.id.llCall) {
                AlertDialog.Builder alertDialogBuilderCall = new AlertDialog.Builder(OrderLimitActivity.this);
                alertDialogBuilderCall
                        .setMessage(getString(R.string.call_hotline) + " " + NUMBER_PHONE_CONTACT_KFC + "?")
                        .setCancelable(false)
                        .setNegativeButton(getString(R.string.cancel_call), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton(getString(R.string.button_call), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callKFC();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialogCall = alertDialogBuilderCall.create();
                // show it
                alertDialogCall.show();
            }

        }
    };

    public void callKFC() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + NUMBER_PHONE_CONTACT_KFC));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (ActivityCompat.checkSelfPermission(OrderLimitActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            // public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            return;
            ActivityCompat.requestPermissions(OrderLimitActivity.this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
        } else {
            startActivity(callIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callKFC();
                }
                break;
        }
    }
}
