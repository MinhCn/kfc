package kfc.vietnam.food_menu_screen.ui;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.ConfigureDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dung on 1/3/17.
 */

public class OperationPolicyActivity extends BaseActivity {
    private WebView wv;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_terms_and_conditions);
//        initView();

        findViewById(R.id.llBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        wv = (WebView) findViewById(R.id.wv);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setSupportZoom(true);
        wv.getSettings().setBuiltInZoomControls(false);
        //improve webView performance
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        wv.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        wv.getSettings().setAppCacheEnabled(true);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSettings.setUseWideViewPort(true);
        webSettings.setSavePassword(true);
        webSettings.setSaveFormData(true);
        webSettings.setEnableSmoothTransition(true);

//        wv.loadUrl("javascript:(function() { $('header').remove(); $('footer').remove(); $('body').css('padding-top',0);})()");

        SharedPreferences preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        final String currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
        if (currentLanguageCode.equals("vn")) {
//            wv.loadUrl(ApiClient.ENDPOINT + "vn/chinh-sach-hoat-dong.html");
            wv.loadUrl("https://kfcvietnam.com.vn/vn/chinh-sach-hoat-dong.html");
        } else {
//            wv.loadUrl(ApiClient.ENDPOINT + "en/chinh-sach-hoat-dong.html");
            wv.loadUrl("https://kfcvietnam.com.vn/en/chinh-sach-hoat-dong.html");
        }

        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wv.loadUrl("javascript:(function() { $('.banner, .nav-header, .nav-fix, .wap-sec-tin-tuc, footer').remove();$('body').attr('style','padding-top: 0px !important');})()");
                        wv.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);
                    }
                }, 500);
                super.onPageFinished(view, url);
            }
        });


//    private void initView(){
//        tv = (WebView) findViewById(R.id.tv);
//        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
////        tv.getSettings().setSupportZoom(false);
////        tv.getSettings().setBuiltInZoomControls(false);
////        tv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
//        findViewById(R.id.llBack).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
////        getConfiguration();
//        loadOperationPolicy();
//    }

//    private void getConfiguration() {
//        final ProgressDialog mProgressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
//        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
//        mProgressDialog.setCancelable(true);
//        mProgressDialog.setCanceledOnTouchOutside(false);
//        mProgressDialog.show();
//
//        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
//            mProgressDialog.dismiss();
//            Toast.makeText(this, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
//            return;
//        }
//        SharedPreferences preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
//        String currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
//        Call<ConfigureDataWrapper> call1 = ApiClient.getJsonClient().getConfiguration(currentLanguageCode);
//        call1.enqueue(new Callback<ConfigureDataWrapper>() {
//
//            @Override
//            public void onFailure(Call<ConfigureDataWrapper> arg0, Throwable arg1) {
//                arg1.printStackTrace();
//
//                mProgressDialog.dismiss();
//            }
//
//            @Override
//            public void onResponse(Call<ConfigureDataWrapper> arg0, Response<ConfigureDataWrapper> arg1) {
//                mProgressDialog.dismiss();
//                ConfigureDataWrapper obj = arg1.body();
//                tv.loadData(obj.data.regulation.content, "text/html; charset=utf-8", "utf-8");
//            }
//        });
//    }


    }
}
