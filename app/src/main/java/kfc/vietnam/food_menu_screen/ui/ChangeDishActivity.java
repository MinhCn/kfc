package kfc.vietnam.food_menu_screen.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.base.BaseActivity;
import kfc.vietnam.common.enums.ChangeDishPositionType;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.ProductWrapper002;
import kfc.vietnam.food_menu_screen.adapter.ChangeDishPagerAdapter;
import kfc.vietnam.food_menu_screen.fragment.ExchangeBurgerComFragment;
import kfc.vietnam.food_menu_screen.fragment.ExchangeDrinksFragment;
import kfc.vietnam.food_menu_screen.fragment.ExchangeGaRanFragment;
import kfc.vietnam.food_menu_screen.fragment.ExchangeSnacksFragment;
import kfc.vietnam.main_screen.ui.DialogFactory;

/**
 * Created by VietRuyn on 01/08/2016.
 */
public class ChangeDishActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    public ViewPager viewPagerChangeDish;
    private ChangeDishPagerAdapter mPagerAdapter;
    private ImageView imgTitle, imgDotSnacks, imgDotDrinks, imgDotGaRan, imgDotBurger;
    private TextView tvTitle;
    private Intent i;
    private int size;
    public int currentViewVisible;

    public ArrayList<ProductWrapper002> lstSnacksAndDessertChange, lstDrinkChange, lstGaRanChange, lstBurgerComChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_change_dish_activity);

        i = getIntent();
        lstSnacksAndDessertChange = (ArrayList<ProductWrapper002>) i.getSerializableExtra("list_snacks_and_dessert_change");
        lstDrinkChange = (ArrayList<ProductWrapper002>) i.getSerializableExtra("lst_drink_change");
        lstGaRanChange = (ArrayList<ProductWrapper002>) i.getSerializableExtra("lst_ga_ran_change");
        lstBurgerComChange = (ArrayList<ProductWrapper002>) i.getSerializableExtra("lst_burger_com_change");

        int positionScreen = i.getExtras().getInt("position_screen", 0);

        currentViewVisible = positionScreen;

        size = 0;
        ArrayList<Integer> type = new ArrayList<>();

        if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0) {
            size = size + 1;
            type.add(ChangeDishPositionType.SNACKS.getValue());
        }

        if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
            size = size + 1;
            type.add(ChangeDishPositionType.DRINKS.getValue());
        }

        if (lstGaRanChange != null && lstGaRanChange.size() > 0) {
            size = size + 1;
            type.add(ChangeDishPositionType.GARAN.getValue());
        }

        if (lstBurgerComChange != null && lstBurgerComChange.size() > 0) {
            size = size + 1;
            type.add(ChangeDishPositionType.BURGER.getValue());
        }

        imgTitle = (ImageView) findViewById(R.id.imgTitle);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        imgDotSnacks = (ImageView) findViewById(R.id.imgDotSnacks);
        imgDotDrinks = (ImageView) findViewById(R.id.imgDotDrinks);
        imgDotGaRan = (ImageView) findViewById(R.id.imgDotGaRan);
        imgDotBurger = (ImageView) findViewById(R.id.imgDotBurger);
        LinearLayout llBack = (LinearLayout) findViewById(R.id.llBack);
        llBack.setOnClickListener(this);

        //check khi chỉ có một loại để change, vd chỉ có đổi nước hoặc đổi thức ăn nhẹ thì phải init cho list ban đầu = empty
        if (lstSnacksAndDessertChange == null || lstSnacksAndDessertChange.size() == 0) {
            if (lstGaRanChange == null || lstGaRanChange.size() == 0)
                if (lstBurgerComChange == null || lstBurgerComChange.size() == 0) {
                    positionScreen = 0;
                    size = 1;
                    type.add(ChangeDishPositionType.DRINKS.getValue());
                    imgTitle.setImageResource(R.drawable.ic_change_water);
                    tvTitle.setText(getString(R.string.water_exchange));
                    imgDotSnacks.setVisibility(View.GONE);
                    imgDotDrinks.setVisibility(View.GONE);
                    imgDotGaRan.setVisibility(View.GONE);
                    imgDotBurger.setVisibility(View.GONE);
                    lstSnacksAndDessertChange = new ArrayList<>();
                    lstGaRanChange = new ArrayList<>();
                    lstBurgerComChange = new ArrayList<>();
                }
        }

        if (lstDrinkChange == null || lstDrinkChange.size() == 0) {
            if (lstGaRanChange == null || lstGaRanChange.size() == 0)
                if (lstBurgerComChange == null || lstBurgerComChange.size() == 0) {
                    positionScreen = 0;
                    size = 1;
                    type.add(ChangeDishPositionType.SNACKS.getValue());
                    tvTitle.setText(getString(R.string.snacks_exchange));
                    imgDotSnacks.setVisibility(View.GONE);
                    imgDotDrinks.setVisibility(View.GONE);
                    imgDotGaRan.setVisibility(View.GONE);
                    imgDotBurger.setVisibility(View.GONE);
                    lstDrinkChange = new ArrayList<>();
                    lstGaRanChange = new ArrayList<>();
                    lstBurgerComChange = new ArrayList<>();
                }
        }

        //add by @ManhNguyen
        if (lstSnacksAndDessertChange == null || lstSnacksAndDessertChange.size() == 0) {
            if (lstDrinkChange == null || lstDrinkChange.size() == 0)
                if (lstBurgerComChange == null || lstBurgerComChange.size() == 0) {
                    positionScreen = 0;
                    size = 1;
                    type.add(ChangeDishPositionType.GARAN.getValue());
                    tvTitle.setText(getString(R.string.garan_exchange));
                    imgDotSnacks.setVisibility(View.GONE);
                    imgDotDrinks.setVisibility(View.GONE);
                    imgDotGaRan.setVisibility(View.GONE);
                    imgDotBurger.setVisibility(View.GONE);
                    lstSnacksAndDessertChange = new ArrayList<>();
                    lstDrinkChange = new ArrayList<>();
                    lstBurgerComChange = new ArrayList<>();
                }

        }

        if (lstSnacksAndDessertChange == null || lstSnacksAndDessertChange.size() == 0) {
            if (lstDrinkChange == null || lstDrinkChange.size() == 0)
                if (lstGaRanChange == null || lstGaRanChange.size() == 0) {
                    positionScreen = 0;
                    size = 1;
                    type.add(ChangeDishPositionType.BURGER.getValue());
                    tvTitle.setText(getString(R.string.burger_exchange));
                    imgDotSnacks.setVisibility(View.GONE);
                    imgDotDrinks.setVisibility(View.GONE);
                    imgDotGaRan.setVisibility(View.GONE);
                    imgDotBurger.setVisibility(View.GONE);
                    lstSnacksAndDessertChange = new ArrayList<>();
                    lstDrinkChange = new ArrayList<>();
                    lstGaRanChange = new ArrayList<>();
                }
        }

        viewPagerChangeDish = (ViewPager) findViewById(R.id.viewPagerChangeDish);
        mPagerAdapter = new ChangeDishPagerAdapter(getSupportFragmentManager(), size, type, positionScreen);
        viewPagerChangeDish.setAdapter(mPagerAdapter);
        viewPagerChangeDish.addOnPageChangeListener(this);

        //Luong: snack -> drink -> ga ran -> burger -> snack
        //Set current view dua theo bien truyen vao la gi
        if (positionScreen == ChangeDishPositionType.SNACKS.getValue()) {
            viewPagerChangeDish.setCurrentItem(0);
        } else if (positionScreen == ChangeDishPositionType.DRINKS.getValue()) {
            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0) {
                viewPagerChangeDish.setCurrentItem(1);
            } else {
                viewPagerChangeDish.setCurrentItem(0);
            }
        } else if (positionScreen == ChangeDishPositionType.GARAN.getValue()) {
            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0) {
                if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
                    viewPagerChangeDish.setCurrentItem(2);
                } else {
                    viewPagerChangeDish.setCurrentItem(1);
                }
            } else if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
                viewPagerChangeDish.setCurrentItem(1);
            } else {
                viewPagerChangeDish.setCurrentItem(0);
            }
        } else if (positionScreen == ChangeDishPositionType.BURGER.getValue()) {
            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0) {
                if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
                    if (lstGaRanChange != null && lstGaRanChange.size() > 0) {
                        viewPagerChangeDish.setCurrentItem(3);
                    } else {
                        viewPagerChangeDish.setCurrentItem(2);
                    }
                } else {
                    if (lstGaRanChange != null && lstGaRanChange.size() > 0) {
                        viewPagerChangeDish.setCurrentItem(2);
                    } else {
                        viewPagerChangeDish.setCurrentItem(1);
                    }
                }
            } else if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
                if (lstGaRanChange != null && lstGaRanChange.size() > 0) {
                    viewPagerChangeDish.setCurrentItem(2);
                } else {
                    viewPagerChangeDish.setCurrentItem(1);
                }
            } else if (lstGaRanChange != null && lstGaRanChange.size() > 0){
                viewPagerChangeDish.setCurrentItem(1);
            } else {
                viewPagerChangeDish.setCurrentItem(0);
            }
        }

        if (size > 1) {
            changeTile();
        }

        checkFirtRunApp();
    }

    public void checkFirtRunApp() {
        SharedPreferences preferencesLanguage = getSharedPreferences(SharedPreferenceKeyType.LANGUAGE_CODE_DATAFILE.toString(), MODE_PRIVATE);
        String currentLanguageCode = preferencesLanguage.getString(SharedPreferenceKeyType.LANGUAGE_CODE.toString(), "en");
        SharedPreferences pref = getSharedPreferences(Const.SHARED_PREF, 0);
        boolean isFirst = pref.getBoolean(SharedPreferenceKeyType.FIRST_CHANGE_FOOD.toString(), false);
        if (!isFirst) {
            DialogFactory.showDialogFirstRunApp(this, currentLanguageCode.equals("vn") ? R.drawable.bg_first_change_food_vn : R.drawable.bg_first_change_food_en);
            SharedPreferences.Editor prefEditor = pref.edit();
            prefEditor.putBoolean(SharedPreferenceKeyType.FIRST_CHANGE_FOOD.toString(), true);
            prefEditor.apply();
        }
    }

    private void changeTile() {
        //Luong: snack -> drink -> ga ran -> burger -> snack
        if (currentViewVisible == ChangeDishPositionType.SNACKS.getValue()) {
            imgTitle.setImageResource(R.drawable.ic_change_snacks);
            tvTitle.setText(getString(R.string.snacks_exchange));
            imgDotSnacks.setImageResource(R.drawable.shape_dot_white);

            if (lstDrinkChange != null && lstDrinkChange.size() > 0)
                imgDotDrinks.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotDrinks.setVisibility(View.GONE);

            if (lstGaRanChange != null && lstGaRanChange.size() > 0)
                imgDotGaRan.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotGaRan.setVisibility(View.GONE);

            if (lstBurgerComChange != null && lstBurgerComChange.size() > 0)
                imgDotBurger.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotBurger.setVisibility(View.GONE);

        } else if (currentViewVisible == ChangeDishPositionType.DRINKS.getValue()) {
            imgTitle.setImageResource(R.drawable.ic_change_water);
            tvTitle.setText(getString(R.string.water_exchange));
            imgDotDrinks.setImageResource(R.drawable.shape_dot_white);

            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0)
                imgDotSnacks.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotSnacks.setVisibility(View.GONE);

            if (lstGaRanChange != null && lstGaRanChange.size() > 0)
                imgDotGaRan.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotGaRan.setVisibility(View.GONE);

            if (lstBurgerComChange != null && lstBurgerComChange.size() > 0)
                imgDotBurger.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotBurger.setVisibility(View.GONE);
        } else if (currentViewVisible == ChangeDishPositionType.GARAN.getValue()) {
            imgTitle.setImageResource(R.drawable.ic_garan);
            tvTitle.setText(getString(R.string.garan_exchange));
            imgDotGaRan.setImageResource(R.drawable.shape_dot_white);

            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0)
                imgDotSnacks.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotSnacks.setVisibility(View.GONE);

            if (lstDrinkChange != null && lstDrinkChange.size() > 0)
                imgDotDrinks.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotDrinks.setVisibility(View.GONE);

            if (lstBurgerComChange != null && lstBurgerComChange.size() > 0)
                imgDotBurger.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotBurger.setVisibility(View.GONE);
        } else if (currentViewVisible == ChangeDishPositionType.BURGER.getValue()) {
            imgTitle.setImageResource(R.drawable.ic_burger);
            tvTitle.setText(getString(R.string.burger_exchange));
            imgDotBurger.setImageResource(R.drawable.shape_dot_white);

            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0)
                imgDotSnacks.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotSnacks.setVisibility(View.GONE);

            if (lstDrinkChange != null && lstDrinkChange.size() > 0)
                imgDotDrinks.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotDrinks.setVisibility(View.GONE);

            if (lstGaRanChange != null && lstGaRanChange.size() > 0)
                imgDotGaRan.setImageResource(R.drawable.shape_dot_red_transparent);
            else
                imgDotGaRan.setVisibility(View.GONE);
        }
    }

    public void backPreviousScreen(boolean isChange) {
        i.putExtra("is_change", isChange);

        ArrayList<ProductWrapper002> lstSnacksAndDessertSelected = new ArrayList<>();
        ArrayList<ProductWrapper002> lstDrinkSelected = new ArrayList<>();
        ArrayList<ProductWrapper002> lstGaRanSelected = new ArrayList<>();
        ArrayList<ProductWrapper002> lstBurgerComSelected = new ArrayList<>();

        if (isChange) {
            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0) {
                for (ProductWrapper002 obj : lstSnacksAndDessertChange)
                    if (obj.isProductSelected)
                        lstSnacksAndDessertSelected.add(obj);
            }

            if (lstDrinkChange != null && lstDrinkChange.size() > 0) {
                for (ProductWrapper002 obj : lstDrinkChange)
                    if (obj.isProductSelected)
                        lstDrinkSelected.add(obj);
            }

            if (lstGaRanChange != null && lstGaRanChange.size() > 0) {
                for (ProductWrapper002 obj : lstGaRanChange)
                    if (obj.isProductSelected)
                        lstGaRanSelected.add(obj);

            }

            if (lstBurgerComChange != null && lstBurgerComChange.size() > 0) {
                for (ProductWrapper002 obj : lstBurgerComChange)
                    if (obj.isProductSelected)
                        lstBurgerComSelected.add(obj);
            }

        }

        if (lstDrinkSelected.size() == 0) {
            if (lstDrinkChange != null && lstDrinkChange.size() > 0)
                lstDrinkSelected.add(lstDrinkChange.get(0));
        }

        if (lstSnacksAndDessertSelected.size() == 0) {
            if (lstSnacksAndDessertChange != null && lstSnacksAndDessertChange.size() > 0)
                lstSnacksAndDessertSelected.add(lstSnacksAndDessertChange.get(0));
        }

        if (lstGaRanSelected.size() == 0) {
            if (lstGaRanChange != null && lstGaRanChange.size() > 0)
                lstGaRanSelected.add(lstGaRanChange.get(0));
        }

        if (lstBurgerComSelected.size() == 0) {
            if (lstBurgerComChange != null && lstBurgerComChange.size() > 0)
                lstBurgerComSelected.add(lstBurgerComChange.get(0));
        }

        i.putExtra("list_snacks_selected", lstSnacksAndDessertSelected);
        i.putExtra("list_drinks_selected", lstDrinkSelected);
        i.putExtra("list_ga_ran_selected", lstGaRanSelected);
        i.putExtra("list_burger_com_selected", lstBurgerComSelected);

        setResult(RequestResultCodeType.RESULT_CHANGE_DISH.getValue(), i);
        finish();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llBack:
                backPreviousScreen(false);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = mPagerAdapter.getRegisteredFragment(position);
        if (fragment instanceof ExchangeSnacksFragment) {
            currentViewVisible = ChangeDishPositionType.SNACKS.getValue();
        } else if (fragment instanceof ExchangeDrinksFragment) {
            currentViewVisible = ChangeDishPositionType.DRINKS.getValue();
        } else if (fragment instanceof ExchangeGaRanFragment) {
            currentViewVisible = ChangeDishPositionType.GARAN.getValue();
        } else if (fragment instanceof ExchangeBurgerComFragment) {
            currentViewVisible = ChangeDishPositionType.BURGER.getValue();
        }

        changeTile();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
