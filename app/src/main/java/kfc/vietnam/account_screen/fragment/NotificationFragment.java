package kfc.vietnam.account_screen.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.adapter.NotificationAdapter;
import kfc.vietnam.common.object.NotificationDataWrapper;

/**
 * Created by VietRuyn on 18/09/2016.
 */
public class NotificationFragment extends Fragment implements View.OnClickListener {

    private ListView lvAddress;
    private ArrayList<NotificationDataWrapper.NotificationWrapper> lisNotification;
    private NotificationAdapter notificationAdapter;
    private RelativeLayout rlActionbar;
    private TextView tvTitle;
    private LinearLayout imgBack;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment_layout, container, false);
        init(view);

        if (lisNotification == null)
            lisNotification = new ArrayList<>();

        return view;
    }

    private void init(View view) {
        lvAddress = (ListView) view.findViewById(R.id.lvAddress);
        lisNotification = new ArrayList<>();
        lisNotification = (ArrayList<NotificationDataWrapper.NotificationWrapper>) getArguments().getSerializable("notification_data");
        notificationAdapter = new NotificationAdapter(getActivity(), R.layout.custom_item_notification, lisNotification);
        lvAddress.setAdapter(notificationAdapter);

        rlActionbar = (RelativeLayout) view.findViewById(R.id.rlActionbar);
        rlActionbar.setVisibility(View.VISIBLE);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvTitle.setAllCaps(true);
        tvTitle.setText(getActivity().getResources().getString(R.string.notification));
        imgBack = (LinearLayout) view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        view.findViewById(R.id.llAddAddress).setVisibility(View.GONE);
        view.findViewById(R.id.layout_empty).setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        notificationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }
}
