package kfc.vietnam.account_screen.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import kfc.vietnam.R;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.UpdateProfileDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 07/09/2016.
 */
public class PersonalInformationFragment extends Fragment implements View.OnClickListener {

    EditText edtName, editEmail, edtSdt, edtPassOld, edtPassNew, edtEnterPass;
    LinearLayout llMale, llFemale, llChangePass, llUpdate, llPassword;
    public LoginWrapper userInfo;
    public MainActivity activity;
    private SharedPreferences preferencesUserInfo;

    private CheckBox cbChangePass, cbMale, cbFemale;

    private String gender = "";
    boolean isChangePass = false;

    RelativeLayout rlBirthday;
    LinearLayout imgBack;

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_information_layout, container, false);
        activity = (MainActivity) getActivity();
        try {
            preferencesUserInfo = getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE);
            if (preferencesUserInfo.contains(SharedPreferenceKeyType.USER_INFO.toString()))
                userInfo = (LoginWrapper) ObjectSerializer.deserialize(preferencesUserInfo.getString(SharedPreferenceKeyType.USER_INFO.toString(), null));
        } catch (Exception e) {
            e.printStackTrace();
        }

        edtName = (EditText) view.findViewById(R.id.edtName);
        imgBack = (LinearLayout) view.findViewById(R.id.imgBack);
        editEmail = (EditText) view.findViewById(R.id.editEmail);
        edtSdt = (EditText) view.findViewById(R.id.edtSdt);
        edtPassOld = (EditText) view.findViewById(R.id.edtPassOld);
        edtPassNew = (EditText) view.findViewById(R.id.edtPassNew);
        edtEnterPass = (EditText) view.findViewById(R.id.edtEnterPass);
        rlBirthday = (RelativeLayout) view.findViewById(R.id.rlBirthday);
        llPassword = (LinearLayout) view.findViewById(R.id.llPassword);
        rlBirthday.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        dateView = (TextView) view.findViewById(R.id.tvDate);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        try {
            if (TextUtils.isEmpty(userInfo.birthday))
                dateView.setText(getActivity().getResources().getString(R.string.day_month_year));
            else {
                dateView.setText(userInfo.birthday.replace("/", " - ").replace("//", " - "));
                String[] timeArray = userInfo.birthday.replace("/", " - ").split(" - ");
                day = Integer.parseInt(timeArray[0]);
                month = Integer.parseInt(timeArray[1]);
                year = Integer.parseInt(timeArray[2]);
            }
        } catch (Exception e) {
            dateView.setText(getActivity().getResources().getString(R.string.day_month_year));
            e.printStackTrace();
        }
        edtName.setText(userInfo.name);
        editEmail.setText(userInfo.email);
        edtSdt.setText(userInfo.phone);

        llMale = (LinearLayout) view.findViewById(R.id.llMale);
        llFemale = (LinearLayout) view.findViewById(R.id.llFemale);
        llChangePass = (LinearLayout) view.findViewById(R.id.llChangePass);
        llUpdate = (LinearLayout) view.findViewById(R.id.llUpdate);
        cbChangePass = (CheckBox) view.findViewById(R.id.cbChangePass);
        cbMale = (CheckBox) view.findViewById(R.id.cbMale);
        cbFemale = (CheckBox) view.findViewById(R.id.cbFemale);

        llMale.setOnClickListener(this);
        llFemale.setOnClickListener(this);
        llChangePass.setOnClickListener(this);
        llUpdate.setOnClickListener(this);

        if (userInfo.gender != null && userInfo.gender.equals("1")) {
            cbMale.setChecked(true);
            cbFemale.setChecked(false);
            gender = "1";
        } else {
            cbMale.setChecked(false);
            cbFemale.setChecked(true);
            gender = "0";
        }

        if (userInfo.usertype.equals("1")) {
            llChangePass.setVisibility(View.VISIBLE);
        } else {
            llChangePass.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMale:
                cbMale.setChecked(true);
                cbFemale.setChecked(false);
                gender = "1";
                break;
            case R.id.llFemale:
                cbMale.setChecked(false);
                cbFemale.setChecked(true);
                gender = "0";
                break;
            case R.id.llChangePass:
                isChangePass = !isChangePass;
                cbChangePass.setChecked(isChangePass);
                if (isChangePass) {
                    llPassword.setVisibility(View.VISIBLE);
                    edtPassOld.requestFocus();
                } else {
                    llPassword.setVisibility(View.GONE);
                    edtPassOld.setText("");
                    edtPassNew.setText("");
                    edtEnterPass.setText("");
                }
                break;
            case R.id.rlBirthday:
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dateView.setText(dayOfMonth + " - " + (monthOfYear + 1) + " - " + year);
                                PersonalInformationFragment.this.day = dayOfMonth;
                                PersonalInformationFragment.this.month = monthOfYear + 1;
                                PersonalInformationFragment.this.year = year;

                            }
                        }, year, month - 1, day);
                datePickerDialog.show();
                break;
            case R.id.llUpdate:
                if (userInfo.usertype.equals("1")) {
                    String name = edtName.getText().toString().trim();
                    String phone = edtSdt.getText().toString().trim();

                    String oldPass = edtPassOld.getText().toString().trim() + "";
                    String newPass = edtPassNew.getText().toString().trim() + "";
                    String confirmPass = edtEnterPass.getText().toString().trim() + "";

                    String md5OldPass = String.valueOf(Hex.encodeHex(DigestUtils.md5(oldPass)));
                    String md5NewPass = String.valueOf(Hex.encodeHex(DigestUtils.md5(newPass)));

                    if (name.isEmpty()) {
                        edtName.requestFocus();
                        edtName.setError(getString(R.string.StepOneFragment_001));
                    } else if (phone.isEmpty() || phone.length() < 10) {
                        edtSdt.requestFocus();
                        edtSdt.setError(getString(R.string.StepOneFragment_003));
                    } else if (cbChangePass.isChecked()) {
                        if (oldPass.isEmpty()) {
                            edtPassOld.requestFocus();
                            edtPassOld.setError(getString(R.string.pls_old_password));
                        } else if (newPass.isEmpty()) {
                            edtPassNew.requestFocus();
                            edtPassNew.setError(getString(R.string.pls_new_password));
                        } else if (newPass.length() < 6) {
                            edtPassNew.requestFocus();
                            edtPassNew.setError(getString(R.string.pls_password_least));
                        } else if (confirmPass.isEmpty()) {
                            edtEnterPass.requestFocus();
                            edtEnterPass.setError(getString(R.string.pls_confirm_password));
                        } else if (!confirmPass.equals(newPass)) {
                            edtEnterPass.requestFocus();
                            edtEnterPass.setError(getString(R.string.pls_match_password));
                        } else if (!userInfo.password.equals(md5OldPass)) {
                            final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
                            mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
                            mProgressDialog.setCancelable(true);
                            mProgressDialog.setCanceledOnTouchOutside(false);
                            mProgressDialog.show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressDialog.dismiss();
                                    edtPassOld.requestFocus();
                                    Toast.makeText(activity, getString(R.string.old_password_incorrect), Toast.LENGTH_SHORT).show();
                                }
                            }, 500);
                        } else if (userInfo.password.equals(md5NewPass)) {
                            final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
                            mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
                            mProgressDialog.setCancelable(true);
                            mProgressDialog.setCanceledOnTouchOutside(false);
                            mProgressDialog.show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressDialog.dismiss();
                                    edtPassNew.requestFocus();
                                    Toast.makeText(activity, getString(R.string.old_new_password_diff), Toast.LENGTH_SHORT).show();
                                }
                            }, 500);
                        } else {
                            updateProfile(userInfo.id, name, editEmail.getText().toString(),
                                    gender, day + "", month + "", year + "", phone, newPass, confirmPass, oldPass);
                        }
                    } else { //api: password: truyền lên rỗng nghĩa là không cập nhật
                        updateProfile(userInfo.id, name, editEmail.getText().toString(),
                                gender, day + "", month + "", year + "", phone, newPass, confirmPass, oldPass);
                    }

                } else { //Account link with fb, gg
                    String name = edtName.getText().toString().trim();
                    String phone = edtSdt.getText().toString().trim();

                    if (name.isEmpty()) {
                        edtName.requestFocus();
                        edtName.setError(getString(R.string.StepOneFragment_001));
                    } else if (phone.isEmpty() || phone.length() < 10) {
                        edtSdt.requestFocus();
                        edtSdt.setError(getString(R.string.StepOneFragment_003));
                    } else {
                        updateProfileFbOrGG(userInfo.id, name, editEmail.getText().toString(),
                                gender, day + "", month + "", year + "", phone);
                    }
                }
                break;
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }

    private void updateProfile(final String userID,
                               final String name,
                               final String email,
                               final String gender,//(1: nam - 0: nữ)
                               final String day,
                               final String month,
                               final String year,
                               final String phone,
                               final String newPass,
                               String newPassRepeat,
                               String oldPass) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<UpdateProfileDataWrapper> call1 = ApiClient.getJsonClient().updateProfile(userID,
                new String(Hex.encodeHex(DigestUtils.md5(userID))),
                name,
                email,
                gender,//(1: nam - 0: nữ)
                day,
                month,
                year,
                phone,
                newPass,
                newPassRepeat,
                oldPass);
        call1.enqueue(new Callback<UpdateProfileDataWrapper>() {

            @Override
            public void onFailure(Call<UpdateProfileDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                Toast.makeText(getActivity(), R.string.update_fail, Toast.LENGTH_SHORT).show();

                mProgressDialog.dismiss();
                Log.e("UpdateError", arg1.toString());
            }

            @Override
            public void onResponse(Call<UpdateProfileDataWrapper> arg0, Response<UpdateProfileDataWrapper> arg1) {
                mProgressDialog.dismiss();

                UpdateProfileDataWrapper obj = arg1.body();

                if (obj.result) {
                    try {
                        userInfo.name = name;
                        userInfo.email = email;
                        userInfo.gender = gender;
                        userInfo.birthday = String.format("%s - %s - %s", day, month, year);
                        userInfo.phone = phone;
                        if (!TextUtils.isEmpty(newPass)){
                            String md5NewPass = String.valueOf(Hex.encodeHex(DigestUtils.md5(newPass)));
                            userInfo.password = md5NewPass;
                        }
//                        getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(userInfo)).commit();
                        getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(), ObjectSerializer.serialize(userInfo)).apply();
//                        getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(),
//                                Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(),
//                                ObjectSerializer.serialize((Serializable) obj.data)).commit();
                        activity.onBackPressed();
                        Toast.makeText(getActivity(), R.string.updateSuccess, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void updateProfileFbOrGG(final String userID,
                                     final String name,
                                     final String email,
                                     final String gender,//(1: nam - 0: nữ)
                                     final String day,
                                     final String month,
                                     final String year,
                                     final String phone) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<UpdateProfileDataWrapper> call1 = ApiClient.getJsonClient().updateProfileFbOrGG(userID,
                new String(Hex.encodeHex(DigestUtils.md5(userID))),
                name,
                email,
                gender,//(1: nam - 0: nữ)
                day,
                month,
                year,
                phone);
        call1.enqueue(new Callback<UpdateProfileDataWrapper>() {

            @Override
            public void onFailure(Call<UpdateProfileDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                Toast.makeText(getActivity(), R.string.update_fail, Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
                Log.e("UpdateError", arg1.toString());
            }

            @Override
            public void onResponse(Call<UpdateProfileDataWrapper> arg0, Response<UpdateProfileDataWrapper> arg1) {
                mProgressDialog.dismiss();

                UpdateProfileDataWrapper obj = arg1.body();

                if (obj.result) {
                    try {
                        userInfo.name = name;
                        userInfo.email = email;
                        userInfo.gender = gender;
                        userInfo.birthday = String.format("%s - %s - %s", day, month, year);
                        userInfo.phone = phone;
//                        getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(),
//                                Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(),
//                                ObjectSerializer.serialize(userInfo)).commit();
                        getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(),
                                Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(),
                                ObjectSerializer.serialize(userInfo)).apply();
//                        getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(),
//                                Context.MODE_PRIVATE).edit().putString(SharedPreferenceKeyType.USER_INFO.toString(),
//                                ObjectSerializer.serialize((Serializable) obj.data)).commit();

                        activity.onBackPressed();
                        Toast.makeText(getActivity(), R.string.updateSuccess, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


}
