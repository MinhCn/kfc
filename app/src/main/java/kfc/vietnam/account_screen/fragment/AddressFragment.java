package kfc.vietnam.account_screen.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.TimerTask;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.adapter.AddressAdapter;
import kfc.vietnam.account_screen.adapter.WardAdapter;
import kfc.vietnam.common.object.AddAddressDataWrapper;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.AddressListDataWrapper;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.object.UpdateShipAddressDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.adapter.DistrictAdapter;
import kfc.vietnam.food_menu_screen.adapter.ProvinceAdapter;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 18/09/2016.
 */
public class AddressFragment extends Fragment implements View.OnClickListener {

    private ListView lvAddress;
    private ArrayList<AddressDataWrapper> lstAddress;
    private AddressAdapter adapterAddress;

    private RelativeLayout rlActionbar, rlOk, rlCancel;
    public LinearLayout llAddAddress;
    public static ScrollView svMore;
    private TextView tvTitle;
    private LinearLayout imgBack;
    private LinearLayout layoutEmpty, layoutImEmpty;
    public static EditText edtFirstNameAndLastName, edtAddress, edtPhoneNumber, edtEmail;
    public static Spinner spnProvince, spnDistrict, spnWard;
    public static boolean isAdd = false;
    private DistrictAdapter districtAdapter;
    private ProvinceAdapter provinceAdapter;
    private WardAdapter wardAdapter;
    private String provinceID;
    private String districtID;
    private String wardID;
    public MainActivity activity;
    public ArrayList<ProvinceWrapper> lstProvince;
    public ArrayList<DistrictWrapper> lstDistrict;
    public ArrayList<DistrictWrapper.WardWapper> lstWard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.list_fragment_layout, container, false);
        init(view);
        getShipAddressList(((MainActivity) getActivity()).userInfo.id);

        return view;
    }

    private void init(View view) {
        layoutImEmpty = (LinearLayout) view.findViewById(R.id.layout_im_empty);
        lvAddress = (ListView) view.findViewById(R.id.lvAddress);
        rlOk = (RelativeLayout) view.findViewById(R.id.rlOk);
        rlCancel = (RelativeLayout) view.findViewById(R.id.rlCancel);
        llAddAddress = (LinearLayout) view.findViewById(R.id.llAddAddress);
        layoutEmpty = (LinearLayout) view.findViewById(R.id.layout_empty);
        svMore = (ScrollView) view.findViewById(R.id.svMore);
        llAddAddress.setVisibility(View.VISIBLE);
        rlActionbar = (RelativeLayout) view.findViewById(R.id.rlActionbar);
        rlActionbar.setVisibility(View.VISIBLE);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvTitle.setText(getActivity().getResources().getString(R.string.address_new));
        imgBack = (LinearLayout) view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        rlOk.setOnClickListener(this);
        rlCancel.setOnClickListener(this);
        llAddAddress.setOnClickListener(this);
        lstAddress = new ArrayList<>();
        adapterAddress = new AddressAdapter(getActivity(), R.layout.custom_item_list_address, lstAddress, ((MainActivity) getActivity()).currentLanguageCode, lvAddress, this);
        lvAddress.setAdapter(adapterAddress);

        edtFirstNameAndLastName = (EditText) view.findViewById(R.id.edtFirstNameAndLastName);
        edtAddress = (EditText) view.findViewById(R.id.edtAddress);
        edtPhoneNumber = (EditText) view.findViewById(R.id.edtPhoneNumber);
        edtEmail = (EditText) view.findViewById(R.id.edtEmail);

        lstProvince = ((MainActivity) getActivity()).lstProvince;
        lstDistrict = lstProvince.get(0).district;
        lstWard = lstDistrict.get(0).lstWard;
        provinceID = lstProvince.get(0).id;
        districtID = lstDistrict.get(0).id;
        wardID = lstWard.get(0).id;

        spnProvince = (Spinner) view.findViewById(R.id.spnProvince);
        provinceAdapter = new ProvinceAdapter(getActivity(), R.layout.custom_item_province_spinner, lstProvince);
        spnProvince.setAdapter(provinceAdapter);
        spnProvince.setOnItemSelectedListener(onProvinceSelected);

        spnDistrict = (Spinner) view.findViewById(R.id.spnDistrict);
        districtAdapter = new DistrictAdapter(getActivity(), R.layout.custom_item_district_spinner, lstDistrict);
        spnDistrict.setAdapter(districtAdapter);
        spnDistrict.setOnItemSelectedListener(onDistrictSelected);

        spnWard = (Spinner) view.findViewById(R.id.spnWard);
        wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
        spnWard.setAdapter(wardAdapter);
        spnWard.setOnItemSelectedListener(onWardSelected);

        layoutEmpty.post(new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams layoutParams = layoutEmpty.getLayoutParams();
                layoutParams.height = layoutImEmpty.getHeight();
                layoutEmpty.setLayoutParams(layoutParams);
            }
        });

//        selectChangeProvince();
    }

    private AdapterView.OnItemSelectedListener onProvinceSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            provinceID = lstProvince.get(i).id;
            lstDistrict = lstProvince.get(i).district;
            districtID = lstDistrict.get(0).id;
            lstWard = lstDistrict.get(0).lstWard;;
            wardID = lstWard.get(0).id;
            if(getActivity()!= null){
                districtAdapter = new DistrictAdapter(getActivity(), R.layout.custom_item_district_spinner, lstDistrict);
                spnDistrict.setAdapter(districtAdapter);
                spnDistrict.setOnItemSelectedListener(onDistrictSelected);
                wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                spnWard.setAdapter(wardAdapter);
                spnWard.setOnItemSelectedListener(onWardSelected);
            }



        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private AdapterView.OnItemSelectedListener onDistrictSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            districtID = lstDistrict.get(i).id;
            lstWard = lstDistrict.get(i).lstWard;
            wardID = lstWard.get(0).id;
            if(getActivity()!= null){
                wardAdapter = new WardAdapter(getActivity(), R.layout.custom_item_ward_spinner, lstWard);
                spnWard.setAdapter(wardAdapter);
                spnWard.setOnItemSelectedListener(onWardSelected);
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private AdapterView.OnItemSelectedListener onWardSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            wardID = lstWard.get(i).id;

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private void getShipAddressList(String userID) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<AddressListDataWrapper> call1 = ApiClient.getJsonClient().getShipAddressList(userID, new String(Hex.encodeHex(DigestUtils.md5(userID))));
        call1.enqueue(new Callback<AddressListDataWrapper>() {

            @Override
            public void onFailure(Call<AddressListDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<AddressListDataWrapper> arg0, Response<AddressListDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    if (arg1.body().data != null){
                        if (arg1.body().data.size() != 0) {
                            lstAddress.clear();
                            lstAddress.addAll(arg1.body().data);
                            lvAddress.setVisibility(View.VISIBLE);
                            adapterAddress.notifyDataSetChanged();
                            layoutEmpty.setVisibility(View.GONE);
                        } else {
                            showEmptyMessage();
                        }
                    }
                }
            }
        });
    }

    private void addShipAddress(String userID, String fullName, String city, String district, String wardID, String address, String phone, String email) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<AddAddressDataWrapper> call1 = ApiClient.getJsonClient().addShipAddress(userID, new String(Hex.encodeHex(DigestUtils.md5(userID))), fullName, city, district, wardID, address, phone, email);
        call1.enqueue(new Callback<AddAddressDataWrapper>() {

            @Override
            public void onFailure(Call<AddAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<AddAddressDataWrapper> arg0, Response<AddAddressDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //SOLUTION TO FIX BUG: java.lang.IllegalStateException: Fragment xxx not attached to Activity
                if (isAdded()) {
                    AddAddressDataWrapper obj = arg1.body();
                    if (obj.result) {
                        isAdd = false;
                        getShipAddressList(((MainActivity) getActivity()).userInfo.id);
                        svMore.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    public void clearErrorField(){
        edtFirstNameAndLastName.setError(null);
        edtAddress.setError(null);
        edtPhoneNumber.setError(null);
        edtEmail.setError(null);
    }

    private void updateShipAddress(String userID,
                                   String id,
                                   String fullname,
                                   String city,
                                   String district,
                                   String wardID,
                                   String address,
                                   String phone,
                                   String email) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<UpdateShipAddressDataWrapper> call1 = ApiClient.getJsonClient().updateShipAddress(userID,
                new String(Hex.encodeHex(DigestUtils.md5(userID))),
                id,
                fullname,
                city,
                district,
                wardID,
                address,
                phone,
                email);
        call1.enqueue(new Callback<UpdateShipAddressDataWrapper>() {

            @Override
            public void onFailure(Call<UpdateShipAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<UpdateShipAddressDataWrapper> arg0, Response<UpdateShipAddressDataWrapper> arg1) {
                mProgressDialog.dismiss();

                UpdateShipAddressDataWrapper obj = arg1.body();
                if (obj.result) {
                    isAdd = false;
                    getShipAddressList(((MainActivity) getActivity()).userInfo.id);
                    svMore.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), R.string.updateSuccess, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
            case R.id.llAddAddress:
                isAdd = true;
                svMore.setVisibility(View.VISIBLE);
                edtFirstNameAndLastName.setText("");
                edtAddress.setText("");
                edtPhoneNumber.setText("");
                edtEmail.setText("");
                spnProvince.setSelection(0);
                spnDistrict.setSelection(0);
                break;
            case R.id.rlOk:
                String name = edtFirstNameAndLastName.getText().toString().trim();
                String address = edtAddress.getText().toString().trim();
                String phoneNumber = edtPhoneNumber.getText().toString().trim();
                String email = edtEmail.getText().toString().trim();
                if (!name.isEmpty()) {
                    if (!address.isEmpty() && address.length() >= 10) {
                        if (((MainActivity) getActivity()).isValidatePhoneNumber(phoneNumber)) {
                            if (!email.isEmpty()) {
                                if (((MainActivity) getActivity()).patternMail(email)) {
                                    clearErrorField();
                                    if (!isAdd)
                                        updateShipAddress(((MainActivity) getActivity()).userInfo.id, AddressAdapter.idAddress, name, provinceID, districtID, wardID, address, phoneNumber, email);
                                    else
                                        addShipAddress(((MainActivity) getActivity()).userInfo.id, name, provinceID, districtID, wardID, address, phoneNumber, email);
                                } else {
                                    edtEmail.requestFocus();
                                    edtEmail.setError(getString(R.string.StepOneFragment_006));
                                }
                            } else {
                                edtEmail.requestFocus();
                                edtEmail.clearComposingText();
                                edtEmail.setError(getString(R.string.StepOneFragment_004));
                            }
                        } else {
                            edtPhoneNumber.requestFocus();
                            edtPhoneNumber.clearComposingText();
                            edtPhoneNumber.setError(getString(R.string.StepOneFragment_003));
                        }
                    } else {
                        edtAddress.requestFocus();
                        edtAddress.clearComposingText();
                        edtAddress.setError(getString(R.string.StepOneFragment_002));
                    }
                } else {
                    edtFirstNameAndLastName.requestFocus();
                    edtFirstNameAndLastName.clearComposingText();
                    edtFirstNameAndLastName.setError(getString(R.string.StepOneFragment_001));
                }
                break;
            case R.id.rlCancel:
                svMore.setVisibility(View.GONE);
                break;

        }
    }

    public void showEmptyMessage() {
        layoutEmpty.setVisibility(View.VISIBLE);
        lvAddress.setVisibility(View.GONE);

    }

}
