package kfc.vietnam.account_screen.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.List;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.adapter.FoodFavoriteAdapter;
import kfc.vietnam.common.object.FoodFavoriteDataWrapper;
import kfc.vietnam.common.utility.DividerItemDecoration;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 18/09/2016.
 */
public class FoodFavoriteFragment extends Fragment implements View.OnClickListener {

    private RecyclerView rcvShoppingCart;
    private ArrayList<FoodFavoriteDataWrapper.FoodFavoriteWrapper> lstData;
    private FoodFavoriteAdapter mAdapter;
    private Context ctx;
    public MainActivity activity;
    private LinearLayout imgBack, layoutEmpty;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.food_favorite_fragment, container, false);
        init(view);

        return view;
    }


    private void init(View view) {

        layoutEmpty = (LinearLayout) view.findViewById(R.id.layout_empty);

        imgBack = (LinearLayout) view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        rcvShoppingCart = (RecyclerView) view.findViewById(R.id.rcvShoppingCart);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvShoppingCart.setLayoutManager(layoutManager);
        rcvShoppingCart.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider_listview));

        Log.e("Favorite", ((MainActivity) getActivity()).userInfo.id + " - "
                + ((MainActivity) getActivity()).currentLanguageCode + " - " + ((MainActivity) getActivity()).userInfo.city_id);

        getFoodFavorite(((MainActivity) getActivity()).userInfo.id, ((MainActivity) getActivity()).currentLanguageCode, ((MainActivity) getActivity()).userInfo.city_id);
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = new Intent();
        intent.setAction("CHANGE_LAYOUT_FOOD_FAVORITE");
        getActivity().sendBroadcast(intent);

    }


    private void getFoodFavorite(String userID,
                                 String lang,
                                 String city) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<FoodFavoriteDataWrapper> call1 = ApiClient.getJsonClient().getFoodFavorite(userID,
                new String(Hex.encodeHex(DigestUtils.md5(userID))),
                lang,
                city);
        call1.enqueue(new Callback<FoodFavoriteDataWrapper>() {

            @Override
            public void onFailure(Call<FoodFavoriteDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<FoodFavoriteDataWrapper> arg0, Response<FoodFavoriteDataWrapper> arg1) {
                mProgressDialog.dismiss();
                lstData = new ArrayList<FoodFavoriteDataWrapper.FoodFavoriteWrapper>();
                FoodFavoriteDataWrapper obj = arg1.body();
                lstData = obj.data;
                if (lstData != null){
                    if (lstData.size() != 0) {
                        mAdapter = new FoodFavoriteAdapter(getActivity(), lstData, FoodFavoriteFragment.this);
                        rcvShoppingCart.setAdapter(mAdapter);
                    } else {
                        showEmptyMess();
                    }
                }
            }
        });
    }

    public void showEmptyMess() {
        layoutEmpty.setVisibility(View.VISIBLE);
        rcvShoppingCart.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }
}
