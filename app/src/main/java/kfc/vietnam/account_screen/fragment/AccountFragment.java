package kfc.vietnam.account_screen.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import kfc.vietnam.R;
import kfc.vietnam.common.enums.Const;
import kfc.vietnam.common.enums.RequestResultCodeType;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.enums.Tag;
import kfc.vietnam.common.object.LoginWrapper;
import kfc.vietnam.common.object.NotificationDataWrapper;
import kfc.vietnam.common.object.OrderHistoryDataWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.fragment.MenuFragment;
import kfc.vietnam.food_menu_screen.fragment.OrderHistoryFragment;
import kfc.vietnam.main_screen.ui.LoginActivity;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 07/09/2016.
 */
public class AccountFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    public LoginWrapper userInfo;
    public FragmentManager fragmentManager;
    public FragmentTransaction fragmentTransaction;
    private RelativeLayout rlPersonalInformation, rlAddress, rlFavoriteFood, rlNotification, rlLogout,
            rlTransactionHistory, rlMain;
    private LinearLayout llCountNoti;
    private ImageView imgAvatar, imgBlurAvatar, imgShowNavigation, imgEdit;
    public LinearLayout llScoreContainer;
    private TextView tvScore, tvName, tvCountNotification;
    private SharedPreferences preferencesUserInfo;
    private AddressFragment addressFragment;
    private PersonalInformationFragment mPersonalInformationFragment;
    public ArrayList<NotificationDataWrapper.NotificationWrapper> listNotification;
    private MainActivity activity;
    private Context ctx;
    private String currentLanguageCode = "";
    public MenuFragment menuFragment;

    GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;

    @Override
    public void onAttach(Context context) {
        this.ctx = context;
        super.onAttach(context);

        activity = (MainActivity) ctx;
        this.currentLanguageCode = activity.currentLanguageCode;
    }



    @Override
    public Animation onCreateAnimation(int transit, final boolean enter, int nextAnim) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation started.");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation repeating.");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(Tag.LOG.getValue(), "Animation ended.");
                if (enter) {
                    activity.processChangeTopMenu(AccountFragment.class);
                }
            }
        });

        return anim;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_account, container, false);
        listNotification = new ArrayList<>();
        init(view);
        FacebookSdk.sdkInitialize(getActivity());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (MainActivity.mGoogleApiClient != null)
            MainActivity.mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (MainActivity.mGoogleApiClient != null && MainActivity.mGoogleApiClient.isConnected()) {
            MainActivity.mGoogleApiClient.stopAutoManage((FragmentActivity) ctx);
            MainActivity.mGoogleApiClient.disconnect();
        }
    }

    private void init(View view) {
        rlPersonalInformation = (RelativeLayout) view.findViewById(R.id.rlPersonalInformation);
        rlAddress = (RelativeLayout) view.findViewById(R.id.rlAddress);
        rlFavoriteFood = (RelativeLayout) view.findViewById(R.id.rlFavoriteFood);
        rlNotification = (RelativeLayout) view.findViewById(R.id.rlNotification);
        llCountNoti = (LinearLayout) view.findViewById(R.id.llCountNoti);
        rlLogout = (RelativeLayout) view.findViewById(R.id.rlLogout);
        rlTransactionHistory = (RelativeLayout) view.findViewById(R.id.rlTransactionHistory);
        rlMain = (RelativeLayout) view.findViewById(R.id.rlMain);
        imgShowNavigation = (ImageView) view.findViewById(R.id.imgShowNavigation);
//        imgEdit = (ImageView) view.findViewById(R.id.imgEdit);

        rlPersonalInformation.setOnClickListener(this);
        rlAddress.setOnClickListener(this);
        rlFavoriteFood.setOnClickListener(this);
        rlNotification.setOnClickListener(this);
        rlLogout.setOnClickListener(this);
        rlTransactionHistory.setOnClickListener(this);
        imgShowNavigation.setOnClickListener(this);
//        imgEdit.setOnClickListener(this);

        tvScore = (TextView) view.findViewById(R.id.tvScore);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvCountNotification = (TextView) view.findViewById(R.id.tvCountNotification);
        imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
        imgBlurAvatar = (ImageView) view.findViewById(R.id.imgBlurAvatar);
        llScoreContainer = (LinearLayout) view.findViewById(R.id.llScoreContainer);

        if(activity.conf != null && activity.conf.point != null && activity.conf.point.checkPoint){
            llScoreContainer.setVisibility(View.VISIBLE);
        } else {
            llScoreContainer.setVisibility(View.GONE);
        }

        addressFragment = new AddressFragment();
    }

    private void initData() {
        try {
//            userInfo = activity.userInfo;
            preferencesUserInfo = getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE);
            if (preferencesUserInfo.contains(SharedPreferenceKeyType.USER_INFO.toString()))
//                if (userInfo == null)
                    userInfo = (LoginWrapper) ObjectSerializer.deserialize(preferencesUserInfo.getString(SharedPreferenceKeyType.USER_INFO.toString(), null));
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (userInfo != null) {
            SharedPreferences pref = getActivity().getSharedPreferences(Const.SHARED_PREF, 0);
            boolean notification = pref.getBoolean(SharedPreferenceKeyType.BLOCK_NOTIFICATION.toString() , false);
            if (notification){
                getNotification(((MainActivity) getActivity()).currentLanguageCode);
            }else {
                llCountNoti.setVisibility(View.GONE);
            }
            rlMain.setVisibility(View.VISIBLE);
            setScoreUser();
            tvName.setText(userInfo.name);
            Glide.with(this)
                    .load(userInfo.avatar)
                    .placeholder(userInfo.gender.equals("1")?R.drawable.ic_nam:R.drawable.ic_nu)
                    .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .fitCenter()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            //progressBar.setVisibility(View.GONE);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Picasso.with(getContext())
                                            .load(userInfo.avatar)
                                            .error(userInfo.gender.equals("1") ? R.drawable.ic_nam : R.drawable.ic_nu)
                                            .transform(new BlurTransformation(getActivity().getApplicationContext(), 50, 1))
                                            .into(imgBlurAvatar);
                                }
                            });
                            return false;
                        }
                    })
                    .bitmapTransform(new CropCircleTransformation(getActivity()))
                    .into(imgAvatar);

            if(TextUtils.isEmpty(userInfo.avatar)){
                Picasso.with(getContext())
                        .load(R.drawable.avatar_blur)
                        .transform(new BlurTransformation(ctx, 50, 1))
                        .into(imgBlurAvatar);
                if (userInfo.gender.equals("1")){
                    Picasso.with(getContext())
                            .load(R.drawable.ic_nam)
                            .into(imgAvatar);
                } else {
                    Picasso.with(getContext())
                            .load(R.drawable.ic_nu)
                            .into(imgAvatar);
                }
                return;
            }

        } else {
            rlMain.setVisibility(View.GONE);
//            openDialog();
        }
    }

    public void setScoreUser() {
//        userInfo = activity.userInfo;
        if(userInfo.point == null || userInfo.point == ""){
            tvScore.setText("0");
        } else {
            tvScore.setText(userInfo.point);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlPersonalInformation:
                mPersonalInformationFragment = new PersonalInformationFragment();
                mPersonalInformationFragment.userInfo = userInfo;
                changeFragment(mPersonalInformationFragment);
                break;
//            case R.id.imgEdit:
//                mPersonalInformationFragment = new PersonalInformationFragment();
//                changeFragment(mPersonalInformationFragment);
//                break;
            case R.id.rlAddress:
                changeFragment(addressFragment);
                break;
            case R.id.rlTransactionHistory:
                getOrderHistory(((MainActivity) getActivity()).userInfo.id, ((MainActivity) getActivity()).currentLanguageCode, ((MainActivity) getActivity()).currentProvinceSelected.id);
                break;
            case R.id.rlNotification:
                Bundle b = new Bundle();
                b.putSerializable("notification_data", listNotification);
                NotificationFragment f = new NotificationFragment();
                f.setArguments(b);
                changeFragment(f);
                break;
            case R.id.imgShowNavigation:
                ((MainActivity) getActivity()).
                        mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.rlFavoriteFood:
                FoodFavoriteFragment favoriteFragment = new FoodFavoriteFragment();
                changeFragment(favoriteFragment);
                break;
            case R.id.rlLogout:
                new AlertDialog.Builder(ctx)
                        .setMessage(R.string.contentLogout)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().getSharedPreferences(SharedPreferenceKeyType.USER_INFO_DATAFILE.toString(), Context.MODE_PRIVATE).edit().clear().commit();
                                activity.userInfo = null;
                                ((MainActivity) getActivity()).showUserInfoInNavigation(false);
                                rlMain.setVisibility(View.GONE);
                                LoginManager.getInstance().logOut(); //Log out from facebook account
                                signOutGG(); //Log out from google account
                                menuFragment = new MenuFragment();
                                activity.changeFragment(menuFragment);
                                activity.resetColor(activity.rlMenu, activity.imgMenu, activity.tvMenu, R.drawable.ic_menu_tabbar_bottom_2);
                                Toast.makeText(getActivity(), getString(R.string.MainActivity_006), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;
        }

    }

    private void signOutGG() {
        Auth.GoogleSignInApi.signOut(MainActivity.mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.e("Error", "onConnectionFailed:" + connectionResult);
    }

    private void getOrderHistory(String userID,
                                 String lang,
                                 String city) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<OrderHistoryDataWrapper> call1 = ApiClient.getJsonClient().getOrderHistory(userID,
                new String(Hex.encodeHex(DigestUtils.md5(userID))),
                lang,
                city);
        call1.enqueue(new Callback<OrderHistoryDataWrapper>() {

            @Override
            public void onFailure(Call<OrderHistoryDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<OrderHistoryDataWrapper> arg0, Response<OrderHistoryDataWrapper> arg1) {
                mProgressDialog.dismiss();

                OrderHistoryDataWrapper obj = arg1.body();
                ArrayList<OrderHistoryDataWrapper.OrderHistoryWrapper> lst = obj.data;

                Bundle b = new Bundle();
                b.putSerializable("transaction_history_data", lst);

                OrderHistoryFragment f = new OrderHistoryFragment();
                f.setArguments(b);
                changeFragment(f);
            }
        });
    }

    private void getNotification(String lang) {
        final ProgressDialog mProgressDialog = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            activity.showNoInternetFragment();
            return;
        }

        Call<NotificationDataWrapper> call1 = ApiClient.getJsonClient().getNotification(lang);
        call1.enqueue(new Callback<NotificationDataWrapper>() {

            @Override
            public void onFailure(Call<NotificationDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();
                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call<NotificationDataWrapper> arg0, Response<NotificationDataWrapper> arg1) {
                mProgressDialog.dismiss();
                listNotification = arg1.body().data;
                int num = 0;
                Set<String> set = ctx.getSharedPreferences(SharedPreferenceKeyType.USER_NOTIFICATION.toString(), Context.MODE_PRIVATE)
                        .getStringSet(SharedPreferenceKeyType.USER_NOTIFICATION.toString(), new HashSet<String>());
                if (set == null)
                    return;
                else if (set.size() == 0) {
                    tvCountNotification.setText(listNotification.size() + "");
                    llCountNoti.setVisibility(View.VISIBLE);
//                    return;
                }
                num = listNotification.size();
                List<String> listSeenMore = new ArrayList<String>(set);
                if (listSeenMore.size() > 0) {
                    for (NotificationDataWrapper.NotificationWrapper notificationWrapper : listNotification) {
                        String id_response = String.valueOf(notificationWrapper.id);
                        if (listSeenMore.contains(id_response)) {
                            notificationWrapper.setRead(true);
                            num--;
                        }
                    }
                }
                tvCountNotification.setText(num + "");
//                if (tvCountNotification.getText().toString().equals("0"))
                if (num > 0){
                    llCountNoti.setVisibility(View.VISIBLE);
                } else {
                    llCountNoti.setVisibility(View.GONE);
                }

            }
        });
    }

    public void changeFragment(final Fragment fragment) {
        String tagName = fragment.getClass().getName();
        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        //fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.replace(R.id.mainframe, fragment, tagName);
        fragmentTransaction.addToBackStack(tagName);
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();

    }

    public void openDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setMessage(R.string.please_log_in_to_view_account_information);

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                getActivity().startActivityForResult(new Intent(getActivity(), LoginActivity.class), RequestResultCodeType.REQUEST_LOGIN.getValue());
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        userInfo = null;
    }
}
