package kfc.vietnam.account_screen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kfc.vietnam.common.object.FoodFavoriteDataWrapper;

/**
 * Created by VietRuyn on 18/09/2016.
 */
public class FavariteAdapter extends ArrayAdapter {
    private String currentLanguageCode = "";
    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<FoodFavoriteDataWrapper> lstData;
    private int resource;


    public FavariteAdapter(Context context, int resource, ArrayList<FoodFavoriteDataWrapper> lstData) {
        super(context, resource, lstData);

        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        this.currentLanguageCode = currentLanguageCode;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        FoodFavoriteDataWrapper obj = lstData.get(position);

        //Change background color of item selected on method onItemClick() when receive command: notifyDataSetChanged()
        holder.llParent.setBackgroundColor(ctx.getResources().getColor(android.R.color.white));


        return convertView;
    }


    static class ViewHolder {
        private LinearLayout llParent, llDeliverToThisAddress, llDeleteAddress;
        private TextView tvTitle, tvAddress, tvPhoneNumber, tvEmail;
        private ImageView imgBackground;
    }


}
