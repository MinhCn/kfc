package kfc.vietnam.account_screen.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.ViewWebNotificationActivity;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.NotificationDataWrapper;

/**
 * Created by VietRuyn on 18/09/2016.
 */
public class NotificationAdapter extends ArrayAdapter {
    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<NotificationDataWrapper.NotificationWrapper> lstData;
    private int resource;
    private int pos;


    public NotificationAdapter(Context context, int resource, ArrayList<NotificationDataWrapper.NotificationWrapper> lstData) {
        super(context, resource, lstData);

        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            holder.tvContent = (TextView) convertView.findViewById(R.id.tvContent);
//            holder.tvDate1 = (TextView) convertView.findViewById(R.id.tvDate1);
//            holder.tvContent1 = (TextView) convertView.findViewById(R.id.tvContent1);

            holder.btnRead = (Button) convertView.findViewById(R.id.btnRead);
            holder.btnReadMore = (Button) convertView.findViewById(R.id.btnReadMore);
//            holder.btnReadMore1 = (Button) convertView.findViewById(R.id.btnReadMore1);
            holder.llRead = (LinearLayout) convertView.findViewById(R.id.llRead);
//            holder.llReaded = (LinearLayout) convertView.findViewById(R.id.llReaded);
//            holder.llRead.setVisibility(View.GONE);
//            holder.llReaded.setVisibility(View.GONE);
//            holder.btnRead.setOnClickListener(this);
//            holder.btnReadMore1.setOnClickListener(this);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (lstData.get(position).isRead()){
            holder.llRead.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            holder.btnRead.setVisibility(View.GONE);
        }else {
            holder.llRead.setBackgroundColor(Color.parseColor("#e1e1e1"));
            holder.btnRead.setVisibility(View.VISIBLE);
        }

        pos = position;
        NotificationDataWrapper.NotificationWrapper obj = lstData.get(position);

        String date = obj.date;
        String fmDate = formateDateFromstring("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", date);
        holder.tvDate.setText(fmDate);
        holder.tvContent.setText(obj.name);

//        if (!obj.isRead) {
//            holder.llRead.setVisibility(View.VISIBLE);
//            holder.tvDate.setText(obj.date);
//            holder.tvContent.setText(obj.name);
//        } else {
//            holder.llReaded.setVisibility(View.VISIBLE);
//            holder.tvDate1.setText(obj.date);
//            holder.tvContent1.setText(obj.name);
//        }
        holder.btnReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ViewWebNotificationActivity.class);
                intent.putExtra("url", lstData.get(pos).getLink());
                ctx.startActivity(intent);
                lstData.get(position).setRead(true);
                setSeenMore(lstData.get(position));
                notifyDataSetChanged();
            }
        });

        holder.btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lstData.get(position).setRead(true);
                setSeenMore(lstData.get(position));
                notifyDataSetChanged();
            }
        });
        return convertView;
    }


    private void setSeenMore(NotificationDataWrapper.NotificationWrapper notificationWrapper) {
        //Set the values
        Set<String> set = ctx.getSharedPreferences(SharedPreferenceKeyType.USER_NOTIFICATION.toString(), Context.MODE_PRIVATE).getStringSet(SharedPreferenceKeyType.USER_NOTIFICATION.toString(), new HashSet<String>());
        if(set==null)
            return;
        String id = String.valueOf(notificationWrapper.id);
        if (!set.contains(id))
            set.add(id);
        ctx.getSharedPreferences(SharedPreferenceKeyType.USER_NOTIFICATION.toString(), Context.MODE_PRIVATE).edit()
                .putStringSet(SharedPreferenceKeyType.USER_NOTIFICATION.toString(), set).apply();
    }

    static class ViewHolder {
        private LinearLayout llRead;
        private TextView tvDate, tvContent;
        private Button btnReadMore;

        private TextView tvDate1, tvContent1;
        private Button btnRead;
    }

    // Format datetime
    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.e("Error", "ParseException - dateFormat");
        }

        return outputDate;

    }


}
