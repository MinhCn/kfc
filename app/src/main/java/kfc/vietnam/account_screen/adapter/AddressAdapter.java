package kfc.vietnam.account_screen.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.fragment.AddressFragment;
import kfc.vietnam.common.object.AddressDataWrapper;
import kfc.vietnam.common.object.DeleteShipAddressDataWrapper;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.webservice.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VietRuyn on 18/09/2016.
 */
public class AddressAdapter extends ArrayAdapter implements AdapterView.OnItemClickListener, View.OnClickListener {
    private String currentLanguageCode = "";
    private Context ctx;
    private LayoutInflater inflater;
    private ArrayList<AddressDataWrapper> lstData;
    private int resource, currentPositionSelected = -1, positionToDelete = -1, positionToUpdate = -1;
    private ListView lvw;
    AddressDataWrapper obj;
    public static String idAddress;
    private boolean isCity;
    private AddressFragment addressFragment;

    public AddressAdapter(Context context, int resource, ArrayList<AddressDataWrapper> lstData, String currentLanguageCode, ListView lvw,AddressFragment addressFragment) {
        super(context, resource, lstData);

        this.ctx = context;
        this.resource = resource;
        this.lstData = lstData;
        this.currentLanguageCode = currentLanguageCode;
        this.lvw = lvw;
        this.addressFragment = addressFragment;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.llParent = (LinearLayout) convertView.findViewById(R.id.llParent);
            holder.llDeliverToThisAddress = (LinearLayout) convertView.findViewById(R.id.llDeliverToThisAddress);
            holder.llDeleteAddress = (LinearLayout) convertView.findViewById(R.id.llDeleteAddress);
            holder.tvFirstNameAndLastName = (TextView) convertView.findViewById(R.id.tvFirstNameAndLastName);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
            holder.tvPhoneNumber = (TextView) convertView.findViewById(R.id.tvPhoneNumber);
            holder.tvEmail = (TextView) convertView.findViewById(R.id.tvEmail);
            holder.tvCity = (TextView) convertView.findViewById(R.id.tvCity);
            holder.tvDistrict = (TextView) convertView.findViewById(R.id.tvDistrict);
            holder.tvWard = (TextView) convertView.findViewById(R.id.tvWard);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        obj = lstData.get(position);

        //Change background color of item selected on method onItemClick() when receive command: notifyDataSetChanged()
        if (position == currentPositionSelected)
            holder.llParent.setBackgroundColor(ctx.getResources().getColor(R.color.gray_efecec));
        else
            holder.llParent.setBackgroundColor(ctx.getResources().getColor(android.R.color.white));

        holder.llDeliverToThisAddress.setOnClickListener(this);
        holder.llDeliverToThisAddress.setTag(obj);
        holder.llDeleteAddress.setOnClickListener(this);
        holder.llDeleteAddress.setTag(obj);

        holder.tvFirstNameAndLastName.setText(ctx.getString(R.string.receiver) + ": " + obj.name);
        holder.tvAddress.setText(ctx.getString(R.string.StepOneFragment_005) + ": " + obj.address);
        holder.tvPhoneNumber.setText(ctx.getString(R.string.phone_number) + ": " + obj.phone);
        holder.tvEmail.setText(ctx.getString(R.string.email) + ": " + obj.email);
        if (obj.nameCity != null)
            holder.tvCity.setText(ctx.getString(R.string.city) + ": " + obj.nameCity);
        if (obj.nameDistrict != null)
            holder.tvDistrict.setText(ctx.getString(R.string.district) + ": " + obj.nameDistrict);
        if (obj.nameWard != null)
            holder.tvWard.setText(ctx.getString(R.string.ward) + ": " + obj.nameWard);

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        currentPositionSelected = position;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llDeliverToThisAddress:
                AddressFragment.svMore.setVisibility(View.VISIBLE);
                positionToUpdate = lvw.getPositionForView(view);
                AddressDataWrapper addressToUpdate = lstData.get(positionToUpdate);
                AddressFragment.isAdd = false;
                AddressFragment.edtAddress.setText(addressToUpdate.address);
                AddressFragment.edtEmail.setText(addressToUpdate.email);
                AddressFragment.edtFirstNameAndLastName.setText(addressToUpdate.name);
                AddressFragment.edtPhoneNumber.setText(addressToUpdate.phone);
                isCity = true;
                AddressFragment.spnProvince.setSelection(getIndex(AddressFragment.spnProvince, addressToUpdate.nameCity, isCity));
                isCity = false;
                AddressFragment.spnDistrict.setSelection(getIndex(AddressFragment.spnDistrict, addressToUpdate.nameDistrict, isCity));
                AddressFragment.spnWard.setSelection(getIndexWard(AddressFragment.spnWard, addressToUpdate.nameWard));
                idAddress = String.valueOf(addressToUpdate.id);
                break;

            case R.id.llDeleteAddress:
                positionToDelete = lvw.getPositionForView(view);

                AddressDataWrapper addressToDelete = (AddressDataWrapper) view.getTag();
                deleteShipAddress(addressToDelete.id + "");
                break;
        }
    }

    static class ViewHolder {
        private LinearLayout llParent, llDeliverToThisAddress, llDeleteAddress;
        private TextView tvFirstNameAndLastName, tvAddress, tvPhoneNumber, tvEmail, tvCity, tvDistrict, tvWard;
        ;
    }

    private void deleteShipAddress(String addressID) {
        final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
        mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
            mProgressDialog.dismiss();
            Toast.makeText(ctx, ctx.getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
            return;
        }

        Call<DeleteShipAddressDataWrapper> call1 = ApiClient.getJsonClient().deleteShipAddress(addressID);
        call1.enqueue(new Callback<DeleteShipAddressDataWrapper>() {

            @Override
            public void onFailure(Call<DeleteShipAddressDataWrapper> arg0, Throwable arg1) {
                arg1.printStackTrace();

                mProgressDialog.dismiss();

                Toast.makeText(ctx, ctx.getString(R.string.StepTwoFragment_003), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<DeleteShipAddressDataWrapper> arg0, Response<DeleteShipAddressDataWrapper> arg1) {
                mProgressDialog.dismiss();

                //remove in list and update screen
                lstData.remove(positionToDelete);
                notifyDataSetChanged();

                Toast.makeText(ctx, ctx.getString(R.string.StepTwoFragment_002), Toast.LENGTH_SHORT).show();
                if (lstData.size() == 0){
                    addressFragment.showEmptyMessage();
                }
            }
        });
    }

    private int getIndex(Spinner spinner, String myString, boolean isCity) {
        int index = 0;
        String name = "";
        for (int i = 0; i < spinner.getCount(); i++) {
            if (isCity) {
                ProvinceWrapper province = (ProvinceWrapper) spinner.getItemAtPosition(i);
                name = province.name;
            } else {
                DistrictWrapper district = (DistrictWrapper) spinner.getItemAtPosition(i);
                name = district.name;
            }
            if (name.equals(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }
    private int getIndexWard(Spinner spinner, String myString) {
        int index = 0;
        String name = "";
        for (int i = 0; i < spinner.getCount(); i++) {
                DistrictWrapper.WardWapper province = (DistrictWrapper.WardWapper) spinner.getItemAtPosition(i);
                name = province.name;
            if (name.equals(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }
}
