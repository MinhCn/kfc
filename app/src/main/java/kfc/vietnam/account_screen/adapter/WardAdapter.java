package kfc.vietnam.account_screen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.common.object.DistrictWrapper;
import kfc.vietnam.common.object.ProvinceWrapper;
import kfc.vietnam.common.utility.FontText;

/**
 * Created by AnhDuc on 30/07/2016.
 */
public class WardAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    private ArrayList<DistrictWrapper.WardWapper> lstData;
    private int resource;
    public String wardID = "";

    public WardAdapter(Context context, int resource, ArrayList<DistrictWrapper.WardWapper> lstData) {
        super(context, resource, lstData);
        this.resource = resource;
        this.lstData = lstData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.tvProvinceName = (FontText) convertView.findViewById(R.id.tvWardName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DistrictWrapper.WardWapper obj = lstData.get(position);
        holder.tvProvinceName.setText(obj.name);
        holder.tvProvinceName.setTag(obj.id);

        return convertView;
    }

    static class ViewHolder {
        private FontText tvProvinceName;
    }
}
