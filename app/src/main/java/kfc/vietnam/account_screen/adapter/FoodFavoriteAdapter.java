package kfc.vietnam.account_screen.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import java.io.IOException;
import java.util.ArrayList;

import kfc.vietnam.R;
import kfc.vietnam.account_screen.fragment.FoodFavoriteFragment;
import kfc.vietnam.common.enums.SharedPreferenceKeyType;
import kfc.vietnam.common.object.DeleteFoodFavoriteDataWrapper;
import kfc.vietnam.common.object.FoodFavoriteDataWrapper;
import kfc.vietnam.common.object.ShoppingCartObject;
import kfc.vietnam.common.utility.NetworkUtil;
import kfc.vietnam.common.utility.ObjectSerializer;
import kfc.vietnam.common.webservice.ApiClient;
import kfc.vietnam.food_menu_screen.fragment.DetailComboOrProductSelectedFragment;
import kfc.vietnam.main_screen.ui.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.string.no;

/**
 * Created by VietRuyn on 19/07/2016.
 */
public class FoodFavoriteAdapter extends RecyclerView.Adapter<FoodFavoriteAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<FoodFavoriteDataWrapper.FoodFavoriteWrapper> lstData;
    private Context ctx;
    private MainActivity activity;
    private String type, productId;
    private FoodFavoriteFragment foodFavoriteFragment;

    public FoodFavoriteAdapter(Context context, ArrayList<FoodFavoriteDataWrapper.FoodFavoriteWrapper> lstData, FoodFavoriteFragment foodFavoriteFragment) {
        this.ctx = context;
        this.lstData = lstData;
        this.foodFavoriteFragment = foodFavoriteFragment;
        activity = (MainActivity) ctx;
    }

    @Override
    public void onBindViewHolder(final FoodFavoriteAdapter.ViewHolder holder, int position) {
        final FoodFavoriteDataWrapper.FoodFavoriteWrapper obj = lstData.get(holder.getAdapterPosition());
        holder.itemView.setTag(holder.getAdapterPosition());

        Glide.with(ctx)
                .load(obj.image)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // NONE if you load from sdcard
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        holder.imgBackground.setImageDrawable(resource);
                    }
                });

        holder.tvTitle.setText(obj.name);

        final ArrayList<String> lst = new ArrayList<>();

        String categoryInfo = "";
        if (obj.type.equals("combo")) {
            for (int i = 0; i < obj.product.size(); i++) {
                lst.add(obj.product.get(i).name);
                String p = obj.product.get(i).name;
                if (i<obj.product.size()-1) {
                    categoryInfo += "● " + p + "\n";
                } else {
                    categoryInfo += "● " + p;
                }
            }

        }

        holder.tvCategoryInfo.setText(categoryInfo);

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lstData != null && lstData.size() > 0) {

                    final ProgressDialog mProgressDialog = new ProgressDialog(ctx, ProgressDialog.THEME_HOLO_DARK);
                    mProgressDialog.setMessage(ctx.getString(R.string.ProgressDialog_Message));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.show();

                    if (NetworkUtil.pingIpAddress(NetworkUtil.IP) <= -1) {
                        mProgressDialog.dismiss();
                        Toast.makeText(ctx, ctx.getString(R.string.network_problem), Toast.LENGTH_SHORT).show();
                        foodFavoriteFragment.activity.showNoInternetFragment();
                        return;
                    }

                    Call<DeleteFoodFavoriteDataWrapper> call1 = ApiClient.getJsonClient().deleteFoodFavorite(obj.id + "", ((MainActivity) ctx).userInfo.id);
                    call1.enqueue(new Callback<DeleteFoodFavoriteDataWrapper>() {

                        @Override
                        public void onFailure(Call<DeleteFoodFavoriteDataWrapper> arg0, Throwable arg1) {
                            arg1.printStackTrace();
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void onResponse(Call<DeleteFoodFavoriteDataWrapper> arg0, Response<DeleteFoodFavoriteDataWrapper> arg1) {
                            DeleteFoodFavoriteDataWrapper obj1 = arg1.body();
                            if (obj1.result) {
                                if (lstData != null && lstData.size() > 0) {
                                    lstData.remove(obj);
                                    notifyDataSetChanged();
                                    Log.d("lstData", lstData.size() + "");

                                    //Check like in cart
                                    SharedPreferences preferencesShoppingCard = ctx.getSharedPreferences(SharedPreferenceKeyType.SHOPPING_CART_DATAFILE.toString(), Context.MODE_PRIVATE);
                                    try {
                                        ArrayList<ShoppingCartObject> lstShoppingCard = (ArrayList<ShoppingCartObject>) ObjectSerializer.deserialize(preferencesShoppingCard.getString(SharedPreferenceKeyType.SHOPPING_CART.toString(), null));

                                        if (lstShoppingCard != null && lstShoppingCard.size() > 0) {
                                            for (int i = 0; i < lstShoppingCard.size(); i++) {
                                                ShoppingCartObject shoppingCartObject = lstShoppingCard.get(i);
                                                if (shoppingCartObject.dishSelected.id.equals(obj.id+"")) {
                                                    shoppingCartObject.productSelected.checkLike = false;
                                                }
                                                lstShoppingCard.set(i, shoppingCartObject);
                                            }
                                            preferencesShoppingCard.edit().putString(SharedPreferenceKeyType.SHOPPING_CART.toString(), ObjectSerializer.serialize(lstShoppingCard)).apply();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                                if (getItemCount() == 0){
                                    foodFavoriteFragment.showEmptyMess();
                                }

                            }

                            mProgressDialog.dismiss();
                        }
                    });

                }
            }
        });

        holder.llAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = obj.type;
                productId = obj.id + "";
                String buy = obj.buy;
                addToCart(productId, type, buy);
            }
        });


    }

    @Override
    public FoodFavoriteAdapter.ViewHolder onCreateViewHolder(ViewGroup vGroup, int i) {
        View view = LayoutInflater.from(vGroup.getContext()).inflate(R.layout.custom_item_favour, vGroup, false);

        return new ViewHolder(view);
    }


    @Override
    public int getItemCount() {
        if (lstData == null){
            return 0;
        } else {
            return lstData.size();
        }
    }

    @Override
    public void onClick(View view) {

    }

    private void addToCart(String id, String type, String buy){
        Bundle b = new Bundle();
        b.putString("id", id);
        b.putString("category_type", type);
        b.putString("buy", buy);

        DetailComboOrProductSelectedFragment f = new DetailComboOrProductSelectedFragment();
        f.setArguments(b);
        ((MainActivity) ctx).changeFragment(f);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgBackground;
        private LinearLayout llAddToCart;
        private ImageView imgDelete;
        private TextView tvTitle, tvCategoryInfo;
        private TextView tvPrice;

        public ViewHolder(View v) {
            super(v);

            imgBackground = (ImageView) v.findViewById(R.id.imgBackground);
            llAddToCart = (LinearLayout) v.findViewById(R.id.llAddToCart);
            imgDelete = (ImageView) v.findViewById(R.id.imgDelete);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvCategoryInfo = (TextView) v.findViewById(R.id.tvCategoryInfo);
            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
        }
    }
}